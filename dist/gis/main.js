(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/Main_masters/BBM/add-bbm/add-bbm.component.css":
/*!****************************************************************!*\
  !*** ./src/app/Main_masters/BBM/add-bbm/add-bbm.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n@import url('https://fonts.googleapis.com/css?family=Numans');\n/* Made with love by Mutiullah Samim*/\nhtml,body{\n\nbackground-size: cover;\nbackground-repeat: no-repeat;\nheight: 100%;\nfont-family: 'Numans', sans-serif;\n}\nlabel\n{\n    color: #fff;\n}\n.container{\nheight: 100%;\nbackground-size: cover;\nwidth:100%;\nbackground-repeat: no-repeat;\n/* align-content: center; */\n}\n.card{\nheight: 700px;\nmargin-top:20px;\nmargin-bottom:20px;\nwidth: 450px;\nbackground-color: rgba(0,0,0,0.5) !important;\n}\n.social_icon span{\nfont-size: 60px;\nmargin-left: 10px;\ncolor: #FFC312;\n}\n.social_icon span:hover{\ncolor: white;\ncursor: pointer;\n}\n.card-header h3{\ncolor: white;\n}\n.social_icon{\nposition: absolute;\nright: 20px;\ntop: -45px;\n}\n.input-group-prepend span{\nwidth: 50px;\nbackground-color: #FFC312;\ncolor: black;\nborder:0 !important;\n}\ninput:focus{\noutline: 0 0 0 0  !important;\nbox-shadow: 0 0 0 0 !important;\n\n}\n.remember{\ncolor: white;\n}\n.remember input\n{\nwidth: 20px;\nheight: 20px;\nmargin-left: 15px;\nmargin-right: 5px;\n}\n.login_btn{\ncolor: black;\nbackground-color: #FFC312;\nwidth: 100px;\n}\n.login_btn:hover{\ncolor: black;\nbackground-color: white;\n}\n.links{\ncolor: white;\n}\n.links a{\nmargin-left: 4px;\n}\noption\n{\n    margin-bottom: 10px!important;\n\n}\n/* *************************************************************************\n**********************  Media query for Mobile screen  */\n@media only screen and (max-width : 768px) {\n  .card{\n      \n    margin-top:auto;\n    margin-bottom:auto;\n      }\n\n}\n@media only screen and (max-width : 480px) {\n    .card{\n        height: 1030px;\n        margin-top:20px;\n        margin-bottom:20px;\n\n        }\n\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0JCTS9hZGQtYmJtL2FkZC1iYm0uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLDZEQUE2RDtBQUY3RCxxQ0FBcUM7QUFJckM7O0FBRUEsc0JBQXNCO0FBQ3RCLDRCQUE0QjtBQUM1QixZQUFZO0FBQ1osaUNBQWlDO0FBQ2pDO0FBQ0E7O0lBRUksV0FBVztBQUNmO0FBQ0E7QUFDQSxZQUFZO0FBQ1osc0JBQXNCO0FBQ3RCLFVBQVU7QUFDViw0QkFBNEI7QUFDNUIsMkJBQTJCO0FBQzNCO0FBRUE7QUFDQSxhQUFhO0FBQ2IsZUFBZTtBQUNmLGtCQUFrQjtBQUNsQixZQUFZO0FBQ1osNENBQTRDO0FBQzVDO0FBRUE7QUFDQSxlQUFlO0FBQ2YsaUJBQWlCO0FBQ2pCLGNBQWM7QUFDZDtBQUVBO0FBQ0EsWUFBWTtBQUNaLGVBQWU7QUFDZjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxrQkFBa0I7QUFDbEIsV0FBVztBQUNYLFVBQVU7QUFDVjtBQUVBO0FBQ0EsV0FBVztBQUNYLHlCQUF5QjtBQUN6QixZQUFZO0FBQ1osbUJBQW1CO0FBQ25CO0FBRUE7QUFDQSw0QkFBNEI7QUFDNUIsOEJBQThCOztBQUU5QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7O0FBRUEsV0FBVztBQUNYLFlBQVk7QUFDWixpQkFBaUI7QUFDakIsaUJBQWlCO0FBQ2pCO0FBRUE7QUFDQSxZQUFZO0FBQ1oseUJBQXlCO0FBQ3pCLFlBQVk7QUFDWjtBQUVBO0FBQ0EsWUFBWTtBQUNaLHVCQUF1QjtBQUN2QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQTs7SUFFSSw2QkFBNkI7O0FBRWpDO0FBSUE7d0RBQ3dEO0FBQ3hEO0VBQ0U7O0lBRUUsZUFBZTtJQUNmLGtCQUFrQjtNQUNoQjs7QUFFTjtBQUVBO0lBQ0k7UUFDSSxjQUFjO1FBQ2QsZUFBZTtRQUNmLGtCQUFrQjs7UUFFbEI7O0FBRVIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvQkJNL2FkZC1iYm0vYWRkLWJibS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogTWFkZSB3aXRoIGxvdmUgYnkgTXV0aXVsbGFoIFNhbWltKi9cblxuQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1OdW1hbnMnKTtcblxuaHRtbCxib2R5e1xuXG5iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbmhlaWdodDogMTAwJTtcbmZvbnQtZmFtaWx5OiAnTnVtYW5zJywgc2Fucy1zZXJpZjtcbn1cbmxhYmVsXG57XG4gICAgY29sb3I6ICNmZmY7XG59XG4uY29udGFpbmVye1xuaGVpZ2h0OiAxMDAlO1xuYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbndpZHRoOjEwMCU7XG5iYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuLyogYWxpZ24tY29udGVudDogY2VudGVyOyAqL1xufVxuXG4uY2FyZHtcbmhlaWdodDogNzAwcHg7XG5tYXJnaW4tdG9wOjIwcHg7XG5tYXJnaW4tYm90dG9tOjIwcHg7XG53aWR0aDogNDUwcHg7XG5iYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuNSkgIWltcG9ydGFudDtcbn1cblxuLnNvY2lhbF9pY29uIHNwYW57XG5mb250LXNpemU6IDYwcHg7XG5tYXJnaW4tbGVmdDogMTBweDtcbmNvbG9yOiAjRkZDMzEyO1xufVxuXG4uc29jaWFsX2ljb24gc3Bhbjpob3ZlcntcbmNvbG9yOiB3aGl0ZTtcbmN1cnNvcjogcG9pbnRlcjtcbn1cblxuLmNhcmQtaGVhZGVyIGgze1xuY29sb3I6IHdoaXRlO1xufVxuXG4uc29jaWFsX2ljb257XG5wb3NpdGlvbjogYWJzb2x1dGU7XG5yaWdodDogMjBweDtcbnRvcDogLTQ1cHg7XG59XG5cbi5pbnB1dC1ncm91cC1wcmVwZW5kIHNwYW57XG53aWR0aDogNTBweDtcbmJhY2tncm91bmQtY29sb3I6ICNGRkMzMTI7XG5jb2xvcjogYmxhY2s7XG5ib3JkZXI6MCAhaW1wb3J0YW50O1xufVxuXG5pbnB1dDpmb2N1c3tcbm91dGxpbmU6IDAgMCAwIDAgICFpbXBvcnRhbnQ7XG5ib3gtc2hhZG93OiAwIDAgMCAwICFpbXBvcnRhbnQ7XG5cbn1cblxuLnJlbWVtYmVye1xuY29sb3I6IHdoaXRlO1xufVxuXG4ucmVtZW1iZXIgaW5wdXRcbntcbndpZHRoOiAyMHB4O1xuaGVpZ2h0OiAyMHB4O1xubWFyZ2luLWxlZnQ6IDE1cHg7XG5tYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLmxvZ2luX2J0bntcbmNvbG9yOiBibGFjaztcbmJhY2tncm91bmQtY29sb3I6ICNGRkMzMTI7XG53aWR0aDogMTAwcHg7XG59XG5cbi5sb2dpbl9idG46aG92ZXJ7XG5jb2xvcjogYmxhY2s7XG5iYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cblxuLmxpbmtze1xuY29sb3I6IHdoaXRlO1xufVxuXG4ubGlua3MgYXtcbm1hcmdpbi1sZWZ0OiA0cHg7XG59XG5vcHRpb25cbntcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4IWltcG9ydGFudDtcblxufVxuXG5cblxuLyogKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuKioqKioqKioqKioqKioqKioqKioqKiAgTWVkaWEgcXVlcnkgZm9yIE1vYmlsZSBzY3JlZW4gICovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGggOiA3NjhweCkge1xuICAuY2FyZHtcbiAgICAgIFxuICAgIG1hcmdpbi10b3A6YXV0bztcbiAgICBtYXJnaW4tYm90dG9tOmF1dG87XG4gICAgICB9XG5cbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoIDogNDgwcHgpIHtcbiAgICAuY2FyZHtcbiAgICAgICAgaGVpZ2h0OiAxMDMwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6MjBweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbToyMHB4O1xuXG4gICAgICAgIH1cblxufSJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/BBM/add-bbm/add-bbm.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/Main_masters/BBM/add-bbm/add-bbm.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"d-flex justify-content-center h-100\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        <h3 style=\"text-align:center\">Add Buyer Or Brand</h3>\n      </div>\n      <div class=\"card-body\">\n        <form class=\"form-login\" role=\"form\" #formRef='ngForm' (ngSubmit)='onSubmit(formRef.value)'>\n          <div class=\"form-group row\">\n            <label for=\"subDep\" class=\"col-sm-5 col-form-label\">Buyer Or Brand Name</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDep\" name=\"newBuyerBrand\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepPassword\" class=\"col-sm-5 col-form-label\">Address</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepPassword\" name=\"address\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepPassword\" class=\"col-sm-5 col-form-label\">Pincode</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepPassword\" name=\"Pincode\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepcfPassword\" class=\"col-sm-5 col-form-label\">Contact Person</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepcfPassword\" name=\"Cperson\" #selectRef='ngModel'\n                required ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Mobile Number</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"mobile\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Alternate Mob No</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"altermobile\" #selectRef='ngModel'\n                required ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">E-Mail ID</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"Email\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Website</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"website\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Remark</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"remarks\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n\n          <div class=\"form-group\" style=\"text-align:center\">\n            <button style=\"padding: 0;\n                border: none;\n                background: none;\" type=\"submit\">\n              <!-- <i class=\"far fa-check-circle\"> -->\n              <i class=\"fas fa-check-circle\" (click)=\" onAddBuyer()\" style=\"font-size:60px;color: yellow;\"></i>\n\n            </button>\n          </div>\n\n        </form>\n      </div>\n\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/BBM/add-bbm/add-bbm.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/Main_masters/BBM/add-bbm/add-bbm.component.ts ***!
  \***************************************************************/
/*! exports provided: AddBbmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddBbmComponent", function() { return AddBbmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddBbmComponent = /** @class */ (function () {
    function AddBbmComponent(router, _http, headerTitleService) {
        this.router = router;
        this._http = _http;
        this.headerTitleService = headerTitleService;
    }
    AddBbmComponent.prototype.ngOnInit = function () {
        this.headerTitleService.setTitle('Buyer/Brand');
    };
    AddBbmComponent.prototype.onSubmit = function (a) {
        var _this = this;
        this.newBuyerBrand = a.newBuyerBrand;
        this.address = a.address;
        this.Pincode = a.Pincode;
        this.Cperson = a.Cperson;
        this.mobile = a.mobile;
        this.altermobile = a.altermobile;
        this.Email = a.Email;
        this.website = a.website;
        this.remarks = a.remarks;
        console.log(a);
        console.log(typeof (a));
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.post('api/add-buyser-brand', a, options)
            .subscribe(function (data) {
            alert('Added Successfully');
            _this.router.navigate(['/main-masters']);
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    AddBbmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-bbm',
            template: __webpack_require__(/*! ./add-bbm.component.html */ "./src/app/Main_masters/BBM/add-bbm/add-bbm.component.html"),
            styles: [__webpack_require__(/*! ./add-bbm.component.css */ "./src/app/Main_masters/BBM/add-bbm/add-bbm.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"]])
    ], AddBbmComponent);
    return AddBbmComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/BBM/edit-bbm/edit-bbm.component.css":
/*!******************************************************************!*\
  !*** ./src/app/Main_masters/BBM/edit-bbm/edit-bbm.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table,tr,th,td\n{\n    border:1px solid #fff;\n}\n#export-button\n{\n    padding-left:20px;\n    padding-right:20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0JCTS9lZGl0LWJibS9lZGl0LWJibS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztJQUVJLHFCQUFxQjtBQUN6QjtBQUNBOztJQUVJLGlCQUFpQjtJQUNqQixrQkFBa0I7QUFDdEIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvQkJNL2VkaXQtYmJtL2VkaXQtYmJtLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZSx0cix0aCx0ZFxue1xuICAgIGJvcmRlcjoxcHggc29saWQgI2ZmZjtcbn1cbiNleHBvcnQtYnV0dG9uXG57XG4gICAgcGFkZGluZy1sZWZ0OjIwcHg7XG4gICAgcGFkZGluZy1yaWdodDoyMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/BBM/edit-bbm/edit-bbm.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/Main_masters/BBM/edit-bbm/edit-bbm.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\n\n<div class=\"container table-responsive table-responsive-sm table-responsive-md\" style=\"text-align: -webkit-center;\">\n  <!-- <button (click)=\"exportAsXLSX()\">Excel Export</button> -->\n\n  <div *ngIf=\"update\">\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.newBuyerBrand>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.address>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.Pincode>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.Cperson>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.mobile>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.altermobile>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.Email>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.website>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.remarks>\n    <button (click)='saveUpdate(dataedit)' type=\"button\" class=\"btn btn-sm btn-info\" style=\"margin-top: -5px;font-size: 13.9px;\">save</button>\n  </div>\n  <br>\n  <br>\n  <table class=\"table\">\n    <thead style=\"text-align: -webkit-center;color: cornsilk;\">\n      <tr class=\"text-color\">\n        <th>Buyer Or Brand Name</th>\n        <th>Address</th>\n        <th>Pincode</th>\n        <th>Contact Person</th>\n        <th>Mobile No</th>\n        <th>Alt Mobile Mo</th>\n        <th>Email Id</th>\n        <th>Website</th>\n        <th>Remark</th>\n        <th>Edit</th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor='let i of data; let in=index' class=\"success\" style=\"color:white\">\n        <td role=\"cell\">{{i.newBuyerBrand}}</td>\n        <td role=\"cell\">{{i.address}}</td>\n        <td role=\"cell\">{{i.Pincode}}</td>\n        <td role=\"cell\">{{i.Cperson}}</td>\n        <td role=\"cell\">{{i.mobile}}</td>\n        <td role=\"cell\">{{i.altermobile}}</td>\n        <td role=\"cell\">{{i.Email}}</td>\n        <td role=\"cell\">{{i.website}}</td>\n        <td role=\"cell\">{{i.remarks}}</td>\n        <td><button (click)='editData(i,in)' type=\"button\" class=\"btn btn-large btn-info\">Edit</button></td>\n      </tr>\n    </tbody>\n  </table>\n\n  <!-- <angular2csv [data[0]]=\"data\" filename=\"test\" [options]=\"options\"></angular2csv> -->\n  <!-- <button (click)=\"exportExcel()\">Export</button> -->\n  <!-- <button id=\"export-button\" type=\"button\" (click)=\"exportExcel()\" class=\"btn btn-info btn-lg \">Export</button> -->\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/BBM/edit-bbm/edit-bbm.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/Main_masters/BBM/edit-bbm/edit-bbm.component.ts ***!
  \*****************************************************************/
/*! exports provided: EditBbmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditBbmComponent", function() { return EditBbmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
/* harmony import */ var src_app_Services_bbm_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/Services/bbm.service */ "./src/app/Services/bbm.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var EditBbmComponent = /** @class */ (function () {
    function EditBbmComponent(_http, BBMSrvice, headerTitleService, 
    // private fileUploadService: FileUploadService,
    router) {
        this._http = _http;
        this.BBMSrvice = BBMSrvice;
        this.headerTitleService = headerTitleService;
        this.router = router;
        this.test = [
            {}
        ];
        this.update = false;
    }
    EditBbmComponent.prototype.ngOnInit = function () {
        var _this = this;
        // this.fileUploadService.getData().subscribe((fileData) => this.data = fileData);
        this.BBMSrvice.getData().subscribe(function (BBMData) { return _this.data = BBMData; });
        this.headerTitleService.setTitle('Buyer/Brand');
    };
    EditBbmComponent.prototype.editData = function (i) {
        this.update = true;
        this.dataedit = i;
    };
    EditBbmComponent.prototype.saveUpdate = function (datasave) {
        console.log("datasaved" + datasave.newBuyerBrand);
        console.log("datasaved" + '' + datasave.newBuyerBrand + '' +
            '' + datasave.address + '' +
            '' + datasave.Pincode + '' +
            '' + datasave.Cperson + '' +
            '' + datasave.mobile);
        var url = "http://localhost:3000/api/fileUpload" + "/" + datasave._id;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["RequestOptions"]({ headers: headers });
        this._http.put(url, datasave, options)
            .subscribe(function (data) {
            alert('saved Successfully');
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    EditBbmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-bbm',
            template: __webpack_require__(/*! ./edit-bbm.component.html */ "./src/app/Main_masters/BBM/edit-bbm/edit-bbm.component.html"),
            styles: [__webpack_require__(/*! ./edit-bbm.component.css */ "./src/app/Main_masters/BBM/edit-bbm/edit-bbm.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"],
            src_app_Services_bbm_service__WEBPACK_IMPORTED_MODULE_4__["BbmService"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], EditBbmComponent);
    return EditBbmComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/BBM/import-bbm/import-bbm.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/Main_masters/BBM/import-bbm/import-bbm.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/* .center-div\n{\n\n} */\n/* @media (max-width: 1000px) {\n    \n    .center-inner{left:25%;top:25%;position:absolute;width:50%;height:300px;background-color: rgba(0,0,0,0.5) !important;text-align:center;max-width:500px;max-height:500px;color:#ffffff}\n    } */\n.center{\n        position: absolute;\n        left: 50%;\n        top: 50%;\n        -webkit-transform: translate(-50%, -50%);\n                transform: translate(-50%, -50%);\n        width: 400px;\n        height: 300px;\n        padding: 20px;  \n        \n        background-color: rgba(0,0,0,0.5) !important;\n        color: white;\n        text-align: center;\n        box-shadow: 0 0 30px rgba(0, 0, 0, 0.2);\n    }\n/* .center-inner{width:400px;height:100%;height:300px;background-color: rgba(0,0,0,0.5) !important;text-align:center;max-width:500px;max-height:500px;color:#ffffff;padding: 20px;border-radius: 20px;}\n  \n    @media (max-width: 428px) {\n    .center-inner{width:300px;height:100%;height:300px;background-color: rgba(0,0,0,0.5) !important;text-align:center;max-width:500px;max-height:500px;color:#ffffff;padding: 20px;border-radius: 20px;}\n    .center{left:50%;top:25%;position:absolute;}\n    } */\n@media only screen and (min-width : 320px) and (max-width :768px) {\n        .center{\n            margin-left:50px;\n            margin-right: 50px;\n            margin-bottom: 50px;\n            }\n    \n    }\n#myFile {\n        display:none;\n    }\ninput[type=\"file\"] {\n        display: none;\n    }\ntable,tr,th,td\n    {\n        border:1px solid #fff;\n    }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0JCTS9pbXBvcnQtYmJtL2ltcG9ydC1iYm0uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0E7OztHQUdHO0FBQ0g7OztPQUdPO0FBRUg7UUFDSSxrQkFBa0I7UUFDbEIsU0FBUztRQUNULFFBQVE7UUFDUix3Q0FBZ0M7Z0JBQWhDLGdDQUFnQztRQUNoQyxZQUFZO1FBQ1osYUFBYTtRQUNiLGFBQWE7O1FBRWIsNENBQTRDO1FBQzVDLFlBQVk7UUFDWixrQkFBa0I7UUFDbEIsdUNBQXVDO0lBQzNDO0FBQ0E7Ozs7O09BS0c7QUFDSDtRQUNJO1lBQ0ksZ0JBQWdCO1lBQ2hCLGtCQUFrQjtZQUNsQixtQkFBbUI7WUFDbkI7O0lBRVI7QUFDQTtRQUNJLFlBQVk7SUFDaEI7QUFDQTtRQUNJLGFBQWE7SUFDakI7QUFDQTs7UUFFSSxxQkFBcUI7SUFDekIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvQkJNL2ltcG9ydC1iYm0vaW1wb3J0LWJibS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG4vKiAuY2VudGVyLWRpdlxue1xuXG59ICovXG4vKiBAbWVkaWEgKG1heC13aWR0aDogMTAwMHB4KSB7XG4gICAgXG4gICAgLmNlbnRlci1pbm5lcntsZWZ0OjI1JTt0b3A6MjUlO3Bvc2l0aW9uOmFic29sdXRlO3dpZHRoOjUwJTtoZWlnaHQ6MzAwcHg7YmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLDAsMCwwLjUpICFpbXBvcnRhbnQ7dGV4dC1hbGlnbjpjZW50ZXI7bWF4LXdpZHRoOjUwMHB4O21heC1oZWlnaHQ6NTAwcHg7Y29sb3I6I2ZmZmZmZn1cbiAgICB9ICovXG4gICAgXG4gICAgLmNlbnRlcntcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICBsZWZ0OiA1MCU7XG4gICAgICAgIHRvcDogNTAlO1xuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgICAgICAgd2lkdGg6IDQwMHB4O1xuICAgICAgICBoZWlnaHQ6IDMwMHB4O1xuICAgICAgICBwYWRkaW5nOiAyMHB4OyAgXG4gICAgICAgIFxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuNSkgIWltcG9ydGFudDtcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIGJveC1zaGFkb3c6IDAgMCAzMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgICB9XG4gICAgLyogLmNlbnRlci1pbm5lcnt3aWR0aDo0MDBweDtoZWlnaHQ6MTAwJTtoZWlnaHQ6MzAwcHg7YmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLDAsMCwwLjUpICFpbXBvcnRhbnQ7dGV4dC1hbGlnbjpjZW50ZXI7bWF4LXdpZHRoOjUwMHB4O21heC1oZWlnaHQ6NTAwcHg7Y29sb3I6I2ZmZmZmZjtwYWRkaW5nOiAyMHB4O2JvcmRlci1yYWRpdXM6IDIwcHg7fVxuICBcbiAgICBAbWVkaWEgKG1heC13aWR0aDogNDI4cHgpIHtcbiAgICAuY2VudGVyLWlubmVye3dpZHRoOjMwMHB4O2hlaWdodDoxMDAlO2hlaWdodDozMDBweDtiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuNSkgIWltcG9ydGFudDt0ZXh0LWFsaWduOmNlbnRlcjttYXgtd2lkdGg6NTAwcHg7bWF4LWhlaWdodDo1MDBweDtjb2xvcjojZmZmZmZmO3BhZGRpbmc6IDIwcHg7Ym9yZGVyLXJhZGl1czogMjBweDt9XG4gICAgLmNlbnRlcntsZWZ0OjUwJTt0b3A6MjUlO3Bvc2l0aW9uOmFic29sdXRlO31cbiAgICB9ICovXG4gICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoIDogMzIwcHgpIGFuZCAobWF4LXdpZHRoIDo3NjhweCkge1xuICAgICAgICAuY2VudGVye1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6NTBweDtcbiAgICAgICAgICAgIG1hcmdpbi1yaWdodDogNTBweDtcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDUwcHg7XG4gICAgICAgICAgICB9XG4gICAgXG4gICAgfVxuICAgICNteUZpbGUge1xuICAgICAgICBkaXNwbGF5Om5vbmU7XG4gICAgfVxuICAgIGlucHV0W3R5cGU9XCJmaWxlXCJdIHtcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICB9XG4gICAgdGFibGUsdHIsdGgsdGRcbiAgICB7XG4gICAgICAgIGJvcmRlcjoxcHggc29saWQgI2ZmZjtcbiAgICB9Il19 */"

/***/ }),

/***/ "./src/app/Main_masters/BBM/import-bbm/import-bbm.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/Main_masters/BBM/import-bbm/import-bbm.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\n\n<div class=\"container table-responsive table-responsive-sm table-responsive-md\" style=\"text-align: -webkit-center;\">\n\n\n    <!-- <div *ngIf=\"update\">\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.newBuyrBrand>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.address>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.Pincode>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.Cperson>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.mobile>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.altermobile>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.Email>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.website>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.remarks>\n    <button (click)='saveUpdate(dataedit)' type=\"button\" class=\"btn btn-sm btn-info\" style=\"margin-top: -5px;font-size: 13.9px;\">save</button>\n  </div>\n  <br>\n  <br> -->\n    <table class=\"table\">\n        <thead style=\"text-align: -webkit-center;color: cornsilk;\">\n            <tr class=\"text-color\">\n                <th>Buyer Or Brand Name</th>\n                <th>Address</th>\n                <th>Pincode</th>\n                <th>Contact Person</th>\n                <th>Mobile No</th>\n                <th>Alt Mobile Mo</th>\n                <th>Email Id</th>\n                <th>Website</th>\n                <th>Remark</th>\n                <th>Edit</th>\n            </tr>\n        </thead>\n        <!-- <tbody>\n      <tr *ngFor='let i of data; let in=index' class=\"success\" style=\"color:white\">\n        <td role=\"cell\">{{i.newBuyerBrand}}</td>\n        <td role=\"cell\">{{i.address}}</td>\n        <td role=\"cell\">{{i.Pincode}}</td>\n        <td role=\"cell\">{{i.Cperson}}</td>\n        <td role=\"cell\">{{i.mobile}}</td>\n        <td role=\"cell\">{{i.altermobile}}</td>\n        <td role=\"cell\">{{i.Email}}</td>\n        <td role=\"cell\">{{i.website}}</td>\n        <td role=\"cell\">{{i.remarks}}</td>\n        <td><button (click)='editData(i,in)' type=\"button\" class=\"btn btn-large btn-info\">Edit</button></td>\n      </tr>\n    </tbody> -->\n    </table>\n    <button id=\"export-button\" type=\"button\" (click)=\"exportExcel()\" class=\"btn btn-info btn-lg \">Export</button>\n\n</div>\n\n<br><br><br>\n<div class=\"center\" style=\"margin-top: 80px;\">\n    <div class=\"center-inner\">\n        <form id=\"login\">\n            <h4>Import File</h4>\n            <hr>\n            <p>* Only CSV File can be import. </p>\n\n            <input type=\"file\" name=\"files\" class=\"form-control\" #uploads (change)=\"onChange(uploads.files)\" multiple\n                value=\"process\" #file />\n            <div class=\"input-group mb-3\">\n                <div class=\"input-group-prepend\">\n                    <button class=\"btn btn-outline-primary\" (click)=\"file.click()\" type=\"button\">Browser</button>\n                </div>\n                <input type=\"text\" value=\"{{fileName}}\" class=\"form-control form-control-lg\" aria-label=\"Default\"\n                    aria-describedby=\"inputGroup-sizing-default\">\n            </div>\n            <br>\n            <div class=\"form-group\" style=\"text-align:center\">\n                <button style=\"padding: 0;\n                        border: none;\n                        background: none;\"\n                    type=\"button\" (click)=\"onUpload()\">\n\n                    <i class=\"fas fa-check-circle\" style=\"font-size:70px;color: yellow;\"></i>\n\n                </button>\n            </div>\n\n        </form>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/BBM/import-bbm/import-bbm.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/Main_masters/BBM/import-bbm/import-bbm.component.ts ***!
  \*********************************************************************/
/*! exports provided: ImportBbmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImportBbmComponent", function() { return ImportBbmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
/* harmony import */ var papaparse__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! papaparse */ "./node_modules/papaparse/papaparse.min.js");
/* harmony import */ var papaparse__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(papaparse__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular2-csv/Angular2-csv */ "./node_modules/angular2-csv/Angular2-csv.js");
/* harmony import */ var angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ImportBbmComponent = /** @class */ (function () {
    function ImportBbmComponent(headerTitleService, http, router) {
        this.headerTitleService = headerTitleService;
        this.http = http;
        this.router = router;
        this.test = [
            {}
        ];
    }
    ImportBbmComponent.prototype.ngOnInit = function () {
        this.headerTitleService.setTitle('Buyer/Brand');
    };
    ImportBbmComponent.prototype.onChange = function (files) {
        var _this = this;
        if (files[0]) {
            console.log(files[0]);
            papaparse__WEBPACK_IMPORTED_MODULE_2__["parse"](files[0], {
                header: true,
                skipEmptyLines: true,
                complete: function (result, file) {
                    console.log(result);
                    _this.dataList = result.data;
                    _this.fileName = file.name;
                    console.log(_this.dataList);
                    var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["Headers"]();
                    headers.append('Content-Type', 'application/json');
                    var options = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["RequestOptions"]({ headers: headers });
                    _this.http.post('api/fileUpload', _this.dataList, options)
                        .subscribe(function (data) {
                        console.log("added successfully");
                    }, function (error) {
                        console.log(JSON.stringify(error.json()));
                    });
                }
            });
        }
    };
    // Navigating Table
    ImportBbmComponent.prototype.onUpload = function () {
        this.router.navigate(['/import-table']);
    };
    ImportBbmComponent.prototype.exportExcel = function () {
        var head = ['newBuyerBrand', 'address', 'Pincode', 'Cperson', 'mobile', 'altermobile', 'Email', 'website', 'remarks'];
        new angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_3__["Angular2Csv"](this.test, 'Buyer Master', { headers: (head) });
        alert("Please Give the Input Details");
        this.router.navigate(['/buyer-brand-import']);
    };
    ImportBbmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-import-bbm',
            template: __webpack_require__(/*! ./import-bbm.component.html */ "./src/app/Main_masters/BBM/import-bbm/import-bbm.component.html"),
            styles: [__webpack_require__(/*! ./import-bbm.component.css */ "./src/app/Main_masters/BBM/import-bbm/import-bbm.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_1__["HeaderTitleService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_5__["Http"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], ImportBbmComponent);
    return ImportBbmComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/BBM/import-table/import-table.component.css":
/*!**************************************************************************!*\
  !*** ./src/app/Main_masters/BBM/import-table/import-table.component.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table,tr,th,td\n{\n    border:1px solid #fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0JCTS9pbXBvcnQtdGFibGUvaW1wb3J0LXRhYmxlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0lBRUkscUJBQXFCO0FBQ3pCIiwiZmlsZSI6InNyYy9hcHAvTWFpbl9tYXN0ZXJzL0JCTS9pbXBvcnQtdGFibGUvaW1wb3J0LXRhYmxlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZSx0cix0aCx0ZFxue1xuICAgIGJvcmRlcjoxcHggc29saWQgI2ZmZjtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/Main_masters/BBM/import-table/import-table.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/Main_masters/BBM/import-table/import-table.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\n\n<div class=\"container table-responsive table-responsive-sm table-responsive-md\" style=\"text-align: -webkit-center;\">\n  <!-- <button (click)=\"exportAsXLSX()\">Excel Export</button> -->\n\n  <div *ngIf=\"update\">\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.newBuyerBrand>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.address>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.Pincode>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.Cperson>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.mobile>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.altermobile>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.Email>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.website>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.remarks>\n    <button (click)='saveUpdate(dataedit)' type=\"button\" class=\"btn btn-sm btn-info\" style=\"margin-top: -5px;font-size: 13.9px;\">save</button>\n  </div>\n  <br>\n  <br>\n  <table class=\"table\">\n    <thead style=\"text-align: -webkit-center;color: cornsilk;\">\n      <tr class=\"text-color\">\n        <th>Buyer Or Brand Name</th>\n        <th>Address</th>\n        <th>Pincode</th>\n        <th>Contact Person</th>\n        <th>Mobile No</th>\n        <th>Alt Mobile Mo</th>\n        <th>Email Id</th>\n        <th>Website</th>\n        <th>Remark</th>\n        <th>Edit</th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor='let i of data; let in=index' class=\"success\" style=\"color:white\">\n        <td role=\"cell\">{{i.newBuyerBrand}}</td>\n        <td role=\"cell\">{{i.address}}</td>\n        <td role=\"cell\">{{i.Pincode}}</td>\n        <td role=\"cell\">{{i.Cperson}}</td>\n        <td role=\"cell\">{{i.mobile}}</td>\n        <td role=\"cell\">{{i.altermobile}}</td>\n        <td role=\"cell\">{{i.Email}}</td>\n        <td role=\"cell\">{{i.website}}</td>\n        <td role=\"cell\">{{i.remarks}}</td>\n        <td><button (click)='editData(i,in)' type=\"button\" class=\"btn btn-large btn-info\">Edit</button></td>\n      </tr>\n    </tbody>\n  </table>\n\n  <!-- <angular2csv [data[0]]=\"data\" filename=\"test\" [options]=\"options\"></angular2csv> -->\n  <!-- <button (click)=\"exportExcel()\">Export</button> -->\n  <!-- <button id=\"export-button\" type=\"button\" (click)=\"exportExcel()\" class=\"btn btn-info btn-lg \">Export</button> -->\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/BBM/import-table/import-table.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/Main_masters/BBM/import-table/import-table.component.ts ***!
  \*************************************************************************/
/*! exports provided: ImportTableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImportTableComponent", function() { return ImportTableComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_Services_file_upload_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/file-upload.service */ "./src/app/Services/file-upload.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ImportTableComponent = /** @class */ (function () {
    function ImportTableComponent(fileService, _http, router) {
        this.fileService = fileService;
        this._http = _http;
        this.router = router;
        this.test = [
            {}
        ];
        this.update = false;
    }
    ImportTableComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.fileService.getData().subscribe(function (fileData) { return _this.data = fileData; });
    };
    ImportTableComponent.prototype.editData = function (i) {
        this.update = true;
        this.dataedit = i;
    };
    ImportTableComponent.prototype.saveUpdate = function (datasave) {
        console.log("datasaved" + datasave.newBuyerBrand);
        console.log("datasaved" + '' + datasave.newBuyerBrand + '' +
            '' + datasave.address + '' +
            '' + datasave.Pincode + '' +
            '' + datasave.Cperson + '' +
            '' + datasave.mobile);
        var url = "http://localhost:3000/api/fileUpload" + "/" + datasave._id;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["RequestOptions"]({ headers: headers });
        this._http.put(url, datasave, options)
            .subscribe(function (data) {
            alert('saved Successfully');
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    ImportTableComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-import-table',
            template: __webpack_require__(/*! ./import-table.component.html */ "./src/app/Main_masters/BBM/import-table/import-table.component.html"),
            styles: [__webpack_require__(/*! ./import-table.component.css */ "./src/app/Main_masters/BBM/import-table/import-table.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_Services_file_upload_service__WEBPACK_IMPORTED_MODULE_3__["FileUploadService"], _angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], ImportTableComponent);
    return ImportTableComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/CADM/cadm-add/cadm-add.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/Main_masters/CADM/cadm-add/cadm-add.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n@import url('https://fonts.googleapis.com/css?family=Numans');\n/* Made with love by Mutiullah Samim*/\nhtml,body{\n\nbackground-size: cover;\nbackground-repeat: no-repeat;\nheight: 100%;\nfont-family: 'Numans', sans-serif;\n}\nlabel\n{\n    color: #fff;\n}\n.container{\nheight: 100%;\nbackground-size: cover;\nwidth:100%;\nbackground-repeat: no-repeat;\n/* align-content: center; */\n}\n.card{\nheight: 700px;\nmargin-top:20px;\nmargin-bottom:20px;\nwidth: 450px;\nbackground-color: rgba(0,0,0,0.5) !important;\n}\n.social_icon span{\nfont-size: 60px;\nmargin-left: 10px;\ncolor: #FFC312;\n}\n.social_icon span:hover{\ncolor: white;\ncursor: pointer;\n}\n.card-header h4{\ncolor: white;\n}\n.social_icon{\nposition: absolute;\nright: 20px;\ntop: -45px;\n}\n.input-group-prepend span{\nwidth: 50px;\nbackground-color: #FFC312;\ncolor: black;\nborder:0 !important;\n}\ninput:focus{\noutline: 0 0 0 0  !important;\nbox-shadow: 0 0 0 0 !important;\n\n}\n.remember{\ncolor: white;\n}\n.remember input\n{\nwidth: 20px;\nheight: 20px;\nmargin-left: 15px;\nmargin-right: 5px;\n}\n.login_btn{\ncolor: black;\nbackground-color: #FFC312;\nwidth: 100px;\n}\n.login_btn:hover{\ncolor: black;\nbackground-color: white;\n}\n.links{\ncolor: white;\n}\n.links a{\nmargin-left: 4px;\n}\noption\n{\n    margin-bottom: 10px!important;\n\n}\n/* *************************************************************************\n**********************  Media query for Mobile screen  */\n@media only screen and (max-width : 768px) {\n  .card{\n      \n    margin-top:auto;\n    margin-bottom:auto;\n      }\n\n}\n@media only screen and (max-width : 480px) {\n    .card{\n        height: 1030px;\n        margin-top:20px;\n        margin-bottom:20px;\n\n        }\n\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0NBRE0vY2FkbS1hZGQvY2FkbS1hZGQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLDZEQUE2RDtBQUY3RCxxQ0FBcUM7QUFJckM7O0FBRUEsc0JBQXNCO0FBQ3RCLDRCQUE0QjtBQUM1QixZQUFZO0FBQ1osaUNBQWlDO0FBQ2pDO0FBQ0E7O0lBRUksV0FBVztBQUNmO0FBQ0E7QUFDQSxZQUFZO0FBQ1osc0JBQXNCO0FBQ3RCLFVBQVU7QUFDViw0QkFBNEI7QUFDNUIsMkJBQTJCO0FBQzNCO0FBRUE7QUFDQSxhQUFhO0FBQ2IsZUFBZTtBQUNmLGtCQUFrQjtBQUNsQixZQUFZO0FBQ1osNENBQTRDO0FBQzVDO0FBRUE7QUFDQSxlQUFlO0FBQ2YsaUJBQWlCO0FBQ2pCLGNBQWM7QUFDZDtBQUVBO0FBQ0EsWUFBWTtBQUNaLGVBQWU7QUFDZjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxrQkFBa0I7QUFDbEIsV0FBVztBQUNYLFVBQVU7QUFDVjtBQUVBO0FBQ0EsV0FBVztBQUNYLHlCQUF5QjtBQUN6QixZQUFZO0FBQ1osbUJBQW1CO0FBQ25CO0FBRUE7QUFDQSw0QkFBNEI7QUFDNUIsOEJBQThCOztBQUU5QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7O0FBRUEsV0FBVztBQUNYLFlBQVk7QUFDWixpQkFBaUI7QUFDakIsaUJBQWlCO0FBQ2pCO0FBRUE7QUFDQSxZQUFZO0FBQ1oseUJBQXlCO0FBQ3pCLFlBQVk7QUFDWjtBQUVBO0FBQ0EsWUFBWTtBQUNaLHVCQUF1QjtBQUN2QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQTs7SUFFSSw2QkFBNkI7O0FBRWpDO0FBSUE7d0RBQ3dEO0FBQ3hEO0VBQ0U7O0lBRUUsZUFBZTtJQUNmLGtCQUFrQjtNQUNoQjs7QUFFTjtBQUVBO0lBQ0k7UUFDSSxjQUFjO1FBQ2QsZUFBZTtRQUNmLGtCQUFrQjs7UUFFbEI7O0FBRVIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvQ0FETS9jYWRtLWFkZC9jYWRtLWFkZC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogTWFkZSB3aXRoIGxvdmUgYnkgTXV0aXVsbGFoIFNhbWltKi9cblxuQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1OdW1hbnMnKTtcblxuaHRtbCxib2R5e1xuXG5iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbmhlaWdodDogMTAwJTtcbmZvbnQtZmFtaWx5OiAnTnVtYW5zJywgc2Fucy1zZXJpZjtcbn1cbmxhYmVsXG57XG4gICAgY29sb3I6ICNmZmY7XG59XG4uY29udGFpbmVye1xuaGVpZ2h0OiAxMDAlO1xuYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbndpZHRoOjEwMCU7XG5iYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuLyogYWxpZ24tY29udGVudDogY2VudGVyOyAqL1xufVxuXG4uY2FyZHtcbmhlaWdodDogNzAwcHg7XG5tYXJnaW4tdG9wOjIwcHg7XG5tYXJnaW4tYm90dG9tOjIwcHg7XG53aWR0aDogNDUwcHg7XG5iYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuNSkgIWltcG9ydGFudDtcbn1cblxuLnNvY2lhbF9pY29uIHNwYW57XG5mb250LXNpemU6IDYwcHg7XG5tYXJnaW4tbGVmdDogMTBweDtcbmNvbG9yOiAjRkZDMzEyO1xufVxuXG4uc29jaWFsX2ljb24gc3Bhbjpob3ZlcntcbmNvbG9yOiB3aGl0ZTtcbmN1cnNvcjogcG9pbnRlcjtcbn1cblxuLmNhcmQtaGVhZGVyIGg0e1xuY29sb3I6IHdoaXRlO1xufVxuXG4uc29jaWFsX2ljb257XG5wb3NpdGlvbjogYWJzb2x1dGU7XG5yaWdodDogMjBweDtcbnRvcDogLTQ1cHg7XG59XG5cbi5pbnB1dC1ncm91cC1wcmVwZW5kIHNwYW57XG53aWR0aDogNTBweDtcbmJhY2tncm91bmQtY29sb3I6ICNGRkMzMTI7XG5jb2xvcjogYmxhY2s7XG5ib3JkZXI6MCAhaW1wb3J0YW50O1xufVxuXG5pbnB1dDpmb2N1c3tcbm91dGxpbmU6IDAgMCAwIDAgICFpbXBvcnRhbnQ7XG5ib3gtc2hhZG93OiAwIDAgMCAwICFpbXBvcnRhbnQ7XG5cbn1cblxuLnJlbWVtYmVye1xuY29sb3I6IHdoaXRlO1xufVxuXG4ucmVtZW1iZXIgaW5wdXRcbntcbndpZHRoOiAyMHB4O1xuaGVpZ2h0OiAyMHB4O1xubWFyZ2luLWxlZnQ6IDE1cHg7XG5tYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLmxvZ2luX2J0bntcbmNvbG9yOiBibGFjaztcbmJhY2tncm91bmQtY29sb3I6ICNGRkMzMTI7XG53aWR0aDogMTAwcHg7XG59XG5cbi5sb2dpbl9idG46aG92ZXJ7XG5jb2xvcjogYmxhY2s7XG5iYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cblxuLmxpbmtze1xuY29sb3I6IHdoaXRlO1xufVxuXG4ubGlua3MgYXtcbm1hcmdpbi1sZWZ0OiA0cHg7XG59XG5vcHRpb25cbntcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4IWltcG9ydGFudDtcblxufVxuXG5cblxuLyogKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuKioqKioqKioqKioqKioqKioqKioqKiAgTWVkaWEgcXVlcnkgZm9yIE1vYmlsZSBzY3JlZW4gICovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGggOiA3NjhweCkge1xuICAuY2FyZHtcbiAgICAgIFxuICAgIG1hcmdpbi10b3A6YXV0bztcbiAgICBtYXJnaW4tYm90dG9tOmF1dG87XG4gICAgICB9XG5cbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoIDogNDgwcHgpIHtcbiAgICAuY2FyZHtcbiAgICAgICAgaGVpZ2h0OiAxMDMwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6MjBweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbToyMHB4O1xuXG4gICAgICAgIH1cblxufSJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/CADM/cadm-add/cadm-add.component.html":
/*!********************************************************************!*\
  !*** ./src/app/Main_masters/CADM/cadm-add/cadm-add.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"d-flex justify-content-center h-100\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        <h4 style=\"text-align:center\">Add Chemical Agent Or Distributor</h4>\n      </div>\n      <div class=\"card-body\">\n        <form class=\"form-login\" role=\"form\" #formRef='ngForm' (ngSubmit)='onSubmit(formRef.value)'>\n          <div class=\"form-group row\">\n            <label for=\"subDep\" class=\"col-sm-5 col-form-label\">Chemical Agent Or Distributor Name</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDep\" name=\"newChemicAgent\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepPassword\" class=\"col-sm-5 col-form-label\">Address</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepPassword\" name=\"address\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepPassword\" class=\"col-sm-5 col-form-label\">Pincode</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepPassword\" name=\"Pincode\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepcfPassword\" class=\"col-sm-5 col-form-label\">Contact Person</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepcfPassword\" name=\"Cperson\" #selectRef='ngModel'\n                required ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Mobile Number</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"mobile\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Alternate Mob No</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"altermobile\" #selectRef='ngModel'\n                required ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">E-Mail ID</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"Email\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">GSTIN Number</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"GSTIN\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Remark</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"remarks\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n\n          <div class=\"form-group\" style=\"text-align:center\">\n            <button style=\"padding: 0;\n                  border: none;\n                  background: none;\" type=\"submit\">\n              <!-- <i class=\"far fa-check-circle\"> -->\n              <i class=\"fas fa-check-circle\" style=\"font-size:60px;color: yellow;\"></i>\n\n            </button>\n          </div>\n\n        </form>\n      </div>\n\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/CADM/cadm-add/cadm-add.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/Main_masters/CADM/cadm-add/cadm-add.component.ts ***!
  \******************************************************************/
/*! exports provided: CadmAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadmAddComponent", function() { return CadmAddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CadmAddComponent = /** @class */ (function () {
    function CadmAddComponent(router, _http, headerTitleService) {
        this.router = router;
        this._http = _http;
        this.headerTitleService = headerTitleService;
    }
    CadmAddComponent.prototype.ngOnInit = function () {
        this.headerTitleService.setTitle('Chemical Agent Distributor Master');
    };
    CadmAddComponent.prototype.onSubmit = function (a) {
        var _this = this;
        this.newChemicAgent = a.newChemicAgent;
        this.address = a.address;
        this.Pincode = a.Pincode;
        this.Cperson = a.Cperson;
        this.mobile = a.mobile;
        this.altermobile = a.altermobile;
        this.Email = a.Email;
        this.GSTIN = a.GSTIN;
        this.remarks = a.remarks;
        console.log(a);
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.post('api/add-chemical-agent-distributor', a, options)
            .subscribe(function (data) {
            alert('Added Successfully');
            _this.router.navigate(['/main-masters']);
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    CadmAddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-cadm-add',
            template: __webpack_require__(/*! ./cadm-add.component.html */ "./src/app/Main_masters/CADM/cadm-add/cadm-add.component.html"),
            styles: [__webpack_require__(/*! ./cadm-add.component.css */ "./src/app/Main_masters/CADM/cadm-add/cadm-add.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"]])
    ], CadmAddComponent);
    return CadmAddComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/CADM/cadm-edit/cadm-edit.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/Main_masters/CADM/cadm-edit/cadm-edit.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table,tr,th,td\n{\n    border:1px solid #fff;\n}\n#export-button\n{\n    padding-left:20px;\n    padding-right:20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0NBRE0vY2FkbS1lZGl0L2NhZG0tZWRpdC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztJQUVJLHFCQUFxQjtBQUN6QjtBQUNBOztJQUVJLGlCQUFpQjtJQUNqQixrQkFBa0I7QUFDdEIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvQ0FETS9jYWRtLWVkaXQvY2FkbS1lZGl0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZSx0cix0aCx0ZFxue1xuICAgIGJvcmRlcjoxcHggc29saWQgI2ZmZjtcbn1cbiNleHBvcnQtYnV0dG9uXG57XG4gICAgcGFkZGluZy1sZWZ0OjIwcHg7XG4gICAgcGFkZGluZy1yaWdodDoyMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/CADM/cadm-edit/cadm-edit.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/Main_masters/CADM/cadm-edit/cadm-edit.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\n<div class=\"container table-responsive-lg table-responsive-sm table-responsive-md\" style=\"text-align: -webkit-center;\">\n  <div *ngIf=\"update\">\n    <input type=\"text\" placeholder=\"New Chemical Agent\" name=\"name\" [(ngModel)]=datatobeedited.newChemicAgent>\n    <input type=\"text\" placeholder=\"address\" name=\"name\" [(ngModel)]=datatobeedited.address>\n    <input type=\"text\" placeholder=\"Pincode\" name=\"name\" [(ngModel)]=datatobeedited.Pincode>\n    <input type=\"text\" placeholder=\"Contact person\" name=\"name\" [(ngModel)]=datatobeedited.Cperson>\n    <input type=\"text\" placeholder=\"mobile\" name=\"name\" [(ngModel)]=datatobeedited.mobile>\n    <input type=\"text\" placeholder=\"Alter Mobile number\" name=\"name\" [(ngModel)]=datatobeedited.altermobile>\n    <input type=\"text\" placeholder=\"Email\" name=\"name\" [(ngModel)]=datatobeedited.Email>\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.GSTIN>\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.remarks>\n    <button (click)='saveUpdate(datatobeedited)' type=\"button\" class=\"btn btn-sm btn-info\" style=\"margin-top: -5px;font-size: 13.9px;\">save</button>\n  </div>\n  <br>\n  <br>\n  <table class=\"table\">\n    <thead style=\"text-align: -webkit-center;color: cornsilk;\">\n      <tr class=\"text-color\">\n        <th>New Chemical Agent Or Distributor Name</th>\n        <th>Address</th>\n        <th>Pincode</th>\n        <th>Contact Person</th>\n        <th>Mobile No</th>\n        <th>Alter Mobile number</th>\n        <th>Email Id</th>\n        <th>GSTIN number</th>\n        <th>Remark</th>\n        <th>Edit</th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor='let i of data; let in=index' class=\"success\" style=\"color: white;\">\n        <td role=\"cell\">{{i.newChemicAgent}}</td>\n        <td role=\"cell\">{{i.address}}</td>\n        <td role=\"cell\">{{i.Pincode}}</td>\n        <td role=\"cell\">{{i.Cperson}}</td>\n        <td role=\"cell\">{{i.mobile}}</td>\n        <td role=\"cell\">{{i.altermobile}}</td>\n        <td role=\"cell\">{{i.Email}}</td>\n        <td role=\"cell\">{{i.GSTIN}}</td>\n        <td role=\"cell\">{{i.remarks}}</td>\n        <td><button (click)='editData(i,in)' type=\"button\" class=\"btn btn-large btn-info\">Edit</button></td>\n      </tr>\n    </tbody>\n  </table>\n  <button id=\"export-button\" type=\"button\" (click)=\"exportExcel()\" class=\"btn btn-info btn-lg \">Export</button>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/CADM/cadm-edit/cadm-edit.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/Main_masters/CADM/cadm-edit/cadm-edit.component.ts ***!
  \********************************************************************/
/*! exports provided: CadmEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadmEditComponent", function() { return CadmEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Services_cadm_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../Services/cadm.service */ "./src/app/Services/cadm.service.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
/* harmony import */ var angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular2-csv/Angular2-csv */ "./node_modules/angular2-csv/Angular2-csv.js");
/* harmony import */ var angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_5__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CadmEditComponent = /** @class */ (function () {
    function CadmEditComponent(CadmService, _http, headerTitleService, router) {
        this.CadmService = CadmService;
        this._http = _http;
        this.headerTitleService = headerTitleService;
        this.router = router;
        this.update = false;
        this.test = [{}];
    }
    CadmEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.CadmService.getData().subscribe(function (CadmData) { return _this.data = CadmData; });
        this.headerTitleService.setTitle('Chemical Agent Distributor  Master');
    };
    CadmEditComponent.prototype.editData = function (i) {
        this.update = true;
        this.datatobeedited = i;
        console.log("obj is" + this.datatobeedited.newChemicAgent);
    };
    CadmEditComponent.prototype.saveUpdate = function (datasaved) {
        console.log("datasaved" + datasaved.newChemicAgent);
        console.log("datasaved" + '' + datasaved.newChemicAgent + '' +
            '' + datasaved.address + '' +
            '' + datasaved.Pincode + '' +
            '' + datasaved.Cperson + '' +
            '' + datasaved.mobile);
        var _url = "http://localhost:3000/api/add-chemical-agent-distributor" + "/" + datasaved._id;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.put(_url, datasaved, options)
            .subscribe(function (data) {
            alert('saved Successfully');
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    CadmEditComponent.prototype.exportExcel = function () {
        var head = [
            'newChemicAgent',
            'address',
            'Pincode',
            'Cperson',
            'mobile',
            'altermobile',
            'Email',
            'GSTIN',
            'remarks'
        ];
        new angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_5__["Angular2Csv"](this.test, 'Chemical Agent Master', { headers: (head) });
        alert("Please Give the Input Details");
        this.router.navigate(['/import-chemical-agent-distributor']);
    };
    CadmEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-cadm-edit',
            template: __webpack_require__(/*! ./cadm-edit.component.html */ "./src/app/Main_masters/CADM/cadm-edit/cadm-edit.component.html"),
            styles: [__webpack_require__(/*! ./cadm-edit.component.css */ "./src/app/Main_masters/CADM/cadm-edit/cadm-edit.component.css")]
        }),
        __metadata("design:paramtypes", [_Services_cadm_service__WEBPACK_IMPORTED_MODULE_1__["CadmService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_4__["HeaderTitleService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], CadmEditComponent);
    return CadmEditComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/CADM/cadm-import/cadm-import.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/Main_masters/CADM/cadm-import/cadm-import.component.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n.center-div\n{\n    position: absolute;\n    left: 50%;\n    top: 50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%);\n    \n    /*\n    This doesn't work\n    margin-left: -25%;\n    margin-top: -25%;\n    */\n    \n    width: 400px;\n    height: 300px;\n  \n    padding: 20px;  \n    background-color: rgba(0,0,0,0.5) !important;\n    color: white;\n    text-align: center;\n    box-shadow: 0 0 30px rgba(0, 0, 0, 0.2);\n}\n/* @media (max-width: 1000px) {\n    \n    .center-inner{left:25%;top:25%;position:absolute;width:50%;height:300px;background-color: rgba(0,0,0,0.5) !important;text-align:center;max-width:500px;max-height:500px;color:#ffffff}\n    } */\n.center{left:50%;top:25%;position:absolute;}\n.center-inner{width:400px;height:100%;margin-left:-250px;height:300px;background-color: rgba(0,0,0,0.5) !important;text-align:center;max-width:500px;max-height:500px;color:#ffffff;padding: 20px;border-radius: 20px;}\n@media (max-width: 428px) {\n    .center-inner{width:300px;height:100%;margin-left:-150px;height:300px;background-color: rgba(0,0,0,0.5) !important;text-align:center;max-width:500px;max-height:500px;color:#ffffff;padding: 20px;border-radius: 20px;}\n    .center{left:50%;top:25%;position:absolute;}\n\n    }\ninput[type=\"file\"] {\n        display: none;\n    }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0NBRE0vY2FkbS1pbXBvcnQvY2FkbS1pbXBvcnQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0E7O0lBRUksa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxRQUFRO0lBQ1Isd0NBQWdDO1lBQWhDLGdDQUFnQzs7SUFFaEM7Ozs7S0FJQzs7SUFFRCxZQUFZO0lBQ1osYUFBYTs7SUFFYixhQUFhO0lBQ2IsNENBQTRDO0lBQzVDLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsdUNBQXVDO0FBQzNDO0FBQ0E7OztPQUdPO0FBRUgsUUFBUSxRQUFRLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDO0FBQzNDLGNBQWMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsNENBQTRDLENBQUMsaUJBQWlCLENBQUMsZUFBZSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUM7QUFFdE47SUFDQSxjQUFjLFdBQVcsQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLDRDQUE0QyxDQUFDLGlCQUFpQixDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDO0lBQ3ROLFFBQVEsUUFBUSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQzs7SUFFM0M7QUFDQTtRQUNJLGFBQWE7SUFDakIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvQ0FETS9jYWRtLWltcG9ydC9jYWRtLWltcG9ydC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG4uY2VudGVyLWRpdlxue1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiA1MCU7XG4gICAgdG9wOiA1MCU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gICAgXG4gICAgLypcbiAgICBUaGlzIGRvZXNuJ3Qgd29ya1xuICAgIG1hcmdpbi1sZWZ0OiAtMjUlO1xuICAgIG1hcmdpbi10b3A6IC0yNSU7XG4gICAgKi9cbiAgICBcbiAgICB3aWR0aDogNDAwcHg7XG4gICAgaGVpZ2h0OiAzMDBweDtcbiAgXG4gICAgcGFkZGluZzogMjBweDsgIFxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgYm94LXNoYWRvdzogMCAwIDMwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xufVxuLyogQG1lZGlhIChtYXgtd2lkdGg6IDEwMDBweCkge1xuICAgIFxuICAgIC5jZW50ZXItaW5uZXJ7bGVmdDoyNSU7dG9wOjI1JTtwb3NpdGlvbjphYnNvbHV0ZTt3aWR0aDo1MCU7aGVpZ2h0OjMwMHB4O2JhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O3RleHQtYWxpZ246Y2VudGVyO21heC13aWR0aDo1MDBweDttYXgtaGVpZ2h0OjUwMHB4O2NvbG9yOiNmZmZmZmZ9XG4gICAgfSAqL1xuICAgIFxuICAgIC5jZW50ZXJ7bGVmdDo1MCU7dG9wOjI1JTtwb3NpdGlvbjphYnNvbHV0ZTt9XG4gICAgLmNlbnRlci1pbm5lcnt3aWR0aDo0MDBweDtoZWlnaHQ6MTAwJTttYXJnaW4tbGVmdDotMjUwcHg7aGVpZ2h0OjMwMHB4O2JhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O3RleHQtYWxpZ246Y2VudGVyO21heC13aWR0aDo1MDBweDttYXgtaGVpZ2h0OjUwMHB4O2NvbG9yOiNmZmZmZmY7cGFkZGluZzogMjBweDtib3JkZXItcmFkaXVzOiAyMHB4O31cbiAgXG4gICAgQG1lZGlhIChtYXgtd2lkdGg6IDQyOHB4KSB7XG4gICAgLmNlbnRlci1pbm5lcnt3aWR0aDozMDBweDtoZWlnaHQ6MTAwJTttYXJnaW4tbGVmdDotMTUwcHg7aGVpZ2h0OjMwMHB4O2JhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O3RleHQtYWxpZ246Y2VudGVyO21heC13aWR0aDo1MDBweDttYXgtaGVpZ2h0OjUwMHB4O2NvbG9yOiNmZmZmZmY7cGFkZGluZzogMjBweDtib3JkZXItcmFkaXVzOiAyMHB4O31cbiAgICAuY2VudGVye2xlZnQ6NTAlO3RvcDoyNSU7cG9zaXRpb246YWJzb2x1dGU7fVxuXG4gICAgfVxuICAgIGlucHV0W3R5cGU9XCJmaWxlXCJdIHtcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICB9Il19 */"

/***/ }),

/***/ "./src/app/Main_masters/CADM/cadm-import/cadm-import.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/Main_masters/CADM/cadm-import/cadm-import.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"center\">\n  <div class=\"center-inner\">\n    <form id=\"login\">\n      <h4>Import File</h4>\n      <hr>\n      <p>* Only CSV File can be import. </p>\n\n      <input type=\"file\" name=\"files\" class=\"form-control\" #uploads (change)=\"onChange(uploads.files)\" multiple value=\"process\"\n        #file />\n      <div class=\"input-group mb-3\">\n        <div class=\"input-group-prepend\">\n          <button class=\"btn btn-outline-primary\" (click)=\"file.click()\" type=\"button\">Browser</button>\n        </div>\n        <input type=\"text\" value=\"{{fileName}}\" class=\"form-control form-control-lg\" aria-label=\"Default\"\n          aria-describedby=\"inputGroup-sizing-default\">\n      </div>\n      <br>\n      <div class=\"form-group\" style=\"text-align:center\">\n        <button style=\"padding: 0;\n                          border: none;\n                          background: none;\"\n          type=\"button\" (click)=\"onUpload()\">\n\n          <i class=\"fas fa-check-circle\" style=\"font-size:70px;color: yellow;\"></i>\n\n        </button>\n      </div>\n\n    </form>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/CADM/cadm-import/cadm-import.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/Main_masters/CADM/cadm-import/cadm-import.component.ts ***!
  \************************************************************************/
/*! exports provided: CadmImportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadmImportComponent", function() { return CadmImportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
/* harmony import */ var papaparse__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! papaparse */ "./node_modules/papaparse/papaparse.min.js");
/* harmony import */ var papaparse__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(papaparse__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CadmImportComponent = /** @class */ (function () {
    function CadmImportComponent(headerTitleService, http, router) {
        this.headerTitleService = headerTitleService;
        this.http = http;
        this.router = router;
    }
    CadmImportComponent.prototype.ngOnInit = function () {
        this.headerTitleService.setTitle('Chemical Agent Distributor Master');
    };
    CadmImportComponent.prototype.onChange = function (files) {
        var _this = this;
        if (files[0]) {
            console.log(files[0]);
            papaparse__WEBPACK_IMPORTED_MODULE_2__["parse"](files[0], {
                header: true,
                skipEmptyLines: true,
                complete: function (result, file) {
                    console.log(result);
                    _this.fileName = file.name;
                    _this.dataList = result.data;
                    console.log(_this.dataList);
                    //post api
                    var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["Headers"]();
                    headers.append('Content-Type', 'application/json');
                    var options = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["RequestOptions"]({ headers: headers });
                    _this.http.post('api/cadmFileUpload', _this.dataList, options)
                        .subscribe(function (data) {
                        console.log("added successfully");
                    }, function (error) {
                        console.log(JSON.stringify(error.json()));
                    });
                }
            });
        }
    };
    CadmImportComponent.prototype.onUpload = function () {
        this.router.navigate(['/edit-chemical-agent-distributor']);
    };
    CadmImportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-cadm-import',
            template: __webpack_require__(/*! ./cadm-import.component.html */ "./src/app/Main_masters/CADM/cadm-import/cadm-import.component.html"),
            styles: [__webpack_require__(/*! ./cadm-import.component.css */ "./src/app/Main_masters/CADM/cadm-import/cadm-import.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_1__["HeaderTitleService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_4__["Http"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], CadmImportComponent);
    return CadmImportComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/CAM/cam-add/cam-add.component.css":
/*!****************************************************************!*\
  !*** ./src/app/Main_masters/CAM/cam-add/cam-add.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n@import url('https://fonts.googleapis.com/css?family=Numans');\n/* Made with love by Mutiullah Samim*/\nhtml,body{\n\nbackground-size: cover;\nbackground-repeat: no-repeat;\nheight: 100%;\nfont-family: 'Numans', sans-serif;\n}\nlabel\n{\n    color: #fff;\n}\n.container{\nheight: 100%;\nbackground-size: cover;\nwidth:100%;\nbackground-repeat: no-repeat;\n/* align-content: center; */\n}\n.card{\nheight: 500px;\nmargin-top:20px;\nmargin-bottom:20px;\nwidth: 450px;\nbackground-color: rgba(0,0,0,0.5) !important;\n}\n.social_icon span{\nfont-size: 60px;\nmargin-left: 10px;\ncolor: #FFC312;\n}\n.social_icon span:hover{\ncolor: white;\ncursor: pointer;\n}\n.card-header h3{\ncolor: white;\n}\n.social_icon{\nposition: absolute;\nright: 20px;\ntop: -45px;\n}\n.input-group-prepend span{\nwidth: 50px;\nbackground-color: #FFC312;\ncolor: black;\nborder:0 !important;\n}\ninput:focus{\noutline: 0 0 0 0  !important;\nbox-shadow: 0 0 0 0 !important;\n\n}\n.remember{\ncolor: white;\n}\n.remember input\n{\nwidth: 20px;\nheight: 20px;\nmargin-left: 15px;\nmargin-right: 5px;\n}\n.login_btn{\ncolor: black;\nbackground-color: #FFC312;\nwidth: 100px;\n}\n.login_btn:hover{\ncolor: black;\nbackground-color: white;\n}\n.links{\ncolor: white;\n}\n.links a{\nmargin-left: 4px;\n}\noption\n{\n    margin-bottom: 10px!important;\n\n}\n/* *************************************************************************\n**********************  Media query for Mobile screen  */\n@media only screen and (max-width : 768px) {\n  .card{\n      \n    margin-top:50px;\n    margin-bottom:auto;\n      }\n\n}\n@media only screen and (max-width : 480px) {\n    .card{\n        height: 500px;\n        margin-top:20px;\n        margin-bottom:20px;\n\n        }\n\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0NBTS9jYW0tYWRkL2NhbS1hZGQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLDZEQUE2RDtBQUY3RCxxQ0FBcUM7QUFJckM7O0FBRUEsc0JBQXNCO0FBQ3RCLDRCQUE0QjtBQUM1QixZQUFZO0FBQ1osaUNBQWlDO0FBQ2pDO0FBQ0E7O0lBRUksV0FBVztBQUNmO0FBQ0E7QUFDQSxZQUFZO0FBQ1osc0JBQXNCO0FBQ3RCLFVBQVU7QUFDViw0QkFBNEI7QUFDNUIsMkJBQTJCO0FBQzNCO0FBRUE7QUFDQSxhQUFhO0FBQ2IsZUFBZTtBQUNmLGtCQUFrQjtBQUNsQixZQUFZO0FBQ1osNENBQTRDO0FBQzVDO0FBRUE7QUFDQSxlQUFlO0FBQ2YsaUJBQWlCO0FBQ2pCLGNBQWM7QUFDZDtBQUVBO0FBQ0EsWUFBWTtBQUNaLGVBQWU7QUFDZjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxrQkFBa0I7QUFDbEIsV0FBVztBQUNYLFVBQVU7QUFDVjtBQUVBO0FBQ0EsV0FBVztBQUNYLHlCQUF5QjtBQUN6QixZQUFZO0FBQ1osbUJBQW1CO0FBQ25CO0FBRUE7QUFDQSw0QkFBNEI7QUFDNUIsOEJBQThCOztBQUU5QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7O0FBRUEsV0FBVztBQUNYLFlBQVk7QUFDWixpQkFBaUI7QUFDakIsaUJBQWlCO0FBQ2pCO0FBRUE7QUFDQSxZQUFZO0FBQ1oseUJBQXlCO0FBQ3pCLFlBQVk7QUFDWjtBQUVBO0FBQ0EsWUFBWTtBQUNaLHVCQUF1QjtBQUN2QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQTs7SUFFSSw2QkFBNkI7O0FBRWpDO0FBSUE7d0RBQ3dEO0FBQ3hEO0VBQ0U7O0lBRUUsZUFBZTtJQUNmLGtCQUFrQjtNQUNoQjs7QUFFTjtBQUVBO0lBQ0k7UUFDSSxhQUFhO1FBQ2IsZUFBZTtRQUNmLGtCQUFrQjs7UUFFbEI7O0FBRVIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvQ0FNL2NhbS1hZGQvY2FtLWFkZC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogTWFkZSB3aXRoIGxvdmUgYnkgTXV0aXVsbGFoIFNhbWltKi9cblxuQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1OdW1hbnMnKTtcblxuaHRtbCxib2R5e1xuXG5iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbmhlaWdodDogMTAwJTtcbmZvbnQtZmFtaWx5OiAnTnVtYW5zJywgc2Fucy1zZXJpZjtcbn1cbmxhYmVsXG57XG4gICAgY29sb3I6ICNmZmY7XG59XG4uY29udGFpbmVye1xuaGVpZ2h0OiAxMDAlO1xuYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbndpZHRoOjEwMCU7XG5iYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuLyogYWxpZ24tY29udGVudDogY2VudGVyOyAqL1xufVxuXG4uY2FyZHtcbmhlaWdodDogNTAwcHg7XG5tYXJnaW4tdG9wOjIwcHg7XG5tYXJnaW4tYm90dG9tOjIwcHg7XG53aWR0aDogNDUwcHg7XG5iYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuNSkgIWltcG9ydGFudDtcbn1cblxuLnNvY2lhbF9pY29uIHNwYW57XG5mb250LXNpemU6IDYwcHg7XG5tYXJnaW4tbGVmdDogMTBweDtcbmNvbG9yOiAjRkZDMzEyO1xufVxuXG4uc29jaWFsX2ljb24gc3Bhbjpob3ZlcntcbmNvbG9yOiB3aGl0ZTtcbmN1cnNvcjogcG9pbnRlcjtcbn1cblxuLmNhcmQtaGVhZGVyIGgze1xuY29sb3I6IHdoaXRlO1xufVxuXG4uc29jaWFsX2ljb257XG5wb3NpdGlvbjogYWJzb2x1dGU7XG5yaWdodDogMjBweDtcbnRvcDogLTQ1cHg7XG59XG5cbi5pbnB1dC1ncm91cC1wcmVwZW5kIHNwYW57XG53aWR0aDogNTBweDtcbmJhY2tncm91bmQtY29sb3I6ICNGRkMzMTI7XG5jb2xvcjogYmxhY2s7XG5ib3JkZXI6MCAhaW1wb3J0YW50O1xufVxuXG5pbnB1dDpmb2N1c3tcbm91dGxpbmU6IDAgMCAwIDAgICFpbXBvcnRhbnQ7XG5ib3gtc2hhZG93OiAwIDAgMCAwICFpbXBvcnRhbnQ7XG5cbn1cblxuLnJlbWVtYmVye1xuY29sb3I6IHdoaXRlO1xufVxuXG4ucmVtZW1iZXIgaW5wdXRcbntcbndpZHRoOiAyMHB4O1xuaGVpZ2h0OiAyMHB4O1xubWFyZ2luLWxlZnQ6IDE1cHg7XG5tYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLmxvZ2luX2J0bntcbmNvbG9yOiBibGFjaztcbmJhY2tncm91bmQtY29sb3I6ICNGRkMzMTI7XG53aWR0aDogMTAwcHg7XG59XG5cbi5sb2dpbl9idG46aG92ZXJ7XG5jb2xvcjogYmxhY2s7XG5iYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cblxuLmxpbmtze1xuY29sb3I6IHdoaXRlO1xufVxuXG4ubGlua3MgYXtcbm1hcmdpbi1sZWZ0OiA0cHg7XG59XG5vcHRpb25cbntcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4IWltcG9ydGFudDtcblxufVxuXG5cblxuLyogKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuKioqKioqKioqKioqKioqKioqKioqKiAgTWVkaWEgcXVlcnkgZm9yIE1vYmlsZSBzY3JlZW4gICovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGggOiA3NjhweCkge1xuICAuY2FyZHtcbiAgICAgIFxuICAgIG1hcmdpbi10b3A6NTBweDtcbiAgICBtYXJnaW4tYm90dG9tOmF1dG87XG4gICAgICB9XG5cbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoIDogNDgwcHgpIHtcbiAgICAuY2FyZHtcbiAgICAgICAgaGVpZ2h0OiA1MDBweDtcbiAgICAgICAgbWFyZ2luLXRvcDoyMHB4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOjIwcHg7XG5cbiAgICAgICAgfVxuXG59Il19 */"

/***/ }),

/***/ "./src/app/Main_masters/CAM/cam-add/cam-add.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/Main_masters/CAM/cam-add/cam-add.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <form class=\"form-login\" role=\"form\" #formRef='ngForm' (ngSubmit)='onSubmit(formRef.value)'>\n    <div class=\"d-flex justify-content-center h-100\">\n      <div class=\"card\">\n        <div class=\"card-header\">\n          <h3 style=\"text-align:center\">Chemical Assign Master</h3>\n        </div>\n        <div class=\"card-body\">\n          <div class=\"form-group row\">\n            <label for=\"seldep\" class=\"col-sm-5 col-form-label\">Chemical Manufacturer</label>\n            <div class=\"col-sm-7\">\n              <select class=\"form-control\" id=\"seldep\" name=\"choice\" id=\"selectdep\" #selectRef='ngModel' required\n                ngModel>\n                <option selected>Chemical Manufacturer</option>\n                <option *ngFor=\"let data of dropdown1\">{{data.newchemicalman}}</option>\n\n              </select>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"seldep\" class=\"col-sm-5 col-form-label\">Chemical Product Name</label>\n            <div class=\"col-sm-7\">\n              <select class=\"form-control\" id=\"seldep\" name=\"choice1\" id=\"selectdep\" #selectRef='ngModel' required\n                ngModel>\n                <option selected>Chemical Product Name</option>\n                <option *ngFor=\"let j of dropdown2\">{{j.newproductname}}</option>\n\n              </select>\n            </div>\n          </div>\n          <br>\n          <div class=\"form-group row\">\n            <label for=\"seldep\" class=\"col-sm-5 col-form-label\">Chemical Agent</label>\n            <div class=\"col-sm-7\">\n              <select class=\"form-control\" id=\"selectdep\" name=\"choice2\" #selectRef='ngModel' required ngModel>\n                <option selected>Chemical Agent</option>\n                <option *ngFor=\"let data1 of dropdown\">{{data1.newChemicAgent}}</option>\n\n              </select>\n            </div>\n          </div>\n          <br>\n          <br>\n          <div class=\"form-group\" style=\"text-align:center\">\n            <button style=\"padding: 0;\n                border: none;\n                background: none;\" type=\"submit\">\n              <!-- <i class=\"far fa-check-circle\"> -->\n              <i class=\"fas fa-check-circle\" style=\"font-size:60px;color: yellow;\"></i>\n            </button>\n          </div>\n        </div>\n      </div>\n    </div>\n  </form>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/CAM/cam-add/cam-add.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/Main_masters/CAM/cam-add/cam-add.component.ts ***!
  \***************************************************************/
/*! exports provided: CamAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CamAddComponent", function() { return CamAddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_cadm_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/cadm.service */ "./src/app/Services/cadm.service.ts");
/* harmony import */ var src_app_Services_cmm_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/Services/cmm.service */ "./src/app/Services/cmm.service.ts");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
/* harmony import */ var src_app_Services_cpm_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/Services/cpm.service */ "./src/app/Services/cpm.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var CamAddComponent = /** @class */ (function () {
    function CamAddComponent(router, _http, Cadmservice, CmmService, CpmService, headerTitleService) {
        this.router = router;
        this._http = _http;
        this.Cadmservice = Cadmservice;
        this.CmmService = CmmService;
        this.CpmService = CpmService;
        this.headerTitleService = headerTitleService;
    }
    CamAddComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.Cadmservice.getData().subscribe(function (CadmData) { return _this.dropdown = CadmData; });
        this.CmmService.getData().subscribe(function (CmmData) { return _this.dropdown1 = CmmData; });
        this.CpmService.getData().subscribe(function (CpmData) { return _this.dropdown2 = CpmData; });
        // header title change
        this.headerTitleService.setTitle('Chemical Assign Master');
    };
    CamAddComponent.prototype.onSubmit = function (a) {
        var _this = this;
        this.choice = a.choice;
        this.choice1 = a.choice1;
        this.choice2 = a.choice2;
        console.log(a);
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.post('/api/add-chemical-assign-master', a, options)
            .subscribe(function (data) {
            alert('Registered Successfully');
            _this.router.navigate(['/main-masters']);
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    CamAddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-cam-add',
            template: __webpack_require__(/*! ./cam-add.component.html */ "./src/app/Main_masters/CAM/cam-add/cam-add.component.html"),
            styles: [__webpack_require__(/*! ./cam-add.component.css */ "./src/app/Main_masters/CAM/cam-add/cam-add.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            src_app_Services_cadm_service__WEBPACK_IMPORTED_MODULE_3__["CadmService"],
            src_app_Services_cmm_service__WEBPACK_IMPORTED_MODULE_4__["CmmService"],
            src_app_Services_cpm_service__WEBPACK_IMPORTED_MODULE_6__["CpmService"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_5__["HeaderTitleService"]])
    ], CamAddComponent);
    return CamAddComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/CAM/cam-assign-list/cam-assign-list.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/Main_masters/CAM/cam-assign-list/cam-assign-list.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table,tr,th,td\n{\n    border:1px solid #fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0NBTS9jYW0tYXNzaWduLWxpc3QvY2FtLWFzc2lnbi1saXN0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0lBRUkscUJBQXFCO0FBQ3pCIiwiZmlsZSI6InNyYy9hcHAvTWFpbl9tYXN0ZXJzL0NBTS9jYW0tYXNzaWduLWxpc3QvY2FtLWFzc2lnbi1saXN0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZSx0cix0aCx0ZFxue1xuICAgIGJvcmRlcjoxcHggc29saWQgI2ZmZjtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/Main_masters/CAM/cam-assign-list/cam-assign-list.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/Main_masters/CAM/cam-assign-list/cam-assign-list.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\r\n<div class=\"container table-responsive-lg table-responsive-sm table-responsive-md\" style=\"text-align: -webkit-center;\">\r\n  <div *ngIf=\"update\">\r\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.choice>\r\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.choice1>\r\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.choice2>\r\n    <button (click)='saveUpdate(datatobeedited)' type=\"button\" class=\"btn btn-sm btn-info\" style=\"margin-top: -5px;font-size: 13.9px;\">save</button>\r\n  </div>\r\n\r\n  <br>\r\n  <br>\r\n  <table class=\"table\">\r\n    <thead style=\"text-align: -webkit-center;color: cornsilk;\">\r\n      <tr class=\"text-color\">\r\n        <th>Chemical Manufacturer</th>\r\n        <th>Chemical Product Name</th>\r\n        <th>Chemical Agent</th>\r\n        <th>Edit</th>\r\n      </tr>\r\n    </thead>\r\n    <tbody>\r\n      <tr *ngFor='let i of test; let in=index' class=\"success\" style=\"text-align: -webkit-center;color: white;\">\r\n        <td>{{i.choice}}</td>\r\n        <td>{{i.choice1}}</td>\r\n        <td>{{i.choice2}}</td>\r\n        <td><button (click)='editData(i,in)' type=\"button\" class=\"btn btn-large btn-info\">Edit</button></td>\r\n      </tr>\r\n    </tbody>\r\n  </table>\r\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/CAM/cam-assign-list/cam-assign-list.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/Main_masters/CAM/cam-assign-list/cam-assign-list.component.ts ***!
  \*******************************************************************************/
/*! exports provided: CamAssignListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CamAssignListComponent", function() { return CamAssignListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Services_cam_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../Services/cam.service */ "./src/app/Services/cam.service.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CamAssignListComponent = /** @class */ (function () {
    function CamAssignListComponent(CamService, _http, headerTitleService) {
        this.CamService = CamService;
        this._http = _http;
        this.headerTitleService = headerTitleService;
        this.update = false;
    }
    CamAssignListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.CamService.getData().subscribe(function (CamData) { return _this.test = CamData; });
        this.headerTitleService.setTitle('chemical Assign Master');
    };
    CamAssignListComponent.prototype.editData = function (i) {
        this.update = true;
        this.datatobeedited = i;
        console.log("obj is" + this.datatobeedited.choice);
    };
    CamAssignListComponent.prototype.saveUpdate = function (datasaved) {
        console.log("datatobesaved" + datasaved.choice);
        console.log("datatobesaved" + '' + datasaved.choice + '' + '' + datasaved.choice1 + '' + datasaved.choice2);
        var _url = "http://localhost:3000/api/add-chemical-assign-master" + "/" + datasaved._id;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.put(_url, datasaved, options)
            .subscribe(function (data) {
            alert('saved Successfully');
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    CamAssignListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-cam-assign-list',
            template: __webpack_require__(/*! ./cam-assign-list.component.html */ "./src/app/Main_masters/CAM/cam-assign-list/cam-assign-list.component.html"),
            styles: [__webpack_require__(/*! ./cam-assign-list.component.css */ "./src/app/Main_masters/CAM/cam-assign-list/cam-assign-list.component.css")]
        }),
        __metadata("design:paramtypes", [_Services_cam_service__WEBPACK_IMPORTED_MODULE_1__["CamService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"]])
    ], CamAssignListComponent);
    return CamAssignListComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/CMM/add-cmm/add-cmm.component.css":
/*!****************************************************************!*\
  !*** ./src/app/Main_masters/CMM/add-cmm/add-cmm.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n@import url('https://fonts.googleapis.com/css?family=Numans');\n/* Made with love by Mutiullah Samim*/\nhtml,body{\r\n\r\nbackground-size: cover;\r\nbackground-repeat: no-repeat;\r\nheight: 100%;\r\nfont-family: 'Numans', sans-serif;\r\n}\nlabel\r\n{\r\n    color: #fff;\r\n}\n.container{\r\nheight: 100%;\r\nbackground-size: cover;\r\nwidth:100%;\r\nbackground-repeat: no-repeat;\r\n/* align-content: center; */\r\n}\n.card{\r\nheight: 700px;\r\nmargin-top:20px;\r\nmargin-bottom:20px;\r\nwidth: 450px;\r\nbackground-color: rgba(0,0,0,0.5) !important;\r\n}\n.social_icon span{\r\nfont-size: 60px;\r\nmargin-left: 10px;\r\ncolor: #FFC312;\r\n}\n.social_icon span:hover{\r\ncolor: white;\r\ncursor: pointer;\r\n}\n.card-header h3{\r\ncolor: white;\r\n}\n.social_icon{\r\nposition: absolute;\r\nright: 20px;\r\ntop: -45px;\r\n}\n.input-group-prepend span{\r\nwidth: 50px;\r\nbackground-color: #FFC312;\r\ncolor: black;\r\nborder:0 !important;\r\n}\ninput:focus{\r\noutline: 0 0 0 0  !important;\r\nbox-shadow: 0 0 0 0 !important;\r\n\r\n}\n.remember{\r\ncolor: white;\r\n}\n.remember input\r\n{\r\nwidth: 20px;\r\nheight: 20px;\r\nmargin-left: 15px;\r\nmargin-right: 5px;\r\n}\n.login_btn{\r\ncolor: black;\r\nbackground-color: #FFC312;\r\nwidth: 100px;\r\n}\n.login_btn:hover{\r\ncolor: black;\r\nbackground-color: white;\r\n}\n.links{\r\ncolor: white;\r\n}\n.links a{\r\nmargin-left: 4px;\r\n}\noption\r\n{\r\n    margin-bottom: 10px!important;\r\n\r\n}\n/* *************************************************************************\r\n**********************  Media query for Mobile screen  */\n@media only screen and (max-width : 768px) {\r\n  .card{\r\n      \r\n    margin-top:auto;\r\n    margin-bottom:auto;\r\n      }\r\n\r\n}\n@media only screen and (max-width : 480px) {\r\n    .card{\r\n        height: 1030px;\r\n        margin-top:20px;\r\n        margin-bottom:20px;\r\n\r\n        }\r\n\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0NNTS9hZGQtY21tL2FkZC1jbW0uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLDZEQUE2RDtBQUY3RCxxQ0FBcUM7QUFJckM7O0FBRUEsc0JBQXNCO0FBQ3RCLDRCQUE0QjtBQUM1QixZQUFZO0FBQ1osaUNBQWlDO0FBQ2pDO0FBQ0E7O0lBRUksV0FBVztBQUNmO0FBQ0E7QUFDQSxZQUFZO0FBQ1osc0JBQXNCO0FBQ3RCLFVBQVU7QUFDViw0QkFBNEI7QUFDNUIsMkJBQTJCO0FBQzNCO0FBRUE7QUFDQSxhQUFhO0FBQ2IsZUFBZTtBQUNmLGtCQUFrQjtBQUNsQixZQUFZO0FBQ1osNENBQTRDO0FBQzVDO0FBRUE7QUFDQSxlQUFlO0FBQ2YsaUJBQWlCO0FBQ2pCLGNBQWM7QUFDZDtBQUVBO0FBQ0EsWUFBWTtBQUNaLGVBQWU7QUFDZjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxrQkFBa0I7QUFDbEIsV0FBVztBQUNYLFVBQVU7QUFDVjtBQUVBO0FBQ0EsV0FBVztBQUNYLHlCQUF5QjtBQUN6QixZQUFZO0FBQ1osbUJBQW1CO0FBQ25CO0FBRUE7QUFDQSw0QkFBNEI7QUFDNUIsOEJBQThCOztBQUU5QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7O0FBRUEsV0FBVztBQUNYLFlBQVk7QUFDWixpQkFBaUI7QUFDakIsaUJBQWlCO0FBQ2pCO0FBRUE7QUFDQSxZQUFZO0FBQ1oseUJBQXlCO0FBQ3pCLFlBQVk7QUFDWjtBQUVBO0FBQ0EsWUFBWTtBQUNaLHVCQUF1QjtBQUN2QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQTs7SUFFSSw2QkFBNkI7O0FBRWpDO0FBSUE7d0RBQ3dEO0FBQ3hEO0VBQ0U7O0lBRUUsZUFBZTtJQUNmLGtCQUFrQjtNQUNoQjs7QUFFTjtBQUVBO0lBQ0k7UUFDSSxjQUFjO1FBQ2QsZUFBZTtRQUNmLGtCQUFrQjs7UUFFbEI7O0FBRVIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvQ01NL2FkZC1jbW0vYWRkLWNtbS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogTWFkZSB3aXRoIGxvdmUgYnkgTXV0aXVsbGFoIFNhbWltKi9cclxuXHJcbkBpbXBvcnQgdXJsKCdodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2Nzcz9mYW1pbHk9TnVtYW5zJyk7XHJcblxyXG5odG1sLGJvZHl7XHJcblxyXG5iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG5iYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG5oZWlnaHQ6IDEwMCU7XHJcbmZvbnQtZmFtaWx5OiAnTnVtYW5zJywgc2Fucy1zZXJpZjtcclxufVxyXG5sYWJlbFxyXG57XHJcbiAgICBjb2xvcjogI2ZmZjtcclxufVxyXG4uY29udGFpbmVye1xyXG5oZWlnaHQ6IDEwMCU7XHJcbmJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbndpZHRoOjEwMCU7XHJcbmJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbi8qIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjsgKi9cclxufVxyXG5cclxuLmNhcmR7XHJcbmhlaWdodDogNzAwcHg7XHJcbm1hcmdpbi10b3A6MjBweDtcclxubWFyZ2luLWJvdHRvbToyMHB4O1xyXG53aWR0aDogNDUwcHg7XHJcbmJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uc29jaWFsX2ljb24gc3BhbntcclxuZm9udC1zaXplOiA2MHB4O1xyXG5tYXJnaW4tbGVmdDogMTBweDtcclxuY29sb3I6ICNGRkMzMTI7XHJcbn1cclxuXHJcbi5zb2NpYWxfaWNvbiBzcGFuOmhvdmVye1xyXG5jb2xvcjogd2hpdGU7XHJcbmN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuLmNhcmQtaGVhZGVyIGgze1xyXG5jb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi5zb2NpYWxfaWNvbntcclxucG9zaXRpb246IGFic29sdXRlO1xyXG5yaWdodDogMjBweDtcclxudG9wOiAtNDVweDtcclxufVxyXG5cclxuLmlucHV0LWdyb3VwLXByZXBlbmQgc3Bhbntcclxud2lkdGg6IDUwcHg7XHJcbmJhY2tncm91bmQtY29sb3I6ICNGRkMzMTI7XHJcbmNvbG9yOiBibGFjaztcclxuYm9yZGVyOjAgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW5wdXQ6Zm9jdXN7XHJcbm91dGxpbmU6IDAgMCAwIDAgICFpbXBvcnRhbnQ7XHJcbmJveC1zaGFkb3c6IDAgMCAwIDAgIWltcG9ydGFudDtcclxuXHJcbn1cclxuXHJcbi5yZW1lbWJlcntcclxuY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4ucmVtZW1iZXIgaW5wdXRcclxue1xyXG53aWR0aDogMjBweDtcclxuaGVpZ2h0OiAyMHB4O1xyXG5tYXJnaW4tbGVmdDogMTVweDtcclxubWFyZ2luLXJpZ2h0OiA1cHg7XHJcbn1cclxuXHJcbi5sb2dpbl9idG57XHJcbmNvbG9yOiBibGFjaztcclxuYmFja2dyb3VuZC1jb2xvcjogI0ZGQzMxMjtcclxud2lkdGg6IDEwMHB4O1xyXG59XHJcblxyXG4ubG9naW5fYnRuOmhvdmVye1xyXG5jb2xvcjogYmxhY2s7XHJcbmJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4ubGlua3N7XHJcbmNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLmxpbmtzIGF7XHJcbm1hcmdpbi1sZWZ0OiA0cHg7XHJcbn1cclxub3B0aW9uXHJcbntcclxuICAgIG1hcmdpbi1ib3R0b206IDEwcHghaW1wb3J0YW50O1xyXG5cclxufVxyXG5cclxuXHJcblxyXG4vKiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbioqKioqKioqKioqKioqKioqKioqKiogIE1lZGlhIHF1ZXJ5IGZvciBNb2JpbGUgc2NyZWVuICAqL1xyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGggOiA3NjhweCkge1xyXG4gIC5jYXJke1xyXG4gICAgICBcclxuICAgIG1hcmdpbi10b3A6YXV0bztcclxuICAgIG1hcmdpbi1ib3R0b206YXV0bztcclxuICAgICAgfVxyXG5cclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoIDogNDgwcHgpIHtcclxuICAgIC5jYXJke1xyXG4gICAgICAgIGhlaWdodDogMTAzMHB4O1xyXG4gICAgICAgIG1hcmdpbi10b3A6MjBweDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOjIwcHg7XHJcblxyXG4gICAgICAgIH1cclxuXHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/Main_masters/CMM/add-cmm/add-cmm.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/Main_masters/CMM/add-cmm/add-cmm.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  <div class=\"d-flex justify-content-center h-100\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">\r\n        <h3 style=\"text-align:center\">Add Chemical Manufacturer</h3>\r\n      </div>\r\n      <div class=\"card-body\">\r\n        <form class=\"form-login\" role=\"form\" #formRef='ngForm' (ngSubmit)='onSubmit(formRef.value)'>\r\n          <div class=\"form-group row\">\r\n            <label for=\"subDep\" class=\"col-sm-5 col-form-label\"> Chemical Manufacturer Name</label>\r\n            <div class=\"col-sm-7\">\r\n              <input type=\"text\" class=\"form-control\" id=\"subDep\" name=\"newchemicalman\" #selectRef='ngModel' required\r\n                ngModel>\r\n            </div>\r\n          </div>\r\n          <div class=\"form-group row\">\r\n            <label for=\"subDepcfPassword\" class=\"col-sm-5 col-form-label\">Contact Person</label>\r\n            <div class=\"col-sm-7\">\r\n              <input type=\"text\" class=\"form-control\" id=\"subDepcfPassword\" name=\"Cperson\" #selectRef='ngModel'\r\n                required ngModel>\r\n            </div>\r\n          </div>\r\n          <div class=\"form-group row\">\r\n            <label for=\"subDepPassword\" class=\"col-sm-5 col-form-label\">Address</label>\r\n            <div class=\"col-sm-7\">\r\n              <input type=\"text\" class=\"form-control\" id=\"subDepPassword\" name=\"address\" #selectRef='ngModel' required\r\n                ngModel>\r\n            </div>\r\n          </div>\r\n          <div class=\"form-group row\">\r\n            <label for=\"subDepPassword\" class=\"col-sm-5 col-form-label\">Pin Code</label>\r\n            <div class=\"col-sm-7\">\r\n              <input type=\"text\" class=\"form-control\" id=\"subDepPassword\" name=\"Pincode\" #selectRef='ngModel' required\r\n                ngModel>\r\n            </div>\r\n          </div>\r\n          <div class=\"form-group row\">\r\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Mobile Number</label>\r\n            <div class=\"col-sm-7\">\r\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"mobile\" #selectRef='ngModel' required\r\n                ngModel>\r\n            </div>\r\n          </div>\r\n          <div class=\"form-group row\">\r\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Landline Number</label>\r\n            <div class=\"col-sm-7\">\r\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"landline\" #selectRef='ngModel' required\r\n                ngModel>\r\n            </div>\r\n          </div>\r\n          <div class=\"form-group row\">\r\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">E-Mail ID</label>\r\n            <div class=\"col-sm-7\">\r\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"Email\" #selectRef='ngModel' required\r\n                ngModel>\r\n            </div>\r\n          </div>\r\n          <div class=\"form-group row\">\r\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">GSTIN Number</label>\r\n            <div class=\"col-sm-7\">\r\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"gstin\" #selectRef='ngModel' required\r\n                ngModel>\r\n            </div>\r\n          </div>\r\n          <div class=\"form-group row\">\r\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Remark</label>\r\n            <div class=\"col-sm-7\">\r\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"remarks\" #selectRef='ngModel' required\r\n                ngModel>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"form-group\" style=\"text-align:center\">\r\n            <button style=\"padding: 0;\r\n                  border: none;\r\n                  background: none;\" type=\"submit\">\r\n              <!-- <i class=\"far fa-check-circle\"> -->\r\n              <i class=\"fas fa-check-circle\" (click)=\" onAddBuyer()\" style=\"font-size:60px;color: yellow;\"></i>\r\n\r\n            </button>\r\n          </div>\r\n\r\n        </form>\r\n      </div>\r\n\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/CMM/add-cmm/add-cmm.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/Main_masters/CMM/add-cmm/add-cmm.component.ts ***!
  \***************************************************************/
/*! exports provided: AddCmmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddCmmComponent", function() { return AddCmmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddCmmComponent = /** @class */ (function () {
    function AddCmmComponent(router, _http, headerTitleService) {
        this.router = router;
        this._http = _http;
        this.headerTitleService = headerTitleService;
    }
    AddCmmComponent.prototype.ngOnInit = function () {
        this.headerTitleService.setTitle('Chemical Manufacturer Master');
    };
    AddCmmComponent.prototype.onSubmit = function (a) {
        var _this = this;
        this.newchemicalman = a.newchemicalman;
        this.address = a.address;
        this.Pincode = a.Pincode;
        this.Cperson = a.Cperson;
        this.mobile = a.mobile;
        this.landline = a.altermobile;
        this.Email = a.Email;
        this.gstin = a.gstin;
        this.remarks = a.remarks;
        console.log(a);
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.post('api/add-chemical-master', a, options)
            .subscribe(function (data) {
            alert('Added Successfully');
            _this.router.navigate(['/main-masters']);
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    AddCmmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-cmm',
            template: __webpack_require__(/*! ./add-cmm.component.html */ "./src/app/Main_masters/CMM/add-cmm/add-cmm.component.html"),
            styles: [__webpack_require__(/*! ./add-cmm.component.css */ "./src/app/Main_masters/CMM/add-cmm/add-cmm.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"]])
    ], AddCmmComponent);
    return AddCmmComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/CMM/edit-cmm/edit-cmm.component.css":
/*!******************************************************************!*\
  !*** ./src/app/Main_masters/CMM/edit-cmm/edit-cmm.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table,tr,th,td\n{\n    border:1px solid #fff;\n}\n\n#export-button\n{\n    padding-left:20px;\n    padding-right:20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0NNTS9lZGl0LWNtbS9lZGl0LWNtbS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztJQUVJLHFCQUFxQjtBQUN6Qjs7QUFFQTs7SUFFSSxpQkFBaUI7SUFDakIsa0JBQWtCO0FBQ3RCIiwiZmlsZSI6InNyYy9hcHAvTWFpbl9tYXN0ZXJzL0NNTS9lZGl0LWNtbS9lZGl0LWNtbS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUsdHIsdGgsdGRcbntcbiAgICBib3JkZXI6MXB4IHNvbGlkICNmZmY7XG59XG5cbiNleHBvcnQtYnV0dG9uXG57XG4gICAgcGFkZGluZy1sZWZ0OjIwcHg7XG4gICAgcGFkZGluZy1yaWdodDoyMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/CMM/edit-cmm/edit-cmm.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/Main_masters/CMM/edit-cmm/edit-cmm.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\r\n<div class=\"container table-responsive-sm table-responsive-md table-responsive-lg\" style=\"text-align: -webkit-center;\">\r\n  <div *ngIf=\"update\">\r\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.newchemicalman>\r\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.address>\r\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.Pincode>\r\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.Cperson>\r\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.mobile>\r\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.landline>\r\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.Email>\r\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.gstin>\r\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.remarks>\r\n    <button (click)='saveUpdate(dataedit)' type=\"button\" class=\"btn btn-sm btn-info\" style=\"margin-top: -5px;font-size: 13.9px;\">save</button>\r\n  </div>\r\n  <br>\r\n  <br>\r\n  <table class=\"table\">\r\n    <thead style=\"text-align: -webkit-center;color: cornsilk;\">\r\n      <tr class=\"text-color\">\r\n        <th>Chemical Manufacturer Name</th>\r\n        <th>Contact Person</th>\r\n        <th>Address</th>\r\n        <th>Pincode</th>\r\n        <th>Mobile No</th>\r\n        <th>Landline</th>\r\n        <th>Email Id</th>\r\n        <th>GSTIN</th>\r\n        <th>Remark</th>\r\n        <th>Edit</th>\r\n      </tr>\r\n    </thead>\r\n    <tbody>\r\n      <tr *ngFor='let i of data; let in=index' class=\"success\" style=\"color:white\">\r\n        <td role=\"cell\">{{i.newchemicalman}}</td>\r\n        <td role=\"cell\">{{i.Cperson}}</td>\r\n        <td role=\"cell\">{{i.address}}</td>\r\n        <td role=\"cell\">{{i.Pincode}}</td>\r\n        <td role=\"cell\">{{i.mobile}}</td>\r\n        <td role=\"cell\">{{i.landline}}</td>\r\n        <td role=\"cell\">{{i.Email}}</td>\r\n        <td role=\"cell\">{{i.gstin}}</td>\r\n        <td role=\"cell\">{{i.remarks}}</td>\r\n        <td><button (click)='editData(i,in)' type=\"button\" class=\"btn btn-large btn-info\">Edit</button></td>\r\n      </tr>\r\n    </tbody>\r\n  </table>\r\n  <button id=\"export-button\" type=\"button\" (click)=\"exportExcel()\" class=\"btn btn-info btn-lg \">Export</button>\r\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/CMM/edit-cmm/edit-cmm.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/Main_masters/CMM/edit-cmm/edit-cmm.component.ts ***!
  \*****************************************************************/
/*! exports provided: EditCmmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditCmmComponent", function() { return EditCmmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Services_cmm_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../Services/cmm.service */ "./src/app/Services/cmm.service.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
/* harmony import */ var angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular2-csv/Angular2-csv */ "./node_modules/angular2-csv/Angular2-csv.js");
/* harmony import */ var angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_5__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var EditCmmComponent = /** @class */ (function () {
    function EditCmmComponent(CmmService, _http, router, headerTitleService) {
        this.CmmService = CmmService;
        this._http = _http;
        this.router = router;
        this.headerTitleService = headerTitleService;
        this.update = false;
        this.test = [{}];
    }
    EditCmmComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.CmmService.getData().subscribe(function (CmmData) { return _this.data = CmmData; });
        this.headerTitleService.setTitle('Chemical Manufacturer Master');
    };
    EditCmmComponent.prototype.editData = function (i) {
        this.update = true;
        this.dataedit = i;
        console.log("obj is" + this.dataedit.newchemicalman);
    };
    EditCmmComponent.prototype.saveUpdate = function (datasave) {
        console.log("datasaved" + datasave.newchemicalman);
        console.log("datasaved" + '' + datasave.newchemicalman + '' +
            '' + datasave.address + '' +
            '' + datasave.Pincode + '' +
            '' + datasave.Cperson + '' +
            '' + datasave.mobile);
        var url = "http://localhost:3000/api/add-chemical-master" + "/" + datasave._id;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.put(url, datasave, options)
            .subscribe(function (data) {
            alert('saved Successfully');
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    EditCmmComponent.prototype.exportExcel = function () {
        var head = [
            'newchemicalman',
            'Cperson',
            'address',
            'Pincode',
            'mobile',
            'landline',
            'Email',
            'gstin',
            'remarks'
        ];
        new angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_5__["Angular2Csv"](this.test, 'Chemical Manufacturer Master', { headers: (head) });
        alert("Please Give the Input Details");
        this.router.navigate(['/import-chemical-manufacturer']);
    };
    EditCmmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-cmm',
            template: __webpack_require__(/*! ./edit-cmm.component.html */ "./src/app/Main_masters/CMM/edit-cmm/edit-cmm.component.html"),
            styles: [__webpack_require__(/*! ./edit-cmm.component.css */ "./src/app/Main_masters/CMM/edit-cmm/edit-cmm.component.css")]
        }),
        __metadata("design:paramtypes", [_Services_cmm_service__WEBPACK_IMPORTED_MODULE_1__["CmmService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_4__["HeaderTitleService"]])
    ], EditCmmComponent);
    return EditCmmComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/CMM/import-cmm/import-cmm.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/Main_masters/CMM/import-cmm/import-cmm.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n.center-div\n{\n    position: absolute;\n    left: 50%;\n    top: 50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%);\n    \n    /*\n    This doesn't work\n    margin-left: -25%;\n    margin-top: -25%;\n    */\n    \n    width: 400px;\n    height: 300px;\n  \n    padding: 20px;  \n    background-color: rgba(0,0,0,0.5) !important;\n    color: white;\n    text-align: center;\n    box-shadow: 0 0 30px rgba(0, 0, 0, 0.2);\n}\n/* @media (max-width: 1000px) {\n    \n    .center-inner{left:25%;top:25%;position:absolute;width:50%;height:300px;background-color: rgba(0,0,0,0.5) !important;text-align:center;max-width:500px;max-height:500px;color:#ffffff}\n    } */\n.center{left:50%;top:25%;position:absolute;}\n.center-inner{width:400px;height:100%;margin-left:-250px;height:300px;background-color: rgba(0,0,0,0.5) !important;text-align:center;max-width:500px;max-height:500px;color:#ffffff;padding: 20px;border-radius: 20px;}\n@media (max-width: 428px) {\n    .center-inner{width:300px;height:100%;margin-left:-150px;height:300px;background-color: rgba(0,0,0,0.5) !important;text-align:center;max-width:500px;max-height:500px;color:#ffffff;padding: 20px;border-radius: 20px;}\n    .center{left:50%;top:25%;position:absolute;}\n\n    }\ninput[type=\"file\"] {\n        display: none;\n    }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0NNTS9pbXBvcnQtY21tL2ltcG9ydC1jbW0uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0E7O0lBRUksa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxRQUFRO0lBQ1Isd0NBQWdDO1lBQWhDLGdDQUFnQzs7SUFFaEM7Ozs7S0FJQzs7SUFFRCxZQUFZO0lBQ1osYUFBYTs7SUFFYixhQUFhO0lBQ2IsNENBQTRDO0lBQzVDLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsdUNBQXVDO0FBQzNDO0FBQ0E7OztPQUdPO0FBRUgsUUFBUSxRQUFRLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDO0FBQzNDLGNBQWMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsNENBQTRDLENBQUMsaUJBQWlCLENBQUMsZUFBZSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUM7QUFFdE47SUFDQSxjQUFjLFdBQVcsQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLDRDQUE0QyxDQUFDLGlCQUFpQixDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDO0lBQ3ROLFFBQVEsUUFBUSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQzs7SUFFM0M7QUFDQTtRQUNJLGFBQWE7SUFDakIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvQ01NL2ltcG9ydC1jbW0vaW1wb3J0LWNtbS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG4uY2VudGVyLWRpdlxue1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiA1MCU7XG4gICAgdG9wOiA1MCU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gICAgXG4gICAgLypcbiAgICBUaGlzIGRvZXNuJ3Qgd29ya1xuICAgIG1hcmdpbi1sZWZ0OiAtMjUlO1xuICAgIG1hcmdpbi10b3A6IC0yNSU7XG4gICAgKi9cbiAgICBcbiAgICB3aWR0aDogNDAwcHg7XG4gICAgaGVpZ2h0OiAzMDBweDtcbiAgXG4gICAgcGFkZGluZzogMjBweDsgIFxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgYm94LXNoYWRvdzogMCAwIDMwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xufVxuLyogQG1lZGlhIChtYXgtd2lkdGg6IDEwMDBweCkge1xuICAgIFxuICAgIC5jZW50ZXItaW5uZXJ7bGVmdDoyNSU7dG9wOjI1JTtwb3NpdGlvbjphYnNvbHV0ZTt3aWR0aDo1MCU7aGVpZ2h0OjMwMHB4O2JhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O3RleHQtYWxpZ246Y2VudGVyO21heC13aWR0aDo1MDBweDttYXgtaGVpZ2h0OjUwMHB4O2NvbG9yOiNmZmZmZmZ9XG4gICAgfSAqL1xuICAgIFxuICAgIC5jZW50ZXJ7bGVmdDo1MCU7dG9wOjI1JTtwb3NpdGlvbjphYnNvbHV0ZTt9XG4gICAgLmNlbnRlci1pbm5lcnt3aWR0aDo0MDBweDtoZWlnaHQ6MTAwJTttYXJnaW4tbGVmdDotMjUwcHg7aGVpZ2h0OjMwMHB4O2JhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O3RleHQtYWxpZ246Y2VudGVyO21heC13aWR0aDo1MDBweDttYXgtaGVpZ2h0OjUwMHB4O2NvbG9yOiNmZmZmZmY7cGFkZGluZzogMjBweDtib3JkZXItcmFkaXVzOiAyMHB4O31cbiAgXG4gICAgQG1lZGlhIChtYXgtd2lkdGg6IDQyOHB4KSB7XG4gICAgLmNlbnRlci1pbm5lcnt3aWR0aDozMDBweDtoZWlnaHQ6MTAwJTttYXJnaW4tbGVmdDotMTUwcHg7aGVpZ2h0OjMwMHB4O2JhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O3RleHQtYWxpZ246Y2VudGVyO21heC13aWR0aDo1MDBweDttYXgtaGVpZ2h0OjUwMHB4O2NvbG9yOiNmZmZmZmY7cGFkZGluZzogMjBweDtib3JkZXItcmFkaXVzOiAyMHB4O31cbiAgICAuY2VudGVye2xlZnQ6NTAlO3RvcDoyNSU7cG9zaXRpb246YWJzb2x1dGU7fVxuXG4gICAgfVxuICAgIGlucHV0W3R5cGU9XCJmaWxlXCJdIHtcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICB9Il19 */"

/***/ }),

/***/ "./src/app/Main_masters/CMM/import-cmm/import-cmm.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/Main_masters/CMM/import-cmm/import-cmm.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"center\">\n  <div class=\"center-inner\">\n    <form id=\"login\">\n      <h4>Import File</h4>\n      <hr>\n      <p>* Only CSV File can be import. </p>\n\n      <input type=\"file\" name=\"files\" class=\"form-control\" #uploads (change)=\"onChange(uploads.files)\" multiple value=\"process\"\n        #file />\n      <div class=\"input-group mb-3\">\n        <div class=\"input-group-prepend\">\n          <button class=\"btn btn-outline-primary\" (click)=\"file.click()\" type=\"button\">Browser</button>\n        </div>\n        <input type=\"text\" value=\"{{fileName}}\" class=\"form-control form-control-lg\" aria-label=\"Default\"\n          aria-describedby=\"inputGroup-sizing-default\">\n      </div>\n      <br>\n      <div class=\"form-group\" style=\"text-align:center\">\n        <button style=\"padding: 0;\n                          border: none;\n                          background: none;\"\n          type=\"button\" (click)=\"onUpload()\">\n\n          <i class=\"fas fa-check-circle\" style=\"font-size:70px;color: yellow;\"></i>\n\n        </button>\n      </div>\n\n    </form>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/CMM/import-cmm/import-cmm.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/Main_masters/CMM/import-cmm/import-cmm.component.ts ***!
  \*********************************************************************/
/*! exports provided: ImportCmmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImportCmmComponent", function() { return ImportCmmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
/* harmony import */ var papaparse__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! papaparse */ "./node_modules/papaparse/papaparse.min.js");
/* harmony import */ var papaparse__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(papaparse__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ImportCmmComponent = /** @class */ (function () {
    function ImportCmmComponent(headerTitleService, http, router) {
        this.headerTitleService = headerTitleService;
        this.http = http;
        this.router = router;
    }
    ImportCmmComponent.prototype.ngOnInit = function () {
        this.headerTitleService.setTitle('Chemical Manufacturer Master');
    };
    // CMM post api
    ImportCmmComponent.prototype.onChange = function (files) {
        var _this = this;
        if (files[0]) {
            console.log(files[0]);
            papaparse__WEBPACK_IMPORTED_MODULE_2__["parse"](files[0], {
                header: true,
                skipEmptyLines: true,
                complete: function (result, file) {
                    console.log(result);
                    _this.fileName = file.name;
                    _this.dataList = result.data;
                    console.log(_this.dataList);
                    var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["Headers"]();
                    headers.append('Content-Type', 'application/json');
                    var options = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["RequestOptions"]({ headers: headers });
                    _this.http.post('api/cmmFileUpload', _this.dataList, options)
                        .subscribe(function (data) {
                        console.log("added successfully");
                    }, function (error) {
                        console.log(JSON.stringify(error.json()));
                    });
                }
            });
        }
    };
    ImportCmmComponent.prototype.onUpload = function () {
        this.router.navigate(['/edit-chemical-master']);
    };
    ImportCmmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-import-cmm',
            template: __webpack_require__(/*! ./import-cmm.component.html */ "./src/app/Main_masters/CMM/import-cmm/import-cmm.component.html"),
            styles: [__webpack_require__(/*! ./import-cmm.component.css */ "./src/app/Main_masters/CMM/import-cmm/import-cmm.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_1__["HeaderTitleService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_4__["Http"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ImportCmmComponent);
    return ImportCmmComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/CPM/cpm-add/cpm-add.component.css":
/*!****************************************************************!*\
  !*** ./src/app/Main_masters/CPM/cpm-add/cpm-add.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n@import url('https://fonts.googleapis.com/css?family=Numans');\n/* Made with love by Mutiullah Samim*/\nhtml,body{\n\nbackground-size: cover;\nbackground-repeat: no-repeat;\nheight: 100%;\nfont-family: 'Numans', sans-serif;\n}\nlabel\n{\n    color: #fff;\n}\n.container{\nheight: 100%;\nbackground-size: cover;\nwidth:100%;\nbackground-repeat: no-repeat;\n/* align-content: center; */\n}\n.card{\nheight: 850px;\nmargin-top:20px;\nmargin-bottom:20px;\nwidth: 450px;\nbackground-color: rgba(0,0,0,0.5) !important;\n}\n.social_icon span{\nfont-size: 60px;\nmargin-left: 10px;\ncolor: #FFC312;\n}\n.social_icon span:hover{\ncolor: white;\ncursor: pointer;\n}\n.card-header h3{\ncolor: white;\n}\n.social_icon{\nposition: absolute;\nright: 20px;\ntop: -45px;\n}\n.input-group-prepend span{\nwidth: 50px;\nbackground-color: #FFC312;\ncolor: black;\nborder:0 !important;\n}\ninput:focus{\noutline: 0 0 0 0  !important;\nbox-shadow: 0 0 0 0 !important;\n\n}\n.remember{\ncolor: white;\n}\n.remember input\n{\nwidth: 20px;\nheight: 20px;\nmargin-left: 15px;\nmargin-right: 5px;\n}\n.login_btn{\ncolor: black;\nbackground-color: #FFC312;\nwidth: 100px;\n}\n.login_btn:hover{\ncolor: black;\nbackground-color: white;\n}\n.links{\ncolor: white;\n}\n.links a{\nmargin-left: 4px;\n}\noption\n{\n    margin-bottom: 10px!important;\n\n}\n/* *************************************************************************\n**********************  Media query for Mobile screen  */\n@media only screen and (max-width : 768px) {\n  .card{\n      \n    margin-top:auto;\n    margin-bottom:auto;\n      }\n\n}\n@media only screen and (max-width : 480px) {\n    .card{\n        height: 1130px;\n        margin-top:20px;\n        margin-bottom:20px;\n\n        }\n\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0NQTS9jcG0tYWRkL2NwbS1hZGQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLDZEQUE2RDtBQUY3RCxxQ0FBcUM7QUFJckM7O0FBRUEsc0JBQXNCO0FBQ3RCLDRCQUE0QjtBQUM1QixZQUFZO0FBQ1osaUNBQWlDO0FBQ2pDO0FBQ0E7O0lBRUksV0FBVztBQUNmO0FBQ0E7QUFDQSxZQUFZO0FBQ1osc0JBQXNCO0FBQ3RCLFVBQVU7QUFDViw0QkFBNEI7QUFDNUIsMkJBQTJCO0FBQzNCO0FBRUE7QUFDQSxhQUFhO0FBQ2IsZUFBZTtBQUNmLGtCQUFrQjtBQUNsQixZQUFZO0FBQ1osNENBQTRDO0FBQzVDO0FBRUE7QUFDQSxlQUFlO0FBQ2YsaUJBQWlCO0FBQ2pCLGNBQWM7QUFDZDtBQUVBO0FBQ0EsWUFBWTtBQUNaLGVBQWU7QUFDZjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxrQkFBa0I7QUFDbEIsV0FBVztBQUNYLFVBQVU7QUFDVjtBQUVBO0FBQ0EsV0FBVztBQUNYLHlCQUF5QjtBQUN6QixZQUFZO0FBQ1osbUJBQW1CO0FBQ25CO0FBRUE7QUFDQSw0QkFBNEI7QUFDNUIsOEJBQThCOztBQUU5QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7O0FBRUEsV0FBVztBQUNYLFlBQVk7QUFDWixpQkFBaUI7QUFDakIsaUJBQWlCO0FBQ2pCO0FBRUE7QUFDQSxZQUFZO0FBQ1oseUJBQXlCO0FBQ3pCLFlBQVk7QUFDWjtBQUVBO0FBQ0EsWUFBWTtBQUNaLHVCQUF1QjtBQUN2QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQTs7SUFFSSw2QkFBNkI7O0FBRWpDO0FBSUE7d0RBQ3dEO0FBQ3hEO0VBQ0U7O0lBRUUsZUFBZTtJQUNmLGtCQUFrQjtNQUNoQjs7QUFFTjtBQUVBO0lBQ0k7UUFDSSxjQUFjO1FBQ2QsZUFBZTtRQUNmLGtCQUFrQjs7UUFFbEI7O0FBRVIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvQ1BNL2NwbS1hZGQvY3BtLWFkZC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogTWFkZSB3aXRoIGxvdmUgYnkgTXV0aXVsbGFoIFNhbWltKi9cblxuQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1OdW1hbnMnKTtcblxuaHRtbCxib2R5e1xuXG5iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbmhlaWdodDogMTAwJTtcbmZvbnQtZmFtaWx5OiAnTnVtYW5zJywgc2Fucy1zZXJpZjtcbn1cbmxhYmVsXG57XG4gICAgY29sb3I6ICNmZmY7XG59XG4uY29udGFpbmVye1xuaGVpZ2h0OiAxMDAlO1xuYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbndpZHRoOjEwMCU7XG5iYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuLyogYWxpZ24tY29udGVudDogY2VudGVyOyAqL1xufVxuXG4uY2FyZHtcbmhlaWdodDogODUwcHg7XG5tYXJnaW4tdG9wOjIwcHg7XG5tYXJnaW4tYm90dG9tOjIwcHg7XG53aWR0aDogNDUwcHg7XG5iYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuNSkgIWltcG9ydGFudDtcbn1cblxuLnNvY2lhbF9pY29uIHNwYW57XG5mb250LXNpemU6IDYwcHg7XG5tYXJnaW4tbGVmdDogMTBweDtcbmNvbG9yOiAjRkZDMzEyO1xufVxuXG4uc29jaWFsX2ljb24gc3Bhbjpob3ZlcntcbmNvbG9yOiB3aGl0ZTtcbmN1cnNvcjogcG9pbnRlcjtcbn1cblxuLmNhcmQtaGVhZGVyIGgze1xuY29sb3I6IHdoaXRlO1xufVxuXG4uc29jaWFsX2ljb257XG5wb3NpdGlvbjogYWJzb2x1dGU7XG5yaWdodDogMjBweDtcbnRvcDogLTQ1cHg7XG59XG5cbi5pbnB1dC1ncm91cC1wcmVwZW5kIHNwYW57XG53aWR0aDogNTBweDtcbmJhY2tncm91bmQtY29sb3I6ICNGRkMzMTI7XG5jb2xvcjogYmxhY2s7XG5ib3JkZXI6MCAhaW1wb3J0YW50O1xufVxuXG5pbnB1dDpmb2N1c3tcbm91dGxpbmU6IDAgMCAwIDAgICFpbXBvcnRhbnQ7XG5ib3gtc2hhZG93OiAwIDAgMCAwICFpbXBvcnRhbnQ7XG5cbn1cblxuLnJlbWVtYmVye1xuY29sb3I6IHdoaXRlO1xufVxuXG4ucmVtZW1iZXIgaW5wdXRcbntcbndpZHRoOiAyMHB4O1xuaGVpZ2h0OiAyMHB4O1xubWFyZ2luLWxlZnQ6IDE1cHg7XG5tYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLmxvZ2luX2J0bntcbmNvbG9yOiBibGFjaztcbmJhY2tncm91bmQtY29sb3I6ICNGRkMzMTI7XG53aWR0aDogMTAwcHg7XG59XG5cbi5sb2dpbl9idG46aG92ZXJ7XG5jb2xvcjogYmxhY2s7XG5iYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cblxuLmxpbmtze1xuY29sb3I6IHdoaXRlO1xufVxuXG4ubGlua3MgYXtcbm1hcmdpbi1sZWZ0OiA0cHg7XG59XG5vcHRpb25cbntcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4IWltcG9ydGFudDtcblxufVxuXG5cblxuLyogKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuKioqKioqKioqKioqKioqKioqKioqKiAgTWVkaWEgcXVlcnkgZm9yIE1vYmlsZSBzY3JlZW4gICovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGggOiA3NjhweCkge1xuICAuY2FyZHtcbiAgICAgIFxuICAgIG1hcmdpbi10b3A6YXV0bztcbiAgICBtYXJnaW4tYm90dG9tOmF1dG87XG4gICAgICB9XG5cbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoIDogNDgwcHgpIHtcbiAgICAuY2FyZHtcbiAgICAgICAgaGVpZ2h0OiAxMTMwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6MjBweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbToyMHB4O1xuXG4gICAgICAgIH1cblxufSJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/CPM/cpm-add/cpm-add.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/Main_masters/CPM/cpm-add/cpm-add.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"d-flex justify-content-center h-100\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        <h3 style=\"text-align:center\">Add New Chemical Product</h3>\n      </div>\n      <div class=\"card-body\">\n        <form class=\"form-login\" role=\"form\" #formRef='ngForm' (ngSubmit)='onSubmit(formRef.value)'>\n          <div class=\"form-group row\">\n            <label for=\"subDep\" class=\"col-sm-5 col-form-label\">Chemical Product Name</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDep\" name=\"newproductname\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"seldep\" class=\"col-sm-5 col-form-label\"> Agent Or Distributor Name</label>\n            <div class=\"col-sm-7\">\n              <select class=\"form-control\" id=\"seldep\" name=\"DistributorName\" #selectRef='ngModel' required ngModel>\n                <option selected>Agent Name</option>\n                <option *ngFor=\"let i of data\">{{i.newChemicAgent}}</option>\n\n              </select>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"seldep\" class=\"col-sm-5 col-form-label\"> Chemical Manufacturer Name</label>\n            <div class=\"col-sm-7\">\n              <select class=\"form-control\" id=\"seldep\" name=\"ManufacturerName\" #selectRef='ngModel' required ngModel>\n                <option selected> Chemical Manufacturer Name</option>\n                <option *ngFor=\"let j of data1\">{{j.newchemicalman}}</option>\n              </select>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepcfPassword\" class=\"col-sm-5 col-form-label\">Category</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepcfPassword\" name=\"Category\" #selectRef='ngModel'\n                required ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Subcategory</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"Subcategory\" #selectRef='ngModel'\n                required ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Chemical Form</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"ChemicalForm\" #selectRef='ngModel'\n                required ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Average Per Day Consumption</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"Consumption\" #selectRef='ngModel'\n                required ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Minimum Order Quantity</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"Quantity\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Reorder Level</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"ReorderLevel\" #selectRef='ngModel'\n                required ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Remark</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"Remark\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n\n          <div class=\"form-group\" style=\"text-align:center\">\n            <button style=\"padding: 0;\n                border: none;\n                background: none;\" type=\"submit\">\n              <!-- <i class=\"far fa-check-circle\"> -->\n              <i class=\"fas fa-check-circle\" style=\"font-size:60px;color: yellow;\"></i>\n            </button>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/CPM/cpm-add/cpm-add.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/Main_masters/CPM/cpm-add/cpm-add.component.ts ***!
  \***************************************************************/
/*! exports provided: CpmAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CpmAddComponent", function() { return CpmAddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_cadm_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/cadm.service */ "./src/app/Services/cadm.service.ts");
/* harmony import */ var src_app_Services_cmm_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/Services/cmm.service */ "./src/app/Services/cmm.service.ts");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CpmAddComponent = /** @class */ (function () {
    function CpmAddComponent(router, _http, Cadmservice, CmmService, headerTitleService) {
        this.router = router;
        this._http = _http;
        this.Cadmservice = Cadmservice;
        this.CmmService = CmmService;
        this.headerTitleService = headerTitleService;
    }
    CpmAddComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.Cadmservice.getData().subscribe(function (CadmData) { return _this.data = CadmData; });
        this.CmmService.getData().subscribe(function (CmmData) { return _this.data1 = CmmData; });
        this.headerTitleService.setTitle('Chemical Product Master');
    };
    CpmAddComponent.prototype.onSubmit = function (a) {
        var _this = this;
        this.newproductname = a.newproductname;
        this.DistributorName = a.DistributorName;
        this.ManufacturerName = a.ManufacturerName;
        this.Category = a.Category;
        this.Subcategory = a.Subcategory;
        this.ChemicalForm = a.ChemicalForm;
        this.Consumption = a.Consumption;
        this.Quantity = a.Quantity;
        this.ReorderLevel = a.ReorderLevel;
        this.Remark = a.Remark;
        console.log(a);
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.post('/api/add-chemical-product', a, options)
            .subscribe(function (data) {
            alert('Registered Successfully');
            _this.router.navigate(['/main-masters']);
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    CpmAddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-cpm-add',
            template: __webpack_require__(/*! ./cpm-add.component.html */ "./src/app/Main_masters/CPM/cpm-add/cpm-add.component.html"),
            styles: [__webpack_require__(/*! ./cpm-add.component.css */ "./src/app/Main_masters/CPM/cpm-add/cpm-add.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            src_app_Services_cadm_service__WEBPACK_IMPORTED_MODULE_3__["CadmService"],
            src_app_Services_cmm_service__WEBPACK_IMPORTED_MODULE_4__["CmmService"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_5__["HeaderTitleService"]])
    ], CpmAddComponent);
    return CpmAddComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/CPM/cpm-edit/cpm-edit.component.css":
/*!******************************************************************!*\
  !*** ./src/app/Main_masters/CPM/cpm-edit/cpm-edit.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table,tr,th,td\n{\n    border:1px solid #fff;\n}\n#export-button\n{\n    padding-left:20px;\n    padding-right:20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0NQTS9jcG0tZWRpdC9jcG0tZWRpdC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztJQUVJLHFCQUFxQjtBQUN6QjtBQUNBOztJQUVJLGlCQUFpQjtJQUNqQixrQkFBa0I7QUFDdEIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvQ1BNL2NwbS1lZGl0L2NwbS1lZGl0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZSx0cix0aCx0ZFxue1xuICAgIGJvcmRlcjoxcHggc29saWQgI2ZmZjtcbn1cbiNleHBvcnQtYnV0dG9uXG57XG4gICAgcGFkZGluZy1sZWZ0OjIwcHg7XG4gICAgcGFkZGluZy1yaWdodDoyMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/CPM/cpm-edit/cpm-edit.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/Main_masters/CPM/cpm-edit/cpm-edit.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <br>\n<div class=\"container\" style=\"text-align: -webkit-center;\">\n    <div *ngIf=\"update\" >\n      <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.ChemicalForm>      \n      <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.Consumption>\n      <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.Quantity>\n      <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.ReorderLevel>\n      <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.Remark>\n      <button (click)='saveUpdate(datatobeedited)' type=\"button\" class=\"btn btn-sm btn-info\" style=\"margin-top: -5px;font-size: 13.9px;\">save</button>\n    </div>\n<div class=\"table-container\">\n  <div>\n    <div class=\"card\">\n      <div class=\"card-body\">\n        <table role=\"table\" style=\"width:100%;margin: 0;padding: 0\">\n          <thead role=\"rowgroup\">\n            <tr>\n              <th colspan=\"12\" style=\"background-color:#5E1D55;color:white;\">\n                <h3>Edit Chemical</h3>\n              </th>\n            </tr>\n            <tr class=\"text-color\"><th role=\"columnheader\">New Product Name</th>\n              <th role=\"columnheader\">Agent/Distributor Name</th>\n              <th role=\"columnheader\">Manufacturer Name</th>\n              <th role=\"columnheader\">Category</th>\n              <th role=\"columnheader\">Subcategory</th>\n              <th role=\"columnheader\">Chemical Form</th>\n              <th role=\"columnheader\">Average Per Day Consumption</th>\n              <th role=\"columnheader\">Minimum Order Quantity</th>\n              <th role=\"columnheader\">Reorder Level</th>\n              <th role=\"columnheader\">Remark</th>\n              <td role=\"columnheader\">Edit</td>\n            </tr>\n          </thead>\n          <tbody>\n              <tr *ngFor='let i of test; let in=index'  class=\"success\" style=\"text-align:-webkit-center;color:white\">\n                <td>{{i.newproductname}}</td>\n                 <td>{{i.DistributorName}}</td>\n                 <td>{{i.ManufacturerName}}</td>           \n                 <td>{{i.Subcategory}}</td>\n                 <td>{{i.ChemicalForm}}</td>\n                 <td>{{i.Consumption}}</td>\n                 <td>{{i.Quantity}}</td>\n                 <td>{{i.ReorderLevel}}</td>\n                 <td>{{i.Remark}}</td>\n                 <td><button (click)='editData(i,in)' type=\"button\" class=\"btn btn-large btn-info\">Edit</button></td>  \n              </tr>\n            </tbody>\n        </table>\n      </div>\n    </div>\n  </div>\n</div>  -->\n<br>\n<div class=\"container table-responsive-lg table-responsive-sm table-responsive-md\" style=\"text-align: -webkit-center;\">\n  <div *ngIf=\"update\">\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.newproductname>\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.DistributorName>\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.ManufacturerName>\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.Category>\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.Subcategory>\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.ChemicalForm>\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.Consumption>\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.Quantity>\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.ReorderLevel>\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.Remark>\n    <button (click)='saveUpdate(datatobeedited)' type=\"button\" class=\"btn btn-sm btn-info\" style=\"margin-top: -5px;font-size: 13.9px;\">save</button>\n  </div>\n\n  <br>\n  <br>\n  <table class=\"table\">\n    <thead style=\"text-align:-webkit-center;color:cornsilk\">\n      <tr class=\"text-color\">\n        <th>Chemical Product Name</th>\n        <th>Agent/Distributor Name</th>\n        <th>Chemical Manufacturer Name</th>\n        <th>Category</th>\n        <th>Subcategory</th>\n        <th>Chemical Form</th>\n        <th>Average Per Day Consumption</th>\n        <th>Minimum Order Quantity</th>\n        <th>Reorder Level</th>\n        <th>Remark</th>\n        <th>Edit</th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor='let i of test; let in=index' class=\"success\" style=\"text-align:-webkit-center;color:white\">\n        <td>{{i.newproductname}}</td>\n        <td>{{i.DistributorName}}</td>\n        <td>{{i.ManufacturerName}}</td>\n        <td>{{i.Category}}</td>\n        <td>{{i.Subcategory}}</td>\n        <td>{{i.ChemicalForm}}</td>\n        <td>{{i.Consumption}}</td>\n        <td>{{i.Quantity}}</td>\n        <td>{{i.ReorderLevel}}</td>\n        <td>{{i.Remark}}</td>\n        <td><button (click)='editData(i,in)' type=\"button\" class=\"btn btn-large btn-info\">Edit</button></td>\n      </tr>\n    </tbody>\n  </table>\n\n  <button id=\"export-button\" type=\"button\" (click)=\"exportExcel()\" class=\"btn btn-info btn-lg \">Export</button>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/CPM/cpm-edit/cpm-edit.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/Main_masters/CPM/cpm-edit/cpm-edit.component.ts ***!
  \*****************************************************************/
/*! exports provided: CpmEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CpmEditComponent", function() { return CpmEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Services_cpm_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../Services/cpm.service */ "./src/app/Services/cpm.service.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
/* harmony import */ var angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular2-csv/Angular2-csv */ "./node_modules/angular2-csv/Angular2-csv.js");
/* harmony import */ var angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CpmEditComponent = /** @class */ (function () {
    function CpmEditComponent(CpmService, _http, router, headerTitleService) {
        this.CpmService = CpmService;
        this._http = _http;
        this.router = router;
        this.headerTitleService = headerTitleService;
        this.test1 = [{}];
        this.update = false;
    }
    CpmEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.CpmService.getData().subscribe(function (testData) { return _this.test = testData; });
        this.headerTitleService.setTitle('Chemical Product Master');
    };
    CpmEditComponent.prototype.editData = function (i) {
        this.update = true;
        this.datatobeedited = i;
        console.log("obj is" + this.datatobeedited.newproductname);
    };
    CpmEditComponent.prototype.saveUpdate = function (datasaved) {
        console.log("datatobesaved" + datasaved.newproductname);
        console.log("datatobesaved" + '' + datasaved.newproductname +
            '' + '' + datasaved.ChemicalForm +
            '' + '' + datasaved.Consumption +
            '' + '' + datasaved.Quantity +
            '' + '' + datasaved.ReorderLevel +
            '' + '' + datasaved.Remark);
        var _url = "http://localhost:3000/api/add-chemical-product" + "/" + datasaved._id;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.put(_url, datasaved, options)
            .subscribe(function (data) {
            alert('saved Successfully');
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    CpmEditComponent.prototype.exportExcel = function () {
        var head = [
            'newproductname',
            'DistributorName',
            'ManufacturerName',
            'Category',
            'Subcategory',
            'ChemicalForm',
            'Consumption',
            'Quantity',
            'ReorderLevel',
            'Remark'
        ];
        new angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_4__["Angular2Csv"](this.test1, 'Chemical Product Master', { headers: (head) });
        alert("Please Give the Input Details");
        this.router.navigate(['/import-chemical-product']);
    };
    CpmEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-cpm-edit',
            template: __webpack_require__(/*! ./cpm-edit.component.html */ "./src/app/Main_masters/CPM/cpm-edit/cpm-edit.component.html"),
            styles: [__webpack_require__(/*! ./cpm-edit.component.css */ "./src/app/Main_masters/CPM/cpm-edit/cpm-edit.component.css")]
        }),
        __metadata("design:paramtypes", [_Services_cpm_service__WEBPACK_IMPORTED_MODULE_1__["CpmService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"]])
    ], CpmEditComponent);
    return CpmEditComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/CPM/cpm-import/cpm-import.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/Main_masters/CPM/cpm-import/cpm-import.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n.center-div\n{\n    position: absolute;\n    left: 50%;\n    top: 50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%);\n    \n    /*\n    This doesn't work\n    margin-left: -25%;\n    margin-top: -25%;\n    */\n    \n    width: 400px;\n    height: 300px;\n  \n    padding: 20px;  \n    background-color: rgba(0,0,0,0.5) !important;\n    color: white;\n    text-align: center;\n    box-shadow: 0 0 30px rgba(0, 0, 0, 0.2);\n}\n/* @media (max-width: 1000px) {\n    \n    .center-inner{left:25%;top:25%;position:absolute;width:50%;height:300px;background-color: rgba(0,0,0,0.5) !important;text-align:center;max-width:500px;max-height:500px;color:#ffffff}\n    } */\n.center{left:50%;top:25%;position:absolute;}\n.center-inner{width:400px;height:100%;margin-left:-250px;height:300px;background-color: rgba(0,0,0,0.5) !important;text-align:center;max-width:500px;max-height:500px;color:#ffffff;padding: 20px;border-radius: 20px;}\n@media (max-width: 428px) {\n    .center-inner{width:300px;height:100%;margin-left:-150px;height:300px;background-color: rgba(0,0,0,0.5) !important;text-align:center;max-width:500px;max-height:500px;color:#ffffff;padding: 20px;border-radius: 20px;}\n    .center{left:50%;top:25%;position:absolute;}\n\n    }\ninput[type=\"file\"] {\n        display: none;\n    }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0NQTS9jcG0taW1wb3J0L2NwbS1pbXBvcnQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0E7O0lBRUksa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxRQUFRO0lBQ1Isd0NBQWdDO1lBQWhDLGdDQUFnQzs7SUFFaEM7Ozs7S0FJQzs7SUFFRCxZQUFZO0lBQ1osYUFBYTs7SUFFYixhQUFhO0lBQ2IsNENBQTRDO0lBQzVDLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsdUNBQXVDO0FBQzNDO0FBQ0E7OztPQUdPO0FBRUgsUUFBUSxRQUFRLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDO0FBQzNDLGNBQWMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsNENBQTRDLENBQUMsaUJBQWlCLENBQUMsZUFBZSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUM7QUFFdE47SUFDQSxjQUFjLFdBQVcsQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLDRDQUE0QyxDQUFDLGlCQUFpQixDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDO0lBQ3ROLFFBQVEsUUFBUSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQzs7SUFFM0M7QUFDQTtRQUNJLGFBQWE7SUFDakIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvQ1BNL2NwbS1pbXBvcnQvY3BtLWltcG9ydC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG4uY2VudGVyLWRpdlxue1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiA1MCU7XG4gICAgdG9wOiA1MCU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gICAgXG4gICAgLypcbiAgICBUaGlzIGRvZXNuJ3Qgd29ya1xuICAgIG1hcmdpbi1sZWZ0OiAtMjUlO1xuICAgIG1hcmdpbi10b3A6IC0yNSU7XG4gICAgKi9cbiAgICBcbiAgICB3aWR0aDogNDAwcHg7XG4gICAgaGVpZ2h0OiAzMDBweDtcbiAgXG4gICAgcGFkZGluZzogMjBweDsgIFxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgYm94LXNoYWRvdzogMCAwIDMwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xufVxuLyogQG1lZGlhIChtYXgtd2lkdGg6IDEwMDBweCkge1xuICAgIFxuICAgIC5jZW50ZXItaW5uZXJ7bGVmdDoyNSU7dG9wOjI1JTtwb3NpdGlvbjphYnNvbHV0ZTt3aWR0aDo1MCU7aGVpZ2h0OjMwMHB4O2JhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O3RleHQtYWxpZ246Y2VudGVyO21heC13aWR0aDo1MDBweDttYXgtaGVpZ2h0OjUwMHB4O2NvbG9yOiNmZmZmZmZ9XG4gICAgfSAqL1xuICAgIFxuICAgIC5jZW50ZXJ7bGVmdDo1MCU7dG9wOjI1JTtwb3NpdGlvbjphYnNvbHV0ZTt9XG4gICAgLmNlbnRlci1pbm5lcnt3aWR0aDo0MDBweDtoZWlnaHQ6MTAwJTttYXJnaW4tbGVmdDotMjUwcHg7aGVpZ2h0OjMwMHB4O2JhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O3RleHQtYWxpZ246Y2VudGVyO21heC13aWR0aDo1MDBweDttYXgtaGVpZ2h0OjUwMHB4O2NvbG9yOiNmZmZmZmY7cGFkZGluZzogMjBweDtib3JkZXItcmFkaXVzOiAyMHB4O31cbiAgXG4gICAgQG1lZGlhIChtYXgtd2lkdGg6IDQyOHB4KSB7XG4gICAgLmNlbnRlci1pbm5lcnt3aWR0aDozMDBweDtoZWlnaHQ6MTAwJTttYXJnaW4tbGVmdDotMTUwcHg7aGVpZ2h0OjMwMHB4O2JhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O3RleHQtYWxpZ246Y2VudGVyO21heC13aWR0aDo1MDBweDttYXgtaGVpZ2h0OjUwMHB4O2NvbG9yOiNmZmZmZmY7cGFkZGluZzogMjBweDtib3JkZXItcmFkaXVzOiAyMHB4O31cbiAgICAuY2VudGVye2xlZnQ6NTAlO3RvcDoyNSU7cG9zaXRpb246YWJzb2x1dGU7fVxuXG4gICAgfVxuICAgIGlucHV0W3R5cGU9XCJmaWxlXCJdIHtcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICB9Il19 */"

/***/ }),

/***/ "./src/app/Main_masters/CPM/cpm-import/cpm-import.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/Main_masters/CPM/cpm-import/cpm-import.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"center\">\n  <div class=\"center-inner\">\n    <form id=\"login\">\n      <h4>Import File</h4>\n      <hr>\n      <p>* Only CSV File can be import. </p>\n\n      <input type=\"file\" name=\"files\" class=\"form-control\" #uploads (change)=\"onChange(uploads.files)\" multiple value=\"process\"\n        #file />\n      <div class=\"input-group mb-3\">\n        <div class=\"input-group-prepend\">\n          <button class=\"btn btn-outline-primary\" (click)=\"file.click()\" type=\"button\">Browser</button>\n        </div>\n        <input type=\"text\" value=\"{{fileName}}\" class=\"form-control form-control-lg\" aria-label=\"Default\"\n          aria-describedby=\"inputGroup-sizing-default\">\n      </div>\n      <br>\n      <div class=\"form-group\" style=\"text-align:center\">\n        <button style=\"padding: 0;\n                          border: none;\n                          background: none;\"\n          type=\"button\" (click)=\"onUpload()\">\n\n          <i class=\"fas fa-check-circle\" style=\"font-size:70px;color: yellow;\"></i>\n\n        </button>\n      </div>\n\n    </form>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/CPM/cpm-import/cpm-import.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/Main_masters/CPM/cpm-import/cpm-import.component.ts ***!
  \*********************************************************************/
/*! exports provided: CpmImportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CpmImportComponent", function() { return CpmImportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
/* harmony import */ var papaparse__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! papaparse */ "./node_modules/papaparse/papaparse.min.js");
/* harmony import */ var papaparse__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(papaparse__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CpmImportComponent = /** @class */ (function () {
    function CpmImportComponent(headerTitleService, http, router) {
        this.headerTitleService = headerTitleService;
        this.http = http;
        this.router = router;
    }
    CpmImportComponent.prototype.ngOnInit = function () {
        this.headerTitleService.setTitle('Chemical Product Master');
    };
    // CPM post api
    CpmImportComponent.prototype.onChange = function (files) {
        var _this = this;
        if (files[0]) {
            console.log(files[0]);
            papaparse__WEBPACK_IMPORTED_MODULE_2__["parse"](files[0], {
                header: true,
                skipEmptyLines: true,
                complete: function (result, file) {
                    console.log(result);
                    _this.fileName = file.name;
                    _this.dataList = result.data;
                    console.log(_this.dataList);
                    var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["Headers"]();
                    headers.append('Content-Type', 'application/json');
                    var options = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["RequestOptions"]({ headers: headers });
                    _this.http.post('api/cpmFileUpload', _this.dataList, options)
                        .subscribe(function (data) {
                        console.log("added successfully");
                    }, function (error) {
                        console.log(JSON.stringify(error.json()));
                    });
                }
            });
        }
    };
    CpmImportComponent.prototype.onUpload = function () {
        this.router.navigate(['/edit-chemical-product']);
    };
    CpmImportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-cpm-import',
            template: __webpack_require__(/*! ./cpm-import.component.html */ "./src/app/Main_masters/CPM/cpm-import/cpm-import.component.html"),
            styles: [__webpack_require__(/*! ./cpm-import.component.css */ "./src/app/Main_masters/CPM/cpm-import/cpm-import.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_1__["HeaderTitleService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_4__["Http"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], CpmImportComponent);
    return CpmImportComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/DMSM/add-dmsm/add-dmsm.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/Main_masters/DMSM/add-dmsm/add-dmsm.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n@import url('https://fonts.googleapis.com/css?family=Numans');\n/* Made with love by Mutiullah Samim*/\nhtml,body{\n\nbackground-size: cover;\nbackground-repeat: no-repeat;\nheight: 100%;\nfont-family: 'Numans', sans-serif;\n}\nlabel\n{\n    color: #fff;\n}\n.container{\nheight: 100%;\nbackground-size: cover;\nwidth:100%;\nbackground-repeat: no-repeat;\n/* align-content: center; */\n}\n.card{\nheight: 700px;\nmargin-top:20px;\nmargin-bottom:20px;\nwidth: 450px;\nbackground-color: rgba(0,0,0,0.5) !important;\n}\n.social_icon span{\nfont-size: 60px;\nmargin-left: 10px;\ncolor: #FFC312;\n}\n.social_icon span:hover{\ncolor: white;\ncursor: pointer;\n}\n.card-header h3{\ncolor: white;\n}\n.social_icon{\nposition: absolute;\nright: 20px;\ntop: -45px;\n}\n.input-group-prepend span{\nwidth: 50px;\nbackground-color: #FFC312;\ncolor: black;\nborder:0 !important;\n}\ninput:focus{\noutline: 0 0 0 0  !important;\nbox-shadow: 0 0 0 0 !important;\n\n}\n.remember{\ncolor: white;\n}\n.remember input\n{\nwidth: 20px;\nheight: 20px;\nmargin-left: 15px;\nmargin-right: 5px;\n}\n.login_btn{\ncolor: black;\nbackground-color: #FFC312;\nwidth: 100px;\n}\n.login_btn:hover{\ncolor: black;\nbackground-color: white;\n}\n.links{\ncolor: white;\n}\n.links a{\nmargin-left: 4px;\n}\noption\n{\n    margin-bottom: 10px!important;\n\n}\n/* *************************************************************************\n**********************  Media query for Mobile screen  */\n@media only screen and (max-width : 768px) {\n  .card{\n      \n    margin-top:auto;\n    margin-bottom:auto;\n      }\n\n}\n@media only screen and (max-width : 480px) {\n    .card{\n        height: 1030px;\n        margin-top:20px;\n        margin-bottom:20px;\n\n        }\n\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0RNU00vYWRkLWRtc20vYWRkLWRtc20uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLDZEQUE2RDtBQUY3RCxxQ0FBcUM7QUFJckM7O0FBRUEsc0JBQXNCO0FBQ3RCLDRCQUE0QjtBQUM1QixZQUFZO0FBQ1osaUNBQWlDO0FBQ2pDO0FBQ0E7O0lBRUksV0FBVztBQUNmO0FBQ0E7QUFDQSxZQUFZO0FBQ1osc0JBQXNCO0FBQ3RCLFVBQVU7QUFDViw0QkFBNEI7QUFDNUIsMkJBQTJCO0FBQzNCO0FBRUE7QUFDQSxhQUFhO0FBQ2IsZUFBZTtBQUNmLGtCQUFrQjtBQUNsQixZQUFZO0FBQ1osNENBQTRDO0FBQzVDO0FBRUE7QUFDQSxlQUFlO0FBQ2YsaUJBQWlCO0FBQ2pCLGNBQWM7QUFDZDtBQUVBO0FBQ0EsWUFBWTtBQUNaLGVBQWU7QUFDZjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxrQkFBa0I7QUFDbEIsV0FBVztBQUNYLFVBQVU7QUFDVjtBQUVBO0FBQ0EsV0FBVztBQUNYLHlCQUF5QjtBQUN6QixZQUFZO0FBQ1osbUJBQW1CO0FBQ25CO0FBRUE7QUFDQSw0QkFBNEI7QUFDNUIsOEJBQThCOztBQUU5QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7O0FBRUEsV0FBVztBQUNYLFlBQVk7QUFDWixpQkFBaUI7QUFDakIsaUJBQWlCO0FBQ2pCO0FBRUE7QUFDQSxZQUFZO0FBQ1oseUJBQXlCO0FBQ3pCLFlBQVk7QUFDWjtBQUVBO0FBQ0EsWUFBWTtBQUNaLHVCQUF1QjtBQUN2QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQTs7SUFFSSw2QkFBNkI7O0FBRWpDO0FBSUE7d0RBQ3dEO0FBQ3hEO0VBQ0U7O0lBRUUsZUFBZTtJQUNmLGtCQUFrQjtNQUNoQjs7QUFFTjtBQUVBO0lBQ0k7UUFDSSxjQUFjO1FBQ2QsZUFBZTtRQUNmLGtCQUFrQjs7UUFFbEI7O0FBRVIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvRE1TTS9hZGQtZG1zbS9hZGQtZG1zbS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogTWFkZSB3aXRoIGxvdmUgYnkgTXV0aXVsbGFoIFNhbWltKi9cblxuQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1OdW1hbnMnKTtcblxuaHRtbCxib2R5e1xuXG5iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbmhlaWdodDogMTAwJTtcbmZvbnQtZmFtaWx5OiAnTnVtYW5zJywgc2Fucy1zZXJpZjtcbn1cbmxhYmVsXG57XG4gICAgY29sb3I6ICNmZmY7XG59XG4uY29udGFpbmVye1xuaGVpZ2h0OiAxMDAlO1xuYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbndpZHRoOjEwMCU7XG5iYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuLyogYWxpZ24tY29udGVudDogY2VudGVyOyAqL1xufVxuXG4uY2FyZHtcbmhlaWdodDogNzAwcHg7XG5tYXJnaW4tdG9wOjIwcHg7XG5tYXJnaW4tYm90dG9tOjIwcHg7XG53aWR0aDogNDUwcHg7XG5iYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuNSkgIWltcG9ydGFudDtcbn1cblxuLnNvY2lhbF9pY29uIHNwYW57XG5mb250LXNpemU6IDYwcHg7XG5tYXJnaW4tbGVmdDogMTBweDtcbmNvbG9yOiAjRkZDMzEyO1xufVxuXG4uc29jaWFsX2ljb24gc3Bhbjpob3ZlcntcbmNvbG9yOiB3aGl0ZTtcbmN1cnNvcjogcG9pbnRlcjtcbn1cblxuLmNhcmQtaGVhZGVyIGgze1xuY29sb3I6IHdoaXRlO1xufVxuXG4uc29jaWFsX2ljb257XG5wb3NpdGlvbjogYWJzb2x1dGU7XG5yaWdodDogMjBweDtcbnRvcDogLTQ1cHg7XG59XG5cbi5pbnB1dC1ncm91cC1wcmVwZW5kIHNwYW57XG53aWR0aDogNTBweDtcbmJhY2tncm91bmQtY29sb3I6ICNGRkMzMTI7XG5jb2xvcjogYmxhY2s7XG5ib3JkZXI6MCAhaW1wb3J0YW50O1xufVxuXG5pbnB1dDpmb2N1c3tcbm91dGxpbmU6IDAgMCAwIDAgICFpbXBvcnRhbnQ7XG5ib3gtc2hhZG93OiAwIDAgMCAwICFpbXBvcnRhbnQ7XG5cbn1cblxuLnJlbWVtYmVye1xuY29sb3I6IHdoaXRlO1xufVxuXG4ucmVtZW1iZXIgaW5wdXRcbntcbndpZHRoOiAyMHB4O1xuaGVpZ2h0OiAyMHB4O1xubWFyZ2luLWxlZnQ6IDE1cHg7XG5tYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLmxvZ2luX2J0bntcbmNvbG9yOiBibGFjaztcbmJhY2tncm91bmQtY29sb3I6ICNGRkMzMTI7XG53aWR0aDogMTAwcHg7XG59XG5cbi5sb2dpbl9idG46aG92ZXJ7XG5jb2xvcjogYmxhY2s7XG5iYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cblxuLmxpbmtze1xuY29sb3I6IHdoaXRlO1xufVxuXG4ubGlua3MgYXtcbm1hcmdpbi1sZWZ0OiA0cHg7XG59XG5vcHRpb25cbntcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4IWltcG9ydGFudDtcblxufVxuXG5cblxuLyogKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuKioqKioqKioqKioqKioqKioqKioqKiAgTWVkaWEgcXVlcnkgZm9yIE1vYmlsZSBzY3JlZW4gICovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGggOiA3NjhweCkge1xuICAuY2FyZHtcbiAgICAgIFxuICAgIG1hcmdpbi10b3A6YXV0bztcbiAgICBtYXJnaW4tYm90dG9tOmF1dG87XG4gICAgICB9XG5cbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoIDogNDgwcHgpIHtcbiAgICAuY2FyZHtcbiAgICAgICAgaGVpZ2h0OiAxMDMwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6MjBweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbToyMHB4O1xuXG4gICAgICAgIH1cblxufSJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/DMSM/add-dmsm/add-dmsm.component.html":
/*!********************************************************************!*\
  !*** ./src/app/Main_masters/DMSM/add-dmsm/add-dmsm.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"d-flex justify-content-center h-100\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        <h3 style=\"text-align:center\">Add Dry Process Material Supplier</h3>\n      </div>\n      <div class=\"card-body\">\n        <form class=\"form-login\" role=\"form\" #formRef='ngForm' (ngSubmit)='onSubmit(formRef.value)'>\n          <div class=\"form-group row\">\n            <label for=\"subDep\" class=\"col-sm-5 col-form-label\"> Dry Process Material Supplier Name</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDep\" name=\"newSupplierName\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepcfPassword\" class=\"col-sm-5 col-form-label\">Contact Person</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepcfPassword\" name=\"Cperson\" #selectRef='ngModel'\n                required ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepPassword\" class=\"col-sm-5 col-form-label\">Address</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepPassword\" name=\"address\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepPassword\" class=\"col-sm-5 col-form-label\">Pin Code</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepPassword\" name=\"Pincode\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Mobile Number</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"mobile\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Landline Number</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"landline\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">E-Mail ID</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"Email\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">TIN Number</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"tin\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Remark</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"remarks\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n\n          <div class=\"form-group\" style=\"text-align:center\">\n            <button style=\"padding: 0;\n                    border: none;\n                    background: none;\" type=\"submit\">\n              <!-- <i class=\"far fa-check-circle\"> -->\n              <i class=\"fas fa-check-circle\" (click)=\" onAddBuyer()\" style=\"font-size:60px;color: yellow;\"></i>\n\n            </button>\n          </div>\n\n        </form>\n      </div>\n\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/DMSM/add-dmsm/add-dmsm.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/Main_masters/DMSM/add-dmsm/add-dmsm.component.ts ***!
  \******************************************************************/
/*! exports provided: AddDmsmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddDmsmComponent", function() { return AddDmsmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddDmsmComponent = /** @class */ (function () {
    function AddDmsmComponent(router, _http, headerTitleService) {
        this.router = router;
        this._http = _http;
        this.headerTitleService = headerTitleService;
    }
    AddDmsmComponent.prototype.ngOnInit = function () {
        this.headerTitleService.setTitle('Dry Material Supplier Master');
    };
    AddDmsmComponent.prototype.onSubmit = function (a) {
        var _this = this;
        this.newSupplierName = a.newSupplierName;
        this.Cperson = a.Cperson;
        this.address = a.address;
        this.Pincode = a.Pincode;
        this.mobile = a.mobile;
        this.landline = a.altermobile;
        this.Email = a.Email;
        this.tin = a.tin;
        this.remarks = a.remarks;
        console.log(a);
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.post('api/add-dry-material', a, options)
            .subscribe(function (data) {
            alert('Added Successfully');
            _this.router.navigate(['/main-masters']);
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    AddDmsmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-dmsm',
            template: __webpack_require__(/*! ./add-dmsm.component.html */ "./src/app/Main_masters/DMSM/add-dmsm/add-dmsm.component.html"),
            styles: [__webpack_require__(/*! ./add-dmsm.component.css */ "./src/app/Main_masters/DMSM/add-dmsm/add-dmsm.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"]])
    ], AddDmsmComponent);
    return AddDmsmComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/DMSM/edit-dmsm/edit-dmsm.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/Main_masters/DMSM/edit-dmsm/edit-dmsm.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table,tr,th,td\n{\n    border:1px solid #fff;\n}\n\n#export-button\n{\n    padding-left:20px;\n    padding-right:20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0RNU00vZWRpdC1kbXNtL2VkaXQtZG1zbS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztJQUVJLHFCQUFxQjtBQUN6Qjs7QUFFQTs7SUFFSSxpQkFBaUI7SUFDakIsa0JBQWtCO0FBQ3RCIiwiZmlsZSI6InNyYy9hcHAvTWFpbl9tYXN0ZXJzL0RNU00vZWRpdC1kbXNtL2VkaXQtZG1zbS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUsdHIsdGgsdGRcbntcbiAgICBib3JkZXI6MXB4IHNvbGlkICNmZmY7XG59XG5cbiNleHBvcnQtYnV0dG9uXG57XG4gICAgcGFkZGluZy1sZWZ0OjIwcHg7XG4gICAgcGFkZGluZy1yaWdodDoyMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/DMSM/edit-dmsm/edit-dmsm.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/Main_masters/DMSM/edit-dmsm/edit-dmsm.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\n<div class=\"container table-responsive-sm table-responsive-md table-responsive-lg\" style=\"text-align: -webkit-center;\">\n  <div *ngIf=\"update\">\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.newSupplierName>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.address>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.Pincode>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.Cperson>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.mobile>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.landline>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.Email>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.tin>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.remarks>\n    <button (click)='saveUpdate(dataedit)' type=\"button\" class=\"btn btn-sm btn-info\" style=\"margin-top: -5px;font-size: 13.9px;\">save</button>\n  </div>\n  <br>\n  <br>\n  <table class=\"table\">\n    <thead style=\"text-align: -webkit-center;color: cornsilk;\">\n      <tr class=\"text-color\">\n        <th>Chemical Manufacturer Name</th>\n        <th>Contact Person</th>\n        <th>Address</th>\n        <th>Pincode</th>\n        <th>Mobile No</th>\n        <th>Landline</th>\n        <th>Email Id</th>\n        <th>GSTIN</th>\n        <th>Remark</th>\n        <th>Edit</th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor='let i of test; let in=index' class=\"success\" style=\"color:white\">\n        <td role=\"cell\">{{i.newSupplierName}}</td>\n        <td role=\"cell\">{{i.Cperson}}</td>\n        <td role=\"cell\">{{i.address}}</td>\n        <td role=\"cell\">{{i.Pincode}}</td>\n        <td role=\"cell\">{{i.mobile}}</td>\n        <td role=\"cell\">{{i.landline}}</td>\n        <td role=\"cell\">{{i.Email}}</td>\n        <td role=\"cell\">{{i.tin}}</td>\n        <td role=\"cell\">{{i.remarks}}</td>\n        <td><button (click)='editData(i,in)' type=\"button\" class=\"btn btn-large btn-info\">Edit</button></td>\n      </tr>\n    </tbody>\n  </table>\n  <button id=\"export-button\" type=\"button\" (click)=\"exportExcel()\" class=\"btn btn-info btn-lg \">Export</button>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/DMSM/edit-dmsm/edit-dmsm.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/Main_masters/DMSM/edit-dmsm/edit-dmsm.component.ts ***!
  \********************************************************************/
/*! exports provided: EditDmsmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditDmsmComponent", function() { return EditDmsmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_Services_dmsm_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/dmsm.service */ "./src/app/Services/dmsm.service.ts");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
/* harmony import */ var angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular2-csv/Angular2-csv */ "./node_modules/angular2-csv/Angular2-csv.js");
/* harmony import */ var angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_5__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var EditDmsmComponent = /** @class */ (function () {
    function EditDmsmComponent(DmsmService, _http, router, headerTitleService) {
        this.DmsmService = DmsmService;
        this._http = _http;
        this.router = router;
        this.headerTitleService = headerTitleService;
        this.update = false;
        this.test1 = [{}];
    }
    EditDmsmComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.DmsmService.getData().subscribe(function (DpmData) { return _this.test = DpmData; });
        this.headerTitleService.setTitle('Dry Material Supplier Master');
    };
    EditDmsmComponent.prototype.editData = function (i) {
        this.update = true;
        this.dataedit = i;
        console.log("obj is" + this.dataedit.newSupplierName);
    };
    EditDmsmComponent.prototype.saveUpdate = function (datasave) {
        console.log("datasaved" + datasave.newSupplierName);
        console.log("datasaved" + '' + datasave.newSupplierName + '' +
            '' + datasave.address + '' +
            '' + datasave.Pincode + '' +
            '' + datasave.Cperson + '' +
            '' + datasave.mobile);
        var url = "http://localhost:3000/api/add-dry-material" + "/" + datasave._id;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["RequestOptions"]({ headers: headers });
        this._http.put(url, datasave, options)
            .subscribe(function (data) {
            alert('saved Successfully');
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    EditDmsmComponent.prototype.exportExcel = function () {
        var head = [
            'newSupplierName',
            'Cperson',
            'address',
            'Pincode',
            'mobile',
            'landline',
            'Email',
            'tin',
            'remarks'
        ];
        new angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_5__["Angular2Csv"](this.test1, 'Dry Process Material Supplier Master', { headers: (head) });
        alert("Please Give the Input Details");
        this.router.navigate(['/import-dry-material']);
    };
    EditDmsmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-dmsm',
            template: __webpack_require__(/*! ./edit-dmsm.component.html */ "./src/app/Main_masters/DMSM/edit-dmsm/edit-dmsm.component.html"),
            styles: [__webpack_require__(/*! ./edit-dmsm.component.css */ "./src/app/Main_masters/DMSM/edit-dmsm/edit-dmsm.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_Services_dmsm_service__WEBPACK_IMPORTED_MODULE_3__["DMSMService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_4__["HeaderTitleService"]])
    ], EditDmsmComponent);
    return EditDmsmComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/DMSM/import-dmsm/import-dmsm.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/Main_masters/DMSM/import-dmsm/import-dmsm.component.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n.center-div\n{\n    position: absolute;\n    left: 50%;\n    top: 50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%);\n    \n    /*\n    This doesn't work\n    margin-left: -25%;\n    margin-top: -25%;\n    */\n    \n    width: 400px;\n    height: 300px;\n  \n    padding: 20px;  \n    background-color: rgba(0,0,0,0.5) !important;\n    color: white;\n    text-align: center;\n    box-shadow: 0 0 30px rgba(0, 0, 0, 0.2);\n}\n/* @media (max-width: 1000px) {\n    \n    .center-inner{left:25%;top:25%;position:absolute;width:50%;height:300px;background-color: rgba(0,0,0,0.5) !important;text-align:center;max-width:500px;max-height:500px;color:#ffffff}\n    } */\n.center{left:50%;top:25%;position:absolute;}\n.center-inner{width:400px;height:100%;margin-left:-250px;height:300px;background-color: rgba(0,0,0,0.5) !important;text-align:center;max-width:500px;max-height:500px;color:#ffffff;padding: 20px;border-radius: 20px;}\n@media (max-width: 428px) {\n    .center-inner{width:300px;height:100%;margin-left:-150px;height:300px;background-color: rgba(0,0,0,0.5) !important;text-align:center;max-width:500px;max-height:500px;color:#ffffff;padding: 20px;border-radius: 20px;}\n    .center{left:50%;top:25%;position:absolute;}\n\n    }\ninput[type=\"file\"] {\n        display: none;\n    }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0RNU00vaW1wb3J0LWRtc20vaW1wb3J0LWRtc20uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0E7O0lBRUksa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxRQUFRO0lBQ1Isd0NBQWdDO1lBQWhDLGdDQUFnQzs7SUFFaEM7Ozs7S0FJQzs7SUFFRCxZQUFZO0lBQ1osYUFBYTs7SUFFYixhQUFhO0lBQ2IsNENBQTRDO0lBQzVDLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsdUNBQXVDO0FBQzNDO0FBQ0E7OztPQUdPO0FBRUgsUUFBUSxRQUFRLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDO0FBQzNDLGNBQWMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsNENBQTRDLENBQUMsaUJBQWlCLENBQUMsZUFBZSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUM7QUFFdE47SUFDQSxjQUFjLFdBQVcsQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLDRDQUE0QyxDQUFDLGlCQUFpQixDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDO0lBQ3ROLFFBQVEsUUFBUSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQzs7SUFFM0M7QUFDQTtRQUNJLGFBQWE7SUFDakIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvRE1TTS9pbXBvcnQtZG1zbS9pbXBvcnQtZG1zbS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG4uY2VudGVyLWRpdlxue1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiA1MCU7XG4gICAgdG9wOiA1MCU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gICAgXG4gICAgLypcbiAgICBUaGlzIGRvZXNuJ3Qgd29ya1xuICAgIG1hcmdpbi1sZWZ0OiAtMjUlO1xuICAgIG1hcmdpbi10b3A6IC0yNSU7XG4gICAgKi9cbiAgICBcbiAgICB3aWR0aDogNDAwcHg7XG4gICAgaGVpZ2h0OiAzMDBweDtcbiAgXG4gICAgcGFkZGluZzogMjBweDsgIFxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgYm94LXNoYWRvdzogMCAwIDMwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xufVxuLyogQG1lZGlhIChtYXgtd2lkdGg6IDEwMDBweCkge1xuICAgIFxuICAgIC5jZW50ZXItaW5uZXJ7bGVmdDoyNSU7dG9wOjI1JTtwb3NpdGlvbjphYnNvbHV0ZTt3aWR0aDo1MCU7aGVpZ2h0OjMwMHB4O2JhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O3RleHQtYWxpZ246Y2VudGVyO21heC13aWR0aDo1MDBweDttYXgtaGVpZ2h0OjUwMHB4O2NvbG9yOiNmZmZmZmZ9XG4gICAgfSAqL1xuICAgIFxuICAgIC5jZW50ZXJ7bGVmdDo1MCU7dG9wOjI1JTtwb3NpdGlvbjphYnNvbHV0ZTt9XG4gICAgLmNlbnRlci1pbm5lcnt3aWR0aDo0MDBweDtoZWlnaHQ6MTAwJTttYXJnaW4tbGVmdDotMjUwcHg7aGVpZ2h0OjMwMHB4O2JhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O3RleHQtYWxpZ246Y2VudGVyO21heC13aWR0aDo1MDBweDttYXgtaGVpZ2h0OjUwMHB4O2NvbG9yOiNmZmZmZmY7cGFkZGluZzogMjBweDtib3JkZXItcmFkaXVzOiAyMHB4O31cbiAgXG4gICAgQG1lZGlhIChtYXgtd2lkdGg6IDQyOHB4KSB7XG4gICAgLmNlbnRlci1pbm5lcnt3aWR0aDozMDBweDtoZWlnaHQ6MTAwJTttYXJnaW4tbGVmdDotMTUwcHg7aGVpZ2h0OjMwMHB4O2JhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O3RleHQtYWxpZ246Y2VudGVyO21heC13aWR0aDo1MDBweDttYXgtaGVpZ2h0OjUwMHB4O2NvbG9yOiNmZmZmZmY7cGFkZGluZzogMjBweDtib3JkZXItcmFkaXVzOiAyMHB4O31cbiAgICAuY2VudGVye2xlZnQ6NTAlO3RvcDoyNSU7cG9zaXRpb246YWJzb2x1dGU7fVxuXG4gICAgfVxuICAgIGlucHV0W3R5cGU9XCJmaWxlXCJdIHtcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICB9Il19 */"

/***/ }),

/***/ "./src/app/Main_masters/DMSM/import-dmsm/import-dmsm.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/Main_masters/DMSM/import-dmsm/import-dmsm.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"center\">\n  <div class=\"center-inner\">\n    <form id=\"login\">\n      <h4>Import File</h4>\n      <hr>\n      <p>* Only CSV File can be import. </p>\n\n      <input type=\"file\" name=\"files\" class=\"form-control\" #uploads (change)=\"onChange(uploads.files)\" multiple value=\"process\"\n        #file />\n      <div class=\"input-group mb-3\">\n        <div class=\"input-group-prepend\">\n          <button class=\"btn btn-outline-primary\" (click)=\"file.click()\" type=\"button\">Browser</button>\n        </div>\n        <input type=\"text\" value=\"{{fileName}}\" class=\"form-control form-control-lg\" aria-label=\"Default\"\n          aria-describedby=\"inputGroup-sizing-default\">\n      </div>\n      <br>\n      <div class=\"form-group\" style=\"text-align:center\">\n        <button style=\"padding: 0;\n                          border: none;\n                          background: none;\"\n          type=\"button\" (click)=\"onUpload()\">\n\n          <i class=\"fas fa-check-circle\" style=\"font-size:70px;color: yellow;\"></i>\n\n        </button>\n      </div>\n\n    </form>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/DMSM/import-dmsm/import-dmsm.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/Main_masters/DMSM/import-dmsm/import-dmsm.component.ts ***!
  \************************************************************************/
/*! exports provided: ImportDmsmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImportDmsmComponent", function() { return ImportDmsmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
/* harmony import */ var papaparse__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! papaparse */ "./node_modules/papaparse/papaparse.min.js");
/* harmony import */ var papaparse__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(papaparse__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ImportDmsmComponent = /** @class */ (function () {
    function ImportDmsmComponent(headerTitleService, http, router) {
        this.headerTitleService = headerTitleService;
        this.http = http;
        this.router = router;
    }
    ImportDmsmComponent.prototype.ngOnInit = function () {
        this.headerTitleService.setTitle('Dry Process  Material Supplier Master ');
    };
    // DMSM api post call
    ImportDmsmComponent.prototype.onChange = function (files) {
        var _this = this;
        if (files[0]) {
            console.log(files[0]);
            papaparse__WEBPACK_IMPORTED_MODULE_2__["parse"](files[0], {
                header: true,
                skipEmptyLines: true,
                complete: function (result, file) {
                    console.log(result);
                    _this.fileName = file.name;
                    _this.dataList = result.data;
                    console.log(_this.dataList);
                    var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["Headers"]();
                    headers.append('Content-Type', 'application/json');
                    var options = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["RequestOptions"]({ headers: headers });
                    _this.http.post('api/dmsmFileUpload', _this.dataList, options)
                        .subscribe(function (data) {
                        console.log("added successfully");
                    }, function (error) {
                        console.log(JSON.stringify(error.json()));
                    });
                }
            });
        }
    };
    ImportDmsmComponent.prototype.onUpload = function () {
        this.router.navigate(['/edit-dry-material']);
    };
    ImportDmsmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-import-dmsm',
            template: __webpack_require__(/*! ./import-dmsm.component.html */ "./src/app/Main_masters/DMSM/import-dmsm/import-dmsm.component.html"),
            styles: [__webpack_require__(/*! ./import-dmsm.component.css */ "./src/app/Main_masters/DMSM/import-dmsm/import-dmsm.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_1__["HeaderTitleService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_4__["Http"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ImportDmsmComponent);
    return ImportDmsmComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/DPM/add-dpm/add-dpm.component.css":
/*!****************************************************************!*\
  !*** ./src/app/Main_masters/DPM/add-dpm/add-dpm.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n@import url('https://fonts.googleapis.com/css?family=Numans');\n/* Made with love by Mutiullah Samim*/\nhtml,body{\r\n\r\nbackground-size: cover;\r\nbackground-repeat: no-repeat;\r\nheight: 100%;\r\nfont-family: 'Numans', sans-serif;\r\n}\nlabel\r\n{\r\n    color: #fff;\r\n}\n.container{\r\nheight: 100%;\r\nbackground-size: cover;\r\nwidth:100%;\r\nbackground-repeat: no-repeat;\r\n/* align-content: center; */\r\n}\n.card{\r\nheight: 300px;\r\nmargin-top:200px;\r\nmargin-bottom:20px;\r\nwidth: 450px;\r\nbackground-color: rgba(0,0,0,0.5) !important;\r\n}\n.social_icon span{\r\nfont-size: 60px;\r\nmargin-left: 10px;\r\ncolor: #FFC312;\r\n}\n.social_icon span:hover{\r\ncolor: white;\r\ncursor: pointer;\r\n}\n.card-header h3{\r\ncolor: white;\r\n}\n.social_icon{\r\nposition: absolute;\r\nright: 20px;\r\ntop: -45px;\r\n}\n.input-group-prepend span{\r\nwidth: 50px;\r\nbackground-color: #FFC312;\r\ncolor: black;\r\nborder:0 !important;\r\n}\ninput:focus{\r\noutline: 0 0 0 0  !important;\r\nbox-shadow: 0 0 0 0 !important;\r\n\r\n}\n.remember{\r\ncolor: white;\r\n}\n.remember input\r\n{\r\nwidth: 20px;\r\nheight: 20px;\r\nmargin-left: 15px;\r\nmargin-right: 5px;\r\n}\n.login_btn{\r\ncolor: black;\r\nbackground-color: #FFC312;\r\nwidth: 100px;\r\n}\n.login_btn:hover{\r\ncolor: black;\r\nbackground-color: white;\r\n}\n.links{\r\ncolor: white;\r\n}\n.links a{\r\nmargin-left: 4px;\r\n}\noption\r\n{\r\n    margin-bottom: 10px!important;\r\n\r\n}\n/* *************************************************************************\r\n**********************  Media query for Mobile screen  */\n@media only screen and (max-width : 768px) {\r\n  .card{\r\n      \r\n    margin-top:auto;\r\n    margin-bottom:auto;\r\n      }\r\n\r\n}\n@media only screen and (max-width : 480px) {\r\n    .card{\r\n        height:330px;\r\n        margin-top:20px;\r\n        margin-bottom:20px;\r\n\r\n        }\r\n\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0RQTS9hZGQtZHBtL2FkZC1kcG0uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLDZEQUE2RDtBQUY3RCxxQ0FBcUM7QUFJckM7O0FBRUEsc0JBQXNCO0FBQ3RCLDRCQUE0QjtBQUM1QixZQUFZO0FBQ1osaUNBQWlDO0FBQ2pDO0FBQ0E7O0lBRUksV0FBVztBQUNmO0FBQ0E7QUFDQSxZQUFZO0FBQ1osc0JBQXNCO0FBQ3RCLFVBQVU7QUFDViw0QkFBNEI7QUFDNUIsMkJBQTJCO0FBQzNCO0FBRUE7QUFDQSxhQUFhO0FBQ2IsZ0JBQWdCO0FBQ2hCLGtCQUFrQjtBQUNsQixZQUFZO0FBQ1osNENBQTRDO0FBQzVDO0FBRUE7QUFDQSxlQUFlO0FBQ2YsaUJBQWlCO0FBQ2pCLGNBQWM7QUFDZDtBQUVBO0FBQ0EsWUFBWTtBQUNaLGVBQWU7QUFDZjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxrQkFBa0I7QUFDbEIsV0FBVztBQUNYLFVBQVU7QUFDVjtBQUVBO0FBQ0EsV0FBVztBQUNYLHlCQUF5QjtBQUN6QixZQUFZO0FBQ1osbUJBQW1CO0FBQ25CO0FBRUE7QUFDQSw0QkFBNEI7QUFDNUIsOEJBQThCOztBQUU5QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7O0FBRUEsV0FBVztBQUNYLFlBQVk7QUFDWixpQkFBaUI7QUFDakIsaUJBQWlCO0FBQ2pCO0FBRUE7QUFDQSxZQUFZO0FBQ1oseUJBQXlCO0FBQ3pCLFlBQVk7QUFDWjtBQUVBO0FBQ0EsWUFBWTtBQUNaLHVCQUF1QjtBQUN2QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQTs7SUFFSSw2QkFBNkI7O0FBRWpDO0FBSUE7d0RBQ3dEO0FBQ3hEO0VBQ0U7O0lBRUUsZUFBZTtJQUNmLGtCQUFrQjtNQUNoQjs7QUFFTjtBQUVBO0lBQ0k7UUFDSSxZQUFZO1FBQ1osZUFBZTtRQUNmLGtCQUFrQjs7UUFFbEI7O0FBRVIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvRFBNL2FkZC1kcG0vYWRkLWRwbS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogTWFkZSB3aXRoIGxvdmUgYnkgTXV0aXVsbGFoIFNhbWltKi9cclxuXHJcbkBpbXBvcnQgdXJsKCdodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2Nzcz9mYW1pbHk9TnVtYW5zJyk7XHJcblxyXG5odG1sLGJvZHl7XHJcblxyXG5iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG5iYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG5oZWlnaHQ6IDEwMCU7XHJcbmZvbnQtZmFtaWx5OiAnTnVtYW5zJywgc2Fucy1zZXJpZjtcclxufVxyXG5sYWJlbFxyXG57XHJcbiAgICBjb2xvcjogI2ZmZjtcclxufVxyXG4uY29udGFpbmVye1xyXG5oZWlnaHQ6IDEwMCU7XHJcbmJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbndpZHRoOjEwMCU7XHJcbmJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbi8qIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjsgKi9cclxufVxyXG5cclxuLmNhcmR7XHJcbmhlaWdodDogMzAwcHg7XHJcbm1hcmdpbi10b3A6MjAwcHg7XHJcbm1hcmdpbi1ib3R0b206MjBweDtcclxud2lkdGg6IDQ1MHB4O1xyXG5iYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuNSkgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnNvY2lhbF9pY29uIHNwYW57XHJcbmZvbnQtc2l6ZTogNjBweDtcclxubWFyZ2luLWxlZnQ6IDEwcHg7XHJcbmNvbG9yOiAjRkZDMzEyO1xyXG59XHJcblxyXG4uc29jaWFsX2ljb24gc3Bhbjpob3ZlcntcclxuY29sb3I6IHdoaXRlO1xyXG5jdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbi5jYXJkLWhlYWRlciBoM3tcclxuY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4uc29jaWFsX2ljb257XHJcbnBvc2l0aW9uOiBhYnNvbHV0ZTtcclxucmlnaHQ6IDIwcHg7XHJcbnRvcDogLTQ1cHg7XHJcbn1cclxuXHJcbi5pbnB1dC1ncm91cC1wcmVwZW5kIHNwYW57XHJcbndpZHRoOiA1MHB4O1xyXG5iYWNrZ3JvdW5kLWNvbG9yOiAjRkZDMzEyO1xyXG5jb2xvcjogYmxhY2s7XHJcbmJvcmRlcjowICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbmlucHV0OmZvY3Vze1xyXG5vdXRsaW5lOiAwIDAgMCAwICAhaW1wb3J0YW50O1xyXG5ib3gtc2hhZG93OiAwIDAgMCAwICFpbXBvcnRhbnQ7XHJcblxyXG59XHJcblxyXG4ucmVtZW1iZXJ7XHJcbmNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLnJlbWVtYmVyIGlucHV0XHJcbntcclxud2lkdGg6IDIwcHg7XHJcbmhlaWdodDogMjBweDtcclxubWFyZ2luLWxlZnQ6IDE1cHg7XHJcbm1hcmdpbi1yaWdodDogNXB4O1xyXG59XHJcblxyXG4ubG9naW5fYnRue1xyXG5jb2xvcjogYmxhY2s7XHJcbmJhY2tncm91bmQtY29sb3I6ICNGRkMzMTI7XHJcbndpZHRoOiAxMDBweDtcclxufVxyXG5cclxuLmxvZ2luX2J0bjpob3ZlcntcclxuY29sb3I6IGJsYWNrO1xyXG5iYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLmxpbmtze1xyXG5jb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi5saW5rcyBhe1xyXG5tYXJnaW4tbGVmdDogNHB4O1xyXG59XHJcbm9wdGlvblxyXG57XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4IWltcG9ydGFudDtcclxuXHJcbn1cclxuXHJcblxyXG5cclxuLyogKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG4qKioqKioqKioqKioqKioqKioqKioqICBNZWRpYSBxdWVyeSBmb3IgTW9iaWxlIHNjcmVlbiAgKi9cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoIDogNzY4cHgpIHtcclxuICAuY2FyZHtcclxuICAgICAgXHJcbiAgICBtYXJnaW4tdG9wOmF1dG87XHJcbiAgICBtYXJnaW4tYm90dG9tOmF1dG87XHJcbiAgICAgIH1cclxuXHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aCA6IDQ4MHB4KSB7XHJcbiAgICAuY2FyZHtcclxuICAgICAgICBoZWlnaHQ6MzMwcHg7XHJcbiAgICAgICAgbWFyZ2luLXRvcDoyMHB4O1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206MjBweDtcclxuXHJcbiAgICAgICAgfVxyXG5cclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/DPM/add-dpm/add-dpm.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/Main_masters/DPM/add-dpm/add-dpm.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  <div class=\"d-flex justify-content-center h-100\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">\r\n        <h3 style=\"text-align:center\"> Add Dry process</h3>\r\n      </div>\r\n      <div class=\"card-body\">\r\n        <form class=\"form-login\" role=\"form\" #formRef='ngForm' (ngSubmit)='onSubmit(formRef.value)'>\r\n          <div class=\"form-group row\">\r\n            <label for=\"gartype\" class=\"col-sm-3 col-form-label\">Add</label>\r\n            <div class=\"col-sm-7\">\r\n              <input type=\"text\" class=\"form-control\" id=\"gartype\" name=\"DpmName\" #seldepRef='ngModel' required ngModel>\r\n            </div>\r\n          </div>\r\n          <br>\r\n          <br>\r\n          <div class=\"form-group\" style=\"text-align:center\">\r\n            <button style=\"padding: 0;\r\n                border: none;\r\n                background: none;\" type=\"submit\">\r\n              <!-- <i class=\"far fa-check-circle\"> -->\r\n              <i class=\"fas fa-check-circle\" style=\"font-size:60px;color: yellow;\"></i>\r\n            </button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/DPM/add-dpm/add-dpm.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/Main_masters/DPM/add-dpm/add-dpm.component.ts ***!
  \***************************************************************/
/*! exports provided: AddDpmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddDpmComponent", function() { return AddDpmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddDpmComponent = /** @class */ (function () {
    function AddDpmComponent(router, _http, headerTitleService) {
        this.router = router;
        this._http = _http;
        this.headerTitleService = headerTitleService;
    }
    AddDpmComponent.prototype.ngOnInit = function () {
        this.headerTitleService.setTitle('Dry Process Master');
    };
    AddDpmComponent.prototype.onSubmit = function (a) {
        var _this = this;
        this.DpmName = a.DpmName,
            console.log(a);
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.post('api/add-supplier-master', a, options)
            .subscribe(function (data) {
            alert('Added Successfully');
            _this.router.navigate(['/main-masters']);
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    AddDpmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-dpm',
            template: __webpack_require__(/*! ./add-dpm.component.html */ "./src/app/Main_masters/DPM/add-dpm/add-dpm.component.html"),
            styles: [__webpack_require__(/*! ./add-dpm.component.css */ "./src/app/Main_masters/DPM/add-dpm/add-dpm.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"]])
    ], AddDpmComponent);
    return AddDpmComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/DPM/edit-dpm/edit-dpm.component.css":
/*!******************************************************************!*\
  !*** ./src/app/Main_masters/DPM/edit-dpm/edit-dpm.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table,tr,th,td\n{\n    border:1px solid #fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0RQTS9lZGl0LWRwbS9lZGl0LWRwbS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztJQUVJLHFCQUFxQjtBQUN6QiIsImZpbGUiOiJzcmMvYXBwL01haW5fbWFzdGVycy9EUE0vZWRpdC1kcG0vZWRpdC1kcG0uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlLHRyLHRoLHRkXG57XG4gICAgYm9yZGVyOjFweCBzb2xpZCAjZmZmO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/DPM/edit-dpm/edit-dpm.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/Main_masters/DPM/edit-dpm/edit-dpm.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\r\n<div class=\"container\" style=\"text-align: -webkit-center;\">\r\n  <div *ngIf=\"update\">\r\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.DpmName>\r\n    <button (click)='saveUpdate(datatobeedited)' type=\"button\" class=\"btn btn-sm btn-info\" style=\"margin-top: -5px;font-size: 13.9px;\">save</button>\r\n  </div>\r\n  <br>\r\n  <br>\r\n  <table class=\"table\">\r\n    <thead style=\"text-align: -webkit-center;color: cornsilk;\">\r\n      <tr class=\"text-color\">\r\n        <th>Dry Process</th>\r\n        <th>Edit</th>\r\n      </tr>\r\n    </thead>\r\n    <tbody>\r\n      <tr *ngFor='let i of test; let in=index' class=\"success\" style=\"text-align: -webkit-center;color: white;\">\r\n        <td>{{i.DpmName}}</td>\r\n        <td><button (click)='editData(i,in)' type=\"button\" class=\"btn btn-large btn-info\">Edit</button></td>\r\n      </tr>\r\n    </tbody>\r\n  </table>\r\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/DPM/edit-dpm/edit-dpm.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/Main_masters/DPM/edit-dpm/edit-dpm.component.ts ***!
  \*****************************************************************/
/*! exports provided: EditDpmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditDpmComponent", function() { return EditDpmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_dpm_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Services/dpm.service */ "./src/app/Services/dpm.service.ts");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditDpmComponent = /** @class */ (function () {
    function EditDpmComponent(DpmService, _http, headerTitleService) {
        this.DpmService = DpmService;
        this._http = _http;
        this.headerTitleService = headerTitleService;
        this.update = false;
    }
    EditDpmComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.DpmService.getData().subscribe(function (DpmData) { return _this.test = DpmData; });
        this.headerTitleService.setTitle('Dry Process Master');
    };
    EditDpmComponent.prototype.editData = function (i) {
        this.update = true;
        this.datatobeedited = i;
        console.log("obj is" + this.datatobeedited.DpmName);
    };
    EditDpmComponent.prototype.saveUpdate = function (datasaved) {
        console.log("datatobesaved" + datasaved.DpmName);
        console.log("datatobesaved" + '' + datasaved.DpmName);
        var _url = "http://localhost:3000/api/add-supplier-master" + "/" + datasaved._id;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["RequestOptions"]({ headers: headers });
        this._http.put(_url, datasaved, options)
            .subscribe(function (data) {
            alert('saved Successfully');
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    EditDpmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-dpm',
            template: __webpack_require__(/*! ./edit-dpm.component.html */ "./src/app/Main_masters/DPM/edit-dpm/edit-dpm.component.html"),
            styles: [__webpack_require__(/*! ./edit-dpm.component.css */ "./src/app/Main_masters/DPM/edit-dpm/edit-dpm.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_Services_dpm_service__WEBPACK_IMPORTED_MODULE_2__["DpmService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"]])
    ], EditDpmComponent);
    return EditDpmComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/DPMPM/add-dpmpm/add-dpmpm.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/Main_masters/DPMPM/add-dpmpm/add-dpmpm.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n@import url('https://fonts.googleapis.com/css?family=Numans');\n/* Made with love by Mutiullah Samim*/\nhtml,body{\n\nbackground-size: cover;\nbackground-repeat: no-repeat;\nheight: 100%;\nfont-family: 'Numans', sans-serif;\n}\nlabel\n{\n    color: #fff;\n}\n.container{\nheight: 100%;\nbackground-size: cover;\nwidth:100%;\nbackground-repeat: no-repeat;\n/* align-content: center; */\n}\n.card{\nheight: 640px;\nmargin-top:20px;\nmargin-bottom:20px;\nwidth: 450px;\nbackground-color: rgba(0,0,0,0.5) !important;\n}\n.social_icon span{\nfont-size: 60px;\nmargin-left: 10px;\ncolor: #FFC312;\n}\n.social_icon span:hover{\ncolor: white;\ncursor: pointer;\n}\n.card-header h3{\ncolor: white;\n}\n.social_icon{\nposition: absolute;\nright: 20px;\ntop: -45px;\n}\n.input-group-prepend span{\nwidth: 50px;\nbackground-color: #FFC312;\ncolor: black;\nborder:0 !important;\n}\ninput:focus{\noutline: 0 0 0 0  !important;\nbox-shadow: 0 0 0 0 !important;\n\n}\n.remember{\ncolor: white;\n}\n.remember input\n{\nwidth: 20px;\nheight: 20px;\nmargin-left: 15px;\nmargin-right: 5px;\n}\n.login_btn{\ncolor: black;\nbackground-color: #FFC312;\nwidth: 100px;\n}\n.login_btn:hover{\ncolor: black;\nbackground-color: white;\n}\n.links{\ncolor: white;\n}\n.links a{\nmargin-left: 4px;\n}\noption\n{\n    margin-bottom: 10px!important;\n\n}\n.bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {\n    width: auto;\n}\n/* *************************************************************************\n**********************  Media query for Mobile screen  */\n@media only screen and (max-width : 768px) {\n  .card{\n      \n    margin-top:auto;\n    margin-bottom:auto;\n      }\n      input[type=text], select, textarea{\n        width: 100%;\n  \n      }\n      #seldep\n      {\n          width:auto;\n      }\n     select\n     {\n        max-width: 100% !important;\n     }\n     option\n     {\n         width:100%;\n     }\n     /*Make select2 responsive*/\n[class*=\"col-\"] select {\n    width:100%!important;\n}\n\n/* [class*=\"col-\"] select .select2-drop {\n    width: 100%!important;\n} */\n \n}\n@media only screen and (max-width : 480px) {\n    .card{\n        height: 800px;\n        margin-top:20px;\n        margin-bottom:20px;\n\n        }\n\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0RQTVBNL2FkZC1kcG1wbS9hZGQtZHBtcG0uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLDZEQUE2RDtBQUY3RCxxQ0FBcUM7QUFJckM7O0FBRUEsc0JBQXNCO0FBQ3RCLDRCQUE0QjtBQUM1QixZQUFZO0FBQ1osaUNBQWlDO0FBQ2pDO0FBQ0E7O0lBRUksV0FBVztBQUNmO0FBQ0E7QUFDQSxZQUFZO0FBQ1osc0JBQXNCO0FBQ3RCLFVBQVU7QUFDViw0QkFBNEI7QUFDNUIsMkJBQTJCO0FBQzNCO0FBRUE7QUFDQSxhQUFhO0FBQ2IsZUFBZTtBQUNmLGtCQUFrQjtBQUNsQixZQUFZO0FBQ1osNENBQTRDO0FBQzVDO0FBRUE7QUFDQSxlQUFlO0FBQ2YsaUJBQWlCO0FBQ2pCLGNBQWM7QUFDZDtBQUVBO0FBQ0EsWUFBWTtBQUNaLGVBQWU7QUFDZjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxrQkFBa0I7QUFDbEIsV0FBVztBQUNYLFVBQVU7QUFDVjtBQUVBO0FBQ0EsV0FBVztBQUNYLHlCQUF5QjtBQUN6QixZQUFZO0FBQ1osbUJBQW1CO0FBQ25CO0FBRUE7QUFDQSw0QkFBNEI7QUFDNUIsOEJBQThCOztBQUU5QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7O0FBRUEsV0FBVztBQUNYLFlBQVk7QUFDWixpQkFBaUI7QUFDakIsaUJBQWlCO0FBQ2pCO0FBRUE7QUFDQSxZQUFZO0FBQ1oseUJBQXlCO0FBQ3pCLFlBQVk7QUFDWjtBQUVBO0FBQ0EsWUFBWTtBQUNaLHVCQUF1QjtBQUN2QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQTs7SUFFSSw2QkFBNkI7O0FBRWpDO0FBRUE7SUFDSSxXQUFXO0FBQ2Y7QUFFQTt3REFDd0Q7QUFDeEQ7RUFDRTs7SUFFRSxlQUFlO0lBQ2Ysa0JBQWtCO01BQ2hCO01BQ0E7UUFDRSxXQUFXOztNQUViO01BQ0E7O1VBRUksVUFBVTtNQUNkO0tBQ0Q7O1FBRUcsMEJBQTBCO0tBQzdCO0tBQ0E7O1NBRUksVUFBVTtLQUNkO0tBQ0EsMEJBQTBCO0FBQy9CO0lBQ0ksb0JBQW9CO0FBQ3hCOztBQUVBOztHQUVHOztBQUVIO0FBRUE7SUFDSTtRQUNJLGFBQWE7UUFDYixlQUFlO1FBQ2Ysa0JBQWtCOztRQUVsQjs7QUFFUiIsImZpbGUiOiJzcmMvYXBwL01haW5fbWFzdGVycy9EUE1QTS9hZGQtZHBtcG0vYWRkLWRwbXBtLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBNYWRlIHdpdGggbG92ZSBieSBNdXRpdWxsYWggU2FtaW0qL1xuXG5AaW1wb3J0IHVybCgnaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3M/ZmFtaWx5PU51bWFucycpO1xuXG5odG1sLGJvZHl7XG5cbmJhY2tncm91bmQtc2l6ZTogY292ZXI7XG5iYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuaGVpZ2h0OiAxMDAlO1xuZm9udC1mYW1pbHk6ICdOdW1hbnMnLCBzYW5zLXNlcmlmO1xufVxubGFiZWxcbntcbiAgICBjb2xvcjogI2ZmZjtcbn1cbi5jb250YWluZXJ7XG5oZWlnaHQ6IDEwMCU7XG5iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xud2lkdGg6MTAwJTtcbmJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4vKiBhbGlnbi1jb250ZW50OiBjZW50ZXI7ICovXG59XG5cbi5jYXJke1xuaGVpZ2h0OiA2NDBweDtcbm1hcmdpbi10b3A6MjBweDtcbm1hcmdpbi1ib3R0b206MjBweDtcbndpZHRoOiA0NTBweDtcbmJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O1xufVxuXG4uc29jaWFsX2ljb24gc3BhbntcbmZvbnQtc2l6ZTogNjBweDtcbm1hcmdpbi1sZWZ0OiAxMHB4O1xuY29sb3I6ICNGRkMzMTI7XG59XG5cbi5zb2NpYWxfaWNvbiBzcGFuOmhvdmVye1xuY29sb3I6IHdoaXRlO1xuY3Vyc29yOiBwb2ludGVyO1xufVxuXG4uY2FyZC1oZWFkZXIgaDN7XG5jb2xvcjogd2hpdGU7XG59XG5cbi5zb2NpYWxfaWNvbntcbnBvc2l0aW9uOiBhYnNvbHV0ZTtcbnJpZ2h0OiAyMHB4O1xudG9wOiAtNDVweDtcbn1cblxuLmlucHV0LWdyb3VwLXByZXBlbmQgc3BhbntcbndpZHRoOiA1MHB4O1xuYmFja2dyb3VuZC1jb2xvcjogI0ZGQzMxMjtcbmNvbG9yOiBibGFjaztcbmJvcmRlcjowICFpbXBvcnRhbnQ7XG59XG5cbmlucHV0OmZvY3Vze1xub3V0bGluZTogMCAwIDAgMCAgIWltcG9ydGFudDtcbmJveC1zaGFkb3c6IDAgMCAwIDAgIWltcG9ydGFudDtcblxufVxuXG4ucmVtZW1iZXJ7XG5jb2xvcjogd2hpdGU7XG59XG5cbi5yZW1lbWJlciBpbnB1dFxue1xud2lkdGg6IDIwcHg7XG5oZWlnaHQ6IDIwcHg7XG5tYXJnaW4tbGVmdDogMTVweDtcbm1hcmdpbi1yaWdodDogNXB4O1xufVxuXG4ubG9naW5fYnRue1xuY29sb3I6IGJsYWNrO1xuYmFja2dyb3VuZC1jb2xvcjogI0ZGQzMxMjtcbndpZHRoOiAxMDBweDtcbn1cblxuLmxvZ2luX2J0bjpob3ZlcntcbmNvbG9yOiBibGFjaztcbmJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuXG4ubGlua3N7XG5jb2xvcjogd2hpdGU7XG59XG5cbi5saW5rcyBhe1xubWFyZ2luLWxlZnQ6IDRweDtcbn1cbm9wdGlvblxue1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHghaW1wb3J0YW50O1xuXG59XG5cbi5ib290c3RyYXAtc2VsZWN0Om5vdChbY2xhc3MqPWNvbC1dKTpub3QoW2NsYXNzKj1mb3JtLWNvbnRyb2xdKTpub3QoLmlucHV0LWdyb3VwLWJ0bikge1xuICAgIHdpZHRoOiBhdXRvO1xufVxuXG4vKiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4qKioqKioqKioqKioqKioqKioqKioqICBNZWRpYSBxdWVyeSBmb3IgTW9iaWxlIHNjcmVlbiAgKi9cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aCA6IDc2OHB4KSB7XG4gIC5jYXJke1xuICAgICAgXG4gICAgbWFyZ2luLXRvcDphdXRvO1xuICAgIG1hcmdpbi1ib3R0b206YXV0bztcbiAgICAgIH1cbiAgICAgIGlucHV0W3R5cGU9dGV4dF0sIHNlbGVjdCwgdGV4dGFyZWF7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICBcbiAgICAgIH1cbiAgICAgICNzZWxkZXBcbiAgICAgIHtcbiAgICAgICAgICB3aWR0aDphdXRvO1xuICAgICAgfVxuICAgICBzZWxlY3RcbiAgICAge1xuICAgICAgICBtYXgtd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgICAgfVxuICAgICBvcHRpb25cbiAgICAge1xuICAgICAgICAgd2lkdGg6MTAwJTtcbiAgICAgfVxuICAgICAvKk1ha2Ugc2VsZWN0MiByZXNwb25zaXZlKi9cbltjbGFzcyo9XCJjb2wtXCJdIHNlbGVjdCB7XG4gICAgd2lkdGg6MTAwJSFpbXBvcnRhbnQ7XG59XG5cbi8qIFtjbGFzcyo9XCJjb2wtXCJdIHNlbGVjdCAuc2VsZWN0Mi1kcm9wIHtcbiAgICB3aWR0aDogMTAwJSFpbXBvcnRhbnQ7XG59ICovXG4gXG59XG5cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aCA6IDQ4MHB4KSB7XG4gICAgLmNhcmR7XG4gICAgICAgIGhlaWdodDogODAwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6MjBweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbToyMHB4O1xuXG4gICAgICAgIH1cblxufSJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/DPMPM/add-dpmpm/add-dpmpm.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/Main_masters/DPMPM/add-dpmpm/add-dpmpm.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"d-flex justify-content-center h-100\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        <h3 style=\"text-align:center\">Add Dry Process Material Product </h3>\n      </div>\n      <div class=\"card-body\">\n        <form class=\"form-login\" role=\"form\" #formRef='ngForm' (ngSubmit)='onSubmit(formRef.value)'>\n          <div class=\"form-group row\">\n            <label for=\"subDep\" class=\"col-sm-5 col-form-label\">Dry Process Material Product Name</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDep\" name=\"dryproductname\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"seldep\" class=\"col-sm-5 col-form-label\"> Dry Process Material Supplier</label>\n            <div class=\"col-sm-7\">\n              <select class=\"form-control form-control-sm form-control-lg \" data-width=\"auto\" id=\"seldep\" name=\"dryDistributorName\"\n                #selectRef='ngModel' required ngModel>\n                <option selected>Agent Name</option>\n                <option *ngFor=\"let l of data\">{{l.DpmName}}</option>\n\n              </select>\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Average Per Day Consumption</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"dryConsumption\" #selectRef='ngModel'\n                required ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Minimum Order Quantity</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"dryQuantity\" #selectRef='ngModel'\n                required ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Reorder Level</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"dryReorderLevel\" #selectRef='ngModel'\n                required ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Remark</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"dryRemark\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n\n          <div class=\"form-group\" style=\"text-align:center\">\n            <button style=\"padding: 0;\n                border: none;\n                background: none;\" type=\"submit\">\n              <!-- <i class=\"far fa-check-circle\"> -->\n              <i class=\"fas fa-check-circle\" style=\"font-size:60px;color: yellow;\"></i>\n            </button>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/DPMPM/add-dpmpm/add-dpmpm.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/Main_masters/DPMPM/add-dpmpm/add-dpmpm.component.ts ***!
  \*********************************************************************/
/*! exports provided: AddDpmpmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddDpmpmComponent", function() { return AddDpmpmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
/* harmony import */ var src_app_Services_dmsm_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/Services/dmsm.service */ "./src/app/Services/dmsm.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AddDpmpmComponent = /** @class */ (function () {
    function AddDpmpmComponent(router, _http, dmsmService, headerTitleService) {
        this.router = router;
        this._http = _http;
        this.dmsmService = dmsmService;
        this.headerTitleService = headerTitleService;
    }
    AddDpmpmComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.headerTitleService.setTitle('Dry Process Material Product Master');
        this.dmsmService.getData().subscribe(function (DmsmData) { return _this.data = DmsmData; });
    };
    AddDpmpmComponent.prototype.onSubmit = function (a) {
        var _this = this;
        this.dryproductname = a.dryproductname;
        this.dryDistributorName = a.dryDistributorName;
        this.dryConsumption = a.dryConsumption;
        this.dryQuantity = a.dryQuantity;
        this.dryReorderLevel = a.dryReorderLevel;
        this.dryRemark = a.dryRemark;
        console.log(a);
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.post('/api/add-Dry-Process-Material-Product-Master', a, options)
            .subscribe(function (data) {
            alert('Registered Successfully');
            _this.router.navigate(['/main-masters']);
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    AddDpmpmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-dpmpm',
            template: __webpack_require__(/*! ./add-dpmpm.component.html */ "./src/app/Main_masters/DPMPM/add-dpmpm/add-dpmpm.component.html"),
            styles: [__webpack_require__(/*! ./add-dpmpm.component.css */ "./src/app/Main_masters/DPMPM/add-dpmpm/add-dpmpm.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            src_app_Services_dmsm_service__WEBPACK_IMPORTED_MODULE_4__["DMSMService"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"]])
    ], AddDpmpmComponent);
    return AddDpmpmComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/DPMPM/edit-dpmpm/edit-dpmpm.component.css":
/*!************************************************************************!*\
  !*** ./src/app/Main_masters/DPMPM/edit-dpmpm/edit-dpmpm.component.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table,tr,th,td\n{\n    border:1px solid #fff;\n}\n#export-button\n{\n    padding-left:20px;\n    padding-right:20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0RQTVBNL2VkaXQtZHBtcG0vZWRpdC1kcG1wbS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztJQUVJLHFCQUFxQjtBQUN6QjtBQUNBOztJQUVJLGlCQUFpQjtJQUNqQixrQkFBa0I7QUFDdEIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvRFBNUE0vZWRpdC1kcG1wbS9lZGl0LWRwbXBtLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZSx0cix0aCx0ZFxue1xuICAgIGJvcmRlcjoxcHggc29saWQgI2ZmZjtcbn1cbiNleHBvcnQtYnV0dG9uXG57XG4gICAgcGFkZGluZy1sZWZ0OjIwcHg7XG4gICAgcGFkZGluZy1yaWdodDoyMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/DPMPM/edit-dpmpm/edit-dpmpm.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/Main_masters/DPMPM/edit-dpmpm/edit-dpmpm.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <br>\n<div class=\"container\" style=\"text-align: -webkit-center;\">\n    <div *ngIf=\"update\" >\n      <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.ChemicalForm>      \n      <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.Consumption>\n      <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.Quantity>\n      <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.ReorderLevel>\n      <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.Remark>\n      <button (click)='saveUpdate(datatobeedited)' type=\"button\" class=\"btn btn-sm btn-info\" style=\"margin-top: -5px;font-size: 13.9px;\">save</button>\n    </div>\n<div class=\"table-container\">\n  <div>\n    <div class=\"card\">\n      <div class=\"card-body\">\n        <table role=\"table\" style=\"width:100%;margin: 0;padding: 0\">\n          <thead role=\"rowgroup\">\n            <tr>\n              <th colspan=\"12\" style=\"background-color:#5E1D55;color:white;\">\n                <h3>Edit Chemical</h3>\n              </th>\n            </tr>\n            <tr class=\"text-color\"><th role=\"columnheader\">New Product Name</th>\n              <th role=\"columnheader\">Agent/Distributor Name</th>\n              <th role=\"columnheader\">Manufacturer Name</th>\n              <th role=\"columnheader\">Category</th>\n              <th role=\"columnheader\">Subcategory</th>\n              <th role=\"columnheader\">Chemical Form</th>\n              <th role=\"columnheader\">Average Per Day Consumption</th>\n              <th role=\"columnheader\">Minimum Order Quantity</th>\n              <th role=\"columnheader\">Reorder Level</th>\n              <th role=\"columnheader\">Remark</th>\n              <td role=\"columnheader\">Edit</td>\n            </tr>\n          </thead>\n          <tbody>\n              <tr *ngFor='let i of test; let in=index'  class=\"success\" style=\"text-align:-webkit-center;color:white\">\n                <td>{{i.newproductname}}</td>\n                 <td>{{i.DistributorName}}</td>\n                 <td>{{i.ManufacturerName}}</td>           \n                 <td>{{i.Subcategory}}</td>\n                 <td>{{i.ChemicalForm}}</td>\n                 <td>{{i.Consumption}}</td>\n                 <td>{{i.Quantity}}</td>\n                 <td>{{i.ReorderLevel}}</td>\n                 <td>{{i.Remark}}</td>\n                 <td><button (click)='editData(i,in)' type=\"button\" class=\"btn btn-large btn-info\">Edit</button></td>  \n              </tr>\n            </tbody>\n        </table>\n      </div>\n    </div>\n  </div>\n</div>  -->\n<br>\n<div class=\"container table-responsive-lg table-responsive-sm table-responsive-md\" style=\"text-align: -webkit-center;\">\n  <div *ngIf=\"update\">\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.dryproductname>\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.dryDistributorName>\n\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.dryConsumption>\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.dryQuantity>\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.dryReorderLevel>\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.dryRemark>\n    <button (click)='saveUpdate(datatobeedited)' type=\"button\" class=\"btn btn-sm btn-info\" style=\"margin-top: -5px;font-size: 13.9px;\">save</button>\n  </div>\n\n  <br>\n  <br>\n  <table class=\"table\">\n    <thead style=\"text-align:-webkit-center;color:cornsilk\">\n      <tr class=\"text-color\">\n        <th role=\"columnheader\">Dry Process Material Product Name</th>\n        <th role=\"columnheader\">Dry Process Material Supplier</th>\n        <th role=\"columnheader\">Average Per Day Consumption</th>\n        <th role=\"columnheader\">Minimum Order Quantity</th>\n        <th role=\"columnheader\">Reorder Level</th>\n        <th role=\"columnheader\">Remark</th>\n        <th role=\"columnheader\">Edit</th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor='let i of test1; let in=index' class=\"success\" style=\"text-align:-webkit-center;color:white\">\n        <td>{{i.dryproductname}}</td>\n        <td>{{i.dryDistributorName}}</td>\n\n        <td>{{i.dryConsumption}}</td>\n        <td>{{i.dryQuantity}}</td>\n        <td>{{i.dryReorderLevel}}</td>\n        <td>{{i.dryRemark}}</td>\n        <td><button (click)='editData(i,in)' type=\"button\" class=\"btn btn-large btn-info\">Edit</button></td>\n      </tr>\n    </tbody>\n  </table>\n  <button id=\"export-button\" type=\"button\" (click)=\"exportExcel()\" class=\"btn btn-info btn-lg \">Export</button>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/DPMPM/edit-dpmpm/edit-dpmpm.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/Main_masters/DPMPM/edit-dpmpm/edit-dpmpm.component.ts ***!
  \***********************************************************************/
/*! exports provided: EditDpmpmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditDpmpmComponent", function() { return EditDpmpmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
/* harmony import */ var src_app_Services_dpmpm_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/dpmpm.service */ "./src/app/Services/dpmpm.service.ts");
/* harmony import */ var angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular2-csv/Angular2-csv */ "./node_modules/angular2-csv/Angular2-csv.js");
/* harmony import */ var angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var EditDpmpmComponent = /** @class */ (function () {
    function EditDpmpmComponent(dpmpmService, _http, router, headerTitleService) {
        this.dpmpmService = dpmpmService;
        this._http = _http;
        this.router = router;
        this.headerTitleService = headerTitleService;
        this.update = false;
        this.test = [{}];
    }
    EditDpmpmComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dpmpmService.getData().subscribe(function (DpmpmData) { return _this.test1 = DpmpmData; });
        this.headerTitleService.setTitle('Dry Process Material Product Master');
    };
    EditDpmpmComponent.prototype.editData = function (i) {
        this.update = true;
        this.datatobeedited = i;
        console.log("obj is" + this.datatobeedited.dryproductname);
    };
    EditDpmpmComponent.prototype.saveUpdate = function (datasaved) {
        console.log("datatobesaved" + datasaved.dryproductname);
        console.log("datatobesaved" + '' + datasaved.dryproductname +
            '' + '' + datasaved.dryConsumption +
            '' + '' + datasaved.dryQuantity +
            '' + '' + datasaved.dryReorderLevel +
            '' + '' + datasaved.dryRemark);
        var _url = "http://localhost:3000/api/add-Dry-Process-Material-Product-Master" + "/" + datasaved._id;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["RequestOptions"]({ headers: headers });
        this._http.put(_url, datasaved, options)
            .subscribe(function (data) {
            alert('saved Successfully');
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    EditDpmpmComponent.prototype.exportExcel = function () {
        var head = [
            'dryproductname',
            'dryDistributorName',
            'dryConsumption',
            'dryQuantity',
            'dryReorderLevel',
            'dryRemark',
        ];
        new angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_4__["Angular2Csv"](this.test, 'Dry Process Material Product Master', { headers: (head) });
        alert("Please Give the Input Details");
        this.router.navigate(['/import-Dry-Process-Material-Product-Master']);
    };
    EditDpmpmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-dpmpm',
            template: __webpack_require__(/*! ./edit-dpmpm.component.html */ "./src/app/Main_masters/DPMPM/edit-dpmpm/edit-dpmpm.component.html"),
            styles: [__webpack_require__(/*! ./edit-dpmpm.component.css */ "./src/app/Main_masters/DPMPM/edit-dpmpm/edit-dpmpm.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_Services_dpmpm_service__WEBPACK_IMPORTED_MODULE_3__["DpmpmService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_2__["HeaderTitleService"]])
    ], EditDpmpmComponent);
    return EditDpmpmComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/DPMPM/import-dpmpm/import-dpmpm.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/Main_masters/DPMPM/import-dpmpm/import-dpmpm.component.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n.center-div\n{\n    position: absolute;\n    left: 50%;\n    top: 50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%);\n    \n    /*\n    This doesn't work\n    margin-left: -25%;\n    margin-top: -25%;\n    */\n    \n    width: 400px;\n    height: 300px;\n  \n    padding: 20px;  \n    background-color: rgba(0,0,0,0.5) !important;\n    color: white;\n    text-align: center;\n    box-shadow: 0 0 30px rgba(0, 0, 0, 0.2);\n}\n/* @media (max-width: 1000px) {\n    \n    .center-inner{left:25%;top:25%;position:absolute;width:50%;height:300px;background-color: rgba(0,0,0,0.5) !important;text-align:center;max-width:500px;max-height:500px;color:#ffffff}\n    } */\n.center{left:50%;top:25%;position:absolute;}\n.center-inner{width:400px;height:100%;margin-left:-250px;height:300px;background-color: rgba(0,0,0,0.5) !important;text-align:center;max-width:500px;max-height:500px;color:#ffffff;padding: 20px;border-radius: 20px;}\n@media (max-width: 428px) {\n    .center-inner{width:300px;height:100%;margin-left:-150px;height:300px;background-color: rgba(0,0,0,0.5) !important;text-align:center;max-width:500px;max-height:500px;color:#ffffff;padding: 20px;border-radius: 20px;}\n    .center{left:50%;top:25%;position:absolute;}\n\n    }\ninput[type=\"file\"] {\n        display: none;\n    }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0RQTVBNL2ltcG9ydC1kcG1wbS9pbXBvcnQtZHBtcG0uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0E7O0lBRUksa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxRQUFRO0lBQ1Isd0NBQWdDO1lBQWhDLGdDQUFnQzs7SUFFaEM7Ozs7S0FJQzs7SUFFRCxZQUFZO0lBQ1osYUFBYTs7SUFFYixhQUFhO0lBQ2IsNENBQTRDO0lBQzVDLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsdUNBQXVDO0FBQzNDO0FBQ0E7OztPQUdPO0FBRUgsUUFBUSxRQUFRLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDO0FBQzNDLGNBQWMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsNENBQTRDLENBQUMsaUJBQWlCLENBQUMsZUFBZSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUM7QUFFdE47SUFDQSxjQUFjLFdBQVcsQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLDRDQUE0QyxDQUFDLGlCQUFpQixDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDO0lBQ3ROLFFBQVEsUUFBUSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQzs7SUFFM0M7QUFDQTtRQUNJLGFBQWE7SUFDakIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvRFBNUE0vaW1wb3J0LWRwbXBtL2ltcG9ydC1kcG1wbS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG4uY2VudGVyLWRpdlxue1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiA1MCU7XG4gICAgdG9wOiA1MCU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gICAgXG4gICAgLypcbiAgICBUaGlzIGRvZXNuJ3Qgd29ya1xuICAgIG1hcmdpbi1sZWZ0OiAtMjUlO1xuICAgIG1hcmdpbi10b3A6IC0yNSU7XG4gICAgKi9cbiAgICBcbiAgICB3aWR0aDogNDAwcHg7XG4gICAgaGVpZ2h0OiAzMDBweDtcbiAgXG4gICAgcGFkZGluZzogMjBweDsgIFxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgYm94LXNoYWRvdzogMCAwIDMwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xufVxuLyogQG1lZGlhIChtYXgtd2lkdGg6IDEwMDBweCkge1xuICAgIFxuICAgIC5jZW50ZXItaW5uZXJ7bGVmdDoyNSU7dG9wOjI1JTtwb3NpdGlvbjphYnNvbHV0ZTt3aWR0aDo1MCU7aGVpZ2h0OjMwMHB4O2JhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O3RleHQtYWxpZ246Y2VudGVyO21heC13aWR0aDo1MDBweDttYXgtaGVpZ2h0OjUwMHB4O2NvbG9yOiNmZmZmZmZ9XG4gICAgfSAqL1xuICAgIFxuICAgIC5jZW50ZXJ7bGVmdDo1MCU7dG9wOjI1JTtwb3NpdGlvbjphYnNvbHV0ZTt9XG4gICAgLmNlbnRlci1pbm5lcnt3aWR0aDo0MDBweDtoZWlnaHQ6MTAwJTttYXJnaW4tbGVmdDotMjUwcHg7aGVpZ2h0OjMwMHB4O2JhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O3RleHQtYWxpZ246Y2VudGVyO21heC13aWR0aDo1MDBweDttYXgtaGVpZ2h0OjUwMHB4O2NvbG9yOiNmZmZmZmY7cGFkZGluZzogMjBweDtib3JkZXItcmFkaXVzOiAyMHB4O31cbiAgXG4gICAgQG1lZGlhIChtYXgtd2lkdGg6IDQyOHB4KSB7XG4gICAgLmNlbnRlci1pbm5lcnt3aWR0aDozMDBweDtoZWlnaHQ6MTAwJTttYXJnaW4tbGVmdDotMTUwcHg7aGVpZ2h0OjMwMHB4O2JhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O3RleHQtYWxpZ246Y2VudGVyO21heC13aWR0aDo1MDBweDttYXgtaGVpZ2h0OjUwMHB4O2NvbG9yOiNmZmZmZmY7cGFkZGluZzogMjBweDtib3JkZXItcmFkaXVzOiAyMHB4O31cbiAgICAuY2VudGVye2xlZnQ6NTAlO3RvcDoyNSU7cG9zaXRpb246YWJzb2x1dGU7fVxuXG4gICAgfVxuICAgIGlucHV0W3R5cGU9XCJmaWxlXCJdIHtcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICB9Il19 */"

/***/ }),

/***/ "./src/app/Main_masters/DPMPM/import-dpmpm/import-dpmpm.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/Main_masters/DPMPM/import-dpmpm/import-dpmpm.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"center\">\n  <div class=\"center-inner\">\n    <form id=\"login\">\n      <h4>Import File</h4>\n      <hr>\n      <p>* Only CSV File can be import. </p>\n\n      <input type=\"file\" name=\"files\" class=\"form-control\" #uploads (change)=\"onChange(uploads.files)\" multiple value=\"process\"\n        #file />\n      <div class=\"input-group mb-3\">\n        <div class=\"input-group-prepend\">\n          <button class=\"btn btn-outline-primary\" (click)=\"file.click()\" type=\"button\">Browser</button>\n        </div>\n        <input type=\"text\" value=\"{{fileName}}\" class=\"form-control form-control-lg\" aria-label=\"Default\"\n          aria-describedby=\"inputGroup-sizing-default\">\n      </div>\n      <br>\n      <div class=\"form-group\" style=\"text-align:center\">\n        <button style=\"padding: 0;\n                          border: none;\n                          background: none;\"\n          type=\"button\" (click)=\"onUpload()\">\n\n          <i class=\"fas fa-check-circle\" style=\"font-size:70px;color: yellow;\"></i>\n\n        </button>\n      </div>\n\n    </form>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/DPMPM/import-dpmpm/import-dpmpm.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/Main_masters/DPMPM/import-dpmpm/import-dpmpm.component.ts ***!
  \***************************************************************************/
/*! exports provided: ImportDpmpmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImportDpmpmComponent", function() { return ImportDpmpmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var papaparse__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! papaparse */ "./node_modules/papaparse/papaparse.min.js");
/* harmony import */ var papaparse__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(papaparse__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ImportDpmpmComponent = /** @class */ (function () {
    function ImportDpmpmComponent(headerTitleService, http, router) {
        this.headerTitleService = headerTitleService;
        this.http = http;
        this.router = router;
    }
    ImportDpmpmComponent.prototype.ngOnInit = function () {
        this.headerTitleService.setTitle('Chemical Product Master');
    };
    ImportDpmpmComponent.prototype.onChange = function (files) {
        var _this = this;
        if (files[0]) {
            console.log(files[0]);
            papaparse__WEBPACK_IMPORTED_MODULE_1__["parse"](files[0], {
                header: true,
                skipEmptyLines: true,
                complete: function (result, file) {
                    console.log(result);
                    _this.dataList = result.data;
                    _this.fileName = file.name;
                    console.log(_this.fileName);
                    console.log(_this.dataList);
                    var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["Headers"]();
                    headers.append('Content-Type', 'application/json');
                    var options = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["RequestOptions"]({ headers: headers });
                    _this.http.post('api/dpmpmFileUpload', _this.dataList, options)
                        .subscribe(function (data) {
                        console.log("added successfully");
                    }, function (error) {
                        console.log(JSON.stringify(error.json()));
                    });
                }
            });
        }
    };
    ImportDpmpmComponent.prototype.onUpload = function () {
        this.router.navigate(['/edit-Dry-Process-Material-Product-Master']);
    };
    ImportDpmpmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-import-dpmpm',
            template: __webpack_require__(/*! ./import-dpmpm.component.html */ "./src/app/Main_masters/DPMPM/import-dpmpm/import-dpmpm.component.html"),
            styles: [__webpack_require__(/*! ./import-dpmpm.component.css */ "./src/app/Main_masters/DPMPM/import-dpmpm/import-dpmpm.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_2__["HeaderTitleService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_4__["Http"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ImportDpmpmComponent);
    return ImportDpmpmComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/FQM/add-fqm/add-fqm.component.css":
/*!****************************************************************!*\
  !*** ./src/app/Main_masters/FQM/add-fqm/add-fqm.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n@import url('https://fonts.googleapis.com/css?family=Numans');\n/* Made with love by Mutiullah Samim*/\nhtml,body{\n\nbackground-size: cover;\nbackground-repeat: no-repeat;\nheight: 100%;\nfont-family: 'Numans', sans-serif;\n}\nlabel\n{\n    color: #fff;\n}\n.container{\nheight: 100%;\nbackground-size: cover;\nwidth:100%;\nbackground-repeat: no-repeat;\n/* align-content: center; */\n}\n.card{\nheight: 300px;\nmargin-top:200px;\nmargin-bottom:20px;\nwidth: 450px;\nbackground-color: rgba(0,0,0,0.5) !important;\n}\n.social_icon span{\nfont-size: 60px;\nmargin-left: 10px;\ncolor: #FFC312;\n}\n.social_icon span:hover{\ncolor: white;\ncursor: pointer;\n}\n.card-header h3{\ncolor: white;\n}\n.social_icon{\nposition: absolute;\nright: 20px;\ntop: -45px;\n}\n.input-group-prepend span{\nwidth: 50px;\nbackground-color: #FFC312;\ncolor: black;\nborder:0 !important;\n}\ninput:focus{\noutline: 0 0 0 0  !important;\nbox-shadow: 0 0 0 0 !important;\n\n}\n.remember{\ncolor: white;\n}\n.remember input\n{\nwidth: 20px;\nheight: 20px;\nmargin-left: 15px;\nmargin-right: 5px;\n}\n.login_btn{\ncolor: black;\nbackground-color: #FFC312;\nwidth: 100px;\n}\n.login_btn:hover{\ncolor: black;\nbackground-color: white;\n}\n.links{\ncolor: white;\n}\n.links a{\nmargin-left: 4px;\n}\noption\n{\n    margin-bottom: 10px!important;\n\n}\n/* *************************************************************************\n**********************  Media query for Mobile screen  */\n@media only screen and (max-width : 768px) {\n  .card{\n      \n    margin-top:auto;\n    margin-bottom:auto;\n      }\n\n}\n@media only screen and (max-width : 480px) {\n    .card{\n        height: 330px;\n        margin-top:20px;\n        margin-bottom:20px;\n\n        }\n\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0ZRTS9hZGQtZnFtL2FkZC1mcW0uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLDZEQUE2RDtBQUY3RCxxQ0FBcUM7QUFJckM7O0FBRUEsc0JBQXNCO0FBQ3RCLDRCQUE0QjtBQUM1QixZQUFZO0FBQ1osaUNBQWlDO0FBQ2pDO0FBQ0E7O0lBRUksV0FBVztBQUNmO0FBQ0E7QUFDQSxZQUFZO0FBQ1osc0JBQXNCO0FBQ3RCLFVBQVU7QUFDViw0QkFBNEI7QUFDNUIsMkJBQTJCO0FBQzNCO0FBRUE7QUFDQSxhQUFhO0FBQ2IsZ0JBQWdCO0FBQ2hCLGtCQUFrQjtBQUNsQixZQUFZO0FBQ1osNENBQTRDO0FBQzVDO0FBRUE7QUFDQSxlQUFlO0FBQ2YsaUJBQWlCO0FBQ2pCLGNBQWM7QUFDZDtBQUVBO0FBQ0EsWUFBWTtBQUNaLGVBQWU7QUFDZjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxrQkFBa0I7QUFDbEIsV0FBVztBQUNYLFVBQVU7QUFDVjtBQUVBO0FBQ0EsV0FBVztBQUNYLHlCQUF5QjtBQUN6QixZQUFZO0FBQ1osbUJBQW1CO0FBQ25CO0FBRUE7QUFDQSw0QkFBNEI7QUFDNUIsOEJBQThCOztBQUU5QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7O0FBRUEsV0FBVztBQUNYLFlBQVk7QUFDWixpQkFBaUI7QUFDakIsaUJBQWlCO0FBQ2pCO0FBRUE7QUFDQSxZQUFZO0FBQ1oseUJBQXlCO0FBQ3pCLFlBQVk7QUFDWjtBQUVBO0FBQ0EsWUFBWTtBQUNaLHVCQUF1QjtBQUN2QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQTs7SUFFSSw2QkFBNkI7O0FBRWpDO0FBSUE7d0RBQ3dEO0FBQ3hEO0VBQ0U7O0lBRUUsZUFBZTtJQUNmLGtCQUFrQjtNQUNoQjs7QUFFTjtBQUVBO0lBQ0k7UUFDSSxhQUFhO1FBQ2IsZUFBZTtRQUNmLGtCQUFrQjs7UUFFbEI7O0FBRVIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvRlFNL2FkZC1mcW0vYWRkLWZxbS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogTWFkZSB3aXRoIGxvdmUgYnkgTXV0aXVsbGFoIFNhbWltKi9cblxuQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1OdW1hbnMnKTtcblxuaHRtbCxib2R5e1xuXG5iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbmhlaWdodDogMTAwJTtcbmZvbnQtZmFtaWx5OiAnTnVtYW5zJywgc2Fucy1zZXJpZjtcbn1cbmxhYmVsXG57XG4gICAgY29sb3I6ICNmZmY7XG59XG4uY29udGFpbmVye1xuaGVpZ2h0OiAxMDAlO1xuYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbndpZHRoOjEwMCU7XG5iYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuLyogYWxpZ24tY29udGVudDogY2VudGVyOyAqL1xufVxuXG4uY2FyZHtcbmhlaWdodDogMzAwcHg7XG5tYXJnaW4tdG9wOjIwMHB4O1xubWFyZ2luLWJvdHRvbToyMHB4O1xud2lkdGg6IDQ1MHB4O1xuYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLDAsMCwwLjUpICFpbXBvcnRhbnQ7XG59XG5cbi5zb2NpYWxfaWNvbiBzcGFue1xuZm9udC1zaXplOiA2MHB4O1xubWFyZ2luLWxlZnQ6IDEwcHg7XG5jb2xvcjogI0ZGQzMxMjtcbn1cblxuLnNvY2lhbF9pY29uIHNwYW46aG92ZXJ7XG5jb2xvcjogd2hpdGU7XG5jdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5jYXJkLWhlYWRlciBoM3tcbmNvbG9yOiB3aGl0ZTtcbn1cblxuLnNvY2lhbF9pY29ue1xucG9zaXRpb246IGFic29sdXRlO1xucmlnaHQ6IDIwcHg7XG50b3A6IC00NXB4O1xufVxuXG4uaW5wdXQtZ3JvdXAtcHJlcGVuZCBzcGFue1xud2lkdGg6IDUwcHg7XG5iYWNrZ3JvdW5kLWNvbG9yOiAjRkZDMzEyO1xuY29sb3I6IGJsYWNrO1xuYm9yZGVyOjAgIWltcG9ydGFudDtcbn1cblxuaW5wdXQ6Zm9jdXN7XG5vdXRsaW5lOiAwIDAgMCAwICAhaW1wb3J0YW50O1xuYm94LXNoYWRvdzogMCAwIDAgMCAhaW1wb3J0YW50O1xuXG59XG5cbi5yZW1lbWJlcntcbmNvbG9yOiB3aGl0ZTtcbn1cblxuLnJlbWVtYmVyIGlucHV0XG57XG53aWR0aDogMjBweDtcbmhlaWdodDogMjBweDtcbm1hcmdpbi1sZWZ0OiAxNXB4O1xubWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5sb2dpbl9idG57XG5jb2xvcjogYmxhY2s7XG5iYWNrZ3JvdW5kLWNvbG9yOiAjRkZDMzEyO1xud2lkdGg6IDEwMHB4O1xufVxuXG4ubG9naW5fYnRuOmhvdmVye1xuY29sb3I6IGJsYWNrO1xuYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG5cbi5saW5rc3tcbmNvbG9yOiB3aGl0ZTtcbn1cblxuLmxpbmtzIGF7XG5tYXJnaW4tbGVmdDogNHB4O1xufVxub3B0aW9uXG57XG4gICAgbWFyZ2luLWJvdHRvbTogMTBweCFpbXBvcnRhbnQ7XG5cbn1cblxuXG5cbi8qICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbioqKioqKioqKioqKioqKioqKioqKiogIE1lZGlhIHF1ZXJ5IGZvciBNb2JpbGUgc2NyZWVuICAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoIDogNzY4cHgpIHtcbiAgLmNhcmR7XG4gICAgICBcbiAgICBtYXJnaW4tdG9wOmF1dG87XG4gICAgbWFyZ2luLWJvdHRvbTphdXRvO1xuICAgICAgfVxuXG59XG5cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aCA6IDQ4MHB4KSB7XG4gICAgLmNhcmR7XG4gICAgICAgIGhlaWdodDogMzMwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6MjBweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbToyMHB4O1xuXG4gICAgICAgIH1cblxufSJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/FQM/add-fqm/add-fqm.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/Main_masters/FQM/add-fqm/add-fqm.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"d-flex justify-content-center h-100\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        <h3 style=\"text-align:center\"> Add Financial Quality Defect Master</h3>\n      </div>\n      <div class=\"card-body\">\n        <form class=\"form-login\" role=\"form\" #formRef='ngForm' (ngSubmit)='onSubmit(formRef.value)'>\n          <div class=\"form-group row\">\n            <label for=\"gartype\" class=\"col-sm-3 col-form-label\">Add</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"gartype\" name=\"DpmName\" #seldepRef='ngModel' required ngModel>\n            </div>\n          </div>\n          <br>\n          <br>\n          <div class=\"form-group\" style=\"text-align:center\">\n            <button style=\"padding: 0;\n                border: none;\n                background: none;\" type=\"submit\">\n              <!-- <i class=\"far fa-check-circle\"> -->\n              <i class=\"fas fa-check-circle\" style=\"font-size:60px;color: yellow;\"></i>\n            </button>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/FQM/add-fqm/add-fqm.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/Main_masters/FQM/add-fqm/add-fqm.component.ts ***!
  \***************************************************************/
/*! exports provided: AddFqmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddFqmComponent", function() { return AddFqmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddFqmComponent = /** @class */ (function () {
    function AddFqmComponent(router, _http, headerTitleService) {
        this.router = router;
        this._http = _http;
        this.headerTitleService = headerTitleService;
    }
    AddFqmComponent.prototype.ngOnInit = function () {
        this.headerTitleService.setTitle('Financial Quality Master');
    };
    AddFqmComponent.prototype.onSubmit = function (a) {
        var _this = this;
        this.DpmName = a.DpmName,
            console.log(a);
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.post('api/add-financial', a, options)
            .subscribe(function (data) {
            alert('Added Successfully');
            _this.router.navigate(['/main-masters']);
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    AddFqmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-fqm',
            template: __webpack_require__(/*! ./add-fqm.component.html */ "./src/app/Main_masters/FQM/add-fqm/add-fqm.component.html"),
            styles: [__webpack_require__(/*! ./add-fqm.component.css */ "./src/app/Main_masters/FQM/add-fqm/add-fqm.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"]])
    ], AddFqmComponent);
    return AddFqmComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/FQM/edit-fqm/edit-fqm.component.css":
/*!******************************************************************!*\
  !*** ./src/app/Main_masters/FQM/edit-fqm/edit-fqm.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table,tr,th,td\n{\n    border:1px solid #fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0ZRTS9lZGl0LWZxbS9lZGl0LWZxbS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztJQUVJLHFCQUFxQjtBQUN6QiIsImZpbGUiOiJzcmMvYXBwL01haW5fbWFzdGVycy9GUU0vZWRpdC1mcW0vZWRpdC1mcW0uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlLHRyLHRoLHRkXG57XG4gICAgYm9yZGVyOjFweCBzb2xpZCAjZmZmO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/FQM/edit-fqm/edit-fqm.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/Main_masters/FQM/edit-fqm/edit-fqm.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\n<div class=\"container\" style=\"text-align: -webkit-center;\">\n  <div *ngIf=\"update\">\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.DpmName>\n    <button (click)='saveUpdate(datatobeedited)' type=\"button\" class=\"btn btn-sm btn-info\" style=\"margin-top: -5px;font-size: 13.9px;\">save</button>\n  </div>\n  <br>\n  <br>\n  <table class=\"table\">\n    <thead style=\"text-align: -webkit-center;color: cornsilk;\">\n      <tr class=\"text-color\">\n        <th>Financial Quality Defect</th>\n        <th>Edit</th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor='let i of test; let in=index' class=\"success\" style=\"text-align: -webkit-center;color: white;\">\n        <td>{{i.DpmName}}</td>\n        <td><button (click)='editData(i,in)' type=\"button\" class=\"btn btn-large btn-info\">Edit</button></td>\n      </tr>\n    </tbody>\n  </table>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/FQM/edit-fqm/edit-fqm.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/Main_masters/FQM/edit-fqm/edit-fqm.component.ts ***!
  \*****************************************************************/
/*! exports provided: EditFqmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditFqmComponent", function() { return EditFqmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_fqm_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Services/fqm.service */ "./src/app/Services/fqm.service.ts");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditFqmComponent = /** @class */ (function () {
    function EditFqmComponent(FqService, _http, headerTitleService) {
        this.FqService = FqService;
        this._http = _http;
        this.headerTitleService = headerTitleService;
        this.update = false;
    }
    EditFqmComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.FqService.getData().subscribe(function (DpmData) { return _this.test = DpmData; });
        this.headerTitleService.setTitle('Financial Quality Master');
    };
    EditFqmComponent.prototype.editData = function (i) {
        this.update = true;
        this.datatobeedited = i;
        console.log("obj is" + this.datatobeedited.DpmName);
    };
    EditFqmComponent.prototype.saveUpdate = function (datasaved) {
        console.log("datatobesaved" + datasaved.DpmName);
        console.log("datatobesaved" + '' + datasaved.DpmName);
        var _url = "http://localhost:3000/api/add-supplier-master" + "/" + datasaved._id;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["RequestOptions"]({ headers: headers });
        this._http.put(_url, datasaved, options)
            .subscribe(function (data) {
            alert('saved Successfully');
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    EditFqmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-fqm',
            template: __webpack_require__(/*! ./edit-fqm.component.html */ "./src/app/Main_masters/FQM/edit-fqm/edit-fqm.component.html"),
            styles: [__webpack_require__(/*! ./edit-fqm.component.css */ "./src/app/Main_masters/FQM/edit-fqm/edit-fqm.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_Services_fqm_service__WEBPACK_IMPORTED_MODULE_2__["FQMService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"]])
    ], EditFqmComponent);
    return EditFqmComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/GIM/gim-add/gim-add.component.css":
/*!****************************************************************!*\
  !*** ./src/app/Main_masters/GIM/gim-add/gim-add.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n@import url('https://fonts.googleapis.com/css?family=Numans');\n/* Made with love by Mutiullah Samim*/\nhtml,body{\n\nbackground-size: cover;\nbackground-repeat: no-repeat;\nheight: 100%;\nfont-family: 'Numans', sans-serif;\n}\nlabel\n{\n    color: #fff;\n}\n.container{\nheight: 100%;\nbackground-size: cover;\nwidth:100%;\nbackground-repeat: no-repeat;\n/* align-content: center; */\n}\n.card{\nheight: 300px;\nmargin-top:200px;\nmargin-bottom:20px;\nwidth: 450px;\nbackground-color: rgba(0,0,0,0.5) !important;\n}\n.social_icon span{\nfont-size: 60px;\nmargin-left: 10px;\ncolor: #FFC312;\n}\n.social_icon span:hover{\ncolor: white;\ncursor: pointer;\n}\n.card-header h3{\ncolor: white;\n}\n.social_icon{\nposition: absolute;\nright: 20px;\ntop: -45px;\n}\n.input-group-prepend span{\nwidth: 50px;\nbackground-color: #FFC312;\ncolor: black;\nborder:0 !important;\n}\ninput:focus{\noutline: 0 0 0 0  !important;\nbox-shadow: 0 0 0 0 !important;\n\n}\n.remember{\ncolor: white;\n}\n.remember input\n{\nwidth: 20px;\nheight: 20px;\nmargin-left: 15px;\nmargin-right: 5px;\n}\n.login_btn{\ncolor: black;\nbackground-color: #FFC312;\nwidth: 100px;\n}\n.login_btn:hover{\ncolor: black;\nbackground-color: white;\n}\n.links{\ncolor: white;\n}\n.links a{\nmargin-left: 4px;\n}\noption\n{\n    margin-bottom: 10px!important;\n\n}\n/* *************************************************************************\n**********************  Media query for Mobile screen  */\n@media only screen and (max-width : 768px) {\n  .card{\n      \n    margin-top:auto;\n    margin-bottom:auto;\n      }\n\n}\n@media only screen and (max-width : 480px) {\n    .card{\n        height: 300px;\n        margin-top:20px;\n        margin-bottom:20px;\n\n        }\n\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0dJTS9naW0tYWRkL2dpbS1hZGQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLDZEQUE2RDtBQUY3RCxxQ0FBcUM7QUFJckM7O0FBRUEsc0JBQXNCO0FBQ3RCLDRCQUE0QjtBQUM1QixZQUFZO0FBQ1osaUNBQWlDO0FBQ2pDO0FBQ0E7O0lBRUksV0FBVztBQUNmO0FBQ0E7QUFDQSxZQUFZO0FBQ1osc0JBQXNCO0FBQ3RCLFVBQVU7QUFDViw0QkFBNEI7QUFDNUIsMkJBQTJCO0FBQzNCO0FBRUE7QUFDQSxhQUFhO0FBQ2IsZ0JBQWdCO0FBQ2hCLGtCQUFrQjtBQUNsQixZQUFZO0FBQ1osNENBQTRDO0FBQzVDO0FBRUE7QUFDQSxlQUFlO0FBQ2YsaUJBQWlCO0FBQ2pCLGNBQWM7QUFDZDtBQUVBO0FBQ0EsWUFBWTtBQUNaLGVBQWU7QUFDZjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxrQkFBa0I7QUFDbEIsV0FBVztBQUNYLFVBQVU7QUFDVjtBQUVBO0FBQ0EsV0FBVztBQUNYLHlCQUF5QjtBQUN6QixZQUFZO0FBQ1osbUJBQW1CO0FBQ25CO0FBRUE7QUFDQSw0QkFBNEI7QUFDNUIsOEJBQThCOztBQUU5QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7O0FBRUEsV0FBVztBQUNYLFlBQVk7QUFDWixpQkFBaUI7QUFDakIsaUJBQWlCO0FBQ2pCO0FBRUE7QUFDQSxZQUFZO0FBQ1oseUJBQXlCO0FBQ3pCLFlBQVk7QUFDWjtBQUVBO0FBQ0EsWUFBWTtBQUNaLHVCQUF1QjtBQUN2QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQTs7SUFFSSw2QkFBNkI7O0FBRWpDO0FBSUE7d0RBQ3dEO0FBQ3hEO0VBQ0U7O0lBRUUsZUFBZTtJQUNmLGtCQUFrQjtNQUNoQjs7QUFFTjtBQUVBO0lBQ0k7UUFDSSxhQUFhO1FBQ2IsZUFBZTtRQUNmLGtCQUFrQjs7UUFFbEI7O0FBRVIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvR0lNL2dpbS1hZGQvZ2ltLWFkZC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogTWFkZSB3aXRoIGxvdmUgYnkgTXV0aXVsbGFoIFNhbWltKi9cblxuQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1OdW1hbnMnKTtcblxuaHRtbCxib2R5e1xuXG5iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbmhlaWdodDogMTAwJTtcbmZvbnQtZmFtaWx5OiAnTnVtYW5zJywgc2Fucy1zZXJpZjtcbn1cbmxhYmVsXG57XG4gICAgY29sb3I6ICNmZmY7XG59XG4uY29udGFpbmVye1xuaGVpZ2h0OiAxMDAlO1xuYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbndpZHRoOjEwMCU7XG5iYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuLyogYWxpZ24tY29udGVudDogY2VudGVyOyAqL1xufVxuXG4uY2FyZHtcbmhlaWdodDogMzAwcHg7XG5tYXJnaW4tdG9wOjIwMHB4O1xubWFyZ2luLWJvdHRvbToyMHB4O1xud2lkdGg6IDQ1MHB4O1xuYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLDAsMCwwLjUpICFpbXBvcnRhbnQ7XG59XG5cbi5zb2NpYWxfaWNvbiBzcGFue1xuZm9udC1zaXplOiA2MHB4O1xubWFyZ2luLWxlZnQ6IDEwcHg7XG5jb2xvcjogI0ZGQzMxMjtcbn1cblxuLnNvY2lhbF9pY29uIHNwYW46aG92ZXJ7XG5jb2xvcjogd2hpdGU7XG5jdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5jYXJkLWhlYWRlciBoM3tcbmNvbG9yOiB3aGl0ZTtcbn1cblxuLnNvY2lhbF9pY29ue1xucG9zaXRpb246IGFic29sdXRlO1xucmlnaHQ6IDIwcHg7XG50b3A6IC00NXB4O1xufVxuXG4uaW5wdXQtZ3JvdXAtcHJlcGVuZCBzcGFue1xud2lkdGg6IDUwcHg7XG5iYWNrZ3JvdW5kLWNvbG9yOiAjRkZDMzEyO1xuY29sb3I6IGJsYWNrO1xuYm9yZGVyOjAgIWltcG9ydGFudDtcbn1cblxuaW5wdXQ6Zm9jdXN7XG5vdXRsaW5lOiAwIDAgMCAwICAhaW1wb3J0YW50O1xuYm94LXNoYWRvdzogMCAwIDAgMCAhaW1wb3J0YW50O1xuXG59XG5cbi5yZW1lbWJlcntcbmNvbG9yOiB3aGl0ZTtcbn1cblxuLnJlbWVtYmVyIGlucHV0XG57XG53aWR0aDogMjBweDtcbmhlaWdodDogMjBweDtcbm1hcmdpbi1sZWZ0OiAxNXB4O1xubWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5sb2dpbl9idG57XG5jb2xvcjogYmxhY2s7XG5iYWNrZ3JvdW5kLWNvbG9yOiAjRkZDMzEyO1xud2lkdGg6IDEwMHB4O1xufVxuXG4ubG9naW5fYnRuOmhvdmVye1xuY29sb3I6IGJsYWNrO1xuYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG5cbi5saW5rc3tcbmNvbG9yOiB3aGl0ZTtcbn1cblxuLmxpbmtzIGF7XG5tYXJnaW4tbGVmdDogNHB4O1xufVxub3B0aW9uXG57XG4gICAgbWFyZ2luLWJvdHRvbTogMTBweCFpbXBvcnRhbnQ7XG5cbn1cblxuXG5cbi8qICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbioqKioqKioqKioqKioqKioqKioqKiogIE1lZGlhIHF1ZXJ5IGZvciBNb2JpbGUgc2NyZWVuICAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoIDogNzY4cHgpIHtcbiAgLmNhcmR7XG4gICAgICBcbiAgICBtYXJnaW4tdG9wOmF1dG87XG4gICAgbWFyZ2luLWJvdHRvbTphdXRvO1xuICAgICAgfVxuXG59XG5cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aCA6IDQ4MHB4KSB7XG4gICAgLmNhcmR7XG4gICAgICAgIGhlaWdodDogMzAwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6MjBweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbToyMHB4O1xuXG4gICAgICAgIH1cblxufSJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/GIM/gim-add/gim-add.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/Main_masters/GIM/gim-add/gim-add.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"d-flex justify-content-center h-100\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        <h3 style=\"text-align:center\">Add Garment Item</h3>\n      </div>\n      <div class=\"card-body\">\n        <form class=\"form-login\"  #formRef='ngForm' (ngSubmit)='onSubmit(formRef.value)'>\n          <div class=\"form-group row\">\n            <label for=\"gartype\" class=\"col-sm-5 col-form-label\">Garment Type</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"gartype\" name=\"AddGarment\" #selectRef='ngModel' required ngModel>\n            </div>\n          </div>\n          <br>\n          <br>\n          <div class=\"form-group\" style=\"text-align:center\">\n            <button style=\"padding: 0;\n                border: none;\n                background: none;\" type=\"submit\">\n              <!-- <i class=\"far fa-check-circle\"> -->\n              <i class=\"fas fa-check-circle\" style=\"font-size:60px;color: yellow;\"></i>\n            </button>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/GIM/gim-add/gim-add.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/Main_masters/GIM/gim-add/gim-add.component.ts ***!
  \***************************************************************/
/*! exports provided: GimAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GimAddComponent", function() { return GimAddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GimAddComponent = /** @class */ (function () {
    function GimAddComponent(router, _http, headerTitleService) {
        this.router = router;
        this._http = _http;
        this.headerTitleService = headerTitleService;
    }
    GimAddComponent.prototype.ngOnInit = function () {
        this.headerTitleService.setTitle('Garment Item Master');
    };
    GimAddComponent.prototype.onSubmit = function (a) {
        var _this = this;
        this.AddGarment = a.AddGarment,
            console.log(a);
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.post('/api/add-garment-item', a, options)
            .subscribe(function (data) {
            alert('added Successfully');
            _this.router.navigate(['/main-masters']);
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    GimAddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-gim-add',
            template: __webpack_require__(/*! ./gim-add.component.html */ "./src/app/Main_masters/GIM/gim-add/gim-add.component.html"),
            styles: [__webpack_require__(/*! ./gim-add.component.css */ "./src/app/Main_masters/GIM/gim-add/gim-add.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"]])
    ], GimAddComponent);
    return GimAddComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/GIM/gim-list/gim-list.component.css":
/*!******************************************************************!*\
  !*** ./src/app/Main_masters/GIM/gim-list/gim-list.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table,tr,th,td\n{\n    border:1px solid #fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0dJTS9naW0tbGlzdC9naW0tbGlzdC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztJQUVJLHFCQUFxQjtBQUN6QiIsImZpbGUiOiJzcmMvYXBwL01haW5fbWFzdGVycy9HSU0vZ2ltLWxpc3QvZ2ltLWxpc3QuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlLHRyLHRoLHRkXG57XG4gICAgYm9yZGVyOjFweCBzb2xpZCAjZmZmO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/GIM/gim-list/gim-list.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/Main_masters/GIM/gim-list/gim-list.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\n<div class=\"container\" style=\"text-align: -webkit-center;\">\n    <div *ngIf=\"update\" >\n      <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.AddGarment >\n      <button (click)='saveUpdate(datatobeedited)' type=\"button\" class=\"btn btn-sm btn-info\" style=\"margin-top: -5px;font-size: 13.9px;\">save</button>\n    </div>\n       \n  <br>\n  <br>\n    <table class=\"table\">\n      <thead style=\"text-align:-webkit-center;color: cornsilk;\">\n        <tr class=\"text-color\">\n          <th>Garment Type</th>\n          <th>Edit</th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr *ngFor='let i of test; let in=index'class=\"success\"style=\"text-align:-webkit-center; color:white;\">\n          <td>{{i.AddGarment}}</td>\n           <td><button (click)='editData(i,in)' type=\"button\" class=\"btn btn-large btn-info\">Edit</button></td>  \n        </tr>\n      </tbody>\n    </table>\n  </div>\n   \n  \n  "

/***/ }),

/***/ "./src/app/Main_masters/GIM/gim-list/gim-list.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/Main_masters/GIM/gim-list/gim-list.component.ts ***!
  \*****************************************************************/
/*! exports provided: GimListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GimListComponent", function() { return GimListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Services_gim_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../Services/gim.service */ "./src/app/Services/gim.service.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GimListComponent = /** @class */ (function () {
    function GimListComponent(GimService, _http, headerTitleService) {
        this.GimService = GimService;
        this._http = _http;
        this.headerTitleService = headerTitleService;
        this.update = false;
    }
    GimListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.GimService.getData().subscribe(function (testData) { return _this.test = testData; });
        this.headerTitleService.setTitle('Garment Item Master');
    };
    GimListComponent.prototype.editData = function (i) {
        this.update = true;
        this.datatobeedited = i;
        console.log("obj is" + this.datatobeedited.AddGarment);
    };
    GimListComponent.prototype.saveUpdate = function (datasaved) {
        console.log("datatobesaved" + datasaved.AddGarment);
        console.log("datatobesaved" + '' + datasaved.AddGarment);
        var _url = "http://localhost:3000/api/add-garment-item" + "/" + datasaved._id;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.put(_url, datasaved, options)
            .subscribe(function (data) {
            alert('saved Successfully');
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    GimListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-gim-list',
            template: __webpack_require__(/*! ./gim-list.component.html */ "./src/app/Main_masters/GIM/gim-list/gim-list.component.html"),
            styles: [__webpack_require__(/*! ./gim-list.component.css */ "./src/app/Main_masters/GIM/gim-list/gim-list.component.css")]
        }),
        __metadata("design:paramtypes", [_Services_gim_service__WEBPACK_IMPORTED_MODULE_1__["GimService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"]])
    ], GimListComponent);
    return GimListComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/GMM/add-gmm/add-gmm.component.css":
/*!****************************************************************!*\
  !*** ./src/app/Main_masters/GMM/add-gmm/add-gmm.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n@import url('https://fonts.googleapis.com/css?family=Numans');\n/* Made with love by Mutiullah Samim*/\nhtml,body{\n\nbackground-size: cover;\nbackground-repeat: no-repeat;\nheight: 100%;\nfont-family: 'Numans', sans-serif;\n}\nlabel\n{\n    color: #fff;\n}\n.container{\nheight: 100%;\nbackground-size: cover;\nwidth:100%;\nbackground-repeat: no-repeat;\n/* align-content: center; */\n}\n.card{\nheight: 700px;\nmargin-top:20px;\nmargin-bottom:20px;\nwidth: 450px;\nbackground-color: rgba(0,0,0,0.5) !important;\n}\n.social_icon span{\nfont-size: 60px;\nmargin-left: 10px;\ncolor: #FFC312;\n}\n.social_icon span:hover{\ncolor: white;\ncursor: pointer;\n}\n.card-header h3{\ncolor: white;\n}\n.social_icon{\nposition: absolute;\nright: 20px;\ntop: -45px;\n}\n.input-group-prepend span{\nwidth: 50px;\nbackground-color: #FFC312;\ncolor: black;\nborder:0 !important;\n}\ninput:focus{\noutline: 0 0 0 0  !important;\nbox-shadow: 0 0 0 0 !important;\n\n}\n.remember{\ncolor: white;\n}\n.remember input\n{\nwidth: 20px;\nheight: 20px;\nmargin-left: 15px;\nmargin-right: 5px;\n}\n.login_btn{\ncolor: black;\nbackground-color: #FFC312;\nwidth: 100px;\n}\n.login_btn:hover{\ncolor: black;\nbackground-color: white;\n}\n.links{\ncolor: white;\n}\n.links a{\nmargin-left: 4px;\n}\noption\n{\n    margin-bottom: 10px!important;\n\n}\n/* *************************************************************************\n**********************  Media query for Mobile screen  */\n@media only screen and (max-width : 768px) {\n  .card{\n      \n    margin-top:auto;\n    margin-bottom:auto;\n      }\n\n}\n@media only screen and (max-width : 480px) {\n    .card{\n        height: 1030px;\n        margin-top:20px;\n        margin-bottom:20px;\n\n        }\n\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0dNTS9hZGQtZ21tL2FkZC1nbW0uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLDZEQUE2RDtBQUY3RCxxQ0FBcUM7QUFJckM7O0FBRUEsc0JBQXNCO0FBQ3RCLDRCQUE0QjtBQUM1QixZQUFZO0FBQ1osaUNBQWlDO0FBQ2pDO0FBQ0E7O0lBRUksV0FBVztBQUNmO0FBQ0E7QUFDQSxZQUFZO0FBQ1osc0JBQXNCO0FBQ3RCLFVBQVU7QUFDViw0QkFBNEI7QUFDNUIsMkJBQTJCO0FBQzNCO0FBRUE7QUFDQSxhQUFhO0FBQ2IsZUFBZTtBQUNmLGtCQUFrQjtBQUNsQixZQUFZO0FBQ1osNENBQTRDO0FBQzVDO0FBRUE7QUFDQSxlQUFlO0FBQ2YsaUJBQWlCO0FBQ2pCLGNBQWM7QUFDZDtBQUVBO0FBQ0EsWUFBWTtBQUNaLGVBQWU7QUFDZjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxrQkFBa0I7QUFDbEIsV0FBVztBQUNYLFVBQVU7QUFDVjtBQUVBO0FBQ0EsV0FBVztBQUNYLHlCQUF5QjtBQUN6QixZQUFZO0FBQ1osbUJBQW1CO0FBQ25CO0FBRUE7QUFDQSw0QkFBNEI7QUFDNUIsOEJBQThCOztBQUU5QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7O0FBRUEsV0FBVztBQUNYLFlBQVk7QUFDWixpQkFBaUI7QUFDakIsaUJBQWlCO0FBQ2pCO0FBRUE7QUFDQSxZQUFZO0FBQ1oseUJBQXlCO0FBQ3pCLFlBQVk7QUFDWjtBQUVBO0FBQ0EsWUFBWTtBQUNaLHVCQUF1QjtBQUN2QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQTs7SUFFSSw2QkFBNkI7O0FBRWpDO0FBSUE7d0RBQ3dEO0FBQ3hEO0VBQ0U7O0lBRUUsZUFBZTtJQUNmLGtCQUFrQjtNQUNoQjs7QUFFTjtBQUVBO0lBQ0k7UUFDSSxjQUFjO1FBQ2QsZUFBZTtRQUNmLGtCQUFrQjs7UUFFbEI7O0FBRVIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvR01NL2FkZC1nbW0vYWRkLWdtbS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogTWFkZSB3aXRoIGxvdmUgYnkgTXV0aXVsbGFoIFNhbWltKi9cblxuQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1OdW1hbnMnKTtcblxuaHRtbCxib2R5e1xuXG5iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbmhlaWdodDogMTAwJTtcbmZvbnQtZmFtaWx5OiAnTnVtYW5zJywgc2Fucy1zZXJpZjtcbn1cbmxhYmVsXG57XG4gICAgY29sb3I6ICNmZmY7XG59XG4uY29udGFpbmVye1xuaGVpZ2h0OiAxMDAlO1xuYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbndpZHRoOjEwMCU7XG5iYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuLyogYWxpZ24tY29udGVudDogY2VudGVyOyAqL1xufVxuXG4uY2FyZHtcbmhlaWdodDogNzAwcHg7XG5tYXJnaW4tdG9wOjIwcHg7XG5tYXJnaW4tYm90dG9tOjIwcHg7XG53aWR0aDogNDUwcHg7XG5iYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuNSkgIWltcG9ydGFudDtcbn1cblxuLnNvY2lhbF9pY29uIHNwYW57XG5mb250LXNpemU6IDYwcHg7XG5tYXJnaW4tbGVmdDogMTBweDtcbmNvbG9yOiAjRkZDMzEyO1xufVxuXG4uc29jaWFsX2ljb24gc3Bhbjpob3ZlcntcbmNvbG9yOiB3aGl0ZTtcbmN1cnNvcjogcG9pbnRlcjtcbn1cblxuLmNhcmQtaGVhZGVyIGgze1xuY29sb3I6IHdoaXRlO1xufVxuXG4uc29jaWFsX2ljb257XG5wb3NpdGlvbjogYWJzb2x1dGU7XG5yaWdodDogMjBweDtcbnRvcDogLTQ1cHg7XG59XG5cbi5pbnB1dC1ncm91cC1wcmVwZW5kIHNwYW57XG53aWR0aDogNTBweDtcbmJhY2tncm91bmQtY29sb3I6ICNGRkMzMTI7XG5jb2xvcjogYmxhY2s7XG5ib3JkZXI6MCAhaW1wb3J0YW50O1xufVxuXG5pbnB1dDpmb2N1c3tcbm91dGxpbmU6IDAgMCAwIDAgICFpbXBvcnRhbnQ7XG5ib3gtc2hhZG93OiAwIDAgMCAwICFpbXBvcnRhbnQ7XG5cbn1cblxuLnJlbWVtYmVye1xuY29sb3I6IHdoaXRlO1xufVxuXG4ucmVtZW1iZXIgaW5wdXRcbntcbndpZHRoOiAyMHB4O1xuaGVpZ2h0OiAyMHB4O1xubWFyZ2luLWxlZnQ6IDE1cHg7XG5tYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLmxvZ2luX2J0bntcbmNvbG9yOiBibGFjaztcbmJhY2tncm91bmQtY29sb3I6ICNGRkMzMTI7XG53aWR0aDogMTAwcHg7XG59XG5cbi5sb2dpbl9idG46aG92ZXJ7XG5jb2xvcjogYmxhY2s7XG5iYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cblxuLmxpbmtze1xuY29sb3I6IHdoaXRlO1xufVxuXG4ubGlua3MgYXtcbm1hcmdpbi1sZWZ0OiA0cHg7XG59XG5vcHRpb25cbntcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4IWltcG9ydGFudDtcblxufVxuXG5cblxuLyogKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuKioqKioqKioqKioqKioqKioqKioqKiAgTWVkaWEgcXVlcnkgZm9yIE1vYmlsZSBzY3JlZW4gICovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGggOiA3NjhweCkge1xuICAuY2FyZHtcbiAgICAgIFxuICAgIG1hcmdpbi10b3A6YXV0bztcbiAgICBtYXJnaW4tYm90dG9tOmF1dG87XG4gICAgICB9XG5cbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoIDogNDgwcHgpIHtcbiAgICAuY2FyZHtcbiAgICAgICAgaGVpZ2h0OiAxMDMwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6MjBweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbToyMHB4O1xuXG4gICAgICAgIH1cblxufSJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/GMM/add-gmm/add-gmm.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/Main_masters/GMM/add-gmm/add-gmm.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"d-flex justify-content-center h-100\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        <h3 style=\"text-align:center\">Add Garment Manufacturer</h3>\n      </div>\n      <div class=\"card-body\">\n        <form class=\"form-login\" #formRef='ngForm' (ngSubmit)='onSubmit(formRef.value)'>\n          <div class=\"form-group row\">\n            <label for=\"subDep\" class=\"col-sm-5 col-form-label\">Garment Manufacturer Name</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDep\" name=\"newGarManf\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepPassword\" class=\"col-sm-5 col-form-label\">Address</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepPassword\" name=\"address\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepPassword\" class=\"col-sm-5 col-form-label\">Pincode</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepPassword\" name=\"Pincode\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepcfPassword\" class=\"col-sm-5 col-form-label\">Contact Person</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepcfPassword\" name=\"Cperson\" #selectRef='ngModel'\n                required ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Mobile Number</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"mobile\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Alternate Mob No</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"altermobile\" #selectRef='ngModel'\n                required ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">E-Mail ID</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"Email\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Website</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"website\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Remark</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" name=\"remarks\" #selectRef='ngModel' required\n                ngModel>\n            </div>\n          </div>\n\n          <div class=\"form-group\" style=\"text-align:center\">\n            <button style=\"padding: 0;\n                border: none;\n                background: none;\" type=\"submit\">\n              <!-- <i class=\"far fa-check-circle\"> -->\n              <i class=\"fas fa-check-circle\" style=\"font-size:60px;color: yellow;\"></i>\n\n            </button>\n          </div>\n\n        </form>\n      </div>\n\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/GMM/add-gmm/add-gmm.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/Main_masters/GMM/add-gmm/add-gmm.component.ts ***!
  \***************************************************************/
/*! exports provided: AddGmmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddGmmComponent", function() { return AddGmmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddGmmComponent = /** @class */ (function () {
    function AddGmmComponent(router, _http, headerTitleService) {
        this.router = router;
        this._http = _http;
        this.headerTitleService = headerTitleService;
    }
    AddGmmComponent.prototype.ngOnInit = function () {
        this.headerTitleService.setTitle('Garment Manufacturer Master');
    };
    AddGmmComponent.prototype.onSubmit = function (a) {
        var _this = this;
        this.newGarManf = a.newGarManf;
        this.address = a.address;
        this.Pincode = a.Pincode;
        this.Cperson = a.Cperson;
        this.mobile = a.mobile;
        this.altermobile = a.altermobile;
        this.Email = a.Email;
        this.website = a.website;
        this.remarks = a.remarks;
        console.log(a);
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.post('api/add-garment-manufacturer-master', a, options)
            .subscribe(function (data) {
            alert('Added Successfully');
            _this.router.navigate(['/main-masters']);
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    AddGmmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-gmm',
            template: __webpack_require__(/*! ./add-gmm.component.html */ "./src/app/Main_masters/GMM/add-gmm/add-gmm.component.html"),
            styles: [__webpack_require__(/*! ./add-gmm.component.css */ "./src/app/Main_masters/GMM/add-gmm/add-gmm.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"]])
    ], AddGmmComponent);
    return AddGmmComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/GMM/import-gmm/import-gmm.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/Main_masters/GMM/import-gmm/import-gmm.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n.center-div\n{\n    position: absolute;\n    left: 50%;\n    top: 50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%);\n    \n    /*\n    This doesn't work\n    margin-left: -25%;\n    margin-top: -25%;\n    */\n    \n    width: 400px;\n    height: 300px;\n  \n    padding: 20px;  \n    background-color: rgba(0,0,0,0.5) !important;\n    color: white;\n    text-align: center;\n    box-shadow: 0 0 30px rgba(0, 0, 0, 0.2);\n}\n/* @media (max-width: 1000px) {\n    \n    .center-inner{left:25%;top:25%;position:absolute;width:50%;height:300px;background-color: rgba(0,0,0,0.5) !important;text-align:center;max-width:500px;max-height:500px;color:#ffffff}\n    } */\n.center{left:50%;top:25%;position:absolute;}\n.center-inner{width:400px;height:100%;margin-left:-250px;height:300px;background-color: rgba(0,0,0,0.5) !important;text-align:center;max-width:500px;max-height:500px;color:#ffffff;padding: 20px;border-radius: 20px;}\n@media (max-width: 428px) {\n    .center-inner{width:300px;height:100%;margin-left:-150px;height:300px;background-color: rgba(0,0,0,0.5) !important;text-align:center;max-width:500px;max-height:500px;color:#ffffff;padding: 20px;border-radius: 20px;}\n    .center{left:50%;top:25%;position:absolute;}\n\n    }\ninput[type=\"file\"] {\n        display: none;\n    }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0dNTS9pbXBvcnQtZ21tL2ltcG9ydC1nbW0uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0E7O0lBRUksa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxRQUFRO0lBQ1Isd0NBQWdDO1lBQWhDLGdDQUFnQzs7SUFFaEM7Ozs7S0FJQzs7SUFFRCxZQUFZO0lBQ1osYUFBYTs7SUFFYixhQUFhO0lBQ2IsNENBQTRDO0lBQzVDLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsdUNBQXVDO0FBQzNDO0FBQ0E7OztPQUdPO0FBRUgsUUFBUSxRQUFRLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDO0FBQzNDLGNBQWMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsNENBQTRDLENBQUMsaUJBQWlCLENBQUMsZUFBZSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUM7QUFFdE47SUFDQSxjQUFjLFdBQVcsQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLDRDQUE0QyxDQUFDLGlCQUFpQixDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDO0lBQ3ROLFFBQVEsUUFBUSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQzs7SUFFM0M7QUFFQTtRQUNJLGFBQWE7SUFDakIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvR01NL2ltcG9ydC1nbW0vaW1wb3J0LWdtbS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG4uY2VudGVyLWRpdlxue1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiA1MCU7XG4gICAgdG9wOiA1MCU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gICAgXG4gICAgLypcbiAgICBUaGlzIGRvZXNuJ3Qgd29ya1xuICAgIG1hcmdpbi1sZWZ0OiAtMjUlO1xuICAgIG1hcmdpbi10b3A6IC0yNSU7XG4gICAgKi9cbiAgICBcbiAgICB3aWR0aDogNDAwcHg7XG4gICAgaGVpZ2h0OiAzMDBweDtcbiAgXG4gICAgcGFkZGluZzogMjBweDsgIFxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgYm94LXNoYWRvdzogMCAwIDMwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xufVxuLyogQG1lZGlhIChtYXgtd2lkdGg6IDEwMDBweCkge1xuICAgIFxuICAgIC5jZW50ZXItaW5uZXJ7bGVmdDoyNSU7dG9wOjI1JTtwb3NpdGlvbjphYnNvbHV0ZTt3aWR0aDo1MCU7aGVpZ2h0OjMwMHB4O2JhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O3RleHQtYWxpZ246Y2VudGVyO21heC13aWR0aDo1MDBweDttYXgtaGVpZ2h0OjUwMHB4O2NvbG9yOiNmZmZmZmZ9XG4gICAgfSAqL1xuICAgIFxuICAgIC5jZW50ZXJ7bGVmdDo1MCU7dG9wOjI1JTtwb3NpdGlvbjphYnNvbHV0ZTt9XG4gICAgLmNlbnRlci1pbm5lcnt3aWR0aDo0MDBweDtoZWlnaHQ6MTAwJTttYXJnaW4tbGVmdDotMjUwcHg7aGVpZ2h0OjMwMHB4O2JhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O3RleHQtYWxpZ246Y2VudGVyO21heC13aWR0aDo1MDBweDttYXgtaGVpZ2h0OjUwMHB4O2NvbG9yOiNmZmZmZmY7cGFkZGluZzogMjBweDtib3JkZXItcmFkaXVzOiAyMHB4O31cbiAgXG4gICAgQG1lZGlhIChtYXgtd2lkdGg6IDQyOHB4KSB7XG4gICAgLmNlbnRlci1pbm5lcnt3aWR0aDozMDBweDtoZWlnaHQ6MTAwJTttYXJnaW4tbGVmdDotMTUwcHg7aGVpZ2h0OjMwMHB4O2JhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O3RleHQtYWxpZ246Y2VudGVyO21heC13aWR0aDo1MDBweDttYXgtaGVpZ2h0OjUwMHB4O2NvbG9yOiNmZmZmZmY7cGFkZGluZzogMjBweDtib3JkZXItcmFkaXVzOiAyMHB4O31cbiAgICAuY2VudGVye2xlZnQ6NTAlO3RvcDoyNSU7cG9zaXRpb246YWJzb2x1dGU7fVxuXG4gICAgfVxuXG4gICAgaW5wdXRbdHlwZT1cImZpbGVcIl0ge1xuICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/GMM/import-gmm/import-gmm.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/Main_masters/GMM/import-gmm/import-gmm.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"center\">\n  <div class=\"center-inner\">\n    <form id=\"login\">\n      <h4>Import File</h4>\n      <hr>\n      <p>* Only CSV File can be import. </p>\n      <input type=\"file\" name=\"files\" class=\"form-control\" #uploads (change)=\"onChange(uploads.files)\" multiple value=\"process\"\n        #file />\n      <div class=\"input-group mb-3\">\n        <div class=\"input-group-prepend\">\n          <button class=\"btn btn-outline-primary\" (click)=\"file.click()\" type=\"button\">Browser</button>\n        </div>\n        <input type=\"text\" value=\"{{fileName}}\" class=\"form-control form-control-lg\" aria-label=\"Default\"\n          aria-describedby=\"inputGroup-sizing-default\">\n      </div>\n      <br>\n      <div class=\"form-group\" style=\"text-align:center\">\n        <button style=\"padding: 0; border: none;background: none;\" type=\"button\" (click)=\"onUpload()\">\n          <i class=\"fas fa-check-circle\" style=\"font-size:70px;color: yellow;\"></i>\n        </button>\n      </div>\n    </form>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/GMM/import-gmm/import-gmm.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/Main_masters/GMM/import-gmm/import-gmm.component.ts ***!
  \*********************************************************************/
/*! exports provided: ImportGmmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImportGmmComponent", function() { return ImportGmmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
/* harmony import */ var papaparse__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! papaparse */ "./node_modules/papaparse/papaparse.min.js");
/* harmony import */ var papaparse__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(papaparse__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ImportGmmComponent = /** @class */ (function () {
    function ImportGmmComponent(headerTitleService, http, router) {
        this.headerTitleService = headerTitleService;
        this.http = http;
        this.router = router;
    }
    ImportGmmComponent.prototype.ngOnInit = function () {
        this.headerTitleService.setTitle('Garment Manufacturer Master');
    };
    // GMM api post call
    ImportGmmComponent.prototype.onChange = function (files) {
        var _this = this;
        if (files[0]) {
            console.log(files[0]);
            papaparse__WEBPACK_IMPORTED_MODULE_2__["parse"](files[0], {
                header: true,
                skipEmptyLines: true,
                complete: function (result, file) {
                    console.log(result);
                    _this.fileName = file.name;
                    _this.dataList = result.data;
                    console.log(_this.dataList);
                    var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["Headers"]();
                    headers.append('Content-Type', 'application/json');
                    var options = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["RequestOptions"]({ headers: headers });
                    _this.http.post('api/gmmFileUpload', _this.dataList, options)
                        .subscribe(function (data) {
                        console.log("added successfully");
                    }, function (error) {
                        console.log(JSON.stringify(error.json()));
                    });
                }
            });
        }
    };
    ImportGmmComponent.prototype.onUpload = function () {
        this.router.navigate(['/edit-garmment-manufacturer']);
    };
    ImportGmmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-import-gmm',
            template: __webpack_require__(/*! ./import-gmm.component.html */ "./src/app/Main_masters/GMM/import-gmm/import-gmm.component.html"),
            styles: [__webpack_require__(/*! ./import-gmm.component.css */ "./src/app/Main_masters/GMM/import-gmm/import-gmm.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_1__["HeaderTitleService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_4__["Http"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ImportGmmComponent);
    return ImportGmmComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/GMM/table-gmm/table-gmm.component.css":
/*!********************************************************************!*\
  !*** ./src/app/Main_masters/GMM/table-gmm/table-gmm.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table,tr,th,td\n{\n    border:1px solid #fff;\n    \n}\n/* .container\n{\n    margin:0 auto;\n    display: flex;\n    align-items: center;\n    justify-content: center;\n} */\n#export-button\n{\n    padding-left:20px;\n    padding-right:20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL0dNTS90YWJsZS1nbW0vdGFibGUtZ21tLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0lBRUkscUJBQXFCOztBQUV6QjtBQUNBOzs7Ozs7R0FNRztBQUNIOztJQUVJLGlCQUFpQjtJQUNqQixrQkFBa0I7QUFDdEIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvR01NL3RhYmxlLWdtbS90YWJsZS1nbW0uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlLHRyLHRoLHRkXG57XG4gICAgYm9yZGVyOjFweCBzb2xpZCAjZmZmO1xuICAgIFxufVxuLyogLmNvbnRhaW5lclxue1xuICAgIG1hcmdpbjowIGF1dG87XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufSAqL1xuI2V4cG9ydC1idXR0b25cbntcbiAgICBwYWRkaW5nLWxlZnQ6MjBweDtcbiAgICBwYWRkaW5nLXJpZ2h0OjIwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/Main_masters/GMM/table-gmm/table-gmm.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/Main_masters/GMM/table-gmm/table-gmm.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\n\n\n<div class=\"container table-responsive-lg table-responsive-sm table-responsive-md\" style=\"text-align: -webkit-center;\">\n\n  <div *ngIf=\"update\" style=\"margin:0 auto;text-align: center\">\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.newGarManf>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.address>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.Pincode>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.Cperson>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.mobile>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.altermobile>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.Email>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.website>\n    <input type=\"text\" name=\"name\" [(ngModel)]=dataedit.remarks>\n    <button (click)='saveUpdate(dataedit)' type=\"button\" class=\"btn btn-sm btn-info\" style=\"margin-top: -5px;font-size: 13.9px;\">save</button>\n  </div>\n  <br>\n  <br>\n  <table class=\"table\">\n    <thead style=\"text-align: -webkit-center;color: cornsilk;\">\n      <tr class=\"text-color\">\n        <th>Garment Master Name</th>\n        <th>Address</th>\n        <th>Pincode</th>\n        <th>Contact Person</th>\n        <th>Mobile No</th>\n        <th>Alt Mobile Mo</th>\n        <th>Email Id</th>\n        <th>Website</th>\n        <th>Remark</th>\n        <th>Edit</th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor='let i of data; let in=index' class=\"success\" style=\"color:white;text-align: -webkit-center;\">\n        <td role=\"cell\">{{i.newGarManf}}</td>\n        <td role=\"cell\">{{i.address}}</td>\n        <td role=\"cell\">{{i.Pincode}}</td>\n        <td role=\"cell\">{{i.Cperson}}</td>\n        <td role=\"cell\">{{i.mobile}}</td>\n        <td role=\"cell\">{{i.altermobile}}</td>\n        <td role=\"cell\">{{i.Email}}</td>\n        <td role=\"cell\">{{i.website}}</td>\n        <td role=\"cell\">{{i.remarks}}</td>\n        <td><button (click)='editData(i,in)' type=\"button\" class=\"btn btn-large btn-info\">Edit</button></td>\n      </tr>\n    </tbody>\n  </table>\n  <button id=\"export-button\" type=\"button\" (click)=\"exportExcel()\" class=\"btn btn-info btn-lg \">Export</button>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/GMM/table-gmm/table-gmm.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/Main_masters/GMM/table-gmm/table-gmm.component.ts ***!
  \*******************************************************************/
/*! exports provided: TableGmmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TableGmmComponent", function() { return TableGmmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Services_gmm_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../Services/gmm.service */ "./src/app/Services/gmm.service.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
/* harmony import */ var angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular2-csv/Angular2-csv */ "./node_modules/angular2-csv/Angular2-csv.js");
/* harmony import */ var angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_5__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TableGmmComponent = /** @class */ (function () {
    function TableGmmComponent(GmmService, _http, router, headerTitleService) {
        this.GmmService = GmmService;
        this._http = _http;
        this.router = router;
        this.headerTitleService = headerTitleService;
        this.update = false;
        this.test = [{}];
    }
    TableGmmComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.GmmService.getData().subscribe(function (GmmData) { return _this.data = GmmData; });
        this.headerTitleService.setTitle('Garment Manufacturer Master');
    };
    TableGmmComponent.prototype.editData = function (i) {
        this.update = true;
        this.dataedit = i;
        console.log("obj is" + this.dataedit.newGarManf);
    };
    TableGmmComponent.prototype.saveUpdate = function (datasave) {
        console.log("datasaved" + datasave.newGarManf);
        console.log("datasaved" + '' + datasave.newGarManf);
        var url = "http://localhost:3000/api/add-garment-manufacturer-master" + "/" + datasave._id;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.put(url, datasave, options)
            .subscribe(function (data) {
            alert('saved Successfully');
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    TableGmmComponent.prototype.exportExcel = function () {
        var head = [
            'newGarManf',
            'address',
            'Pincode',
            'Cperson',
            'mobile',
            'altermobile',
            'Email',
            'website',
            'remarks'
        ];
        new angular2_csv_Angular2_csv__WEBPACK_IMPORTED_MODULE_5__["Angular2Csv"](this.test, 'Garment-Manufacturer-Master', { headers: (head) });
        alert("Please Give the Input Details");
        this.router.navigate(['/garment-manufacturer-import']);
    };
    TableGmmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-table-gmm',
            template: __webpack_require__(/*! ./table-gmm.component.html */ "./src/app/Main_masters/GMM/table-gmm/table-gmm.component.html"),
            styles: [__webpack_require__(/*! ./table-gmm.component.css */ "./src/app/Main_masters/GMM/table-gmm/table-gmm.component.css")]
        }),
        __metadata("design:paramtypes", [_Services_gmm_service__WEBPACK_IMPORTED_MODULE_1__["GmmService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_4__["HeaderTitleService"]])
    ], TableGmmComponent);
    return TableGmmComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/TEM/add-tem/add-tem.component.css":
/*!****************************************************************!*\
  !*** ./src/app/Main_masters/TEM/add-tem/add-tem.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n@import url('https://fonts.googleapis.com/css?family=Numans');\n/* Made with love by Mutiullah Samim*/\nhtml,body{\n\nbackground-size: cover;\nbackground-repeat: no-repeat;\nheight: 100%;\nfont-family: 'Numans', sans-serif;\n}\nlabel\n{\n    color: #fff;\n}\n.container{\nheight: 100%;\nbackground-size: cover;\nwidth:100%;\nbackground-repeat: no-repeat;\n/* align-content: center; */\n}\n.card{\nheight: 300px;\nmargin-top:200px;\nmargin-bottom:20px;\nwidth: 450px;\nbackground-color: rgba(0,0,0,0.5) !important;\n}\n.social_icon span{\nfont-size: 60px;\nmargin-left: 10px;\ncolor: #FFC312;\n}\n.social_icon span:hover{\ncolor: white;\ncursor: pointer;\n}\n.card-header h3{\ncolor: white;\n}\n.social_icon{\nposition: absolute;\nright: 20px;\ntop: -45px;\n}\n.input-group-prepend span{\nwidth: 50px;\nbackground-color: #FFC312;\ncolor: black;\nborder:0 !important;\n}\ninput:focus{\noutline: 0 0 0 0  !important;\nbox-shadow: 0 0 0 0 !important;\n\n}\n.remember{\ncolor: white;\n}\n.remember input\n{\nwidth: 20px;\nheight: 20px;\nmargin-left: 15px;\nmargin-right: 5px;\n}\n.login_btn{\ncolor: black;\nbackground-color: #FFC312;\nwidth: 100px;\n}\n.login_btn:hover{\ncolor: black;\nbackground-color: white;\n}\n.links{\ncolor: white;\n}\n.links a{\nmargin-left: 4px;\n}\noption\n{\n    margin-bottom: 10px!important;\n\n}\n/* *************************************************************************\n**********************  Media query for Mobile screen  */\n@media only screen and (max-width : 768px) {\n  .card{\n      \n    margin-top:auto;\n    margin-bottom:auto;\n      }\n\n}\n@media only screen and (max-width : 480px) {\n    .card{\n        height: 330px;\n        margin-top:20px;\n        margin-bottom:20px;\n\n        }\n\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL1RFTS9hZGQtdGVtL2FkZC10ZW0uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLDZEQUE2RDtBQUY3RCxxQ0FBcUM7QUFJckM7O0FBRUEsc0JBQXNCO0FBQ3RCLDRCQUE0QjtBQUM1QixZQUFZO0FBQ1osaUNBQWlDO0FBQ2pDO0FBQ0E7O0lBRUksV0FBVztBQUNmO0FBQ0E7QUFDQSxZQUFZO0FBQ1osc0JBQXNCO0FBQ3RCLFVBQVU7QUFDViw0QkFBNEI7QUFDNUIsMkJBQTJCO0FBQzNCO0FBRUE7QUFDQSxhQUFhO0FBQ2IsZ0JBQWdCO0FBQ2hCLGtCQUFrQjtBQUNsQixZQUFZO0FBQ1osNENBQTRDO0FBQzVDO0FBRUE7QUFDQSxlQUFlO0FBQ2YsaUJBQWlCO0FBQ2pCLGNBQWM7QUFDZDtBQUVBO0FBQ0EsWUFBWTtBQUNaLGVBQWU7QUFDZjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxrQkFBa0I7QUFDbEIsV0FBVztBQUNYLFVBQVU7QUFDVjtBQUVBO0FBQ0EsV0FBVztBQUNYLHlCQUF5QjtBQUN6QixZQUFZO0FBQ1osbUJBQW1CO0FBQ25CO0FBRUE7QUFDQSw0QkFBNEI7QUFDNUIsOEJBQThCOztBQUU5QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7O0FBRUEsV0FBVztBQUNYLFlBQVk7QUFDWixpQkFBaUI7QUFDakIsaUJBQWlCO0FBQ2pCO0FBRUE7QUFDQSxZQUFZO0FBQ1oseUJBQXlCO0FBQ3pCLFlBQVk7QUFDWjtBQUVBO0FBQ0EsWUFBWTtBQUNaLHVCQUF1QjtBQUN2QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQTs7SUFFSSw2QkFBNkI7O0FBRWpDO0FBSUE7d0RBQ3dEO0FBQ3hEO0VBQ0U7O0lBRUUsZUFBZTtJQUNmLGtCQUFrQjtNQUNoQjs7QUFFTjtBQUVBO0lBQ0k7UUFDSSxhQUFhO1FBQ2IsZUFBZTtRQUNmLGtCQUFrQjs7UUFFbEI7O0FBRVIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvVEVNL2FkZC10ZW0vYWRkLXRlbS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogTWFkZSB3aXRoIGxvdmUgYnkgTXV0aXVsbGFoIFNhbWltKi9cblxuQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1OdW1hbnMnKTtcblxuaHRtbCxib2R5e1xuXG5iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbmhlaWdodDogMTAwJTtcbmZvbnQtZmFtaWx5OiAnTnVtYW5zJywgc2Fucy1zZXJpZjtcbn1cbmxhYmVsXG57XG4gICAgY29sb3I6ICNmZmY7XG59XG4uY29udGFpbmVye1xuaGVpZ2h0OiAxMDAlO1xuYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbndpZHRoOjEwMCU7XG5iYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuLyogYWxpZ24tY29udGVudDogY2VudGVyOyAqL1xufVxuXG4uY2FyZHtcbmhlaWdodDogMzAwcHg7XG5tYXJnaW4tdG9wOjIwMHB4O1xubWFyZ2luLWJvdHRvbToyMHB4O1xud2lkdGg6IDQ1MHB4O1xuYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLDAsMCwwLjUpICFpbXBvcnRhbnQ7XG59XG5cbi5zb2NpYWxfaWNvbiBzcGFue1xuZm9udC1zaXplOiA2MHB4O1xubWFyZ2luLWxlZnQ6IDEwcHg7XG5jb2xvcjogI0ZGQzMxMjtcbn1cblxuLnNvY2lhbF9pY29uIHNwYW46aG92ZXJ7XG5jb2xvcjogd2hpdGU7XG5jdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5jYXJkLWhlYWRlciBoM3tcbmNvbG9yOiB3aGl0ZTtcbn1cblxuLnNvY2lhbF9pY29ue1xucG9zaXRpb246IGFic29sdXRlO1xucmlnaHQ6IDIwcHg7XG50b3A6IC00NXB4O1xufVxuXG4uaW5wdXQtZ3JvdXAtcHJlcGVuZCBzcGFue1xud2lkdGg6IDUwcHg7XG5iYWNrZ3JvdW5kLWNvbG9yOiAjRkZDMzEyO1xuY29sb3I6IGJsYWNrO1xuYm9yZGVyOjAgIWltcG9ydGFudDtcbn1cblxuaW5wdXQ6Zm9jdXN7XG5vdXRsaW5lOiAwIDAgMCAwICAhaW1wb3J0YW50O1xuYm94LXNoYWRvdzogMCAwIDAgMCAhaW1wb3J0YW50O1xuXG59XG5cbi5yZW1lbWJlcntcbmNvbG9yOiB3aGl0ZTtcbn1cblxuLnJlbWVtYmVyIGlucHV0XG57XG53aWR0aDogMjBweDtcbmhlaWdodDogMjBweDtcbm1hcmdpbi1sZWZ0OiAxNXB4O1xubWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5sb2dpbl9idG57XG5jb2xvcjogYmxhY2s7XG5iYWNrZ3JvdW5kLWNvbG9yOiAjRkZDMzEyO1xud2lkdGg6IDEwMHB4O1xufVxuXG4ubG9naW5fYnRuOmhvdmVye1xuY29sb3I6IGJsYWNrO1xuYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG5cbi5saW5rc3tcbmNvbG9yOiB3aGl0ZTtcbn1cblxuLmxpbmtzIGF7XG5tYXJnaW4tbGVmdDogNHB4O1xufVxub3B0aW9uXG57XG4gICAgbWFyZ2luLWJvdHRvbTogMTBweCFpbXBvcnRhbnQ7XG5cbn1cblxuXG5cbi8qICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbioqKioqKioqKioqKioqKioqKioqKiogIE1lZGlhIHF1ZXJ5IGZvciBNb2JpbGUgc2NyZWVuICAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoIDogNzY4cHgpIHtcbiAgLmNhcmR7XG4gICAgICBcbiAgICBtYXJnaW4tdG9wOmF1dG87XG4gICAgbWFyZ2luLWJvdHRvbTphdXRvO1xuICAgICAgfVxuXG59XG5cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aCA6IDQ4MHB4KSB7XG4gICAgLmNhcmR7XG4gICAgICAgIGhlaWdodDogMzMwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6MjBweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbToyMHB4O1xuXG4gICAgICAgIH1cblxufSJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/TEM/add-tem/add-tem.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/Main_masters/TEM/add-tem/add-tem.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"d-flex justify-content-center h-100\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        <h3 style=\"text-align:center\"> Add Transit Error</h3>\n      </div>\n      <div class=\"card-body\">\n        <form class=\"form-login\" role=\"form\" #formRef='ngForm' (ngSubmit)='onSubmit(formRef.value)'>\n          <div class=\"form-group row\">\n            <label for=\"gartype\" class=\"col-sm-3 col-form-label\">Add</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"gartype\" name=\"DpmName\" #seldepRef='ngModel' required ngModel>\n            </div>\n          </div>\n          <br>\n          <br>\n          <div class=\"form-group\" style=\"text-align:center\">\n            <button style=\"padding: 0;\n                border: none;\n                background: none;\" type=\"submit\">\n              <!-- <i class=\"far fa-check-circle\"> -->\n              <i class=\"fas fa-check-circle\" style=\"font-size:60px;color: yellow;\"></i>\n            </button>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/TEM/add-tem/add-tem.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/Main_masters/TEM/add-tem/add-tem.component.ts ***!
  \***************************************************************/
/*! exports provided: AddTemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddTemComponent", function() { return AddTemComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddTemComponent = /** @class */ (function () {
    function AddTemComponent(router, _http, headerTitleService) {
        this.router = router;
        this._http = _http;
        this.headerTitleService = headerTitleService;
    }
    AddTemComponent.prototype.ngOnInit = function () {
        this.headerTitleService.setTitle('Transit Error Master');
    };
    AddTemComponent.prototype.onSubmit = function (a) {
        var _this = this;
        this.DpmName = a.DpmName,
            console.log(a);
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.post('api/add-transit', a, options)
            .subscribe(function (data) {
            alert('Added Successfully');
            _this.router.navigate(['/main-masters']);
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    AddTemComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-tem',
            template: __webpack_require__(/*! ./add-tem.component.html */ "./src/app/Main_masters/TEM/add-tem/add-tem.component.html"),
            styles: [__webpack_require__(/*! ./add-tem.component.css */ "./src/app/Main_masters/TEM/add-tem/add-tem.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"]])
    ], AddTemComponent);
    return AddTemComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/TEM/edit-tem/edit-tem.component.css":
/*!******************************************************************!*\
  !*** ./src/app/Main_masters/TEM/edit-tem/edit-tem.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table,tr,th,td\n{\n    border:1px solid #fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL1RFTS9lZGl0LXRlbS9lZGl0LXRlbS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztJQUVJLHFCQUFxQjtBQUN6QiIsImZpbGUiOiJzcmMvYXBwL01haW5fbWFzdGVycy9URU0vZWRpdC10ZW0vZWRpdC10ZW0uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlLHRyLHRoLHRkXG57XG4gICAgYm9yZGVyOjFweCBzb2xpZCAjZmZmO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/TEM/edit-tem/edit-tem.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/Main_masters/TEM/edit-tem/edit-tem.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\n<div class=\"container\" style=\"text-align: -webkit-center;\">\n  <div *ngIf=\"update\">\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.DpmName>\n    <button (click)='saveUpdate(datatobeedited)' type=\"button\" class=\"btn btn-sm btn-info\" style=\"margin-top: -5px;font-size: 13.9px;\">save</button>\n  </div>\n  <br>\n  <br>\n  <table class=\"table\">\n    <thead style=\"text-align: -webkit-center;color: cornsilk;\">\n      <tr class=\"text-color\">\n        <th>Transit Error</th>\n        <th>Edit</th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor='let i of test; let in=index' class=\"success\" style=\"text-align: -webkit-center;color: white;\">\n        <td>{{i.DpmName}}</td>\n        <td><button (click)='editData(i,in)' type=\"button\" class=\"btn btn-large btn-info\">Edit</button></td>\n      </tr>\n    </tbody>\n  </table>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/TEM/edit-tem/edit-tem.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/Main_masters/TEM/edit-tem/edit-tem.component.ts ***!
  \*****************************************************************/
/*! exports provided: EditTemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditTemComponent", function() { return EditTemComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_tem_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Services/tem.service */ "./src/app/Services/tem.service.ts");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditTemComponent = /** @class */ (function () {
    function EditTemComponent(TemService, _http, headerTitleService) {
        this.TemService = TemService;
        this._http = _http;
        this.headerTitleService = headerTitleService;
        this.update = false;
    }
    EditTemComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.TemService.getData().subscribe(function (DpmData) { return _this.test = DpmData; });
        this.headerTitleService.setTitle('Transit Error Master');
    };
    EditTemComponent.prototype.editData = function (i) {
        this.update = true;
        this.datatobeedited = i;
        console.log("obj is" + this.datatobeedited.DpmName);
    };
    EditTemComponent.prototype.saveUpdate = function (datasaved) {
        console.log("datatobesaved" + datasaved.DpmName);
        console.log("datatobesaved" + '' + datasaved.DpmName);
        var _url = "http://localhost:3000/api/add-transit" + "/" + datasaved._id;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["RequestOptions"]({ headers: headers });
        this._http.put(_url, datasaved, options)
            .subscribe(function (data) {
            alert('saved Successfully');
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    EditTemComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-tem',
            template: __webpack_require__(/*! ./edit-tem.component.html */ "./src/app/Main_masters/TEM/edit-tem/edit-tem.component.html"),
            styles: [__webpack_require__(/*! ./edit-tem.component.css */ "./src/app/Main_masters/TEM/edit-tem/edit-tem.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_Services_tem_service__WEBPACK_IMPORTED_MODULE_2__["TEMService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"]])
    ], EditTemComponent);
    return EditTemComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/UQDM/add-uqdm/add-uqdm.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/Main_masters/UQDM/add-uqdm/add-uqdm.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n@import url('https://fonts.googleapis.com/css?family=Numans');\n/* Made with love by Mutiullah Samim*/\nhtml,body{\n\nbackground-size: cover;\nbackground-repeat: no-repeat;\nheight: 100%;\nfont-family: 'Numans', sans-serif;\n}\nlabel\n{\n    color: #fff;\n}\n.container{\nheight: 100%;\nbackground-size: cover;\nwidth:100%;\nbackground-repeat: no-repeat;\n/* align-content: center; */\n}\n.card{\nheight: 300px;\nmargin-top:200px;\nmargin-bottom:20px;\nwidth: 450px;\nbackground-color: rgba(0,0,0,0.5) !important;\n}\n.social_icon span{\nfont-size: 60px;\nmargin-left: 10px;\ncolor: #FFC312;\n}\n.social_icon span:hover{\ncolor: white;\ncursor: pointer;\n}\n.card-header h3{\ncolor: white;\n}\n.social_icon{\nposition: absolute;\nright: 20px;\ntop: -45px;\n}\n.input-group-prepend span{\nwidth: 50px;\nbackground-color: #FFC312;\ncolor: black;\nborder:0 !important;\n}\ninput:focus{\noutline: 0 0 0 0  !important;\nbox-shadow: 0 0 0 0 !important;\n\n}\n.remember{\ncolor: white;\n}\n.remember input\n{\nwidth: 20px;\nheight: 20px;\nmargin-left: 15px;\nmargin-right: 5px;\n}\n.login_btn{\ncolor: black;\nbackground-color: #FFC312;\nwidth: 100px;\n}\n.login_btn:hover{\ncolor: black;\nbackground-color: white;\n}\n.links{\ncolor: white;\n}\n.links a{\nmargin-left: 4px;\n}\noption\n{\n    margin-bottom: 10px!important;\n\n}\n/* *************************************************************************\n**********************  Media query for Mobile screen  */\n@media only screen and (max-width : 768px) {\n  .card{\n      \n    margin-top:auto;\n    margin-bottom:auto;\n      }\n\n}\n@media only screen and (max-width : 480px) {\n    .card{\n        height:330px;\n        margin-top:20px;\n        margin-bottom:20px;\n\n        }\n\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL1VRRE0vYWRkLXVxZG0vYWRkLXVxZG0uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLDZEQUE2RDtBQUY3RCxxQ0FBcUM7QUFJckM7O0FBRUEsc0JBQXNCO0FBQ3RCLDRCQUE0QjtBQUM1QixZQUFZO0FBQ1osaUNBQWlDO0FBQ2pDO0FBQ0E7O0lBRUksV0FBVztBQUNmO0FBQ0E7QUFDQSxZQUFZO0FBQ1osc0JBQXNCO0FBQ3RCLFVBQVU7QUFDViw0QkFBNEI7QUFDNUIsMkJBQTJCO0FBQzNCO0FBRUE7QUFDQSxhQUFhO0FBQ2IsZ0JBQWdCO0FBQ2hCLGtCQUFrQjtBQUNsQixZQUFZO0FBQ1osNENBQTRDO0FBQzVDO0FBRUE7QUFDQSxlQUFlO0FBQ2YsaUJBQWlCO0FBQ2pCLGNBQWM7QUFDZDtBQUVBO0FBQ0EsWUFBWTtBQUNaLGVBQWU7QUFDZjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxrQkFBa0I7QUFDbEIsV0FBVztBQUNYLFVBQVU7QUFDVjtBQUVBO0FBQ0EsV0FBVztBQUNYLHlCQUF5QjtBQUN6QixZQUFZO0FBQ1osbUJBQW1CO0FBQ25CO0FBRUE7QUFDQSw0QkFBNEI7QUFDNUIsOEJBQThCOztBQUU5QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7O0FBRUEsV0FBVztBQUNYLFlBQVk7QUFDWixpQkFBaUI7QUFDakIsaUJBQWlCO0FBQ2pCO0FBRUE7QUFDQSxZQUFZO0FBQ1oseUJBQXlCO0FBQ3pCLFlBQVk7QUFDWjtBQUVBO0FBQ0EsWUFBWTtBQUNaLHVCQUF1QjtBQUN2QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQTs7SUFFSSw2QkFBNkI7O0FBRWpDO0FBSUE7d0RBQ3dEO0FBQ3hEO0VBQ0U7O0lBRUUsZUFBZTtJQUNmLGtCQUFrQjtNQUNoQjs7QUFFTjtBQUVBO0lBQ0k7UUFDSSxZQUFZO1FBQ1osZUFBZTtRQUNmLGtCQUFrQjs7UUFFbEI7O0FBRVIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvVVFETS9hZGQtdXFkbS9hZGQtdXFkbS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogTWFkZSB3aXRoIGxvdmUgYnkgTXV0aXVsbGFoIFNhbWltKi9cblxuQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1OdW1hbnMnKTtcblxuaHRtbCxib2R5e1xuXG5iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbmhlaWdodDogMTAwJTtcbmZvbnQtZmFtaWx5OiAnTnVtYW5zJywgc2Fucy1zZXJpZjtcbn1cbmxhYmVsXG57XG4gICAgY29sb3I6ICNmZmY7XG59XG4uY29udGFpbmVye1xuaGVpZ2h0OiAxMDAlO1xuYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbndpZHRoOjEwMCU7XG5iYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuLyogYWxpZ24tY29udGVudDogY2VudGVyOyAqL1xufVxuXG4uY2FyZHtcbmhlaWdodDogMzAwcHg7XG5tYXJnaW4tdG9wOjIwMHB4O1xubWFyZ2luLWJvdHRvbToyMHB4O1xud2lkdGg6IDQ1MHB4O1xuYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLDAsMCwwLjUpICFpbXBvcnRhbnQ7XG59XG5cbi5zb2NpYWxfaWNvbiBzcGFue1xuZm9udC1zaXplOiA2MHB4O1xubWFyZ2luLWxlZnQ6IDEwcHg7XG5jb2xvcjogI0ZGQzMxMjtcbn1cblxuLnNvY2lhbF9pY29uIHNwYW46aG92ZXJ7XG5jb2xvcjogd2hpdGU7XG5jdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5jYXJkLWhlYWRlciBoM3tcbmNvbG9yOiB3aGl0ZTtcbn1cblxuLnNvY2lhbF9pY29ue1xucG9zaXRpb246IGFic29sdXRlO1xucmlnaHQ6IDIwcHg7XG50b3A6IC00NXB4O1xufVxuXG4uaW5wdXQtZ3JvdXAtcHJlcGVuZCBzcGFue1xud2lkdGg6IDUwcHg7XG5iYWNrZ3JvdW5kLWNvbG9yOiAjRkZDMzEyO1xuY29sb3I6IGJsYWNrO1xuYm9yZGVyOjAgIWltcG9ydGFudDtcbn1cblxuaW5wdXQ6Zm9jdXN7XG5vdXRsaW5lOiAwIDAgMCAwICAhaW1wb3J0YW50O1xuYm94LXNoYWRvdzogMCAwIDAgMCAhaW1wb3J0YW50O1xuXG59XG5cbi5yZW1lbWJlcntcbmNvbG9yOiB3aGl0ZTtcbn1cblxuLnJlbWVtYmVyIGlucHV0XG57XG53aWR0aDogMjBweDtcbmhlaWdodDogMjBweDtcbm1hcmdpbi1sZWZ0OiAxNXB4O1xubWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5sb2dpbl9idG57XG5jb2xvcjogYmxhY2s7XG5iYWNrZ3JvdW5kLWNvbG9yOiAjRkZDMzEyO1xud2lkdGg6IDEwMHB4O1xufVxuXG4ubG9naW5fYnRuOmhvdmVye1xuY29sb3I6IGJsYWNrO1xuYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG5cbi5saW5rc3tcbmNvbG9yOiB3aGl0ZTtcbn1cblxuLmxpbmtzIGF7XG5tYXJnaW4tbGVmdDogNHB4O1xufVxub3B0aW9uXG57XG4gICAgbWFyZ2luLWJvdHRvbTogMTBweCFpbXBvcnRhbnQ7XG5cbn1cblxuXG5cbi8qICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbioqKioqKioqKioqKioqKioqKioqKiogIE1lZGlhIHF1ZXJ5IGZvciBNb2JpbGUgc2NyZWVuICAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoIDogNzY4cHgpIHtcbiAgLmNhcmR7XG4gICAgICBcbiAgICBtYXJnaW4tdG9wOmF1dG87XG4gICAgbWFyZ2luLWJvdHRvbTphdXRvO1xuICAgICAgfVxuXG59XG5cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aCA6IDQ4MHB4KSB7XG4gICAgLmNhcmR7XG4gICAgICAgIGhlaWdodDozMzBweDtcbiAgICAgICAgbWFyZ2luLXRvcDoyMHB4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOjIwcHg7XG5cbiAgICAgICAgfVxuXG59Il19 */"

/***/ }),

/***/ "./src/app/Main_masters/UQDM/add-uqdm/add-uqdm.component.html":
/*!********************************************************************!*\
  !*** ./src/app/Main_masters/UQDM/add-uqdm/add-uqdm.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"d-flex justify-content-center h-100\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        <h3 style=\"text-align:center\"> Add Unwash Quality Defect</h3>\n      </div>\n      <div class=\"card-body\">\n        <form class=\"form-login\" role=\"form\" #formRef='ngForm' (ngSubmit)='onSubmit(formRef.value)'>\n          <div class=\"form-group row\">\n            <label for=\"gartype\" class=\"col-sm-3 col-form-label\">Add</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"gartype\" name=\"DpmName\" #seldepRef='ngModel' required ngModel>\n            </div>\n          </div>\n          <br>\n          <br>\n          <div class=\"form-group\" style=\"text-align:center\">\n            <button style=\"padding: 0;\n                border: none;\n                background: none;\" type=\"submit\">\n              <!-- <i class=\"far fa-check-circle\"> -->\n              <i class=\"fas fa-check-circle\" style=\"font-size:60px;color: yellow;\"></i>\n            </button>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/UQDM/add-uqdm/add-uqdm.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/Main_masters/UQDM/add-uqdm/add-uqdm.component.ts ***!
  \******************************************************************/
/*! exports provided: AddUqdmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddUqdmComponent", function() { return AddUqdmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddUqdmComponent = /** @class */ (function () {
    function AddUqdmComponent(router, _http, headerTitleService) {
        this.router = router;
        this._http = _http;
        this.headerTitleService = headerTitleService;
    }
    AddUqdmComponent.prototype.ngOnInit = function () {
        this.headerTitleService.setTitle('Unwash Quality Defect Master');
    };
    AddUqdmComponent.prototype.onSubmit = function (a) {
        var _this = this;
        this.DpmName = a.DpmName,
            console.log(a);
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.post('api/add-unwash', a, options)
            .subscribe(function (data) {
            alert('Added Successfully');
            _this.router.navigate(['/main-masters']);
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    AddUqdmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-uqdm',
            template: __webpack_require__(/*! ./add-uqdm.component.html */ "./src/app/Main_masters/UQDM/add-uqdm/add-uqdm.component.html"),
            styles: [__webpack_require__(/*! ./add-uqdm.component.css */ "./src/app/Main_masters/UQDM/add-uqdm/add-uqdm.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"]])
    ], AddUqdmComponent);
    return AddUqdmComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/UQDM/edit-uqdm/edit-uqdm.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/Main_masters/UQDM/edit-uqdm/edit-uqdm.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table,tr,th,td\n{\n    border:1px solid #fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL1VRRE0vZWRpdC11cWRtL2VkaXQtdXFkbS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztJQUVJLHFCQUFxQjtBQUN6QiIsImZpbGUiOiJzcmMvYXBwL01haW5fbWFzdGVycy9VUURNL2VkaXQtdXFkbS9lZGl0LXVxZG0uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlLHRyLHRoLHRkXG57XG4gICAgYm9yZGVyOjFweCBzb2xpZCAjZmZmO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/UQDM/edit-uqdm/edit-uqdm.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/Main_masters/UQDM/edit-uqdm/edit-uqdm.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\n<div class=\"container\" style=\"text-align: -webkit-center;\">\n  <div *ngIf=\"update\">\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.DpmName>\n    <button (click)='saveUpdate(datatobeedited)' type=\"button\" class=\"btn btn-sm btn-info\" style=\"margin-top: -5px;font-size: 13.9px;\">save</button>\n  </div>\n  <br>\n  <br>\n  <table class=\"table\">\n    <thead style=\"text-align: -webkit-center;color: cornsilk;\">\n      <tr class=\"text-color\">\n        <th>Unwash Quality Defect</th>\n        <th>Edit</th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor='let i of test; let in=index' class=\"success\" style=\"text-align: -webkit-center;color: white;\">\n        <td>{{i.DpmName}}</td>\n        <td><button (click)='editData(i,in)' type=\"button\" class=\"btn btn-large btn-info\">Edit</button></td>\n      </tr>\n    </tbody>\n  </table>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/UQDM/edit-uqdm/edit-uqdm.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/Main_masters/UQDM/edit-uqdm/edit-uqdm.component.ts ***!
  \********************************************************************/
/*! exports provided: EditUqdmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditUqdmComponent", function() { return EditUqdmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_uqdm_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Services/uqdm.service */ "./src/app/Services/uqdm.service.ts");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditUqdmComponent = /** @class */ (function () {
    function EditUqdmComponent(UqdmService, _http, headerTitleService) {
        this.UqdmService = UqdmService;
        this._http = _http;
        this.headerTitleService = headerTitleService;
        this.update = false;
    }
    EditUqdmComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.UqdmService.getData().subscribe(function (DpmData) { return _this.test = DpmData; });
        this.headerTitleService.setTitle('Unwash Quality Defect Master');
    };
    EditUqdmComponent.prototype.editData = function (i) {
        this.update = true;
        this.datatobeedited = i;
        console.log("obj is" + this.datatobeedited.DpmName);
    };
    EditUqdmComponent.prototype.saveUpdate = function (datasaved) {
        console.log("datatobesaved" + datasaved.DpmName);
        console.log("datatobesaved" + '' + datasaved.DpmName);
        var _url = "http://localhost:3000/api/add-unwash" + "/" + datasaved._id;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["RequestOptions"]({ headers: headers });
        this._http.put(_url, datasaved, options)
            .subscribe(function (data) {
            alert('saved Successfully');
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    EditUqdmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-uqdm',
            template: __webpack_require__(/*! ./edit-uqdm.component.html */ "./src/app/Main_masters/UQDM/edit-uqdm/edit-uqdm.component.html"),
            styles: [__webpack_require__(/*! ./edit-uqdm.component.css */ "./src/app/Main_masters/UQDM/edit-uqdm/edit-uqdm.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_Services_uqdm_service__WEBPACK_IMPORTED_MODULE_2__["UQDMService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"]])
    ], EditUqdmComponent);
    return EditUqdmComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/WPM/wpm-assign/wpm-assign.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/Main_masters/WPM/wpm-assign/wpm-assign.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n@import url('https://fonts.googleapis.com/css?family=Numans');\n/* Made with love by Mutiullah Samim*/\n.container{\nheight: 100%;\nbackground-size: cover;\nwidth:100%;\nbackground-repeat: no-repeat;\nposition: absolute;\nleft: 50%;\ntop: 50%;\n-webkit-transform: translate(-50%, -50%);\n        transform: translate(-50%, -50%);\n/* align-content: center; */\n}\n.card{\nheight: 370px;\nmargin-top: auto;\nmargin-bottom: auto;\nwidth: 450px;\ncolor:#fff;\nborder-radius: 20px;\nbackground-color: rgba(0,0,0,0.5) !important;\n}\n.social_icon span{\nfont-size: 60px;\nmargin-left: 10px;\ncolor: #FFC312;\n}\n.social_icon span:hover{\ncolor: white;\ncursor: pointer;\n}\n.card-header h3{\ncolor: white;\n}\n.social_icon{\nposition: absolute;\nright: 20px;\ntop: -45px;\n}\n.input-group-prepend span{\nwidth: 50px;\nbackground-color: #FFC312;\ncolor: black;\nborder:0 !important;\n}\ninput:focus{\noutline: 0 0 0 0  !important;\nbox-shadow: 0 0 0 0 !important;\n\n}\n.remember{\ncolor: white;\n}\n.remember input\n{\nwidth: 20px;\nheight: 20px;\nmargin-left: 15px;\nmargin-right: 5px;\n}\n.login_btn{\ncolor: black;\nbackground-color: #FFC312;\nwidth: 100px;\n}\n.login_btn:hover{\ncolor: black;\nbackground-color: white;\n}\n.links{\ncolor: white;\n}\n.links a{\nmargin-left: 4px;\n}\n@media only screen and (max-width : 480px) {\n    .card{\n        height:400px;\n        margin-top:60px;\n        margin-bottom:20px;\n\n        }\n\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL1dQTS93cG0tYXNzaWduL3dwbS1hc3NpZ24uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLDZEQUE2RDtBQUY3RCxxQ0FBcUM7QUFLckM7QUFDQSxZQUFZO0FBQ1osc0JBQXNCO0FBQ3RCLFVBQVU7QUFDViw0QkFBNEI7QUFDNUIsa0JBQWtCO0FBQ2xCLFNBQVM7QUFDVCxRQUFRO0FBQ1Isd0NBQWdDO1FBQWhDLGdDQUFnQztBQUNoQywyQkFBMkI7QUFDM0I7QUFFQTtBQUNBLGFBQWE7QUFDYixnQkFBZ0I7QUFDaEIsbUJBQW1CO0FBQ25CLFlBQVk7QUFDWixVQUFVO0FBQ1YsbUJBQW1CO0FBQ25CLDRDQUE0QztBQUM1QztBQUVBO0FBQ0EsZUFBZTtBQUNmLGlCQUFpQjtBQUNqQixjQUFjO0FBQ2Q7QUFFQTtBQUNBLFlBQVk7QUFDWixlQUFlO0FBQ2Y7QUFFQTtBQUNBLFlBQVk7QUFDWjtBQUVBO0FBQ0Esa0JBQWtCO0FBQ2xCLFdBQVc7QUFDWCxVQUFVO0FBQ1Y7QUFFQTtBQUNBLFdBQVc7QUFDWCx5QkFBeUI7QUFDekIsWUFBWTtBQUNaLG1CQUFtQjtBQUNuQjtBQUVBO0FBQ0EsNEJBQTRCO0FBQzVCLDhCQUE4Qjs7QUFFOUI7QUFFQTtBQUNBLFlBQVk7QUFDWjtBQUVBOztBQUVBLFdBQVc7QUFDWCxZQUFZO0FBQ1osaUJBQWlCO0FBQ2pCLGlCQUFpQjtBQUNqQjtBQUVBO0FBQ0EsWUFBWTtBQUNaLHlCQUF5QjtBQUN6QixZQUFZO0FBQ1o7QUFFQTtBQUNBLFlBQVk7QUFDWix1QkFBdUI7QUFDdkI7QUFFQTtBQUNBLFlBQVk7QUFDWjtBQUVBO0FBQ0EsZ0JBQWdCO0FBQ2hCO0FBRUE7SUFDSTtRQUNJLFlBQVk7UUFDWixlQUFlO1FBQ2Ysa0JBQWtCOztRQUVsQjs7QUFFUiIsImZpbGUiOiJzcmMvYXBwL01haW5fbWFzdGVycy9XUE0vd3BtLWFzc2lnbi93cG0tYXNzaWduLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBNYWRlIHdpdGggbG92ZSBieSBNdXRpdWxsYWggU2FtaW0qL1xuXG5AaW1wb3J0IHVybCgnaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3M/ZmFtaWx5PU51bWFucycpO1xuXG5cbi5jb250YWluZXJ7XG5oZWlnaHQ6IDEwMCU7XG5iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xud2lkdGg6MTAwJTtcbmJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG5wb3NpdGlvbjogYWJzb2x1dGU7XG5sZWZ0OiA1MCU7XG50b3A6IDUwJTtcbnRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuLyogYWxpZ24tY29udGVudDogY2VudGVyOyAqL1xufVxuXG4uY2FyZHtcbmhlaWdodDogMzcwcHg7XG5tYXJnaW4tdG9wOiBhdXRvO1xubWFyZ2luLWJvdHRvbTogYXV0bztcbndpZHRoOiA0NTBweDtcbmNvbG9yOiNmZmY7XG5ib3JkZXItcmFkaXVzOiAyMHB4O1xuYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLDAsMCwwLjUpICFpbXBvcnRhbnQ7XG59XG5cbi5zb2NpYWxfaWNvbiBzcGFue1xuZm9udC1zaXplOiA2MHB4O1xubWFyZ2luLWxlZnQ6IDEwcHg7XG5jb2xvcjogI0ZGQzMxMjtcbn1cblxuLnNvY2lhbF9pY29uIHNwYW46aG92ZXJ7XG5jb2xvcjogd2hpdGU7XG5jdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5jYXJkLWhlYWRlciBoM3tcbmNvbG9yOiB3aGl0ZTtcbn1cblxuLnNvY2lhbF9pY29ue1xucG9zaXRpb246IGFic29sdXRlO1xucmlnaHQ6IDIwcHg7XG50b3A6IC00NXB4O1xufVxuXG4uaW5wdXQtZ3JvdXAtcHJlcGVuZCBzcGFue1xud2lkdGg6IDUwcHg7XG5iYWNrZ3JvdW5kLWNvbG9yOiAjRkZDMzEyO1xuY29sb3I6IGJsYWNrO1xuYm9yZGVyOjAgIWltcG9ydGFudDtcbn1cblxuaW5wdXQ6Zm9jdXN7XG5vdXRsaW5lOiAwIDAgMCAwICAhaW1wb3J0YW50O1xuYm94LXNoYWRvdzogMCAwIDAgMCAhaW1wb3J0YW50O1xuXG59XG5cbi5yZW1lbWJlcntcbmNvbG9yOiB3aGl0ZTtcbn1cblxuLnJlbWVtYmVyIGlucHV0XG57XG53aWR0aDogMjBweDtcbmhlaWdodDogMjBweDtcbm1hcmdpbi1sZWZ0OiAxNXB4O1xubWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5sb2dpbl9idG57XG5jb2xvcjogYmxhY2s7XG5iYWNrZ3JvdW5kLWNvbG9yOiAjRkZDMzEyO1xud2lkdGg6IDEwMHB4O1xufVxuXG4ubG9naW5fYnRuOmhvdmVye1xuY29sb3I6IGJsYWNrO1xuYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG5cbi5saW5rc3tcbmNvbG9yOiB3aGl0ZTtcbn1cblxuLmxpbmtzIGF7XG5tYXJnaW4tbGVmdDogNHB4O1xufVxuXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGggOiA0ODBweCkge1xuICAgIC5jYXJke1xuICAgICAgICBoZWlnaHQ6NDAwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6NjBweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbToyMHB4O1xuXG4gICAgICAgIH1cblxufVxuIl19 */"

/***/ }),

/***/ "./src/app/Main_masters/WPM/wpm-assign/wpm-assign.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/Main_masters/WPM/wpm-assign/wpm-assign.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"assining-form\">\n  <form class=\"assining-form\" role=\"form\" #formRef='ngForm' (ngSubmit)='onSubmit(formRef.value)'>\n    <div class=\"container\">\n      <div class=\"d-flex justify-content-center h-100\">\n        <div class=\"card\">\n          <div class=\"card-header\">\n            <h3 style=\"text-align:center\">Wash Assign Process</h3>\n          </div>\n          <div class=\"card-body\">\n\n            <div class=\"form-group row\">\n              <label for=\"seldep\" class=\"col-sm-5 col-form-label\">Wash Process</label>\n              <div class=\"col-sm-7\">\n                <select class=\"form-control\" id=\"seldep\" name=\"seldep\" #seldepRef='ngModel' required ngModel>\n                  <option selected>Select Department</option>\n                  <option>Washing</option>\n                  <option>Dyeing</option>\n\n                </select>\n              </div>\n            </div>\n            <br>\n            <div class=\"form-group row\">\n              <label for=\"subDep\" class=\"col-sm-5 col-form-label\">Sub-Process</label>\n              <div class=\"col-sm-7\">\n                <input type=\"text\" class=\"form-control\" id=\"subDep\" name=\"subDep1\" #suDep1Ref='ngModel' required\n                  ngModel>\n              </div>\n            </div>\n            <br>\n            <div class=\"form-group\" style=\"text-align:center\">\n              <button style=\"padding: 0;\n              border: none;\n              background: none;\" type=\"submit\">\n                <!-- <i class=\"far fa-check-circle\"> -->\n                <i class=\"fas fa-check-circle\" style=\"font-size:60px;color: yellow;\"></i>\n              </button>\n            </div>\n\n          </div>\n        </div>\n      </div>\n    </div>\n  </form>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/WPM/wpm-assign/wpm-assign.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/Main_masters/WPM/wpm-assign/wpm-assign.component.ts ***!
  \*********************************************************************/
/*! exports provided: WpmAssignComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WpmAssignComponent", function() { return WpmAssignComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var WpmAssignComponent = /** @class */ (function () {
    function WpmAssignComponent(router, _http, headerTitleService) {
        this.router = router;
        this._http = _http;
        this.headerTitleService = headerTitleService;
    }
    WpmAssignComponent.prototype.ngOnInit = function () {
        this.headerTitleService.setTitle('Wash Process Master');
    };
    WpmAssignComponent.prototype.onSubmit = function (a) {
        var _this = this;
        this.seldep = a.seldep,
            this.subDep1 = a.subDep1,
            //console.log(a.seldep + "   " + a.subDep1);
            console.log(a);
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.post('/api/assign-wash-process', a, options)
            .subscribe(function (data) {
            alert('assigned Successfully');
            _this.router.navigate(['/main-masters']);
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    WpmAssignComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-wpm-assign',
            template: __webpack_require__(/*! ./wpm-assign.component.html */ "./src/app/Main_masters/WPM/wpm-assign/wpm-assign.component.html"),
            styles: [__webpack_require__(/*! ./wpm-assign.component.css */ "./src/app/Main_masters/WPM/wpm-assign/wpm-assign.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"]])
    ], WpmAssignComponent);
    return WpmAssignComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/WPM/wpm-edit/wpm-edit.component.css":
/*!******************************************************************!*\
  !*** ./src/app/Main_masters/WPM/wpm-edit/wpm-edit.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table,tr,th,td\n{\n    border:1px solid #fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL1dQTS93cG0tZWRpdC93cG0tZWRpdC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztJQUVJLHFCQUFxQjtBQUN6QiIsImZpbGUiOiJzcmMvYXBwL01haW5fbWFzdGVycy9XUE0vd3BtLWVkaXQvd3BtLWVkaXQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlLHRyLHRoLHRkXG57XG4gICAgYm9yZGVyOjFweCBzb2xpZCAjZmZmO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/WPM/wpm-edit/wpm-edit.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/Main_masters/WPM/wpm-edit/wpm-edit.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\n<div class=\"container\" style=\"text-align: -webkit-center;\">\n  <div *ngIf=\"update\">\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.seldep>\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.subDep1>\n    <button (click)='saveUpdate(datatobeedited)' type=\"button\" class=\"btn btn-sm btn-info\" style=\"margin-top: -5px;font-size: 13.9px;\">save</button>\n  </div>\n  <br>\n  <br>\n  <table class=\"table\">\n    <thead style=\"text-align: -webkit-center; color:cornsilk\">\n      <tr class=\"text-color\">\n        <th>Wash Process</th>\n        <th>Sub-Process</th>\n        <th>Edit</th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor='let i of test; let in=index' class=\"success\" style=\"text-align: -webkit-center; color:white\">\n        <td>{{i.seldep}}</td>\n        <td>{{i.subDep1}}</td>\n        <td><button (click)='editData(i,in)' type=\"button\" class=\"btn btn-large btn-info\">Edit</button></td>\n      </tr>\n    </tbody>\n  </table>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/WPM/wpm-edit/wpm-edit.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/Main_masters/WPM/wpm-edit/wpm-edit.component.ts ***!
  \*****************************************************************/
/*! exports provided: WpmEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WpmEditComponent", function() { return WpmEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Services_wpm_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../Services/wpm.service */ "./src/app/Services/wpm.service.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var WpmEditComponent = /** @class */ (function () {
    function WpmEditComponent(WpmService, _http, headerTitleService) {
        this.WpmService = WpmService;
        this._http = _http;
        this.headerTitleService = headerTitleService;
        this.update = false;
    }
    WpmEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.WpmService.getData().subscribe(function (testData) { return _this.test = testData; });
        this.headerTitleService.setTitle('Wash Process Master');
    };
    WpmEditComponent.prototype.editData = function (i) {
        this.update = true;
        this.datatobeedited = i;
        console.log("obj is" + this.datatobeedited.subDepSelect);
    };
    WpmEditComponent.prototype.saveUpdate = function (datasaved) {
        console.log("datatobesaved" + datasaved.subDepSelect);
        console.log("datatobesaved" + '' + datasaved.subDepPassword + '' + '' + datasaved.subDepRemark);
        var _url = "http://localhost:3000/api/assign-wash-process" + "/" + datasaved._id;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.put(_url, datasaved, options)
            .subscribe(function (data) {
            alert('saved Successfully');
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    WpmEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-wpm-edit',
            template: __webpack_require__(/*! ./wpm-edit.component.html */ "./src/app/Main_masters/WPM/wpm-edit/wpm-edit.component.html"),
            styles: [__webpack_require__(/*! ./wpm-edit.component.css */ "./src/app/Main_masters/WPM/wpm-edit/wpm-edit.component.css")]
        }),
        __metadata("design:paramtypes", [_Services_wpm_service__WEBPACK_IMPORTED_MODULE_1__["WpmService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"]])
    ], WpmEditComponent);
    return WpmEditComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/WSM/wsm-add/wsm-add.component.css":
/*!****************************************************************!*\
  !*** ./src/app/Main_masters/WSM/wsm-add/wsm-add.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n@import url('https://fonts.googleapis.com/css?family=Numans');\n/* Made with love by Mutiullah Samim*/\nhtml,body{\n\nbackground-size: cover;\nbackground-repeat: no-repeat;\nheight: 100%;\nfont-family: 'Numans', sans-serif;\n}\nlabel\n{\n    color: #fff;\n}\n.container{\nheight: 100%;\nbackground-size: cover;\nwidth:100%;\nbackground-repeat: no-repeat;\n/* align-content: center; */\n}\n.card{\nheight: 300px;\nmargin-top:200px;\nmargin-bottom:20px;\nwidth: 450px;\nbackground-color: rgba(0,0,0,0.5) !important;\n}\n.social_icon span{\nfont-size: 60px;\nmargin-left: 10px;\ncolor: #FFC312;\n}\n.social_icon span:hover{\ncolor: white;\ncursor: pointer;\n}\n.card-header h3{\ncolor: white;\n}\n.social_icon{\nposition: absolute;\nright: 20px;\ntop: -45px;\n}\n.input-group-prepend span{\nwidth: 50px;\nbackground-color: #FFC312;\ncolor: black;\nborder:0 !important;\n}\ninput:focus{\noutline: 0 0 0 0  !important;\nbox-shadow: 0 0 0 0 !important;\n\n}\n.remember{\ncolor: white;\n}\n.remember input\n{\nwidth: 20px;\nheight: 20px;\nmargin-left: 15px;\nmargin-right: 5px;\n}\n.login_btn{\ncolor: black;\nbackground-color: #FFC312;\nwidth: 100px;\n}\n.login_btn:hover{\ncolor: black;\nbackground-color: white;\n}\n.links{\ncolor: white;\n}\n.links a{\nmargin-left: 4px;\n}\noption\n{\n    margin-bottom: 10px!important;\n\n}\n/* *************************************************************************\n**********************  Media query for Mobile screen  */\n@media only screen and (max-width : 768px) {\n  .card{\n      \n    margin-top:auto;\n    margin-bottom:auto;\n      }\n\n}\n@media only screen and (max-width : 480px) {\n    .card{\n        height: 320px;\n        margin-top:20px;\n        margin-bottom:20px;\n\n        }\n\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL1dTTS93c20tYWRkL3dzbS1hZGQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLDZEQUE2RDtBQUY3RCxxQ0FBcUM7QUFJckM7O0FBRUEsc0JBQXNCO0FBQ3RCLDRCQUE0QjtBQUM1QixZQUFZO0FBQ1osaUNBQWlDO0FBQ2pDO0FBQ0E7O0lBRUksV0FBVztBQUNmO0FBQ0E7QUFDQSxZQUFZO0FBQ1osc0JBQXNCO0FBQ3RCLFVBQVU7QUFDViw0QkFBNEI7QUFDNUIsMkJBQTJCO0FBQzNCO0FBRUE7QUFDQSxhQUFhO0FBQ2IsZ0JBQWdCO0FBQ2hCLGtCQUFrQjtBQUNsQixZQUFZO0FBQ1osNENBQTRDO0FBQzVDO0FBRUE7QUFDQSxlQUFlO0FBQ2YsaUJBQWlCO0FBQ2pCLGNBQWM7QUFDZDtBQUVBO0FBQ0EsWUFBWTtBQUNaLGVBQWU7QUFDZjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxrQkFBa0I7QUFDbEIsV0FBVztBQUNYLFVBQVU7QUFDVjtBQUVBO0FBQ0EsV0FBVztBQUNYLHlCQUF5QjtBQUN6QixZQUFZO0FBQ1osbUJBQW1CO0FBQ25CO0FBRUE7QUFDQSw0QkFBNEI7QUFDNUIsOEJBQThCOztBQUU5QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7O0FBRUEsV0FBVztBQUNYLFlBQVk7QUFDWixpQkFBaUI7QUFDakIsaUJBQWlCO0FBQ2pCO0FBRUE7QUFDQSxZQUFZO0FBQ1oseUJBQXlCO0FBQ3pCLFlBQVk7QUFDWjtBQUVBO0FBQ0EsWUFBWTtBQUNaLHVCQUF1QjtBQUN2QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQTs7SUFFSSw2QkFBNkI7O0FBRWpDO0FBSUE7d0RBQ3dEO0FBQ3hEO0VBQ0U7O0lBRUUsZUFBZTtJQUNmLGtCQUFrQjtNQUNoQjs7QUFFTjtBQUVBO0lBQ0k7UUFDSSxhQUFhO1FBQ2IsZUFBZTtRQUNmLGtCQUFrQjs7UUFFbEI7O0FBRVIiLCJmaWxlIjoic3JjL2FwcC9NYWluX21hc3RlcnMvV1NNL3dzbS1hZGQvd3NtLWFkZC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogTWFkZSB3aXRoIGxvdmUgYnkgTXV0aXVsbGFoIFNhbWltKi9cblxuQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1OdW1hbnMnKTtcblxuaHRtbCxib2R5e1xuXG5iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbmhlaWdodDogMTAwJTtcbmZvbnQtZmFtaWx5OiAnTnVtYW5zJywgc2Fucy1zZXJpZjtcbn1cbmxhYmVsXG57XG4gICAgY29sb3I6ICNmZmY7XG59XG4uY29udGFpbmVye1xuaGVpZ2h0OiAxMDAlO1xuYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbndpZHRoOjEwMCU7XG5iYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuLyogYWxpZ24tY29udGVudDogY2VudGVyOyAqL1xufVxuXG4uY2FyZHtcbmhlaWdodDogMzAwcHg7XG5tYXJnaW4tdG9wOjIwMHB4O1xubWFyZ2luLWJvdHRvbToyMHB4O1xud2lkdGg6IDQ1MHB4O1xuYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLDAsMCwwLjUpICFpbXBvcnRhbnQ7XG59XG5cbi5zb2NpYWxfaWNvbiBzcGFue1xuZm9udC1zaXplOiA2MHB4O1xubWFyZ2luLWxlZnQ6IDEwcHg7XG5jb2xvcjogI0ZGQzMxMjtcbn1cblxuLnNvY2lhbF9pY29uIHNwYW46aG92ZXJ7XG5jb2xvcjogd2hpdGU7XG5jdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5jYXJkLWhlYWRlciBoM3tcbmNvbG9yOiB3aGl0ZTtcbn1cblxuLnNvY2lhbF9pY29ue1xucG9zaXRpb246IGFic29sdXRlO1xucmlnaHQ6IDIwcHg7XG50b3A6IC00NXB4O1xufVxuXG4uaW5wdXQtZ3JvdXAtcHJlcGVuZCBzcGFue1xud2lkdGg6IDUwcHg7XG5iYWNrZ3JvdW5kLWNvbG9yOiAjRkZDMzEyO1xuY29sb3I6IGJsYWNrO1xuYm9yZGVyOjAgIWltcG9ydGFudDtcbn1cblxuaW5wdXQ6Zm9jdXN7XG5vdXRsaW5lOiAwIDAgMCAwICAhaW1wb3J0YW50O1xuYm94LXNoYWRvdzogMCAwIDAgMCAhaW1wb3J0YW50O1xuXG59XG5cbi5yZW1lbWJlcntcbmNvbG9yOiB3aGl0ZTtcbn1cblxuLnJlbWVtYmVyIGlucHV0XG57XG53aWR0aDogMjBweDtcbmhlaWdodDogMjBweDtcbm1hcmdpbi1sZWZ0OiAxNXB4O1xubWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5sb2dpbl9idG57XG5jb2xvcjogYmxhY2s7XG5iYWNrZ3JvdW5kLWNvbG9yOiAjRkZDMzEyO1xud2lkdGg6IDEwMHB4O1xufVxuXG4ubG9naW5fYnRuOmhvdmVye1xuY29sb3I6IGJsYWNrO1xuYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG5cbi5saW5rc3tcbmNvbG9yOiB3aGl0ZTtcbn1cblxuLmxpbmtzIGF7XG5tYXJnaW4tbGVmdDogNHB4O1xufVxub3B0aW9uXG57XG4gICAgbWFyZ2luLWJvdHRvbTogMTBweCFpbXBvcnRhbnQ7XG5cbn1cblxuXG5cbi8qICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbioqKioqKioqKioqKioqKioqKioqKiogIE1lZGlhIHF1ZXJ5IGZvciBNb2JpbGUgc2NyZWVuICAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoIDogNzY4cHgpIHtcbiAgLmNhcmR7XG4gICAgICBcbiAgICBtYXJnaW4tdG9wOmF1dG87XG4gICAgbWFyZ2luLWJvdHRvbTphdXRvO1xuICAgICAgfVxuXG59XG5cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aCA6IDQ4MHB4KSB7XG4gICAgLmNhcmR7XG4gICAgICAgIGhlaWdodDogMzIwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6MjBweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbToyMHB4O1xuXG4gICAgICAgIH1cblxufSJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/WSM/wsm-add/wsm-add.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/Main_masters/WSM/wsm-add/wsm-add.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"d-flex justify-content-center h-100\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        <h3 style=\"text-align:center\"> Add Wash Step</h3>\n      </div>\n      <div class=\"card-body\">\n        <form class=\"form-login\" role=\"form\" #formRef='ngForm' (ngSubmit)='onSubmit(formRef.value)'>\n          <div class=\"form-group row\">\n            <label for=\"gartype\" class=\"col-sm-3 col-form-label\">Steps</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"gartype\" name=\"AWS\"  #seldepRef='ngModel' required ngModel>\n            </div>\n          </div>\n          <br>\n          <br>\n          <div class=\"form-group\" style=\"text-align:center\">\n            <button style=\"padding: 0;\n                border: none;\n                background: none;\" type=\"submit\">\n              <!-- <i class=\"far fa-check-circle\"> -->\n              <i class=\"fas fa-check-circle\" style=\"font-size:60px;color: yellow;\"></i>\n            </button>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/WSM/wsm-add/wsm-add.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/Main_masters/WSM/wsm-add/wsm-add.component.ts ***!
  \***************************************************************/
/*! exports provided: WsmAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WsmAddComponent", function() { return WsmAddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var WsmAddComponent = /** @class */ (function () {
    function WsmAddComponent(router, _http, headerTitleService) {
        this.router = router;
        this._http = _http;
        this.headerTitleService = headerTitleService;
    }
    WsmAddComponent.prototype.ngOnInit = function () {
        this.headerTitleService.setTitle('Wash Step Master');
    };
    WsmAddComponent.prototype.onSubmit = function (a) {
        var _this = this;
        this.AWS = a.AWS,
            console.log(a);
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.post('/api/wash-step-master-list-edit', a, options)
            .subscribe(function (data) {
            alert('added Successfully');
            _this.router.navigate(['/main-masters']);
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    WsmAddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-wsm-add',
            template: __webpack_require__(/*! ./wsm-add.component.html */ "./src/app/Main_masters/WSM/wsm-add/wsm-add.component.html"),
            styles: [__webpack_require__(/*! ./wsm-add.component.css */ "./src/app/Main_masters/WSM/wsm-add/wsm-add.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"]])
    ], WsmAddComponent);
    return WsmAddComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/WSM/wsm-edit-list/wsm-edit-list.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/Main_masters/WSM/wsm-edit-list/wsm-edit-list.component.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* Made with love by Mutiullah Samim*/\ntable,tr,th,td\n{\n    border:1px solid #fff;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL1dTTS93c20tZWRpdC1saXN0L3dzbS1lZGl0LWxpc3QuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxxQ0FBcUM7QUFDckM7O0lBRUkscUJBQXFCO0FBQ3pCIiwiZmlsZSI6InNyYy9hcHAvTWFpbl9tYXN0ZXJzL1dTTS93c20tZWRpdC1saXN0L3dzbS1lZGl0LWxpc3QuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIE1hZGUgd2l0aCBsb3ZlIGJ5IE11dGl1bGxhaCBTYW1pbSovXG50YWJsZSx0cix0aCx0ZFxue1xuICAgIGJvcmRlcjoxcHggc29saWQgI2ZmZjtcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/WSM/wsm-edit-list/wsm-edit-list.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/Main_masters/WSM/wsm-edit-list/wsm-edit-list.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\n<div class=\"container\" style=\"text-align: -webkit-center;\">\n  <div *ngIf=\"update\">\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.AWS>\n    <button (click)='saveUpdate(datatobeedited)' type=\"button\" class=\"btn btn-sm btn-info\" style=\"margin-top: -5px;font-size: 13.9px;\">save</button>\n  </div>\n  <br>\n  <br>\n  <table class=\"table\">\n    <thead style=\"text-align: -webkit-center;color: cornsilk;\">\n      <tr class=\"text-color\">\n        <th> Wash Step</th>\n        <th>Edit</th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor='let i of test; let in=index' class=\"success\" style=\"text-align: -webkit-center;color: white;\">\n        <td>{{i.AWS}}</td>\n        <td><button (click)='editData(i,in)' type=\"button\" class=\"btn btn-large btn-info\">Edit</button></td>\n      </tr>\n    </tbody>\n  </table>\n</div>"

/***/ }),

/***/ "./src/app/Main_masters/WSM/wsm-edit-list/wsm-edit-list.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/Main_masters/WSM/wsm-edit-list/wsm-edit-list.component.ts ***!
  \***************************************************************************/
/*! exports provided: WsmEditListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WsmEditListComponent", function() { return WsmEditListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Services_wsm_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../Services/wsm.service */ "./src/app/Services/wsm.service.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var WsmEditListComponent = /** @class */ (function () {
    function WsmEditListComponent(WsmService, _http, headerTitleService) {
        this.WsmService = WsmService;
        this._http = _http;
        this.headerTitleService = headerTitleService;
        this.update = false;
    }
    WsmEditListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.WsmService.getData().subscribe(function (testData) { return _this.test = testData; });
        this.headerTitleService.setTitle('Wash Step Master');
    };
    WsmEditListComponent.prototype.editData = function (i) {
        this.update = true;
        this.datatobeedited = i;
        console.log("obj is" + this.datatobeedited.AWS);
    };
    WsmEditListComponent.prototype.saveUpdate = function (datasaved) {
        console.log("datatobesaved" + datasaved.AWS);
        console.log("datatobesaved" + '' + datasaved.AWS);
        var _url = "http://localhost:3000/api/wash-step-master-list-edit" + "/" + datasaved._id;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.put(_url, datasaved, options)
            .subscribe(function (data) {
            alert('saved Successfully');
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    WsmEditListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-wsm-edit-list',
            template: __webpack_require__(/*! ./wsm-edit-list.component.html */ "./src/app/Main_masters/WSM/wsm-edit-list/wsm-edit-list.component.html"),
            styles: [__webpack_require__(/*! ./wsm-edit-list.component.css */ "./src/app/Main_masters/WSM/wsm-edit-list/wsm-edit-list.component.css")]
        }),
        __metadata("design:paramtypes", [_Services_wsm_service__WEBPACK_IMPORTED_MODULE_1__["WsmService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"],
            src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"]])
    ], WsmEditListComponent);
    return WsmEditListComponent;
}());



/***/ }),

/***/ "./src/app/Main_masters/main-master/main-master.component.css":
/*!********************************************************************!*\
  !*** ./src/app/Main_masters/main-master/main-master.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".main-master\n{\n    text-align: center;\n}\na\n{\n\ttext-decoration: none;\n}\n/* Main Container -  this controls the size of the circle */\n.circle_container\n{\n\twidth : 210px;\n\theight :210px;\n\tmargin : 0 auto;\n    padding : 0;\n    text-align: center;\n    display: inline-block;\n    padding: 20px;\n    \n/*\tborder : 1px solid red; */\n}\n.circle_container a{\n\ttext-decoration: none;\n}\n/* Circle Main draws the actual circle */\n.circle_main\n{\n\twidth : 100%;\n\tbackground-color: #3ece98;\n\theight : 100%;\n\tborder-radius : 50%;\n\tborder : 6px solid #ffffff;\t/* can alter thickness and colour of circle on this line */\n\tmargin : 0 auto;\n    padding : 0;\n\ttext-align: center;\n\toverflow: hidden;\n}\n/* Circle Text Container - constrains text area to within the circle */\n.circle_text_container\n{\n\t/* area constraints */\n\twidth : 70%;\n\theight : 70%;\n\tmax-width : 70%;\n\tmax-height : 70%;\n\tmargin : 0;\n\tpadding : 0;\n\t/* some position nudging to center the text area */\n\tposition : relative;\n\tleft : 15%;\n\ttop : 15%;\n\t/* preserve 3d prevents blurring sometimes caused by the text centering in the next class */\n\t-webkit-transform-style : preserve-3d;\n\t        transform-style : preserve-3d;\n\t/*border : 1px solid green;*/\n}\n/* Circle Text - the appearance of the text within the circle plus vertical centering */\n.circle_text\n{\n\t/* change font/size/etc here */\n\tfont: 18px \"Tahoma\", Arial, Serif;\t\n    text-align : center;\n    color:#fff;\n\t/* vertical centering technique */\n\tposition : relative;\n\ttop : 58%;\n\t-webkit-transform : translateY(-50%);\n\t        transform : translateY(-50%);\n}\n/* Popup */\n.circle_container1\n{\n\twidth : 130px;\n\theight : 130px;\n\tmargin : 0 auto;\n    padding : 0;\n    text-align: center;\n    /* display: inline-block; */\n\t/* padding: 20px; */\n\tpadding: 5px;\n\tfont-size: 15px;\n\t\n/*\tborder : 1px solid red; */\n}\n.circle_container1 a{\n\ttext-decoration: none;\n}\n/* Circle Main draws the actual circle */\n.circle_main1\n{\n\twidth : 100%;\n\tbackground-color: #3ece98;\n\theight : 100%;\n\tborder-radius : 50%;\n\tborder : 6px solid #ffffff;\t/* can alter thickness and colour of circle on this line */\n\tmargin : 0 auto;\n    padding : 0;\n\ttext-align: center;\n\toverflow: hidden;\n}\n/* Circle Text Container - constrains text area to within the circle */\n.circle_text_container1\n{\n\t/* area constraints */\n\twidth : 70%;\n\theight : 70%;\n\tmax-width : 70%;\n\tmax-height : 70%;\n\tmargin : 0;\n\tpadding : 0;\n\t/* some position nudging to center the text area */\n\tposition : relative;\n\tleft : 15%;\n\ttop : 15%;\n\t\n\t/* preserve 3d prevents blurring sometimes caused by the text centering in the next class */\n\t-webkit-transform-style : preserve-3d;\n\t        transform-style : preserve-3d;\n\t\n\t/*border : 1px solid green;*/\n}\n/* Circle Text - the appearance of the text within the circle plus vertical centering */\n.circle_text1\n{\n\t/* change font/size/etc here */\n\tfont: 18px \"Tahoma\", Arial, Serif;\t\n    text-align : center;\n    color:#fff;\n\t\n\t/* vertical centering technique */\n\tposition : relative;\n\ttop : 50%;\n\t-webkit-transform : translateY(-50%);\n\t        transform : translateY(-50%);\n}\n/* Popup */\n.circle_container2\n{\n\twidth : 200px;\n\theight : 200px;\n\tmargin : 0 auto;\n    padding : 0;\n    text-align: center;\n    display: inline-block;\n    padding: 20px;\n    \n/*\tborder : 1px solid red; */\n}\n.circle_container2 a\n{\n\ttext-decoration: none;\n}\n/* Circle Main draws the actual circle */\n.circle_main2\n{\n\twidth : 100%;\n\tbackground-color: #3ece98;\n\theight : 100%;\n\tborder-radius : 50%;\n\tborder : 6px solid #ffffff;\t/* can alter thickness and colour of circle on this line */\n\tmargin : 0 auto;\n    padding : 0;\n\ttext-align: center;\n\toverflow: hidden;\n}\n/* Circle Text Container - constrains text area to within the circle */\n.circle_text_container2\n{\n\t/* area constraints */\n\twidth : 70%;\n\theight : 70%;\n\tmax-width : 70%;\n\tmax-height : 70%;\n\tmargin : 0;\n\tpadding : 0;\n\t/* some position nudging to center the text area */\n\tposition : relative;\n\tleft : 15%;\n\ttop : 15%;\n\t/* preserve 3d prevents blurring sometimes caused by the text centering in the next class */\n\t-webkit-transform-style : preserve-3d;\n\t        transform-style : preserve-3d;\n\t/*border : 1px solid green;*/\n}\n/* Circle Text - the appearance of the text within the circle plus vertical centering */\n.circle_text2\n{\n\t/* change font/size/etc here */\n\tfont: 18px \"Tahoma\", Arial, Serif;\t\n    text-align : center;\n    color:#fff;\n\t/* vertical centering technique */\n\tposition : relative;\n\ttop : 50%;\n\t-webkit-transform : translateY(-50%);\n\t        transform : translateY(-50%);\n}\n/* Assign Master */\n.assign-master\n{\n\twidth : 200px;\n\theight :100px;\n\tmargin : 0 auto;\n    padding : 0;\n    text-align: center;\n    display: inline-block;\n    padding: 20px;\n}\n/* Assign Master */\n.administrator\n{\n\twidth : 250px;\n\theight : 120px;\n\tmargin : 0 auto;\n    padding : 0;\n    text-align: center;\n    display: inline-block;\n\tpadding: 20px;\n\t/* background-color: transparent; */\n\ttext-decoration: none;\n}\n.washing-master\n{\n\twidth : 300px;\n\theight :100px;\n\tmargin : 0 auto;\n    padding : 0;\n    text-align: center;\n    display: inline-block;\n\tpadding: 20px;\n\t/* background-color: transparent; */\n\ttext-decoration: none;\t\n}\n/* Full width inout fields */\ninput[type=text], input[type=password]\n {\n\t width: 90%;\n\t padding: 12px 20px;\n\t margin: 8px 20px;\n\t display: inline-block;\n\t border: 1px solid #ccc;\n\t box-sizing: border-box;\n\t font-size: 10px;\n\t \n }\n/* Set a style for all buttons */\nbutton\n {\n\tbackground-color: transparent; \n\tfont: 18px \"Tahoma\", Arial, Serif;\t\n\ttext-align: -moz-center;\n    text-align: -webkit-center;\n    color:#fff;\n\t/* vertical centering technique */\n\tposition : relative;\n\ttop : 50%;\n\tmargin-top: 50%;\n\tborder: none;\n\t-webkit-transform : translateY(-50%);\n\t        transform : translateY(-50%);\n }\n.button1\n {\n\tbackground-color: transparent; \n\tfont: 18px \"Tahoma\", Arial, Serif;\t\n\ttext-align: -moz-center;\n    text-align: -webkit-center;\n    color:#fff;\n\t/* vertical centering technique */\n\tposition : relative;\n\ttop : 50%;\n\tmargin-top: 50%;\n\tborder: none;\n\t-webkit-transform : translateY(-50%);\n\t        transform : translateY(-50%);\n\tmargin-left: -20px;\n    margin-right: -20px\n }\n.button2\n {\n\tbackground-color: transparent; \n\tfont: 18px \"Tahoma\", Arial, Serif;\t\n\ttext-align: -moz-center;\n    text-align: -webkit-center;\n    color:#fff;\n\t/* vertical centering technique */\n\tposition : relative;\n\tmargin-top: 30%;\n    margin-left: -20px;\n    border: none;\n    margin-right: -20px;\n\t-webkit-transform : translateY(-50%);\n\t        transform : translateY(-50%);\n }\n.button3\n {\n\tbackground-color: transparent; \n\tfont: 18px \"Tahoma\", Arial, Serif;\t\n\ttext-align: -moz-center;\n    text-align: -webkit-center;\n    color:#fff;\n\t/* vertical centering technique */\n\tposition : relative;\n\tmargin-top: 50%;\n    border: none;\n    margin-right: -20px;\n    margin-left: -20px;\n\t-webkit-transform : translateY(-50%);\n\t        transform : translateY(-50%);\n }\n.button4\n {\n\tbackground-color: transparent; \n\tfont: 18px \"Tahoma\", Arial, Serif;\t\n\ttext-align: -moz-center;\n    text-align: -webkit-center;\n    color:#fff;\n\t/* vertical centering technique */\n\tposition : relative;\n\ttop : 50%;\n\tmargin-top: 30%;\n    margin-left: -20px;\n    margin-right: -20px;\n\t-webkit-transform : translateY(-50%);\n\t        transform : translateY(-50%);\n }\n.button5\n {\n\tbackground-color: transparent; \n\tfont: 18px \"Tahoma\", Arial, Serif;\t\n\ttext-align: -moz-center;\n    text-align: -webkit-center;\n    color:#fff;\n\t/* vertical centering technique */\n\tposition : relative;\n\ttop : 50%;\n\tmargin-top: 50%;\n    margin-right: -20px;\n    border: none;\n    margin-left: -20px;\n\t-webkit-transform : translateY(-50%);\n\t        transform : translateY(-50%);\n }\n.button6\n {\n\tbackground-color: transparent; \n\tfont: 18px \"Tahoma\", Arial, Serif;\t\n\ttext-align: -moz-center;\n    text-align: -webkit-center;\n    color:#fff;\n\t/* vertical centering technique */\n\tposition : relative;\n\ttop : 50%;\n\tmargin-top: 60%;\n\n    border: none;\n    margin-left: -20px;\n    margin-right: -20px;\n\t-webkit-transform : translateY(-50%);\n\t        transform : translateY(-50%);\n }\n.button7\n {\n\tbackground-color: transparent; \n\tfont: 18px \"Tahoma\", Arial, Serif;\t\n\ttext-align: -moz-center;\n    text-align: -webkit-center;\n    color:#fff;\n\t/* vertical centering technique */\n\tposition : relative;\n\ttop : 50%;\n\tmargin-top: 30%;\n    border: none;\n    margin-right: -20px;\n    margin-left: -20px;\n\t-webkit-transform : translateY(-50%);\n\t        transform : translateY(-50%);\n }\n.button8\n {\n\tbackground-color: transparent; \n\tfont: 18px \"Tahoma\", Arial, Serif;\t\n\ttext-align: -moz-center;\n    text-align: -webkit-center;\n    color:#fff;\n\t/* vertical centering technique */\n\tposition : relative;\n\ttop : 50%;\n\tmargin-top: 30%;\n    margin-right: -20px;\n    border: none;\n    margin-left: -20px;\n\t-webkit-transform : translateY(-50%);\n\t        transform : translateY(-50%);\n }\n.button9\n {\n\tbackground-color: transparent; \n\tfont: 18px \"Tahoma\", Arial, Serif;\t\n\ttext-align: -moz-center;\n    text-align: -webkit-center;\n    color:#fff;\n\t/* vertical centering technique */\n\tposition : relative;\n\ttop : 50%;\n\tmargin-top: 30%;\n\tmargin-right: -20px;\n    border: none;\n    margin-left: -20px;\n\t-webkit-transform : translateY(-50%);\n\t        transform : translateY(-50%);\n }\n.button10\n {\n\tbackground-color: transparent;  \n\tfont: 18px \"Tahoma\", Arial, Serif;\t\n\ttext-align: -moz-center;\n    text-align: -webkit-center;\n    color:#fff;\n\t/* vertical centering technique */\n\tposition : relative;\n\ttop : 50%;\n\tmargin-top: 30%;\n    border: none;\n    margin-right: -20px;\n    margin-left: -20px;\n\t-webkit-transform : translateY(-50%);\n\t        transform : translateY(-50%);\n }\n.button11\n {\n\tbackground-color: transparent; \n\tfont: 18px \"Tahoma\", Arial, Serif;\t\n\ttext-align: -moz-center;\n    text-align: -webkit-center;\n    color:#fff;\n\t/* vertical centering technique */\n\tposition : relative;\n\ttop : 50%;\n\tmargin-top: 50%;\n    border: none;\n    margin-right: -20px;\n    margin-left: -20px;\n\n\t-webkit-transform : translateY(-50%);\n\n\t        transform : translateY(-50%);\n }\n.button12\n {\n\tbackground-color: transparent; \n\tfont: 18px \"Tahoma\", Arial, Serif;\t\n\ttext-align: -moz-center;\n    text-align: -webkit-center;\n    color:#fff;\n\t/* vertical centering technique */\n\tposition : relative;\n\ttop : 50%;\n\tmargin-top: 30%;\n    border: none;\n    margin-right: -20px;\n    margin-left: -20px;\n\t-webkit-transform : translateY(-50%);\n\t        transform : translateY(-50%);\n }\n.button13\n {\n\tbackground-color: transparent;  \n\tfont: 18px \"Tahoma\", Arial, Serif;\t\n\ttext-align: -moz-center;\n    text-align: -webkit-center;\n    color:#fff;\n\t/* vertical centering technique */\n\tposition : relative;\n\ttop : 50%;\n\tmargin-top: 30%;\n    margin-right: -20px;\n    border: none;\n    margin-left: -20px;\n\t-webkit-transform : translateY(-50%);\n\t        transform : translateY(-50%);\n }\n.button14\n {\n\tbackground-color: transparent; \n\tfont: 18px \"Tahoma\", Arial, Serif;\t\n\ttext-align: -moz-center;\n    text-align: -webkit-center;\n    color:#fff;\n\t/* vertical centering technique */\n\tposition : relative;\n\ttop : 50%;\n\tmargin-top: 30%;\n    border: none;\n    margin-left: -10px;\n    margin-right: -10px;\n\t-webkit-transform : translateY(-50%);\n\t        transform : translateY(-50%);\n }\n.button15\n {\n\tbackground-color: transparent; \n\tfont: 18px \"Tahoma\", Arial, Serif;\t\n\ttext-align: -moz-center;\n    text-align: -webkit-center;\n    color:#fff;\n\t/* vertical centering technique */\n\tposition : relative;\n\ttop: 50%;\n    margin-top: 40%;\n    border: none;\n\tmargin-left: -30px;\n\tmargin-right: -30px;\n\t-webkit-transform : translateY(-50%);\n\t        transform : translateY(-50%);\n }\n/* the Modal (background) */\n.modal\n {\n\t display: none;\n\t position: fixed;\n\t z-index: 1;\n\t left: 0;\n\t top: 0;\n\t width: 100%;\n\t /* overflow: auto; */\n\t background-color: rgba(0,0,0,0.4); \n\t \n }\n/*Modal Content Box*/\n.modal-content\n {\n\t /* It removes the background box */\n\t background-color:transparent; \n\t margin: 4% auto 15% auto;\n\t border: 1px solid #888;\n\t width: 100%;\n\t height: 100%;\n\t padding-bottom:50px;\n\t border: none;\n }\n.close\n {\n\t position: absolute;\n\t top: 0;\n\t color: white;\n\t font-size: 35px;\n\t font-weight: bold;\n\t padding: 20px;\n\t margin-left: 30px;\n\tmargin-top:90px;\n }\n.close:hover,.close focus\n {\n\t color: red;\n\t cursor: pointer;\n }\n.close1\n{\n\tposition: absolute;\n\n\ttop: 0;\n\tcolor: white;\n\tfont-size: 35px;\n\tfont-weight: bold;\n\tpadding: 20px;\n\tmargin-left: 30px;\n    margin-top: 90px;\n\topacity: .5;\n\ttext-shadow: 0 1px 0 #fff;\n}\n.close1:hover,.close1 focus\n{\n\tcolor: red;\n\tcursor: pointer;\n}\n.close2\n{\n\tposition: absolute;\n\n\ttop: 0;\n\tcolor: white;\n\tfont-size: 35px;\n\tfont-weight: bold;\n\tpadding: 20px;\n\tmargin-left: 30px;\n    margin-top: 90px;\n\topacity: .5;\n\ttext-shadow: 0 1px 0 #fff;\n}\n.close2:hover,.close2 focus\n{\n\tcolor: red;\n\tcursor: pointer;\n}\n/* Close button for inline block*/\n/* .close\n {\n\tposition: absolute;\n    \n    font-size: 35px;\n    font-weight: bold;\n    padding: 20px;\n    margin-left: -25px;\n    margin-top: -45px;\n\t\n} */\n.close3\n{\n\tposition: absolute;\n\n\ttop: 0;\n\tcolor: white;\n\tfont-size: 35px;\n\tfont-weight: bold;\n\tpadding: 20px;\n\tmargin-left: 30px;\n    margin-top: 90px;\n\topacity: .5;\n\ttext-shadow: 0 1px 0 #fff;\n}\n.close3:hover,.close3 focus\n{\n\tcolor: red;\n\tcursor: pointer;\n}\n.close4\n{\n\tposition: absolute;\n\n\ttop: 0;\n\tcolor: white;\n\tfont-size: 35px;\n\tfont-weight: bold;\n\tpadding: 20px;\n    margin-left: 30px;\n    margin-top: 90px;\n}\n.close4:hover,.close4 focus\n {\n\t color: red;\n\t cursor: pointer;\n }\n.close5\n{\n\tposition: absolute;\n\n\ttop: 0;\n\tcolor: white;\n\tfont-size: 35px;\n\tfont-weight: bold;\n\tpadding: 20px;\n    margin-left: 30px;\n    margin-top: 90px;\n\topacity: .5;\n\ttext-shadow: 0 1px 0 #fff;\n}\n.close5:hover,.close5 focus\n {\n\t color: red;\n\t cursor: pointer;\n }\n.close6\n {\n\t position: absolute;\n\t \n\t top: 0;\n\t color: white;\n\t font-size: 35px;\n\t font-weight: bold;\n\t padding: 20px;\n\t margin-left: 30px;\n\t margin-top:90px;\n }\n.close6:hover,.close6 focus\n {\n\t color: red;\n\t cursor: pointer;\n }\n.close7\n{\n\tposition: absolute;\n\ttop: 0;\n\tcolor: white;\n\tfont-size: 35px;\n\tfont-weight: bold;\n\tpadding: 20px;\n    margin-left: 30px;\n    margin-top: 90px;\n\topacity: .5;\n\ttext-shadow: 0 1px 0 #fff;\n}\n.close7:hover,.close7 focus\n{\n\tcolor: red;\n\tcursor: pointer;\n}\n.close8\n{\n\tposition: absolute;\n\n\ttop: 0;\n\tcolor: white;\n\tfont-size: 35px;\n\tfont-weight: bold;\n\tpadding: 20px;\n\tmargin-left: 30px;\n    margin-top: 90px;\n\topacity: .5;\n\ttext-shadow: 0 1px 0 #fff;\n}\n.close8:hover,.close8 focus\n{\n\tcolor: red;\n\tcursor: pointer;\n}\n.close9\n{\n\tposition: absolute;\n\ttop: 0;\n\tcolor: white;\n\tfont-size: 35px;\n\tfont-weight: bold;\n\tpadding: 20px;\n\tmargin-left: 30px;\n    margin-top:90px;\n\topacity: .5;\n\ttext-shadow: 0 1px 0 #fff;\n}\n.close9:hover,.close9 focus\n{\n\tcolor: red;\n\tcursor: pointer;\n}\n.close10\n\t{\n\t\tposition: absolute;\n\t\tposition: absolute;\n\t\ttop: 0;\n\t\tcolor: white;\n\t\tfont-size: 35px;\n\t\tfont-weight: bold;\n\t\tpadding: 20px;\n\t\tmargin-left: 30px;\n\t\tmargin-top:90px;\n\t}\n.close10:hover,.close10 focus\n\t{\n\t\tcolor: red;\n\t\tcursor: pointer;\n\t}\n.close11\n\t{\n\t\tposition: absolute;\n\t\ttop: 0;\n\t\tcolor: white;\n\t\tfont-size: 35px;\n\t\tfont-weight: bold;\n\t\tpadding: 20px;\n\t\tmargin-left: 30px;\n\t\tmargin-top:90px;\n\t\topacity: .5;\n\t\ttext-shadow: 0 1px 0 #fff;\n\t}\n.close11:hover,.close11 focus\n\t{\n\t\tcolor: red;\n\t\tcursor: pointer;\n\t}\n.close12\n\t{\n\t\tposition: absolute;\n\n\t\ttop: 0;\n\t\tcolor: white;\n\t\tfont-size: 35px;\n\t\tfont-weight: bold;\n\t\tpadding: 20px;\n\t\tmargin-left: 30px;\n\t    margin-top:90px;\n\t\topacity: .5;\n\t\ttext-shadow: 0 1px 0 #fff;\n\t}\n.close12:hover,.close12 focus\n\t{\n\t\tcolor: red;\n\t\tcursor: pointer;\n\t}\n.close13\n\t{\n\t\tposition: absolute;\n\t\t\n\t\ttop: 0;\n\t\tcolor: white;\n\t\tfont-size: 35px;\n\t\tfont-weight: bold;\n\t\tpadding: 20px;\n\t\tmargin-left: 30px;\n\t\tmargin-top:90px;\n\t}\n.close13:hover,.close13 focus\n\t{\n\t\tcolor: red;\n\t\tcursor: pointer;\n\t}\n.close14\n\t{\n\t\tposition: absolute;\n\t\t\n\t\ttop: 0;\n\t\tcolor: white;\n\t\tfont-size: 35px;\n\t\tfont-weight: bold;\n\t\tpadding: 20px;\n\t\tmargin-left: 30px;\n\t\tmargin-top:90px;\n\t}\n.close14:hover,.close14 focus\n\t{\n\t\tcolor: red;\n\t\tcursor: pointer;\n\t}\n.close15\n\t{\n\t\tposition: absolute;\n\t\t\n\t\ttop: 0;\n\t\tcolor: white;\n\t\tfont-size: 35px;\n\t\tfont-weight: bold;\n\t\tpadding: 20px;\n\t\tmargin-left: 30px;\n\t\tmargin-top:90px;\n\t}\n.close15:hover,.close15 focus\n\t{\n\t\tcolor: red;\n\t\tcursor: pointer;\n\t}\n/* Add Zoom Animation */\n.animate\n {\n\t -webkit-animation: zoom 0.6s;\n\t         animation: zoom 0.6s;\n }\n@-webkit-keyframes zoom\n {\n\t from {-webkit-transform: scale(0);transform: scale(0)}\n\t to {-webkit-transform: scale(1);transform: scale(1)}\n }\n@keyframes zoom\n {\n\t from {-webkit-transform: scale(0);transform: scale(0)}\n\t to {-webkit-transform: scale(1);transform: scale(1)}\n }\n.Popup\n {\n\t margin-top: 120px;\n }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTWFpbl9tYXN0ZXJzL21haW4tbWFzdGVyL21haW4tbWFzdGVyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0lBRUksa0JBQWtCO0FBQ3RCO0FBQ0E7O0NBRUMscUJBQXFCO0FBQ3RCO0FBQ0EsMkRBQTJEO0FBQzNEOztDQUVDLGFBQWE7Q0FDYixhQUFhO0NBQ2IsZUFBZTtJQUNaLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIscUJBQXFCO0lBQ3JCLGFBQWE7O0FBRWpCLDRCQUE0QjtBQUM1QjtBQUNBO0NBQ0MscUJBQXFCO0FBQ3RCO0FBRUEsd0NBQXdDO0FBQ3hDOztDQUVDLFlBQVk7Q0FDWix5QkFBeUI7Q0FDekIsYUFBYTtDQUNiLG1CQUFtQjtDQUNuQiwwQkFBMEIsRUFBRSwwREFBMEQ7Q0FDdEYsZUFBZTtJQUNaLFdBQVc7Q0FDZCxrQkFBa0I7Q0FDbEIsZ0JBQWdCO0FBQ2pCO0FBRUEsc0VBQXNFO0FBQ3RFOztDQUVDLHFCQUFxQjtDQUNyQixXQUFXO0NBQ1gsWUFBWTtDQUNaLGVBQWU7Q0FDZixnQkFBZ0I7Q0FDaEIsVUFBVTtDQUNWLFdBQVc7Q0FDWCxrREFBa0Q7Q0FDbEQsbUJBQW1CO0NBQ25CLFVBQVU7Q0FDVixTQUFTO0NBQ1QsMkZBQTJGO0NBQzNGLHFDQUE2QjtTQUE3Qiw2QkFBNkI7Q0FDN0IsNEJBQTRCO0FBQzdCO0FBRUEsdUZBQXVGO0FBQ3ZGOztDQUVDLDhCQUE4QjtDQUM5QixpQ0FBaUM7SUFDOUIsbUJBQW1CO0lBQ25CLFVBQVU7Q0FDYixpQ0FBaUM7Q0FDakMsbUJBQW1CO0NBQ25CLFNBQVM7Q0FDVCxvQ0FBNEI7U0FBNUIsNEJBQTRCO0FBQzdCO0FBR0EsVUFBVTtBQUVWOztDQUVDLGFBQWE7Q0FDYixjQUFjO0NBQ2QsZUFBZTtJQUNaLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsMkJBQTJCO0NBQzlCLG1CQUFtQjtDQUNuQixZQUFZO0NBQ1osZUFBZTs7QUFFaEIsNEJBQTRCO0FBQzVCO0FBQ0E7Q0FDQyxxQkFBcUI7QUFDdEI7QUFFQSx3Q0FBd0M7QUFDeEM7O0NBRUMsWUFBWTtDQUNaLHlCQUF5QjtDQUN6QixhQUFhO0NBQ2IsbUJBQW1CO0NBQ25CLDBCQUEwQixFQUFFLDBEQUEwRDtDQUN0RixlQUFlO0lBQ1osV0FBVztDQUNkLGtCQUFrQjtDQUNsQixnQkFBZ0I7QUFDakI7QUFFQSxzRUFBc0U7QUFDdEU7O0NBRUMscUJBQXFCO0NBQ3JCLFdBQVc7Q0FDWCxZQUFZO0NBQ1osZUFBZTtDQUNmLGdCQUFnQjtDQUNoQixVQUFVO0NBQ1YsV0FBVztDQUNYLGtEQUFrRDtDQUNsRCxtQkFBbUI7Q0FDbkIsVUFBVTtDQUNWLFNBQVM7O0NBRVQsMkZBQTJGO0NBQzNGLHFDQUE2QjtTQUE3Qiw2QkFBNkI7O0NBRTdCLDRCQUE0QjtBQUM3QjtBQUVBLHVGQUF1RjtBQUN2Rjs7Q0FFQyw4QkFBOEI7Q0FDOUIsaUNBQWlDO0lBQzlCLG1CQUFtQjtJQUNuQixVQUFVOztDQUViLGlDQUFpQztDQUNqQyxtQkFBbUI7Q0FDbkIsU0FBUztDQUNULG9DQUE0QjtTQUE1Qiw0QkFBNEI7QUFDN0I7QUFFQSxVQUFVO0FBRVY7O0NBRUMsYUFBYTtDQUNiLGNBQWM7Q0FDZCxlQUFlO0lBQ1osV0FBVztJQUNYLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIsYUFBYTs7QUFFakIsNEJBQTRCO0FBQzVCO0FBQ0E7O0NBRUMscUJBQXFCO0FBQ3RCO0FBRUEsd0NBQXdDO0FBQ3hDOztDQUVDLFlBQVk7Q0FDWix5QkFBeUI7Q0FDekIsYUFBYTtDQUNiLG1CQUFtQjtDQUNuQiwwQkFBMEIsRUFBRSwwREFBMEQ7Q0FDdEYsZUFBZTtJQUNaLFdBQVc7Q0FDZCxrQkFBa0I7Q0FDbEIsZ0JBQWdCO0FBQ2pCO0FBRUEsc0VBQXNFO0FBQ3RFOztDQUVDLHFCQUFxQjtDQUNyQixXQUFXO0NBQ1gsWUFBWTtDQUNaLGVBQWU7Q0FDZixnQkFBZ0I7Q0FDaEIsVUFBVTtDQUNWLFdBQVc7Q0FDWCxrREFBa0Q7Q0FDbEQsbUJBQW1CO0NBQ25CLFVBQVU7Q0FDVixTQUFTO0NBQ1QsMkZBQTJGO0NBQzNGLHFDQUE2QjtTQUE3Qiw2QkFBNkI7Q0FDN0IsNEJBQTRCO0FBQzdCO0FBRUEsdUZBQXVGO0FBQ3ZGOztDQUVDLDhCQUE4QjtDQUM5QixpQ0FBaUM7SUFDOUIsbUJBQW1CO0lBQ25CLFVBQVU7Q0FDYixpQ0FBaUM7Q0FDakMsbUJBQW1CO0NBQ25CLFNBQVM7Q0FDVCxvQ0FBNEI7U0FBNUIsNEJBQTRCO0FBQzdCO0FBR0Esa0JBQWtCO0FBQ2xCOztDQUVDLGFBQWE7Q0FDYixhQUFhO0NBQ2IsZUFBZTtJQUNaLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIscUJBQXFCO0lBQ3JCLGFBQWE7QUFDakI7QUFFQSxrQkFBa0I7QUFDbEI7O0NBRUMsYUFBYTtDQUNiLGNBQWM7Q0FDZCxlQUFlO0lBQ1osV0FBVztJQUNYLGtCQUFrQjtJQUNsQixxQkFBcUI7Q0FDeEIsYUFBYTtDQUNiLG1DQUFtQztDQUNuQyxxQkFBcUI7QUFDdEI7QUFFQTs7Q0FFQyxhQUFhO0NBQ2IsYUFBYTtDQUNiLGVBQWU7SUFDWixXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLHFCQUFxQjtDQUN4QixhQUFhO0NBQ2IsbUNBQW1DO0NBQ25DLHFCQUFxQjtBQUN0QjtBQUVDLDRCQUE0QjtBQUM1Qjs7RUFFQyxVQUFVO0VBQ1Ysa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsc0JBQXNCO0VBQ3RCLHNCQUFzQjtFQUN0QixlQUFlOztDQUVoQjtBQUlBLGdDQUFnQztBQUVoQzs7Q0FFQSw2QkFBNkI7Q0FDN0IsaUNBQWlDO0NBQ2pDLHVCQUF1QjtJQUNwQiwwQkFBMEI7SUFDMUIsVUFBVTtDQUNiLGlDQUFpQztDQUNqQyxtQkFBbUI7Q0FDbkIsU0FBUztDQUNULGVBQWU7Q0FDZixZQUFZO0NBQ1osb0NBQTRCO1NBQTVCLDRCQUE0QjtDQUM1QjtBQUVBOztDQUVBLDZCQUE2QjtDQUM3QixpQ0FBaUM7Q0FDakMsdUJBQXVCO0lBQ3BCLDBCQUEwQjtJQUMxQixVQUFVO0NBQ2IsaUNBQWlDO0NBQ2pDLG1CQUFtQjtDQUNuQixTQUFTO0NBQ1QsZUFBZTtDQUNmLFlBQVk7Q0FDWixvQ0FBNEI7U0FBNUIsNEJBQTRCO0NBQzVCLGtCQUFrQjtJQUNmO0NBQ0g7QUFFQTs7Q0FFQSw2QkFBNkI7Q0FDN0IsaUNBQWlDO0NBQ2pDLHVCQUF1QjtJQUNwQiwwQkFBMEI7SUFDMUIsVUFBVTtDQUNiLGlDQUFpQztDQUNqQyxtQkFBbUI7Q0FDbkIsZUFBZTtJQUNaLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osbUJBQW1CO0NBQ3RCLG9DQUE0QjtTQUE1Qiw0QkFBNEI7Q0FDNUI7QUFFQTs7Q0FFQSw2QkFBNkI7Q0FDN0IsaUNBQWlDO0NBQ2pDLHVCQUF1QjtJQUNwQiwwQkFBMEI7SUFDMUIsVUFBVTtDQUNiLGlDQUFpQztDQUNqQyxtQkFBbUI7Q0FDbkIsZUFBZTtJQUNaLFlBQVk7SUFDWixtQkFBbUI7SUFDbkIsa0JBQWtCO0NBQ3JCLG9DQUE0QjtTQUE1Qiw0QkFBNEI7Q0FDNUI7QUFFQTs7Q0FFQSw2QkFBNkI7Q0FDN0IsaUNBQWlDO0NBQ2pDLHVCQUF1QjtJQUNwQiwwQkFBMEI7SUFDMUIsVUFBVTtDQUNiLGlDQUFpQztDQUNqQyxtQkFBbUI7Q0FDbkIsU0FBUztDQUNULGVBQWU7SUFDWixrQkFBa0I7SUFDbEIsbUJBQW1CO0NBQ3RCLG9DQUE0QjtTQUE1Qiw0QkFBNEI7Q0FDNUI7QUFFQTs7Q0FFQSw2QkFBNkI7Q0FDN0IsaUNBQWlDO0NBQ2pDLHVCQUF1QjtJQUNwQiwwQkFBMEI7SUFDMUIsVUFBVTtDQUNiLGlDQUFpQztDQUNqQyxtQkFBbUI7Q0FDbkIsU0FBUztDQUNULGVBQWU7SUFDWixtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLGtCQUFrQjtDQUNyQixvQ0FBNEI7U0FBNUIsNEJBQTRCO0NBQzVCO0FBRUE7O0NBRUEsNkJBQTZCO0NBQzdCLGlDQUFpQztDQUNqQyx1QkFBdUI7SUFDcEIsMEJBQTBCO0lBQzFCLFVBQVU7Q0FDYixpQ0FBaUM7Q0FDakMsbUJBQW1CO0NBQ25CLFNBQVM7Q0FDVCxlQUFlOztJQUVaLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsbUJBQW1CO0NBQ3RCLG9DQUE0QjtTQUE1Qiw0QkFBNEI7Q0FDNUI7QUFFQTs7Q0FFQSw2QkFBNkI7Q0FDN0IsaUNBQWlDO0NBQ2pDLHVCQUF1QjtJQUNwQiwwQkFBMEI7SUFDMUIsVUFBVTtDQUNiLGlDQUFpQztDQUNqQyxtQkFBbUI7Q0FDbkIsU0FBUztDQUNULGVBQWU7SUFDWixZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLGtCQUFrQjtDQUNyQixvQ0FBNEI7U0FBNUIsNEJBQTRCO0NBQzVCO0FBRUE7O0NBRUEsNkJBQTZCO0NBQzdCLGlDQUFpQztDQUNqQyx1QkFBdUI7SUFDcEIsMEJBQTBCO0lBQzFCLFVBQVU7Q0FDYixpQ0FBaUM7Q0FDakMsbUJBQW1CO0NBQ25CLFNBQVM7Q0FDVCxlQUFlO0lBQ1osbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixrQkFBa0I7Q0FDckIsb0NBQTRCO1NBQTVCLDRCQUE0QjtDQUM1QjtBQUVBOztDQUVBLDZCQUE2QjtDQUM3QixpQ0FBaUM7Q0FDakMsdUJBQXVCO0lBQ3BCLDBCQUEwQjtJQUMxQixVQUFVO0NBQ2IsaUNBQWlDO0NBQ2pDLG1CQUFtQjtDQUNuQixTQUFTO0NBQ1QsZUFBZTtDQUNmLG1CQUFtQjtJQUNoQixZQUFZO0lBQ1osa0JBQWtCO0NBQ3JCLG9DQUE0QjtTQUE1Qiw0QkFBNEI7Q0FDNUI7QUFFQTs7Q0FFQSw2QkFBNkI7Q0FDN0IsaUNBQWlDO0NBQ2pDLHVCQUF1QjtJQUNwQiwwQkFBMEI7SUFDMUIsVUFBVTtDQUNiLGlDQUFpQztDQUNqQyxtQkFBbUI7Q0FDbkIsU0FBUztDQUNULGVBQWU7SUFDWixZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLGtCQUFrQjtDQUNyQixvQ0FBNEI7U0FBNUIsNEJBQTRCO0NBQzVCO0FBRUE7O0NBRUEsNkJBQTZCO0NBQzdCLGlDQUFpQztDQUNqQyx1QkFBdUI7SUFDcEIsMEJBQTBCO0lBQzFCLFVBQVU7Q0FDYixpQ0FBaUM7Q0FDakMsbUJBQW1CO0NBQ25CLFNBQVM7Q0FDVCxlQUFlO0lBQ1osWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixrQkFBa0I7O0NBRXJCLG9DQUE0Qjs7U0FBNUIsNEJBQTRCO0NBQzVCO0FBRUE7O0NBRUEsNkJBQTZCO0NBQzdCLGlDQUFpQztDQUNqQyx1QkFBdUI7SUFDcEIsMEJBQTBCO0lBQzFCLFVBQVU7Q0FDYixpQ0FBaUM7Q0FDakMsbUJBQW1CO0NBQ25CLFNBQVM7Q0FDVCxlQUFlO0lBQ1osWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixrQkFBa0I7Q0FDckIsb0NBQTRCO1NBQTVCLDRCQUE0QjtDQUM1QjtBQUVBOztDQUVBLDZCQUE2QjtDQUM3QixpQ0FBaUM7Q0FDakMsdUJBQXVCO0lBQ3BCLDBCQUEwQjtJQUMxQixVQUFVO0NBQ2IsaUNBQWlDO0NBQ2pDLG1CQUFtQjtDQUNuQixTQUFTO0NBQ1QsZUFBZTtJQUNaLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osa0JBQWtCO0NBQ3JCLG9DQUE0QjtTQUE1Qiw0QkFBNEI7Q0FDNUI7QUFHQTs7Q0FFQSw2QkFBNkI7Q0FDN0IsaUNBQWlDO0NBQ2pDLHVCQUF1QjtJQUNwQiwwQkFBMEI7SUFDMUIsVUFBVTtDQUNiLGlDQUFpQztDQUNqQyxtQkFBbUI7Q0FDbkIsU0FBUztDQUNULGVBQWU7SUFDWixZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLG1CQUFtQjtDQUN0QixvQ0FBNEI7U0FBNUIsNEJBQTRCO0NBQzVCO0FBSUE7O0NBRUEsNkJBQTZCO0NBQzdCLGlDQUFpQztDQUNqQyx1QkFBdUI7SUFDcEIsMEJBQTBCO0lBQzFCLFVBQVU7Q0FDYixpQ0FBaUM7Q0FDakMsbUJBQW1CO0NBQ25CLFFBQVE7SUFDTCxlQUFlO0lBQ2YsWUFBWTtDQUNmLGtCQUFrQjtDQUNsQixtQkFBbUI7Q0FDbkIsb0NBQTRCO1NBQTVCLDRCQUE0QjtDQUM1QjtBQUVBLDJCQUEyQjtBQUMzQjs7RUFFQyxhQUFhO0VBQ2IsZUFBZTtFQUNmLFVBQVU7RUFDVixPQUFPO0VBQ1AsTUFBTTtFQUNOLFdBQVc7RUFDWCxvQkFBb0I7RUFDcEIsaUNBQWlDOztDQUVsQztBQUVBLG9CQUFvQjtBQUNwQjs7RUFFQyxrQ0FBa0M7RUFDbEMsNEJBQTRCO0VBQzVCLHdCQUF3QjtFQUN4QixzQkFBc0I7RUFDdEIsV0FBVztFQUNYLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsWUFBWTtDQUNiO0FBR0E7O0VBRUMsa0JBQWtCO0VBQ2xCLE1BQU07RUFDTixZQUFZO0VBQ1osZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixhQUFhO0VBQ2IsaUJBQWlCO0NBQ2xCLGVBQWU7Q0FDZjtBQUdBOztFQUVDLFVBQVU7RUFDVixlQUFlO0NBQ2hCO0FBRUQ7O0NBRUMsa0JBQWtCOztDQUVsQixNQUFNO0NBQ04sWUFBWTtDQUNaLGVBQWU7Q0FDZixpQkFBaUI7Q0FDakIsYUFBYTtDQUNiLGlCQUFpQjtJQUNkLGdCQUFnQjtDQUNuQixXQUFXO0NBQ1gseUJBQXlCO0FBQzFCO0FBRUE7O0NBRUMsVUFBVTtDQUNWLGVBQWU7QUFDaEI7QUFFQTs7Q0FFQyxrQkFBa0I7O0NBRWxCLE1BQU07Q0FDTixZQUFZO0NBQ1osZUFBZTtDQUNmLGlCQUFpQjtDQUNqQixhQUFhO0NBQ2IsaUJBQWlCO0lBQ2QsZ0JBQWdCO0NBQ25CLFdBQVc7Q0FDWCx5QkFBeUI7QUFDMUI7QUFFQTs7Q0FFQyxVQUFVO0NBQ1YsZUFBZTtBQUNoQjtBQUNDLGlDQUFpQztBQUNoQzs7Ozs7Ozs7OztHQVVDO0FBQ0g7O0NBRUMsa0JBQWtCOztDQUVsQixNQUFNO0NBQ04sWUFBWTtDQUNaLGVBQWU7Q0FDZixpQkFBaUI7Q0FDakIsYUFBYTtDQUNiLGlCQUFpQjtJQUNkLGdCQUFnQjtDQUNuQixXQUFXO0NBQ1gseUJBQXlCO0FBQzFCO0FBRUE7O0NBRUMsVUFBVTtDQUNWLGVBQWU7QUFDaEI7QUFFQTs7Q0FFQyxrQkFBa0I7O0NBRWxCLE1BQU07Q0FDTixZQUFZO0NBQ1osZUFBZTtDQUNmLGlCQUFpQjtDQUNqQixhQUFhO0lBQ1YsaUJBQWlCO0lBQ2pCLGdCQUFnQjtBQUNwQjtBQUNBOztFQUVFLFVBQVU7RUFDVixlQUFlO0NBQ2hCO0FBRUQ7O0NBRUMsa0JBQWtCOztDQUVsQixNQUFNO0NBQ04sWUFBWTtDQUNaLGVBQWU7Q0FDZixpQkFBaUI7Q0FDakIsYUFBYTtJQUNWLGlCQUFpQjtJQUNqQixnQkFBZ0I7Q0FDbkIsV0FBVztDQUNYLHlCQUF5QjtBQUMxQjtBQUNBOztFQUVFLFVBQVU7RUFDVixlQUFlO0NBQ2hCO0FBRUQ7O0VBRUUsa0JBQWtCOztFQUVsQixNQUFNO0VBQ04sWUFBWTtFQUNaLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsYUFBYTtFQUNiLGlCQUFpQjtFQUNqQixlQUFlO0NBQ2hCO0FBR0E7O0VBRUMsVUFBVTtFQUNWLGVBQWU7Q0FDaEI7QUFHRDs7Q0FFQyxrQkFBa0I7Q0FDbEIsTUFBTTtDQUNOLFlBQVk7Q0FDWixlQUFlO0NBQ2YsaUJBQWlCO0NBQ2pCLGFBQWE7SUFDVixpQkFBaUI7SUFDakIsZ0JBQWdCO0NBQ25CLFdBQVc7Q0FDWCx5QkFBeUI7QUFDMUI7QUFFQTs7Q0FFQyxVQUFVO0NBQ1YsZUFBZTtBQUNoQjtBQUdBOztDQUVDLGtCQUFrQjs7Q0FFbEIsTUFBTTtDQUNOLFlBQVk7Q0FDWixlQUFlO0NBQ2YsaUJBQWlCO0NBQ2pCLGFBQWE7Q0FDYixpQkFBaUI7SUFDZCxnQkFBZ0I7Q0FDbkIsV0FBVztDQUNYLHlCQUF5QjtBQUMxQjtBQUVBOztDQUVDLFVBQVU7Q0FDVixlQUFlO0FBQ2hCO0FBR0E7O0NBRUMsa0JBQWtCO0NBQ2xCLE1BQU07Q0FDTixZQUFZO0NBQ1osZUFBZTtDQUNmLGlCQUFpQjtDQUNqQixhQUFhO0NBQ2IsaUJBQWlCO0lBQ2QsZUFBZTtDQUNsQixXQUFXO0NBQ1gseUJBQXlCO0FBQzFCO0FBRUE7O0NBRUMsVUFBVTtDQUNWLGVBQWU7QUFDaEI7QUFFQTs7RUFFRSxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLE1BQU07RUFDTixZQUFZO0VBQ1osZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixhQUFhO0VBQ2IsaUJBQWlCO0VBQ2pCLGVBQWU7Q0FDaEI7QUFHQTs7RUFFQyxVQUFVO0VBQ1YsZUFBZTtDQUNoQjtBQUdBOztFQUVDLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sWUFBWTtFQUNaLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsYUFBYTtFQUNiLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsV0FBVztFQUNYLHlCQUF5QjtDQUMxQjtBQUVBOztFQUVDLFVBQVU7RUFDVixlQUFlO0NBQ2hCO0FBSUE7O0VBRUMsa0JBQWtCOztFQUVsQixNQUFNO0VBQ04sWUFBWTtFQUNaLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsYUFBYTtFQUNiLGlCQUFpQjtLQUNkLGVBQWU7RUFDbEIsV0FBVztFQUNYLHlCQUF5QjtDQUMxQjtBQUVBOztFQUVDLFVBQVU7RUFDVixlQUFlO0NBQ2hCO0FBR0E7O0VBRUMsa0JBQWtCOztFQUVsQixNQUFNO0VBQ04sWUFBWTtFQUNaLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsYUFBYTtFQUNiLGlCQUFpQjtFQUNqQixlQUFlO0NBQ2hCO0FBR0E7O0VBRUMsVUFBVTtFQUNWLGVBQWU7Q0FDaEI7QUFHQTs7RUFFQyxrQkFBa0I7O0VBRWxCLE1BQU07RUFDTixZQUFZO0VBQ1osZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixhQUFhO0VBQ2IsaUJBQWlCO0VBQ2pCLGVBQWU7Q0FDaEI7QUFHQTs7RUFFQyxVQUFVO0VBQ1YsZUFBZTtDQUNoQjtBQUdBOztFQUVDLGtCQUFrQjs7RUFFbEIsTUFBTTtFQUNOLFlBQVk7RUFDWixlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLGFBQWE7RUFDYixpQkFBaUI7RUFDakIsZUFBZTtDQUNoQjtBQUdBOztFQUVDLFVBQVU7RUFDVixlQUFlO0NBQ2hCO0FBQ0EsdUJBQXVCO0FBQ3ZCOztFQUVDLDRCQUFvQjtVQUFwQixvQkFBb0I7Q0FDckI7QUFDQTs7RUFFQyxNQUFNLDJCQUFrQixDQUFsQixtQkFBbUI7RUFDekIsSUFBSSwyQkFBa0IsQ0FBbEIsbUJBQW1CO0NBQ3hCO0FBSkE7O0VBRUMsTUFBTSwyQkFBa0IsQ0FBbEIsbUJBQW1CO0VBQ3pCLElBQUksMkJBQWtCLENBQWxCLG1CQUFtQjtDQUN4QjtBQUdBOztFQUVDLGlCQUFpQjtDQUNsQiIsImZpbGUiOiJzcmMvYXBwL01haW5fbWFzdGVycy9tYWluLW1hc3Rlci9tYWluLW1hc3Rlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1haW4tbWFzdGVyXG57XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuYVxue1xuXHR0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG4vKiBNYWluIENvbnRhaW5lciAtICB0aGlzIGNvbnRyb2xzIHRoZSBzaXplIG9mIHRoZSBjaXJjbGUgKi9cbi5jaXJjbGVfY29udGFpbmVyXG57XG5cdHdpZHRoIDogMjEwcHg7XG5cdGhlaWdodCA6MjEwcHg7XG5cdG1hcmdpbiA6IDAgYXV0bztcbiAgICBwYWRkaW5nIDogMDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHBhZGRpbmc6IDIwcHg7XG4gICAgXG4vKlx0Ym9yZGVyIDogMXB4IHNvbGlkIHJlZDsgKi9cbn1cbi5jaXJjbGVfY29udGFpbmVyIGF7XG5cdHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cblxuLyogQ2lyY2xlIE1haW4gZHJhd3MgdGhlIGFjdHVhbCBjaXJjbGUgKi9cbi5jaXJjbGVfbWFpblxue1xuXHR3aWR0aCA6IDEwMCU7XG5cdGJhY2tncm91bmQtY29sb3I6ICMzZWNlOTg7XG5cdGhlaWdodCA6IDEwMCU7XG5cdGJvcmRlci1yYWRpdXMgOiA1MCU7XG5cdGJvcmRlciA6IDZweCBzb2xpZCAjZmZmZmZmO1x0LyogY2FuIGFsdGVyIHRoaWNrbmVzcyBhbmQgY29sb3VyIG9mIGNpcmNsZSBvbiB0aGlzIGxpbmUgKi9cblx0bWFyZ2luIDogMCBhdXRvO1xuICAgIHBhZGRpbmcgOiAwO1xuXHR0ZXh0LWFsaWduOiBjZW50ZXI7XG5cdG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi8qIENpcmNsZSBUZXh0IENvbnRhaW5lciAtIGNvbnN0cmFpbnMgdGV4dCBhcmVhIHRvIHdpdGhpbiB0aGUgY2lyY2xlICovXG4uY2lyY2xlX3RleHRfY29udGFpbmVyXG57XG5cdC8qIGFyZWEgY29uc3RyYWludHMgKi9cblx0d2lkdGggOiA3MCU7XG5cdGhlaWdodCA6IDcwJTtcblx0bWF4LXdpZHRoIDogNzAlO1xuXHRtYXgtaGVpZ2h0IDogNzAlO1xuXHRtYXJnaW4gOiAwO1xuXHRwYWRkaW5nIDogMDtcblx0Lyogc29tZSBwb3NpdGlvbiBudWRnaW5nIHRvIGNlbnRlciB0aGUgdGV4dCBhcmVhICovXG5cdHBvc2l0aW9uIDogcmVsYXRpdmU7XG5cdGxlZnQgOiAxNSU7XG5cdHRvcCA6IDE1JTtcblx0LyogcHJlc2VydmUgM2QgcHJldmVudHMgYmx1cnJpbmcgc29tZXRpbWVzIGNhdXNlZCBieSB0aGUgdGV4dCBjZW50ZXJpbmcgaW4gdGhlIG5leHQgY2xhc3MgKi9cblx0dHJhbnNmb3JtLXN0eWxlIDogcHJlc2VydmUtM2Q7XG5cdC8qYm9yZGVyIDogMXB4IHNvbGlkIGdyZWVuOyovXG59XG5cbi8qIENpcmNsZSBUZXh0IC0gdGhlIGFwcGVhcmFuY2Ugb2YgdGhlIHRleHQgd2l0aGluIHRoZSBjaXJjbGUgcGx1cyB2ZXJ0aWNhbCBjZW50ZXJpbmcgKi9cbi5jaXJjbGVfdGV4dFxue1xuXHQvKiBjaGFuZ2UgZm9udC9zaXplL2V0YyBoZXJlICovXG5cdGZvbnQ6IDE4cHggXCJUYWhvbWFcIiwgQXJpYWwsIFNlcmlmO1x0XG4gICAgdGV4dC1hbGlnbiA6IGNlbnRlcjtcbiAgICBjb2xvcjojZmZmO1xuXHQvKiB2ZXJ0aWNhbCBjZW50ZXJpbmcgdGVjaG5pcXVlICovXG5cdHBvc2l0aW9uIDogcmVsYXRpdmU7XG5cdHRvcCA6IDU4JTtcblx0dHJhbnNmb3JtIDogdHJhbnNsYXRlWSgtNTAlKTtcbn1cblxuXG4vKiBQb3B1cCAqL1xuXG4uY2lyY2xlX2NvbnRhaW5lcjFcbntcblx0d2lkdGggOiAxMzBweDtcblx0aGVpZ2h0IDogMTMwcHg7XG5cdG1hcmdpbiA6IDAgYXV0bztcbiAgICBwYWRkaW5nIDogMDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgLyogZGlzcGxheTogaW5saW5lLWJsb2NrOyAqL1xuXHQvKiBwYWRkaW5nOiAyMHB4OyAqL1xuXHRwYWRkaW5nOiA1cHg7XG5cdGZvbnQtc2l6ZTogMTVweDtcblx0XG4vKlx0Ym9yZGVyIDogMXB4IHNvbGlkIHJlZDsgKi9cbn1cbi5jaXJjbGVfY29udGFpbmVyMSBhe1xuXHR0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbi8qIENpcmNsZSBNYWluIGRyYXdzIHRoZSBhY3R1YWwgY2lyY2xlICovXG4uY2lyY2xlX21haW4xXG57XG5cdHdpZHRoIDogMTAwJTtcblx0YmFja2dyb3VuZC1jb2xvcjogIzNlY2U5ODtcblx0aGVpZ2h0IDogMTAwJTtcblx0Ym9yZGVyLXJhZGl1cyA6IDUwJTtcblx0Ym9yZGVyIDogNnB4IHNvbGlkICNmZmZmZmY7XHQvKiBjYW4gYWx0ZXIgdGhpY2tuZXNzIGFuZCBjb2xvdXIgb2YgY2lyY2xlIG9uIHRoaXMgbGluZSAqL1xuXHRtYXJnaW4gOiAwIGF1dG87XG4gICAgcGFkZGluZyA6IDA7XG5cdHRleHQtYWxpZ246IGNlbnRlcjtcblx0b3ZlcmZsb3c6IGhpZGRlbjtcbn1cblxuLyogQ2lyY2xlIFRleHQgQ29udGFpbmVyIC0gY29uc3RyYWlucyB0ZXh0IGFyZWEgdG8gd2l0aGluIHRoZSBjaXJjbGUgKi9cbi5jaXJjbGVfdGV4dF9jb250YWluZXIxXG57XG5cdC8qIGFyZWEgY29uc3RyYWludHMgKi9cblx0d2lkdGggOiA3MCU7XG5cdGhlaWdodCA6IDcwJTtcblx0bWF4LXdpZHRoIDogNzAlO1xuXHRtYXgtaGVpZ2h0IDogNzAlO1xuXHRtYXJnaW4gOiAwO1xuXHRwYWRkaW5nIDogMDtcblx0Lyogc29tZSBwb3NpdGlvbiBudWRnaW5nIHRvIGNlbnRlciB0aGUgdGV4dCBhcmVhICovXG5cdHBvc2l0aW9uIDogcmVsYXRpdmU7XG5cdGxlZnQgOiAxNSU7XG5cdHRvcCA6IDE1JTtcblx0XG5cdC8qIHByZXNlcnZlIDNkIHByZXZlbnRzIGJsdXJyaW5nIHNvbWV0aW1lcyBjYXVzZWQgYnkgdGhlIHRleHQgY2VudGVyaW5nIGluIHRoZSBuZXh0IGNsYXNzICovXG5cdHRyYW5zZm9ybS1zdHlsZSA6IHByZXNlcnZlLTNkO1xuXHRcblx0Lypib3JkZXIgOiAxcHggc29saWQgZ3JlZW47Ki9cbn1cblxuLyogQ2lyY2xlIFRleHQgLSB0aGUgYXBwZWFyYW5jZSBvZiB0aGUgdGV4dCB3aXRoaW4gdGhlIGNpcmNsZSBwbHVzIHZlcnRpY2FsIGNlbnRlcmluZyAqL1xuLmNpcmNsZV90ZXh0MVxue1xuXHQvKiBjaGFuZ2UgZm9udC9zaXplL2V0YyBoZXJlICovXG5cdGZvbnQ6IDE4cHggXCJUYWhvbWFcIiwgQXJpYWwsIFNlcmlmO1x0XG4gICAgdGV4dC1hbGlnbiA6IGNlbnRlcjtcbiAgICBjb2xvcjojZmZmO1xuXHRcblx0LyogdmVydGljYWwgY2VudGVyaW5nIHRlY2huaXF1ZSAqL1xuXHRwb3NpdGlvbiA6IHJlbGF0aXZlO1xuXHR0b3AgOiA1MCU7XG5cdHRyYW5zZm9ybSA6IHRyYW5zbGF0ZVkoLTUwJSk7XG59XG5cbi8qIFBvcHVwICovXG5cbi5jaXJjbGVfY29udGFpbmVyMlxue1xuXHR3aWR0aCA6IDIwMHB4O1xuXHRoZWlnaHQgOiAyMDBweDtcblx0bWFyZ2luIDogMCBhdXRvO1xuICAgIHBhZGRpbmcgOiAwO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgcGFkZGluZzogMjBweDtcbiAgICBcbi8qXHRib3JkZXIgOiAxcHggc29saWQgcmVkOyAqL1xufVxuLmNpcmNsZV9jb250YWluZXIyIGFcbntcblx0dGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuXG4vKiBDaXJjbGUgTWFpbiBkcmF3cyB0aGUgYWN0dWFsIGNpcmNsZSAqL1xuLmNpcmNsZV9tYWluMlxue1xuXHR3aWR0aCA6IDEwMCU7XG5cdGJhY2tncm91bmQtY29sb3I6ICMzZWNlOTg7XG5cdGhlaWdodCA6IDEwMCU7XG5cdGJvcmRlci1yYWRpdXMgOiA1MCU7XG5cdGJvcmRlciA6IDZweCBzb2xpZCAjZmZmZmZmO1x0LyogY2FuIGFsdGVyIHRoaWNrbmVzcyBhbmQgY29sb3VyIG9mIGNpcmNsZSBvbiB0aGlzIGxpbmUgKi9cblx0bWFyZ2luIDogMCBhdXRvO1xuICAgIHBhZGRpbmcgOiAwO1xuXHR0ZXh0LWFsaWduOiBjZW50ZXI7XG5cdG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi8qIENpcmNsZSBUZXh0IENvbnRhaW5lciAtIGNvbnN0cmFpbnMgdGV4dCBhcmVhIHRvIHdpdGhpbiB0aGUgY2lyY2xlICovXG4uY2lyY2xlX3RleHRfY29udGFpbmVyMlxue1xuXHQvKiBhcmVhIGNvbnN0cmFpbnRzICovXG5cdHdpZHRoIDogNzAlO1xuXHRoZWlnaHQgOiA3MCU7XG5cdG1heC13aWR0aCA6IDcwJTtcblx0bWF4LWhlaWdodCA6IDcwJTtcblx0bWFyZ2luIDogMDtcblx0cGFkZGluZyA6IDA7XG5cdC8qIHNvbWUgcG9zaXRpb24gbnVkZ2luZyB0byBjZW50ZXIgdGhlIHRleHQgYXJlYSAqL1xuXHRwb3NpdGlvbiA6IHJlbGF0aXZlO1xuXHRsZWZ0IDogMTUlO1xuXHR0b3AgOiAxNSU7XG5cdC8qIHByZXNlcnZlIDNkIHByZXZlbnRzIGJsdXJyaW5nIHNvbWV0aW1lcyBjYXVzZWQgYnkgdGhlIHRleHQgY2VudGVyaW5nIGluIHRoZSBuZXh0IGNsYXNzICovXG5cdHRyYW5zZm9ybS1zdHlsZSA6IHByZXNlcnZlLTNkO1xuXHQvKmJvcmRlciA6IDFweCBzb2xpZCBncmVlbjsqL1xufVxuXG4vKiBDaXJjbGUgVGV4dCAtIHRoZSBhcHBlYXJhbmNlIG9mIHRoZSB0ZXh0IHdpdGhpbiB0aGUgY2lyY2xlIHBsdXMgdmVydGljYWwgY2VudGVyaW5nICovXG4uY2lyY2xlX3RleHQyXG57XG5cdC8qIGNoYW5nZSBmb250L3NpemUvZXRjIGhlcmUgKi9cblx0Zm9udDogMThweCBcIlRhaG9tYVwiLCBBcmlhbCwgU2VyaWY7XHRcbiAgICB0ZXh0LWFsaWduIDogY2VudGVyO1xuICAgIGNvbG9yOiNmZmY7XG5cdC8qIHZlcnRpY2FsIGNlbnRlcmluZyB0ZWNobmlxdWUgKi9cblx0cG9zaXRpb24gOiByZWxhdGl2ZTtcblx0dG9wIDogNTAlO1xuXHR0cmFuc2Zvcm0gOiB0cmFuc2xhdGVZKC01MCUpO1xufVxuXG4gXG4vKiBBc3NpZ24gTWFzdGVyICovXG4uYXNzaWduLW1hc3Rlclxue1xuXHR3aWR0aCA6IDIwMHB4O1xuXHRoZWlnaHQgOjEwMHB4O1xuXHRtYXJnaW4gOiAwIGF1dG87XG4gICAgcGFkZGluZyA6IDA7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBwYWRkaW5nOiAyMHB4O1xufVxuXG4vKiBBc3NpZ24gTWFzdGVyICovXG4uYWRtaW5pc3RyYXRvclxue1xuXHR3aWR0aCA6IDI1MHB4O1xuXHRoZWlnaHQgOiAxMjBweDtcblx0bWFyZ2luIDogMCBhdXRvO1xuICAgIHBhZGRpbmcgOiAwO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG5cdHBhZGRpbmc6IDIwcHg7XG5cdC8qIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50OyAqL1xuXHR0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbi53YXNoaW5nLW1hc3Rlclxue1xuXHR3aWR0aCA6IDMwMHB4O1xuXHRoZWlnaHQgOjEwMHB4O1xuXHRtYXJnaW4gOiAwIGF1dG87XG4gICAgcGFkZGluZyA6IDA7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcblx0cGFkZGluZzogMjBweDtcblx0LyogYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7ICovXG5cdHRleHQtZGVjb3JhdGlvbjogbm9uZTtcdFxufVxuXG4gLyogRnVsbCB3aWR0aCBpbm91dCBmaWVsZHMgKi9cbiBpbnB1dFt0eXBlPXRleHRdLCBpbnB1dFt0eXBlPXBhc3N3b3JkXVxuIHtcblx0IHdpZHRoOiA5MCU7XG5cdCBwYWRkaW5nOiAxMnB4IDIwcHg7XG5cdCBtYXJnaW46IDhweCAyMHB4O1xuXHQgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuXHQgYm9yZGVyOiAxcHggc29saWQgI2NjYztcblx0IGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG5cdCBmb250LXNpemU6IDEwcHg7XG5cdCBcbiB9XG4gXG4gXG4gXG4gLyogU2V0IGEgc3R5bGUgZm9yIGFsbCBidXR0b25zICovXG5cbiBidXR0b25cbiB7XG5cdGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50OyBcblx0Zm9udDogMThweCBcIlRhaG9tYVwiLCBBcmlhbCwgU2VyaWY7XHRcblx0dGV4dC1hbGlnbjogLW1vei1jZW50ZXI7XG4gICAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XG4gICAgY29sb3I6I2ZmZjtcblx0LyogdmVydGljYWwgY2VudGVyaW5nIHRlY2huaXF1ZSAqL1xuXHRwb3NpdGlvbiA6IHJlbGF0aXZlO1xuXHR0b3AgOiA1MCU7XG5cdG1hcmdpbi10b3A6IDUwJTtcblx0Ym9yZGVyOiBub25lO1xuXHR0cmFuc2Zvcm0gOiB0cmFuc2xhdGVZKC01MCUpO1xuIH1cblxuIC5idXR0b24xXG4ge1xuXHRiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDsgXG5cdGZvbnQ6IDE4cHggXCJUYWhvbWFcIiwgQXJpYWwsIFNlcmlmO1x0XG5cdHRleHQtYWxpZ246IC1tb3otY2VudGVyO1xuICAgIHRleHQtYWxpZ246IC13ZWJraXQtY2VudGVyO1xuICAgIGNvbG9yOiNmZmY7XG5cdC8qIHZlcnRpY2FsIGNlbnRlcmluZyB0ZWNobmlxdWUgKi9cblx0cG9zaXRpb24gOiByZWxhdGl2ZTtcblx0dG9wIDogNTAlO1xuXHRtYXJnaW4tdG9wOiA1MCU7XG5cdGJvcmRlcjogbm9uZTtcblx0dHJhbnNmb3JtIDogdHJhbnNsYXRlWSgtNTAlKTtcblx0bWFyZ2luLWxlZnQ6IC0yMHB4O1xuICAgIG1hcmdpbi1yaWdodDogLTIwcHhcbiB9XG5cbiAuYnV0dG9uMlxuIHtcblx0YmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7IFxuXHRmb250OiAxOHB4IFwiVGFob21hXCIsIEFyaWFsLCBTZXJpZjtcdFxuXHR0ZXh0LWFsaWduOiAtbW96LWNlbnRlcjtcbiAgICB0ZXh0LWFsaWduOiAtd2Via2l0LWNlbnRlcjtcbiAgICBjb2xvcjojZmZmO1xuXHQvKiB2ZXJ0aWNhbCBjZW50ZXJpbmcgdGVjaG5pcXVlICovXG5cdHBvc2l0aW9uIDogcmVsYXRpdmU7XG5cdG1hcmdpbi10b3A6IDMwJTtcbiAgICBtYXJnaW4tbGVmdDogLTIwcHg7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIG1hcmdpbi1yaWdodDogLTIwcHg7XG5cdHRyYW5zZm9ybSA6IHRyYW5zbGF0ZVkoLTUwJSk7XG4gfVxuXG4gLmJ1dHRvbjNcbiB7XG5cdGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50OyBcblx0Zm9udDogMThweCBcIlRhaG9tYVwiLCBBcmlhbCwgU2VyaWY7XHRcblx0dGV4dC1hbGlnbjogLW1vei1jZW50ZXI7XG4gICAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XG4gICAgY29sb3I6I2ZmZjtcblx0LyogdmVydGljYWwgY2VudGVyaW5nIHRlY2huaXF1ZSAqL1xuXHRwb3NpdGlvbiA6IHJlbGF0aXZlO1xuXHRtYXJnaW4tdG9wOiA1MCU7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIG1hcmdpbi1yaWdodDogLTIwcHg7XG4gICAgbWFyZ2luLWxlZnQ6IC0yMHB4O1xuXHR0cmFuc2Zvcm0gOiB0cmFuc2xhdGVZKC01MCUpO1xuIH1cblxuIC5idXR0b240XG4ge1xuXHRiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDsgXG5cdGZvbnQ6IDE4cHggXCJUYWhvbWFcIiwgQXJpYWwsIFNlcmlmO1x0XG5cdHRleHQtYWxpZ246IC1tb3otY2VudGVyO1xuICAgIHRleHQtYWxpZ246IC13ZWJraXQtY2VudGVyO1xuICAgIGNvbG9yOiNmZmY7XG5cdC8qIHZlcnRpY2FsIGNlbnRlcmluZyB0ZWNobmlxdWUgKi9cblx0cG9zaXRpb24gOiByZWxhdGl2ZTtcblx0dG9wIDogNTAlO1xuXHRtYXJnaW4tdG9wOiAzMCU7XG4gICAgbWFyZ2luLWxlZnQ6IC0yMHB4O1xuICAgIG1hcmdpbi1yaWdodDogLTIwcHg7XG5cdHRyYW5zZm9ybSA6IHRyYW5zbGF0ZVkoLTUwJSk7XG4gfVxuXG4gLmJ1dHRvbjVcbiB7XG5cdGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50OyBcblx0Zm9udDogMThweCBcIlRhaG9tYVwiLCBBcmlhbCwgU2VyaWY7XHRcblx0dGV4dC1hbGlnbjogLW1vei1jZW50ZXI7XG4gICAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XG4gICAgY29sb3I6I2ZmZjtcblx0LyogdmVydGljYWwgY2VudGVyaW5nIHRlY2huaXF1ZSAqL1xuXHRwb3NpdGlvbiA6IHJlbGF0aXZlO1xuXHR0b3AgOiA1MCU7XG5cdG1hcmdpbi10b3A6IDUwJTtcbiAgICBtYXJnaW4tcmlnaHQ6IC0yMHB4O1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBtYXJnaW4tbGVmdDogLTIwcHg7XG5cdHRyYW5zZm9ybSA6IHRyYW5zbGF0ZVkoLTUwJSk7XG4gfVxuXG4gLmJ1dHRvbjZcbiB7XG5cdGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50OyBcblx0Zm9udDogMThweCBcIlRhaG9tYVwiLCBBcmlhbCwgU2VyaWY7XHRcblx0dGV4dC1hbGlnbjogLW1vei1jZW50ZXI7XG4gICAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XG4gICAgY29sb3I6I2ZmZjtcblx0LyogdmVydGljYWwgY2VudGVyaW5nIHRlY2huaXF1ZSAqL1xuXHRwb3NpdGlvbiA6IHJlbGF0aXZlO1xuXHR0b3AgOiA1MCU7XG5cdG1hcmdpbi10b3A6IDYwJTtcblxuICAgIGJvcmRlcjogbm9uZTtcbiAgICBtYXJnaW4tbGVmdDogLTIwcHg7XG4gICAgbWFyZ2luLXJpZ2h0OiAtMjBweDtcblx0dHJhbnNmb3JtIDogdHJhbnNsYXRlWSgtNTAlKTtcbiB9XG5cbiAuYnV0dG9uN1xuIHtcblx0YmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7IFxuXHRmb250OiAxOHB4IFwiVGFob21hXCIsIEFyaWFsLCBTZXJpZjtcdFxuXHR0ZXh0LWFsaWduOiAtbW96LWNlbnRlcjtcbiAgICB0ZXh0LWFsaWduOiAtd2Via2l0LWNlbnRlcjtcbiAgICBjb2xvcjojZmZmO1xuXHQvKiB2ZXJ0aWNhbCBjZW50ZXJpbmcgdGVjaG5pcXVlICovXG5cdHBvc2l0aW9uIDogcmVsYXRpdmU7XG5cdHRvcCA6IDUwJTtcblx0bWFyZ2luLXRvcDogMzAlO1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBtYXJnaW4tcmlnaHQ6IC0yMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAtMjBweDtcblx0dHJhbnNmb3JtIDogdHJhbnNsYXRlWSgtNTAlKTtcbiB9XG5cbiAuYnV0dG9uOFxuIHtcblx0YmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7IFxuXHRmb250OiAxOHB4IFwiVGFob21hXCIsIEFyaWFsLCBTZXJpZjtcdFxuXHR0ZXh0LWFsaWduOiAtbW96LWNlbnRlcjtcbiAgICB0ZXh0LWFsaWduOiAtd2Via2l0LWNlbnRlcjtcbiAgICBjb2xvcjojZmZmO1xuXHQvKiB2ZXJ0aWNhbCBjZW50ZXJpbmcgdGVjaG5pcXVlICovXG5cdHBvc2l0aW9uIDogcmVsYXRpdmU7XG5cdHRvcCA6IDUwJTtcblx0bWFyZ2luLXRvcDogMzAlO1xuICAgIG1hcmdpbi1yaWdodDogLTIwcHg7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIG1hcmdpbi1sZWZ0OiAtMjBweDtcblx0dHJhbnNmb3JtIDogdHJhbnNsYXRlWSgtNTAlKTtcbiB9XG5cbiAuYnV0dG9uOVxuIHtcblx0YmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7IFxuXHRmb250OiAxOHB4IFwiVGFob21hXCIsIEFyaWFsLCBTZXJpZjtcdFxuXHR0ZXh0LWFsaWduOiAtbW96LWNlbnRlcjtcbiAgICB0ZXh0LWFsaWduOiAtd2Via2l0LWNlbnRlcjtcbiAgICBjb2xvcjojZmZmO1xuXHQvKiB2ZXJ0aWNhbCBjZW50ZXJpbmcgdGVjaG5pcXVlICovXG5cdHBvc2l0aW9uIDogcmVsYXRpdmU7XG5cdHRvcCA6IDUwJTtcblx0bWFyZ2luLXRvcDogMzAlO1xuXHRtYXJnaW4tcmlnaHQ6IC0yMHB4O1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBtYXJnaW4tbGVmdDogLTIwcHg7XG5cdHRyYW5zZm9ybSA6IHRyYW5zbGF0ZVkoLTUwJSk7XG4gfVxuXG4gLmJ1dHRvbjEwXG4ge1xuXHRiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDsgIFxuXHRmb250OiAxOHB4IFwiVGFob21hXCIsIEFyaWFsLCBTZXJpZjtcdFxuXHR0ZXh0LWFsaWduOiAtbW96LWNlbnRlcjtcbiAgICB0ZXh0LWFsaWduOiAtd2Via2l0LWNlbnRlcjtcbiAgICBjb2xvcjojZmZmO1xuXHQvKiB2ZXJ0aWNhbCBjZW50ZXJpbmcgdGVjaG5pcXVlICovXG5cdHBvc2l0aW9uIDogcmVsYXRpdmU7XG5cdHRvcCA6IDUwJTtcblx0bWFyZ2luLXRvcDogMzAlO1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBtYXJnaW4tcmlnaHQ6IC0yMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAtMjBweDtcblx0dHJhbnNmb3JtIDogdHJhbnNsYXRlWSgtNTAlKTtcbiB9XG5cbiAuYnV0dG9uMTFcbiB7XG5cdGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50OyBcblx0Zm9udDogMThweCBcIlRhaG9tYVwiLCBBcmlhbCwgU2VyaWY7XHRcblx0dGV4dC1hbGlnbjogLW1vei1jZW50ZXI7XG4gICAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XG4gICAgY29sb3I6I2ZmZjtcblx0LyogdmVydGljYWwgY2VudGVyaW5nIHRlY2huaXF1ZSAqL1xuXHRwb3NpdGlvbiA6IHJlbGF0aXZlO1xuXHR0b3AgOiA1MCU7XG5cdG1hcmdpbi10b3A6IDUwJTtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgbWFyZ2luLXJpZ2h0OiAtMjBweDtcbiAgICBtYXJnaW4tbGVmdDogLTIwcHg7XG5cblx0dHJhbnNmb3JtIDogdHJhbnNsYXRlWSgtNTAlKTtcbiB9XG5cbiAuYnV0dG9uMTJcbiB7XG5cdGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50OyBcblx0Zm9udDogMThweCBcIlRhaG9tYVwiLCBBcmlhbCwgU2VyaWY7XHRcblx0dGV4dC1hbGlnbjogLW1vei1jZW50ZXI7XG4gICAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XG4gICAgY29sb3I6I2ZmZjtcblx0LyogdmVydGljYWwgY2VudGVyaW5nIHRlY2huaXF1ZSAqL1xuXHRwb3NpdGlvbiA6IHJlbGF0aXZlO1xuXHR0b3AgOiA1MCU7XG5cdG1hcmdpbi10b3A6IDMwJTtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgbWFyZ2luLXJpZ2h0OiAtMjBweDtcbiAgICBtYXJnaW4tbGVmdDogLTIwcHg7XG5cdHRyYW5zZm9ybSA6IHRyYW5zbGF0ZVkoLTUwJSk7XG4gfVxuXG4gLmJ1dHRvbjEzXG4ge1xuXHRiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDsgIFxuXHRmb250OiAxOHB4IFwiVGFob21hXCIsIEFyaWFsLCBTZXJpZjtcdFxuXHR0ZXh0LWFsaWduOiAtbW96LWNlbnRlcjtcbiAgICB0ZXh0LWFsaWduOiAtd2Via2l0LWNlbnRlcjtcbiAgICBjb2xvcjojZmZmO1xuXHQvKiB2ZXJ0aWNhbCBjZW50ZXJpbmcgdGVjaG5pcXVlICovXG5cdHBvc2l0aW9uIDogcmVsYXRpdmU7XG5cdHRvcCA6IDUwJTtcblx0bWFyZ2luLXRvcDogMzAlO1xuICAgIG1hcmdpbi1yaWdodDogLTIwcHg7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIG1hcmdpbi1sZWZ0OiAtMjBweDtcblx0dHJhbnNmb3JtIDogdHJhbnNsYXRlWSgtNTAlKTtcbiB9XG5cbiBcbiAuYnV0dG9uMTRcbiB7XG5cdGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50OyBcblx0Zm9udDogMThweCBcIlRhaG9tYVwiLCBBcmlhbCwgU2VyaWY7XHRcblx0dGV4dC1hbGlnbjogLW1vei1jZW50ZXI7XG4gICAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XG4gICAgY29sb3I6I2ZmZjtcblx0LyogdmVydGljYWwgY2VudGVyaW5nIHRlY2huaXF1ZSAqL1xuXHRwb3NpdGlvbiA6IHJlbGF0aXZlO1xuXHR0b3AgOiA1MCU7XG5cdG1hcmdpbi10b3A6IDMwJTtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xuICAgIG1hcmdpbi1yaWdodDogLTEwcHg7XG5cdHRyYW5zZm9ybSA6IHRyYW5zbGF0ZVkoLTUwJSk7XG4gfVxuXG5cbiBcbiAuYnV0dG9uMTVcbiB7XG5cdGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50OyBcblx0Zm9udDogMThweCBcIlRhaG9tYVwiLCBBcmlhbCwgU2VyaWY7XHRcblx0dGV4dC1hbGlnbjogLW1vei1jZW50ZXI7XG4gICAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XG4gICAgY29sb3I6I2ZmZjtcblx0LyogdmVydGljYWwgY2VudGVyaW5nIHRlY2huaXF1ZSAqL1xuXHRwb3NpdGlvbiA6IHJlbGF0aXZlO1xuXHR0b3A6IDUwJTtcbiAgICBtYXJnaW4tdG9wOiA0MCU7XG4gICAgYm9yZGVyOiBub25lO1xuXHRtYXJnaW4tbGVmdDogLTMwcHg7XG5cdG1hcmdpbi1yaWdodDogLTMwcHg7XG5cdHRyYW5zZm9ybSA6IHRyYW5zbGF0ZVkoLTUwJSk7XG4gfVxuXG4gLyogdGhlIE1vZGFsIChiYWNrZ3JvdW5kKSAqL1xuIC5tb2RhbFxuIHtcblx0IGRpc3BsYXk6IG5vbmU7XG5cdCBwb3NpdGlvbjogZml4ZWQ7XG5cdCB6LWluZGV4OiAxO1xuXHQgbGVmdDogMDtcblx0IHRvcDogMDtcblx0IHdpZHRoOiAxMDAlO1xuXHQgLyogb3ZlcmZsb3c6IGF1dG87ICovXG5cdCBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuNCk7IFxuXHQgXG4gfVxuIFxuIC8qTW9kYWwgQ29udGVudCBCb3gqL1xuIC5tb2RhbC1jb250ZW50XG4ge1xuXHQgLyogSXQgcmVtb3ZlcyB0aGUgYmFja2dyb3VuZCBib3ggKi9cblx0IGJhY2tncm91bmQtY29sb3I6dHJhbnNwYXJlbnQ7IFxuXHQgbWFyZ2luOiA0JSBhdXRvIDE1JSBhdXRvO1xuXHQgYm9yZGVyOiAxcHggc29saWQgIzg4ODtcblx0IHdpZHRoOiAxMDAlO1xuXHQgaGVpZ2h0OiAxMDAlO1xuXHQgcGFkZGluZy1ib3R0b206NTBweDtcblx0IGJvcmRlcjogbm9uZTtcbiB9XG5cblxuIC5jbG9zZVxuIHtcblx0IHBvc2l0aW9uOiBhYnNvbHV0ZTtcblx0IHRvcDogMDtcblx0IGNvbG9yOiB3aGl0ZTtcblx0IGZvbnQtc2l6ZTogMzVweDtcblx0IGZvbnQtd2VpZ2h0OiBib2xkO1xuXHQgcGFkZGluZzogMjBweDtcblx0IG1hcmdpbi1sZWZ0OiAzMHB4O1xuXHRtYXJnaW4tdG9wOjkwcHg7XG4gfVxuIFxuXG4gLmNsb3NlOmhvdmVyLC5jbG9zZSBmb2N1c1xuIHtcblx0IGNvbG9yOiByZWQ7XG5cdCBjdXJzb3I6IHBvaW50ZXI7XG4gfVxuXG4uY2xvc2UxXG57XG5cdHBvc2l0aW9uOiBhYnNvbHV0ZTtcblxuXHR0b3A6IDA7XG5cdGNvbG9yOiB3aGl0ZTtcblx0Zm9udC1zaXplOiAzNXB4O1xuXHRmb250LXdlaWdodDogYm9sZDtcblx0cGFkZGluZzogMjBweDtcblx0bWFyZ2luLWxlZnQ6IDMwcHg7XG4gICAgbWFyZ2luLXRvcDogOTBweDtcblx0b3BhY2l0eTogLjU7XG5cdHRleHQtc2hhZG93OiAwIDFweCAwICNmZmY7XG59XG5cbi5jbG9zZTE6aG92ZXIsLmNsb3NlMSBmb2N1c1xue1xuXHRjb2xvcjogcmVkO1xuXHRjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5jbG9zZTJcbntcblx0cG9zaXRpb246IGFic29sdXRlO1xuXG5cdHRvcDogMDtcblx0Y29sb3I6IHdoaXRlO1xuXHRmb250LXNpemU6IDM1cHg7XG5cdGZvbnQtd2VpZ2h0OiBib2xkO1xuXHRwYWRkaW5nOiAyMHB4O1xuXHRtYXJnaW4tbGVmdDogMzBweDtcbiAgICBtYXJnaW4tdG9wOiA5MHB4O1xuXHRvcGFjaXR5OiAuNTtcblx0dGV4dC1zaGFkb3c6IDAgMXB4IDAgI2ZmZjtcbn1cblxuLmNsb3NlMjpob3ZlciwuY2xvc2UyIGZvY3VzXG57XG5cdGNvbG9yOiByZWQ7XG5cdGN1cnNvcjogcG9pbnRlcjtcbn1cbiAvKiBDbG9zZSBidXR0b24gZm9yIGlubGluZSBibG9jayovXG4gIC8qIC5jbG9zZVxuIHtcblx0cG9zaXRpb246IGFic29sdXRlO1xuICAgIFxuICAgIGZvbnQtc2l6ZTogMzVweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBwYWRkaW5nOiAyMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAtMjVweDtcbiAgICBtYXJnaW4tdG9wOiAtNDVweDtcblx0XG59ICovXG4uY2xvc2UzXG57XG5cdHBvc2l0aW9uOiBhYnNvbHV0ZTtcblxuXHR0b3A6IDA7XG5cdGNvbG9yOiB3aGl0ZTtcblx0Zm9udC1zaXplOiAzNXB4O1xuXHRmb250LXdlaWdodDogYm9sZDtcblx0cGFkZGluZzogMjBweDtcblx0bWFyZ2luLWxlZnQ6IDMwcHg7XG4gICAgbWFyZ2luLXRvcDogOTBweDtcblx0b3BhY2l0eTogLjU7XG5cdHRleHQtc2hhZG93OiAwIDFweCAwICNmZmY7XG59XG5cbi5jbG9zZTM6aG92ZXIsLmNsb3NlMyBmb2N1c1xue1xuXHRjb2xvcjogcmVkO1xuXHRjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5jbG9zZTRcbntcblx0cG9zaXRpb246IGFic29sdXRlO1xuXG5cdHRvcDogMDtcblx0Y29sb3I6IHdoaXRlO1xuXHRmb250LXNpemU6IDM1cHg7XG5cdGZvbnQtd2VpZ2h0OiBib2xkO1xuXHRwYWRkaW5nOiAyMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAzMHB4O1xuICAgIG1hcmdpbi10b3A6IDkwcHg7XG59XG4uY2xvc2U0OmhvdmVyLC5jbG9zZTQgZm9jdXNcbiB7XG5cdCBjb2xvcjogcmVkO1xuXHQgY3Vyc29yOiBwb2ludGVyO1xuIH1cbiBcbi5jbG9zZTVcbntcblx0cG9zaXRpb246IGFic29sdXRlO1xuXG5cdHRvcDogMDtcblx0Y29sb3I6IHdoaXRlO1xuXHRmb250LXNpemU6IDM1cHg7XG5cdGZvbnQtd2VpZ2h0OiBib2xkO1xuXHRwYWRkaW5nOiAyMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAzMHB4O1xuICAgIG1hcmdpbi10b3A6IDkwcHg7XG5cdG9wYWNpdHk6IC41O1xuXHR0ZXh0LXNoYWRvdzogMCAxcHggMCAjZmZmO1xufVxuLmNsb3NlNTpob3ZlciwuY2xvc2U1IGZvY3VzXG4ge1xuXHQgY29sb3I6IHJlZDtcblx0IGN1cnNvcjogcG9pbnRlcjtcbiB9XG4gXG4uY2xvc2U2XG4ge1xuXHQgcG9zaXRpb246IGFic29sdXRlO1xuXHQgXG5cdCB0b3A6IDA7XG5cdCBjb2xvcjogd2hpdGU7XG5cdCBmb250LXNpemU6IDM1cHg7XG5cdCBmb250LXdlaWdodDogYm9sZDtcblx0IHBhZGRpbmc6IDIwcHg7XG5cdCBtYXJnaW4tbGVmdDogMzBweDtcblx0IG1hcmdpbi10b3A6OTBweDtcbiB9XG4gXG5cbiAuY2xvc2U2OmhvdmVyLC5jbG9zZTYgZm9jdXNcbiB7XG5cdCBjb2xvcjogcmVkO1xuXHQgY3Vyc29yOiBwb2ludGVyO1xuIH1cblxuIFxuLmNsb3NlN1xue1xuXHRwb3NpdGlvbjogYWJzb2x1dGU7XG5cdHRvcDogMDtcblx0Y29sb3I6IHdoaXRlO1xuXHRmb250LXNpemU6IDM1cHg7XG5cdGZvbnQtd2VpZ2h0OiBib2xkO1xuXHRwYWRkaW5nOiAyMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAzMHB4O1xuICAgIG1hcmdpbi10b3A6IDkwcHg7XG5cdG9wYWNpdHk6IC41O1xuXHR0ZXh0LXNoYWRvdzogMCAxcHggMCAjZmZmO1xufVxuXG4uY2xvc2U3OmhvdmVyLC5jbG9zZTcgZm9jdXNcbntcblx0Y29sb3I6IHJlZDtcblx0Y3Vyc29yOiBwb2ludGVyO1xufVxuXG5cbi5jbG9zZThcbntcblx0cG9zaXRpb246IGFic29sdXRlO1xuXG5cdHRvcDogMDtcblx0Y29sb3I6IHdoaXRlO1xuXHRmb250LXNpemU6IDM1cHg7XG5cdGZvbnQtd2VpZ2h0OiBib2xkO1xuXHRwYWRkaW5nOiAyMHB4O1xuXHRtYXJnaW4tbGVmdDogMzBweDtcbiAgICBtYXJnaW4tdG9wOiA5MHB4O1xuXHRvcGFjaXR5OiAuNTtcblx0dGV4dC1zaGFkb3c6IDAgMXB4IDAgI2ZmZjtcbn1cblxuLmNsb3NlODpob3ZlciwuY2xvc2U4IGZvY3VzXG57XG5cdGNvbG9yOiByZWQ7XG5cdGN1cnNvcjogcG9pbnRlcjtcbn1cblxuXG4uY2xvc2U5XG57XG5cdHBvc2l0aW9uOiBhYnNvbHV0ZTtcblx0dG9wOiAwO1xuXHRjb2xvcjogd2hpdGU7XG5cdGZvbnQtc2l6ZTogMzVweDtcblx0Zm9udC13ZWlnaHQ6IGJvbGQ7XG5cdHBhZGRpbmc6IDIwcHg7XG5cdG1hcmdpbi1sZWZ0OiAzMHB4O1xuICAgIG1hcmdpbi10b3A6OTBweDtcblx0b3BhY2l0eTogLjU7XG5cdHRleHQtc2hhZG93OiAwIDFweCAwICNmZmY7XG59XG5cbi5jbG9zZTk6aG92ZXIsLmNsb3NlOSBmb2N1c1xue1xuXHRjb2xvcjogcmVkO1xuXHRjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5jbG9zZTEwXG5cdHtcblx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XG5cdFx0cG9zaXRpb246IGFic29sdXRlO1xuXHRcdHRvcDogMDtcblx0XHRjb2xvcjogd2hpdGU7XG5cdFx0Zm9udC1zaXplOiAzNXB4O1xuXHRcdGZvbnQtd2VpZ2h0OiBib2xkO1xuXHRcdHBhZGRpbmc6IDIwcHg7XG5cdFx0bWFyZ2luLWxlZnQ6IDMwcHg7XG5cdFx0bWFyZ2luLXRvcDo5MHB4O1xuXHR9XG5cblx0XG5cdC5jbG9zZTEwOmhvdmVyLC5jbG9zZTEwIGZvY3VzXG5cdHtcblx0XHRjb2xvcjogcmVkO1xuXHRcdGN1cnNvcjogcG9pbnRlcjtcblx0fVxuXG5cdFx0XG5cdC5jbG9zZTExXG5cdHtcblx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XG5cdFx0dG9wOiAwO1xuXHRcdGNvbG9yOiB3aGl0ZTtcblx0XHRmb250LXNpemU6IDM1cHg7XG5cdFx0Zm9udC13ZWlnaHQ6IGJvbGQ7XG5cdFx0cGFkZGluZzogMjBweDtcblx0XHRtYXJnaW4tbGVmdDogMzBweDtcblx0XHRtYXJnaW4tdG9wOjkwcHg7XG5cdFx0b3BhY2l0eTogLjU7XG5cdFx0dGV4dC1zaGFkb3c6IDAgMXB4IDAgI2ZmZjtcblx0fVxuXG5cdC5jbG9zZTExOmhvdmVyLC5jbG9zZTExIGZvY3VzXG5cdHtcblx0XHRjb2xvcjogcmVkO1xuXHRcdGN1cnNvcjogcG9pbnRlcjtcblx0fVxuXHRcblxuXHRcdFxuXHQuY2xvc2UxMlxuXHR7XG5cdFx0cG9zaXRpb246IGFic29sdXRlO1xuXG5cdFx0dG9wOiAwO1xuXHRcdGNvbG9yOiB3aGl0ZTtcblx0XHRmb250LXNpemU6IDM1cHg7XG5cdFx0Zm9udC13ZWlnaHQ6IGJvbGQ7XG5cdFx0cGFkZGluZzogMjBweDtcblx0XHRtYXJnaW4tbGVmdDogMzBweDtcblx0ICAgIG1hcmdpbi10b3A6OTBweDtcblx0XHRvcGFjaXR5OiAuNTtcblx0XHR0ZXh0LXNoYWRvdzogMCAxcHggMCAjZmZmO1xuXHR9XG5cblx0LmNsb3NlMTI6aG92ZXIsLmNsb3NlMTIgZm9jdXNcblx0e1xuXHRcdGNvbG9yOiByZWQ7XG5cdFx0Y3Vyc29yOiBwb2ludGVyO1xuXHR9XG5cblx0XG5cdC5jbG9zZTEzXG5cdHtcblx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XG5cdFx0XG5cdFx0dG9wOiAwO1xuXHRcdGNvbG9yOiB3aGl0ZTtcblx0XHRmb250LXNpemU6IDM1cHg7XG5cdFx0Zm9udC13ZWlnaHQ6IGJvbGQ7XG5cdFx0cGFkZGluZzogMjBweDtcblx0XHRtYXJnaW4tbGVmdDogMzBweDtcblx0XHRtYXJnaW4tdG9wOjkwcHg7XG5cdH1cblx0XG4gICBcblx0LmNsb3NlMTM6aG92ZXIsLmNsb3NlMTMgZm9jdXNcblx0e1xuXHRcdGNvbG9yOiByZWQ7XG5cdFx0Y3Vyc29yOiBwb2ludGVyO1xuXHR9XG5cblx0XG5cdC5jbG9zZTE0XG5cdHtcblx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XG5cdFx0XG5cdFx0dG9wOiAwO1xuXHRcdGNvbG9yOiB3aGl0ZTtcblx0XHRmb250LXNpemU6IDM1cHg7XG5cdFx0Zm9udC13ZWlnaHQ6IGJvbGQ7XG5cdFx0cGFkZGluZzogMjBweDtcblx0XHRtYXJnaW4tbGVmdDogMzBweDtcblx0XHRtYXJnaW4tdG9wOjkwcHg7XG5cdH1cblx0XG4gICBcblx0LmNsb3NlMTQ6aG92ZXIsLmNsb3NlMTQgZm9jdXNcblx0e1xuXHRcdGNvbG9yOiByZWQ7XG5cdFx0Y3Vyc29yOiBwb2ludGVyO1xuXHR9XG5cblxuXHQuY2xvc2UxNVxuXHR7XG5cdFx0cG9zaXRpb246IGFic29sdXRlO1xuXHRcdFxuXHRcdHRvcDogMDtcblx0XHRjb2xvcjogd2hpdGU7XG5cdFx0Zm9udC1zaXplOiAzNXB4O1xuXHRcdGZvbnQtd2VpZ2h0OiBib2xkO1xuXHRcdHBhZGRpbmc6IDIwcHg7XG5cdFx0bWFyZ2luLWxlZnQ6IDMwcHg7XG5cdFx0bWFyZ2luLXRvcDo5MHB4O1xuXHR9XG5cdFxuICAgXG5cdC5jbG9zZTE1OmhvdmVyLC5jbG9zZTE1IGZvY3VzXG5cdHtcblx0XHRjb2xvcjogcmVkO1xuXHRcdGN1cnNvcjogcG9pbnRlcjtcblx0fVxuIC8qIEFkZCBab29tIEFuaW1hdGlvbiAqL1xuIC5hbmltYXRlXG4ge1xuXHQgYW5pbWF0aW9uOiB6b29tIDAuNnM7XG4gfVxuIEBrZXlmcmFtZXMgem9vbVxuIHtcblx0IGZyb20ge3RyYW5zZm9ybTogc2NhbGUoMCl9XG5cdCB0byB7dHJhbnNmb3JtOiBzY2FsZSgxKX1cbiB9XG5cblxuIC5Qb3B1cFxuIHtcblx0IG1hcmdpbi10b3A6IDEyMHB4O1xuIH1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/Main_masters/main-master/main-master.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/Main_masters/main-master/main-master.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--   main master  -->\n\n<div class=\"main-master\">\n\n  <div>\n    <div class=\"washing-master\">\n      <a routerLink=\"#\">\n        <div style=\"background:skyblue;padding: 10px;border-radius: 10px; text-decoration: none;\">\n          <p style=\"font-size:20px;text-decoration: none;\"> Master Of Washing WIP</p>\n        </div>\n      </a>\n    </div>\n  </div>\n  <div>\n    <!-- first row of circle  -->\n\n    <!-- MouseOver(Hover) Functionality -->\n    <div class=\"circle_container\">\n      <!-- <a routerLink=\"/garment-manufacturer-master-menus\"> -->\n      <div class=\"circle_main\" onclick=\"document.getElementById('modal-wrapper5').style.display='block'\">\n        <div class=\"circle_text_container\">\n          <div class=\"circle_text\">\n            <button class=\"button14\">\n              Buyer Or Brand Master\n            </button>\n          </div>\n        </div>\n      </div>\n      <!-- </a> -->\n    </div>\n\n    <div id=\"modal-wrapper5\" class=\"modal\">\n      <form class=\"modal-content6 animate\" action=\"#\">\n\n\n        <div class=\"Popup col-md-12\">\n          <div class=\"imgcontainer\">\n            <span onclick=\"document.getElementById('modal-wrapper5').style.display='none'\" class=\"close\">&times;</span>\n          </div>\n          <!-- second row of circle  -->\n          <div class=\"circle_container1\">\n            <a routerLink=\"/add-buyser-brand\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Add\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/edit-buyer-brand\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Edit\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/buyer-brand-import\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Import\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n        </div>\n      </form>\n    </div>\n    <script>\n      //If User clicks outside of the modal. Modal will close\n\n      var modal = document.getElementById('modal-wrapper5')\n      window.onclick = function (event) {\n        if (event.target == modal) {\n          modal.style.display = \"none\";\n        }\n      }\n    </script>\n    <!-- MouseOver(Hover) Functionality Ends -->\n\n    <!-- MouseOver(Hover) Functionality -->\n    <div class=\"circle_container\">\n      <!-- <a routerLink=\"/garment-manufacturer-master-menus\"> -->\n      <div class=\"circle_main\" onclick=\"document.getElementById('modal-wrapper14').style.display='block'\">\n        <div class=\"circle_text_container\">\n          <div class=\"circle_text\">\n            <button class=\"button1\">\n              Garment Manufacturer Master\n            </button>\n          </div>\n        </div>\n      </div>\n      <!-- </a> -->\n    </div>\n\n    <div id=\"modal-wrapper14\" class=\"modal \">\n      <form class=\"modal-content4 animate\" action=\"#\">\n\n\n        <div class=\"Popup col-md-12\">\n          <div class=\"imgcontainer\">\n            <span onclick=\"document.getElementById('modal-wrapper14').style.display='none'\" class=\"close13\">&times;</span>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/add-garment-manufacturer-master\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Add\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/edit-garmment-manufacturer\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Edit\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/garment-manufacturer-import\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Import\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n        </div>\n      </form>\n    </div>\n    <script>\n      //If User clicks outside of the modal. Modal will close\n\n      var modal = document.getElementById('modal-wrapper14')\n      window.onclick = function (event) {\n        if (event.target == modal) {\n          modal.style.display = \"none\";\n        }\n      }\n    </script>\n    <!-- MouseOver(Hover) Functionality Ends -->\n\n    <!-- MouseOver(Hover) Functionality -->\n    <div class=\"circle_container\">\n      <!-- <a routerLink=\"/garment-manufacturer-master-menus\"> -->\n      <div class=\"circle_main\" onclick=\"document.getElementById('modal-wrapper1').style.display='block'\">\n        <div class=\"circle_text_container\">\n          <div class=\"circle_text\">\n            <button class=\"button2\">\n              Garment Item Master\n            </button>\n          </div>\n        </div>\n      </div>\n      <!-- </a> -->\n    </div>\n\n    <div id=\"modal-wrapper1\" class=\"modal\">\n      <form class=\"modal-content3 animate\" action=\"#\">\n\n\n        <div class=\"Popup col-md-12\">\n          <div class=\"imgcontainer\">\n            <span onclick=\"document.getElementById('modal-wrapper1').style.display='none'\" class=\"close10\">&times;</span>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/add-garment-item\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Add\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/edit-garment-item\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Edit\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n\n        </div>\n      </form>\n    </div>\n    <script>\n      //If User clicks outside of the modal. Modal will close\n\n      var modal = document.getElementById('modal-wrapper1')\n      window.onclick = function (event) {\n        if (event.target == modal) {\n          modal.style.display = \"none\";\n        }\n      }\n    </script>\n    <!-- MouseOver(Hover) Functionality Ends -->\n\n    <!-- MouseOver(Hover) Functionality -->\n    <div class=\"circle_container\">\n      <!-- <a routerLink=\"/garment-manufacturer-master-menus\"> -->\n      <div class=\"circle_main\" onclick=\"document.getElementById('modal-wrapper13').style.display='block'\">\n        <div class=\"circle_text_container\">\n          <div class=\"circle_text\">\n            <button class=\"button3\">\n              Dry Process Material Supplier Master\n            </button>\n          </div>\n        </div>\n      </div>\n      <!-- </a> -->\n    </div>\n\n    <div id=\"modal-wrapper13\" class=\"modal\">\n      <form class=\"modal-content14 animate\" action=\"#\">\n\n        <div class=\"Popup col-md-12\">\n          <div class=\"imgcontainer\">\n            <span onclick=\"document.getElementById('modal-wrapper13').style.display='none'\" class=\"close6\">&times;</span>\n          </div>\n\n          <!-- second row of circle  -->\n          <div class=\"circle_container1\">\n            <a routerLink=\"/add-dry-material\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Add\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/edit-dry-material\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Edit\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/import-dry-material\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Import\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n\n        </div>\n      </form>\n    </div>\n    <script>\n      //If User clicks outside of the modal. Modal will close\n\n      var modal = document.getElementById('modal-wrapper5')\n      window.onclick = function (event) {\n        if (event.target == modal) {\n          modal.style.display = \"none\";\n        }\n      }\n    </script>\n    <!-- MouseOver(Hover) Functionality Ends -->\n\n    <!-- MouseOver(Hover) Functionality -->\n    <div class=\"circle_container\">\n      <!-- <a routerLink=\"/garment-manufacturer-master-menus\"> -->\n      <div class=\"circle_main\" onclick=\"document.getElementById('modal-wrapper3').style.display='block'\">\n        <div class=\"circle_text_container\">\n          <div class=\"circle_text\">\n            <!-- <div class=\"first\"> -->\n            <button class=\"button4\">\n              Wash Process Master\n            </button>\n            <!-- </div> -->\n          </div>\n        </div>\n      </div>\n      <!-- </a> -->\n    </div>\n\n    <div id=\"modal-wrapper3\" class=\"modal\">\n      <form class=\"modal-content1 animate\" action=\"#\">\n\n\n        <!-- <div class=\"test\">    -->\n        <div class=\"Popup col-md-12\">\n          <div class=\"imgcontainer\">\n            <span onclick=\"document.getElementById('modal-wrapper3').style.display='none'\" class=\"close4\">&times;</span>\n          </div>\n          <!-- <div class=\"second\"> -->\n          <div class=\"circle_container1\">\n            <a routerLink=\"/assign-wash-process\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Add\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n          <!-- </div> -->\n\n          <!-- <div class=\"second\"> -->\n          <div class=\"circle_container1\">\n            <a routerLink=\"/edit-wash-process\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Edit\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n          <!-- </div> -->\n\n        </div>\n        <!-- </div>  -->\n\n      </form>\n    </div><!-- End of the main master  -->\n    <script>\n      //If User clicks outside of the modal. Modal will close\n\n      var modal = document.getElementById('modal-wrapper3')\n      window.onclick = function (event) {\n        if (event.target == modal) {\n          modal.style.display = \"none\";\n        }\n      }\n    </script>\n    <!-- MouseOver(Hover) Functionality Ends -->\n\n\n  </div><!-- End of first row of circle  -->\n\n  <div>\n    <!-- second row of circle  -->\n\n    <!-- MouseOver(Hover) Functionality -->\n    <div class=\"circle_container\">\n      <!-- <a routerLink=\"/garment-manufacturer-master-menus\"> -->\n      <div class=\"circle_main\" onclick=\"document.getElementById('modal-wrapper12').style.display='block'\">\n        <div class=\"circle_text_container\">\n          <div class=\"circle_text\">\n            <button class=\"button5\">\n              Chemical Manufacturer Master\n            </button>\n          </div>\n        </div>\n      </div>\n      <!-- </a> -->\n    </div>\n\n    <div id=\"modal-wrapper12\" class=\"modal\">\n      <form class=\"modal-content13 animate\" action=\"#\">\n\n\n        <div class=\"Popup col-md-12\">\n          <div class=\"imgcontainer\">\n            <span onclick=\"document.getElementById('modal-wrapper12').style.display='none'\" class=\"close11\">&times;</span>\n          </div>\n          <!-- second row of circle  -->\n          <div class=\"circle_container1\">\n            <a routerLink=\"/add-chemical-master\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Add\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/edit-chemical-master\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Edit\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n\n          <div class=\"circle_container1\">\n            <a routerLink=\"/import-chemical-manufacturer\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Import\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n\n        </div>\n      </form>\n    </div>\n    <script>\n      //If User clicks outside of the modal. Modal will close\n\n      var modal = document.getElementById('modal-wrapper5')\n      window.onclick = function (event) {\n        if (event.target == modal) {\n          modal.style.display = \"none\";\n        }\n      }\n    </script>\n    <!-- MouseOver(Hover) Functionality Ends -->\n\n    <!-- MouseOver(Hover) Functionality -->\n    <div class=\"circle_container\">\n      <!-- <a routerLink=\"/garment-manufacturer-master-menus\"> -->\n      <div class=\"circle_main\" onclick=\"document.getElementById('modal-wrapper6').style.display='block'\">\n        <div class=\"circle_text_container\">\n          <div class=\"circle_text\">\n            <button class=\"button6\">\n              Chemical Agent Or Distributor Master\n            </button>\n          </div>\n        </div>\n      </div>\n      <!-- </a> -->\n    </div>\n\n    <div id=\"modal-wrapper6\" class=\"modal\">\n      <form class=\"modal-content7 animate\" action=\"#\">\n\n\n        <div class=\"Popup col-md-12\">\n          <div class=\"imgcontainer\">\n            <span onclick=\"document.getElementById('modal-wrapper6').style.display='none'\" class=\"close1\">&times;</span>\n          </div>\n          <!-- second row of circle  -->\n          <div class=\"circle_container1\">\n            <a routerLink=\"/add-chemical-agent-distributor\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Add\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/edit-chemical-agent-distributor\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Edit\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/import-chemical-agent-distributor\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Import\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n\n        </div>\n      </form>\n    </div>\n    <script>\n      //If User clicks outside of the modal. Modal will close\n\n      var modal = document.getElementById('modal-wrapper6')\n      window.onclick = function (event) {\n        if (event.target == modal) {\n          modal.style.display = \"none\";\n        }\n      }\n    </script>\n    <!-- MouseOver(Hover) Functionality Ends -->\n\n    <!-- MouseOver(Hover) Functionality -->\n    <div class=\"circle_container\">\n      <!-- <a routerLink=\"/garment-manufacturer-master-menus\"> -->\n      <div class=\"circle_main\" onclick=\"document.getElementById('modal-wrapper4').style.display='block'\">\n        <div class=\"circle_text_container\">\n          <div class=\"circle_text\">\n            <button class=\"button7\">\n              Chemical Product Master\n            </button>\n          </div>\n        </div>\n      </div>\n      <!-- </a> -->\n    </div>\n\n    <div id=\"modal-wrapper4\" class=\"modal\">\n      <form class=\"modal-content5 animate\" action=\"#\">\n\n\n        <div class=\"Popup col-md-12\">\n          <div class=\"imgcontainer\">\n            <span onclick=\"document.getElementById('modal-wrapper4').style.display='none'\" class=\"close12\">&times;</span>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/add-chemical-product\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Add\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/edit-chemical-product\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Edit\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/import-chemical-product\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Import\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n        </div>\n      </form>\n    </div>\n    <script>\n      //If User clicks outside of the modal. Modal will close\n\n      var modal = document.getElementById('modal-wrapper4')\n      window.onclick = function (event) {\n        if (event.target == modal) {\n          modal.style.display = \"none\";\n        }\n      }\n    </script>\n    <!-- MouseOver(Hover) Functionality Ends -->\n\n    <!-- MouseOver(Hover) Functionality -->\n    <div class=\"circle_container\">\n      <!-- <a routerLink=\"/garment-manufacturer-master-menus\"> -->\n      <div class=\"circle_main\" onclick=\"document.getElementById('modal-wrapper7').style.display='block'\">\n        <div class=\"circle_text_container\">\n          <div class=\"circle_text\">\n            <button class=\"button8\">\n              Chemical Assign Master\n            </button>\n          </div>\n        </div>\n      </div>\n      <!-- </a> -->\n    </div>\n\n    <div id=\"modal-wrapper7\" class=\"modal\">\n      <form class=\"modal-content11 animate\" action=\"#\">\n\n\n        <div class=\"Popup col-md-12\">\n          <div class=\"imgcontainer\">\n            <span onclick=\"document.getElementById('modal-wrapper7').style.display='none'\" class=\"close3\">&times;</span>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/add-chemical-assign-master\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Add\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/chemical-assign-master-assign-list\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Edit\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n\n        </div>\n      </form>\n    </div>\n    <script>\n      //If User clicks outside of the modal. Modal will close\n\n      var modal = document.getElementById('modal-wrapper7')\n      window.onclick = function (event) {\n        if (event.target == modal) {\n          modal.style.display = \"none\";\n        }\n      }\n    </script>\n    <!-- MouseOver(Hover) Functionality Ends -->\n\n\n\n    <!-- MouseOver(Hover) Functionality -->\n    <div class=\"circle_container\">\n      <!-- <a routerLink=\"/garment-manufacturer-master-menus\"> -->\n      <div class=\"circle_main\" onclick=\"document.getElementById('modal-wrapper2').style.display='block'\">\n        <div class=\"circle_text_container\">\n          <div class=\"circle_text\">\n            <button class=\"button9\">\n              Wash Step Master\n            </button>\n          </div>\n        </div>\n      </div>\n      <!-- </a> -->\n    </div>\n\n    <div id=\"modal-wrapper2\" class=\"modal\">\n      <form class=\"modal-content2 animate\" action=\"#\">\n\n        <div class=\"Popup col-md-12\">\n          <div class=\"imgcontainer\">\n            <span onclick=\"document.getElementById('modal-wrapper2').style.display='none'\" class=\"close5\">&times;</span>\n          </div>\n\n          <div class=\"circle_container1\">\n            <a routerLink=\"/add-wash-step-master\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Add\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/wash-step-master-list-edit\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Edit\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n\n        </div>\n      </form>\n    </div>\n    <script>\n      //If User clicks outside of the modal. Modal will close\n\n      var modal = document.getElementById('modal-wrapper2')\n      window.onclick = function (event) {\n        if (event.target == modal) {\n          modal.style.display = \"none\";\n        }\n      }\n    </script>\n    <!-- MouseOver(Hover) Functionality Ends -->\n\n  </div><!-- End second row of circle  -->\n\n\n  <div>\n    <!-- third row of circle  -->\n\n    <div class=\"circle_container\">\n      <!-- <a routerLink=\"/garment-manufacturer-master-menus\"> -->\n      <div class=\"circle_main\" onclick=\"document.getElementById('modal-wrapper15').style.display='block'\">\n        <div class=\"circle_text_container\">\n          <div class=\"circle_text\">\n            <button class=\"button11\">\n              Dry Process Material Product Master\n            </button>\n          </div>\n        </div>\n      </div>\n      <!-- </a> -->\n    </div>\n\n    <div id=\"modal-wrapper15\" class=\"modal\">\n      <form class=\"modal-content15 animate\" action=\"#\">\n\n\n        <div class=\"Popup col-md-12\">\n          <div class=\"imgcontainer\">\n            <span onclick=\"document.getElementById('modal-wrapper15').style.display='none'\" class=\"close14\">&times;</span>\n          </div>\n          <!-- second row of circle  -->\n          <div class=\"circle_container1\">\n            <a routerLink=\"/add-Dry-Process-Material-Product-Master\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Add\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/edit-Dry-Process-Material-Product-Master\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Edit\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/import-Dry-Process-Material-Product-Master\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Import\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n        </div>\n      </form>\n    </div>\n    <script>\n      //If User clicks outside of the modal. Modal will close\n\n      var modal = document.getElementById('modal-wrapper15')\n      window.onclick = function (event) {\n        if (event.target == modal) {\n          modal.style.display = \"none\";\n        }\n      }\n    </script>\n    <!-- MouseOver(Hover) Functionality Ends -->\n\n\n    <!-- MouseOver(Hover) Functionality -->\n    <div class=\"circle_container\">\n      <!-- <a routerLink=\"/garment-manufacturer-master-menus\"> -->\n      <div class=\"circle_main\" onclick=\"document.getElementById('modal-wrapper8').style.display='block'\">\n        <div class=\"circle_text_container\">\n          <div class=\"circle_text\">\n            <button class=\"button10\">\n              Dry Process Master\n            </button>\n          </div>\n        </div>\n      </div>\n      <!-- </a> -->\n    </div>\n\n    <div id=\"modal-wrapper8\" class=\"modal\">\n      <form class=\"modal-content8 animate\" action=\"#\">\n\n\n        <div class=\"Popup col-md-12\">\n          <div class=\"imgcontainer\">\n            <span onclick=\"document.getElementById('modal-wrapper8').style.display='none'\" class=\"close7\">&times;</span>\n          </div>\n          <!-- second row of circle  -->\n          <div class=\"circle_container1\">\n            <a routerLink=\"/add-supplier-master\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Add\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/edit-supplier-master\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Edit\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n\n        </div>\n      </form>\n    </div>\n    <script>\n      //If User clicks outside of the modal. Modal will close\n\n      var modal = document.getElementById('modal-wrapper8')\n      window.onclick = function (event) {\n        if (event.target == modal) {\n          modal.style.display = \"none\";\n        }\n      }\n    </script>\n    <!-- MouseOver(Hover) Functionality Ends -->\n    <!-- MouseOver(Hover) Functionality -->\n    <div class=\"circle_container\">\n      <!-- <a routerLink=\"/garment-manufacturer-master-menus\"> -->\n      <div class=\"circle_main\" onclick=\"document.getElementById('modal-wrapper9').style.display='block'\">\n        <div class=\"circle_text_container\">\n          <div class=\"circle_text\">\n            <button class=\"button15\">\n              Unwash Quality Defect Master\n            </button>\n          </div>\n        </div>\n      </div>\n      <!-- </a> -->\n    </div>\n\n    <div id=\"modal-wrapper9\" class=\"modal\">\n      <form class=\"modal-content9 animate\" action=\"#\">\n\n\n        <div class=\"Popup col-md-12\">\n          <div class=\"imgcontainer\">\n            <span onclick=\"document.getElementById('modal-wrapper9').style.display='none'\" class=\"close8\">&times;</span>\n          </div>\n          <!-- second row of circle  -->\n          <div class=\"circle_container1\">\n            <a routerLink=\"/add-unwash\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Add\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/edit-unwash\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Edit\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n\n        </div>\n      </form>\n    </div>\n    <script>\n      //If User clicks outside of the modal. Modal will close\n\n      var modal = document.getElementById('modal-wrapper9')\n      window.onclick = function (event) {\n        if (event.target == modal) {\n          modal.style.display = \"none\";\n        }\n      }\n    </script>\n\n    <!-- MouseOver(Hover) Functionality Ends -->\n\n    <!-- MouseOver(Hover) Functionality -->\n    <div class=\"circle_container\">\n      <!-- <a routerLink=\"/garment-manufacturer-master-menus\"> -->\n      <div class=\"circle_main\" onclick=\"document.getElementById('modal-wrapper10').style.display='block'\">\n        <div class=\"circle_text_container\">\n          <div class=\"circle_text\">\n            <button class=\"button12\">\n              Final Quality Defect Master\n            </button>\n          </div>\n        </div>\n      </div>\n      <!-- </a> -->\n    </div>\n\n    <div id=\"modal-wrapper10\" class=\"modal\">\n      <form class=\"modal-content10 animate\" action=\"#\">\n\n\n        <div class=\"Popup col-md-12\">\n          <div class=\"imgcontainer\">\n            <span onclick=\"document.getElementById('modal-wrapper10').style.display='none'\" class=\"close9\">&times;</span>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/add-financial\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Add\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/edit-financial\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Edit\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n\n        </div>\n      </form>\n    </div>\n    <script>\n      //If User clicks outside of the modal. Modal will close\n\n      var modal = document.getElementById('modal-wrapper10')\n      window.onclick = function (event) {\n        if (event.target == modal) {\n          modal.style.display = \"none\";\n        }\n      }\n    </script>\n    <!-- MouseOver(Hover) Functionality Ends -->\n\n\n\n    <!-- MouseOver(Hover) Functionality -->\n    <div class=\"circle_container\">\n      <!-- <a routerLink=\"/garment-manufacturer-master-menus\"> -->\n      <div class=\"circle_main\" onclick=\"document.getElementById('modal-wrapper11').style.display='block'\">\n        <div class=\"circle_text_container\">\n          <div class=\"circle_text\">\n            <button class=\"button13\">\n              Transit Error Master\n            </button>\n          </div>\n        </div>\n      </div>\n      <!-- </a> -->\n    </div>\n\n    <div id=\"modal-wrapper11\" class=\"modal\">\n      <form class=\"modal-content12 animate\" action=\"#\">\n\n\n        <div class=\"Popup col-md-12\">\n          <div class=\"imgcontainer\">\n            <span onclick=\"document.getElementById('modal-wrapper11').style.display='none'\" class=\"close2\">&times;</span>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/add-transit\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Add\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n          <div class=\"circle_container1\">\n            <a routerLink=\"/edit-transit\">\n              <div class=\"circle_main1\">\n                <div class=\"circle_text_container1\">\n                  <div class=\"circle_text1\">\n                    Edit\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n\n        </div>\n      </form>\n    </div>\n    <script>\n      //If User clicks outside of the modal. Modal will close\n\n      var modal = document.getElementById('modal-wrapper11')\n      window.onclick = function (event) {\n        if (event.target == modal) {\n          modal.style.display = \"none\";\n        }\n      }\n    </script>\n    <!-- MouseOver(Hover) Functionality Ends -->\n\n\n\n  </div><!-- End third row of circle  -->\n\n\n\n  <!-- <div>\n      <div class=\"administrator\">\n        <a routerLink=\"#\">\n         <div style=\"background:skyblue;padding: 10px;border-radius: 10px; text-decoration: none;\">\n           <p style=\"font-size:20px;text-decoration: none;\"> Administrator Dpt<br> Password creator</p>\n         </div>\n        </a>\n       </div> \n     </div>  -->\n  <!-- Assign Master  -->\n  <!-- <div class=\"assign-master\">\n    <a routerLink=\"/chemical-assign-master-menus\">\n      <div style=\"background:skyblue;padding: 10px;border-radius: 10px;\">\n        <i class=\"far fa-file-alt fa-2x\"></i>\n        <p style=\"font-size:25px;\">Assign Master</p>\n      </div>\n    </a>\n  </div> -->\n</div><!-- End of the main master  -->"

/***/ }),

/***/ "./src/app/Main_masters/main-master/main-master.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/Main_masters/main-master/main-master.component.ts ***!
  \*******************************************************************/
/*! exports provided: MainMasterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainMasterComponent", function() { return MainMasterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MainMasterComponent = /** @class */ (function () {
    function MainMasterComponent(headerTitleService) {
        this.headerTitleService = headerTitleService;
    }
    MainMasterComponent.prototype.ngOnInit = function () {
        this.headerTitleService.LogTitle('Logout');
    };
    MainMasterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-main-master',
            template: __webpack_require__(/*! ./main-master.component.html */ "./src/app/Main_masters/main-master/main-master.component.html"),
            styles: [__webpack_require__(/*! ./main-master.component.css */ "./src/app/Main_masters/main-master/main-master.component.css")]
        }),
        __metadata("design:paramtypes", [src_app_Services_header_title_service__WEBPACK_IMPORTED_MODULE_1__["HeaderTitleService"]])
    ], MainMasterComponent);
    return MainMasterComponent;
}());



/***/ }),

/***/ "./src/app/Services/bbm.service.ts":
/*!*****************************************!*\
  !*** ./src/app/Services/bbm.service.ts ***!
  \*****************************************/
/*! exports provided: BbmService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BbmService", function() { return BbmService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BbmService = /** @class */ (function () {
    function BbmService(_http) {
        this._http = _http;
        //post data url
        this.url1 = "http://localhost:3000/api/add-buyser-brand";
        // get data url
        this._url = "http://localhost:3000/api/add-buyser-brand";
    }
    BbmService.prototype.getData = function () {
        return this._http.get(this._url)
            //.map((response:Response)=> <any[]>response.json())
            .map(function (response) { return response.json(); });
    };
    BbmService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], BbmService);
    return BbmService;
}());



/***/ }),

/***/ "./src/app/Services/cadm.service.ts":
/*!******************************************!*\
  !*** ./src/app/Services/cadm.service.ts ***!
  \******************************************/
/*! exports provided: CadmService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadmService", function() { return CadmService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CadmService = /** @class */ (function () {
    function CadmService(_http) {
        this._http = _http;
        //post data url
        this.url1 = "http://localhost:3000/api/add-chemical-agent-distributor";
        // get data url
        this._url = "http://localhost:3000/api/add-chemical-agent-distributor";
    }
    CadmService.prototype.getData = function () {
        return this._http.get(this._url)
            //.map((response:Response)=> <any[]>response.json())
            .map(function (response) { return response.json(); });
    };
    CadmService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], CadmService);
    return CadmService;
}());



/***/ }),

/***/ "./src/app/Services/cam.service.ts":
/*!*****************************************!*\
  !*** ./src/app/Services/cam.service.ts ***!
  \*****************************************/
/*! exports provided: CamService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CamService", function() { return CamService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CamService = /** @class */ (function () {
    function CamService(_http) {
        this._http = _http;
        //post data url
        this.url1 = "http://localhost:3000/api/add-chemical-assign-master";
        // get data url
        this._url = "http://localhost:3000/api/add-chemical-assign-master";
    }
    CamService.prototype.getData = function () {
        return this._http.get(this._url)
            //.map((response:Response)=> <any[]>response.json())
            .map(function (response) { return response.json(); });
    };
    CamService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], CamService);
    return CamService;
}());



/***/ }),

/***/ "./src/app/Services/cmm.service.ts":
/*!*****************************************!*\
  !*** ./src/app/Services/cmm.service.ts ***!
  \*****************************************/
/*! exports provided: CmmService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CmmService", function() { return CmmService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CmmService = /** @class */ (function () {
    function CmmService(_http) {
        this._http = _http;
        //post data url
        this.url1 = "http://localhost:3000/api/add-chemical-master";
        // get data url
        this._url = "http://localhost:3000/api/add-chemical-master";
    }
    CmmService.prototype.getData = function () {
        return this._http.get(this._url)
            //.map((response:Response)=> <any[]>response.json())
            .map(function (response) { return response.json(); });
    };
    CmmService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], CmmService);
    return CmmService;
}());



/***/ }),

/***/ "./src/app/Services/cpm.service.ts":
/*!*****************************************!*\
  !*** ./src/app/Services/cpm.service.ts ***!
  \*****************************************/
/*! exports provided: CpmService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CpmService", function() { return CpmService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CpmService = /** @class */ (function () {
    function CpmService(_http) {
        this._http = _http;
        //post data url
        this.url1 = "http://localhost:3000/api/add-chemical-product";
        // get data url
        this._url = "http://localhost:3000/api/add-chemical-product";
    }
    CpmService.prototype.getData = function () {
        return this._http.get(this._url)
            //.map((response:Response)=> <any[]>response.json())
            .map(function (response) { return response.json(); });
    };
    CpmService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], CpmService);
    return CpmService;
}());



/***/ }),

/***/ "./src/app/Services/dmsm.service.ts":
/*!******************************************!*\
  !*** ./src/app/Services/dmsm.service.ts ***!
  \******************************************/
/*! exports provided: DMSMService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DMSMService", function() { return DMSMService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DMSMService = /** @class */ (function () {
    function DMSMService(_http) {
        this._http = _http;
        //post data url
        this.url1 = "http://localhost:3000/api/add-dry-material";
        // get data url
        this._url = "http://localhost:3000/api/add-dry-material";
    }
    DMSMService.prototype.getData = function () {
        return this._http.get(this._url)
            //.map((response:Response)=> <any[]>response.json())
            .map(function (response) { return response.json(); });
    };
    DMSMService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], DMSMService);
    return DMSMService;
}());



/***/ }),

/***/ "./src/app/Services/dpm.service.ts":
/*!*****************************************!*\
  !*** ./src/app/Services/dpm.service.ts ***!
  \*****************************************/
/*! exports provided: DpmService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DpmService", function() { return DpmService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DpmService = /** @class */ (function () {
    function DpmService(_http) {
        this._http = _http;
        //post data url
        this.url1 = "http://localhost:3000/api/add-supplier-master";
        // get data url
        this._url = "http://localhost:3000/api/add-supplier-master";
    }
    DpmService.prototype.getData = function () {
        return this._http.get(this._url)
            //.map((response:Response)=> <any[]>response.json())
            .map(function (response) { return response.json(); });
    };
    DpmService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], DpmService);
    return DpmService;
}());



/***/ }),

/***/ "./src/app/Services/dpmpm.service.ts":
/*!*******************************************!*\
  !*** ./src/app/Services/dpmpm.service.ts ***!
  \*******************************************/
/*! exports provided: DpmpmService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DpmpmService", function() { return DpmpmService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DpmpmService = /** @class */ (function () {
    function DpmpmService(_http) {
        this._http = _http;
        //post data url
        this.url1 = "http://localhost:3000/api/add-Dry-Process-Material-Product-Master";
        // get data url
        this._url = "http://localhost:3000/api/add-Dry-Process-Material-Product-Master";
    }
    DpmpmService.prototype.getData = function () {
        return this._http.get(this._url)
            //.map((response:Response)=> <any[]>response.json())
            .map(function (response) { return response.json(); });
    };
    DpmpmService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], DpmpmService);
    return DpmpmService;
}());



/***/ }),

/***/ "./src/app/Services/excel.service.ts":
/*!*******************************************!*\
  !*** ./src/app/Services/excel.service.ts ***!
  \*******************************************/
/*! exports provided: ExcelService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExcelService", function() { return ExcelService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! file-saver */ "./node_modules/file-saver/dist/FileSaver.min.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var xlsx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! xlsx */ "./node_modules/xlsx/xlsx.js");
/* harmony import */ var xlsx__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(xlsx__WEBPACK_IMPORTED_MODULE_2__);
// import { Injectable } from '@angular/core';
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// @Injectable({
//   providedIn: 'root'
// })
// export class ExcelService {
//   constructor() { }
// }



var EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
var EXCEL_EXTENSION = '.xlsx';
var ExcelService = /** @class */ (function () {
    function ExcelService() {
    }
    ExcelService.prototype.exportAsExcelFile = function (json, excelFileName) {
        var worksheet = xlsx__WEBPACK_IMPORTED_MODULE_2__["utils"].json_to_sheet(json);
        console.log('worksheet', worksheet);
        var workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        var excelBuffer = xlsx__WEBPACK_IMPORTED_MODULE_2__["write"](workbook, { bookType: 'xlsx', type: 'array' });
        //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    };
    ExcelService.prototype.saveAsExcelFile = function (buffer, fileName) {
        var data = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        file_saver__WEBPACK_IMPORTED_MODULE_1__["saveAs"](data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    };
    ExcelService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], ExcelService);
    return ExcelService;
}());



/***/ }),

/***/ "./src/app/Services/file-upload.service.ts":
/*!*************************************************!*\
  !*** ./src/app/Services/file-upload.service.ts ***!
  \*************************************************/
/*! exports provided: FileUploadService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileUploadService", function() { return FileUploadService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
// import { Injectable } from '@angular/core';
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// @Injectable({
//   providedIn: 'root'
// })
// export class FileUploadService {
//   constructor() { }
// }



var FileUploadService = /** @class */ (function () {
    function FileUploadService(_http) {
        this._http = _http;
        //post data url
        this.url1 = "http://localhost:3000/api/fileUpload";
        // get data url
        this._url = "http://localhost:3000/api/fileUpload";
    }
    FileUploadService.prototype.getData = function () {
        return this._http.get(this._url)
            //.map((response:Response)=> <any[]>response.json())
            .map(function (response) { return response.json(); });
    };
    FileUploadService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], FileUploadService);
    return FileUploadService;
}());



/***/ }),

/***/ "./src/app/Services/fqm.service.ts":
/*!*****************************************!*\
  !*** ./src/app/Services/fqm.service.ts ***!
  \*****************************************/
/*! exports provided: FQMService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FQMService", function() { return FQMService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FQMService = /** @class */ (function () {
    function FQMService(_http) {
        this._http = _http;
        this.url1 = "http://localhost:3000/api/add-financial";
        // get data url
        this._url = "http://localhost:3000/api/add-financial";
    }
    FQMService.prototype.getData = function () {
        return this._http.get(this._url)
            //.map((response:Response)=> <any[]>response.json())
            .map(function (response) { return response.json(); });
    };
    FQMService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], FQMService);
    return FQMService;
}());



/***/ }),

/***/ "./src/app/Services/gim.service.ts":
/*!*****************************************!*\
  !*** ./src/app/Services/gim.service.ts ***!
  \*****************************************/
/*! exports provided: GimService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GimService", function() { return GimService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GimService = /** @class */ (function () {
    function GimService(_http) {
        this._http = _http;
        //post data url
        this.url1 = "http://localhost:3000/api/add-garment-item";
        // get data url
        this._url = "http://localhost:3000/api/add-garment-item";
    }
    GimService.prototype.getData = function () {
        return this._http.get(this._url)
            //.map((response:Response)=> <any[]>response.json())
            .map(function (response) { return response.json(); });
    };
    GimService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], GimService);
    return GimService;
}());



/***/ }),

/***/ "./src/app/Services/gmm.service.ts":
/*!*****************************************!*\
  !*** ./src/app/Services/gmm.service.ts ***!
  \*****************************************/
/*! exports provided: GmmService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GmmService", function() { return GmmService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GmmService = /** @class */ (function () {
    function GmmService(_http) {
        this._http = _http;
        //post data url
        this.url1 = "http://localhost:3000/api/add-garment-manufacturer-master";
        // get data url
        this._url = "http://localhost:3000/api/add-garment-manufacturer-master";
    }
    GmmService.prototype.getData = function () {
        return this._http.get(this._url)
            //.map((response:Response)=> <any[]>response.json())
            .map(function (response) { return response.json(); });
    };
    GmmService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], GmmService);
    return GmmService;
}());



/***/ }),

/***/ "./src/app/Services/header-title.service.ts":
/*!**************************************************!*\
  !*** ./src/app/Services/header-title.service.ts ***!
  \**************************************************/
/*! exports provided: HeaderTitleService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderTitleService", function() { return HeaderTitleService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/BehaviorSubject */ "./node_modules/rxjs-compat/_esm5/BehaviorSubject.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HeaderTitleService = /** @class */ (function () {
    function HeaderTitleService() {
        this.title = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]('Masters');
        this.logout = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]('Logout');
    }
    HeaderTitleService.prototype.setTitle = function (title) {
        this.title.next(title);
    };
    HeaderTitleService.prototype.LogTitle = function (logout) {
        this.logout.next(logout);
    };
    HeaderTitleService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], HeaderTitleService);
    return HeaderTitleService;
}());



/***/ }),

/***/ "./src/app/Services/mydata.service.ts":
/*!********************************************!*\
  !*** ./src/app/Services/mydata.service.ts ***!
  \********************************************/
/*! exports provided: MydataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MydataService", function() { return MydataService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MydataService = /** @class */ (function () {
    // url = "http://localhost:3000/api/assign-wash-process";
    // url2 = "http://localhost:3000/api/wash-step-master-list-edit";
    // url3 ="http://localhost:3000/api/add-garment-manufacturer-master";
    // url4 ='http://localhost:3000/api/add-buyser-brand';
    // url5 = "http://localhost:3000/api/add-chemical-agent-distributor";
    function MydataService(_http) {
        this._http = _http;
        //post data url
        this.url1 = "http://localhost:3000/api/data";
        // get data url
        this._url = "http://localhost:3000/api/data";
    }
    MydataService.prototype.getData = function () {
        return this._http.get(this._url)
            //.map((response:Response)=> <any[]>response.json())
            .map(function (response) { return response.json(); });
    };
    MydataService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], MydataService);
    return MydataService;
}());



/***/ }),

/***/ "./src/app/Services/search.pipe.ts":
/*!*****************************************!*\
  !*** ./src/app/Services/search.pipe.ts ***!
  \*****************************************/
/*! exports provided: SearchPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPipe", function() { return SearchPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var SearchPipe = /** @class */ (function () {
    function SearchPipe() {
    }
    SearchPipe.prototype.transform = function (value, args) {
        if (!args) {
            return value;
        }
        return value.filter(function (val) {
            var rVal = (val.id.toLocaleLowerCase().includes(args)) || (val.subDepSelect.toLocaleLowerCase().includes(args));
            return rVal;
        });
    };
    SearchPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'customerEmailFilter'
        })
    ], SearchPipe);
    return SearchPipe;
}());



/***/ }),

/***/ "./src/app/Services/tem.service.ts":
/*!*****************************************!*\
  !*** ./src/app/Services/tem.service.ts ***!
  \*****************************************/
/*! exports provided: TEMService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TEMService", function() { return TEMService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TEMService = /** @class */ (function () {
    function TEMService(_http) {
        this._http = _http;
        this.url1 = "http://localhost:3000/api/add-transit";
        // get data url
        this._url = "http://localhost:3000/api/add-transit";
    }
    TEMService.prototype.getData = function () {
        return this._http.get(this._url)
            //.map((response:Response)=> <any[]>response.json())
            .map(function (response) { return response.json(); });
    };
    TEMService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], TEMService);
    return TEMService;
}());



/***/ }),

/***/ "./src/app/Services/uqdm.service.ts":
/*!******************************************!*\
  !*** ./src/app/Services/uqdm.service.ts ***!
  \******************************************/
/*! exports provided: UQDMService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UQDMService", function() { return UQDMService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UQDMService = /** @class */ (function () {
    function UQDMService(_http) {
        this._http = _http;
        //post data url
        this.url1 = "http://localhost:3000/api/add-unwash";
        // get data url
        this._url = "http://localhost:3000/api/add-unwash";
    }
    UQDMService.prototype.getData = function () {
        return this._http.get(this._url)
            //.map((response:Response)=> <any[]>response.json())
            .map(function (response) { return response.json(); });
    };
    UQDMService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], UQDMService);
    return UQDMService;
}());



/***/ }),

/***/ "./src/app/Services/wpm.service.ts":
/*!*****************************************!*\
  !*** ./src/app/Services/wpm.service.ts ***!
  \*****************************************/
/*! exports provided: WpmService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WpmService", function() { return WpmService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var WpmService = /** @class */ (function () {
    function WpmService(_http) {
        this._http = _http;
        //post data url
        this.url1 = "http://localhost:3000/api/assign-wash-process";
        // get data url
        this._url = "http://localhost:3000/api/assign-wash-process";
    }
    WpmService.prototype.getData = function () {
        return this._http.get(this._url)
            //.map((response:Response)=> <any[]>response.json())
            .map(function (response) { return response.json(); });
    };
    WpmService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], WpmService);
    return WpmService;
}());



/***/ }),

/***/ "./src/app/Services/wsm.service.ts":
/*!*****************************************!*\
  !*** ./src/app/Services/wsm.service.ts ***!
  \*****************************************/
/*! exports provided: WsmService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WsmService", function() { return WsmService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var WsmService = /** @class */ (function () {
    function WsmService(_http) {
        this._http = _http;
        //post data url
        this.url1 = "http://localhost:3000/api/wash-step-master-list-edit";
        // get data url
        this._url = "http://localhost:3000/api/wash-step-master-list-edit";
    }
    WsmService.prototype.getData = function () {
        return this._http.get(this._url)
            //.map((response:Response)=> <any[]>response.json())
            .map(function (response) { return response.json(); });
    };
    WsmService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], WsmService);
    return WsmService;
}());



/***/ }),

/***/ "./src/app/app-routing/app-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/app-routing/app-routing.module.ts ***!
  \***************************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _signin_signin_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../signin/signin.component */ "./src/app/signin/signin.component.ts");
/* harmony import */ var _reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../reset-password/reset-password.component */ "./src/app/reset-password/reset-password.component.ts");
/* harmony import */ var _customer_login_customer_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../customer-login/customer-login.component */ "./src/app/customer-login/customer-login.component.ts");
/* harmony import */ var _sub_department_sub_department_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../sub-department/sub-department.component */ "./src/app/sub-department/sub-department.component.ts");
/* harmony import */ var _sub_dep_list_sub_dep_list_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../sub-dep-list/sub-dep-list.component */ "./src/app/sub-dep-list/sub-dep-list.component.ts");
/* harmony import */ var _edit_sub_dep_edit_sub_dep_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../edit-sub-dep/edit-sub-dep.component */ "./src/app/edit-sub-dep/edit-sub-dep.component.ts");
/* harmony import */ var _updated_sub_dep_updated_sub_dep_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../updated-sub-dep/updated-sub-dep.component */ "./src/app/updated-sub-dep/updated-sub-dep.component.ts");
/* harmony import */ var _logout_logout_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../logout/logout.component */ "./src/app/logout/logout.component.ts");
/* harmony import */ var _getdata_getdata_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../getdata/getdata.component */ "./src/app/getdata/getdata.component.ts");
/* harmony import */ var _Main_masters_main_master_main_master_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../Main_masters/main-master/main-master.component */ "./src/app/Main_masters/main-master/main-master.component.ts");
/* harmony import */ var _Main_masters_GMM_add_gmm_add_gmm_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../Main_masters/GMM/add-gmm/add-gmm.component */ "./src/app/Main_masters/GMM/add-gmm/add-gmm.component.ts");
/* harmony import */ var _Main_masters_GMM_table_gmm_table_gmm_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../Main_masters/GMM/table-gmm/table-gmm.component */ "./src/app/Main_masters/GMM/table-gmm/table-gmm.component.ts");
/* harmony import */ var _Main_masters_GMM_import_gmm_import_gmm_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../Main_masters/GMM/import-gmm/import-gmm.component */ "./src/app/Main_masters/GMM/import-gmm/import-gmm.component.ts");
/* harmony import */ var _Main_masters_BBM_add_bbm_add_bbm_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../Main_masters/BBM/add-bbm/add-bbm.component */ "./src/app/Main_masters/BBM/add-bbm/add-bbm.component.ts");
/* harmony import */ var _Main_masters_BBM_edit_bbm_edit_bbm_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../Main_masters/BBM/edit-bbm/edit-bbm.component */ "./src/app/Main_masters/BBM/edit-bbm/edit-bbm.component.ts");
/* harmony import */ var _Main_masters_BBM_import_bbm_import_bbm_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../Main_masters/BBM/import-bbm/import-bbm.component */ "./src/app/Main_masters/BBM/import-bbm/import-bbm.component.ts");
/* harmony import */ var _Main_masters_WPM_wpm_assign_wpm_assign_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../Main_masters/WPM/wpm-assign/wpm-assign.component */ "./src/app/Main_masters/WPM/wpm-assign/wpm-assign.component.ts");
/* harmony import */ var _Main_masters_WPM_wpm_edit_wpm_edit_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../Main_masters/WPM/wpm-edit/wpm-edit.component */ "./src/app/Main_masters/WPM/wpm-edit/wpm-edit.component.ts");
/* harmony import */ var _Main_masters_GIM_gim_add_gim_add_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../Main_masters/GIM/gim-add/gim-add.component */ "./src/app/Main_masters/GIM/gim-add/gim-add.component.ts");
/* harmony import */ var _Main_masters_GIM_gim_list_gim_list_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../Main_masters/GIM/gim-list/gim-list.component */ "./src/app/Main_masters/GIM/gim-list/gim-list.component.ts");
/* harmony import */ var _Main_masters_CADM_cadm_add_cadm_add_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../Main_masters/CADM/cadm-add/cadm-add.component */ "./src/app/Main_masters/CADM/cadm-add/cadm-add.component.ts");
/* harmony import */ var _Main_masters_CADM_cadm_edit_cadm_edit_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../Main_masters/CADM/cadm-edit/cadm-edit.component */ "./src/app/Main_masters/CADM/cadm-edit/cadm-edit.component.ts");
/* harmony import */ var _Main_masters_CADM_cadm_import_cadm_import_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ../Main_masters/CADM/cadm-import/cadm-import.component */ "./src/app/Main_masters/CADM/cadm-import/cadm-import.component.ts");
/* harmony import */ var _Main_masters_CPM_cpm_add_cpm_add_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ../Main_masters/CPM/cpm-add/cpm-add.component */ "./src/app/Main_masters/CPM/cpm-add/cpm-add.component.ts");
/* harmony import */ var _Main_masters_CPM_cpm_edit_cpm_edit_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ../Main_masters/CPM/cpm-edit/cpm-edit.component */ "./src/app/Main_masters/CPM/cpm-edit/cpm-edit.component.ts");
/* harmony import */ var _Main_masters_CPM_cpm_import_cpm_import_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ../Main_masters/CPM/cpm-import/cpm-import.component */ "./src/app/Main_masters/CPM/cpm-import/cpm-import.component.ts");
/* harmony import */ var _Main_masters_CAM_cam_add_cam_add_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ../Main_masters/CAM/cam-add/cam-add.component */ "./src/app/Main_masters/CAM/cam-add/cam-add.component.ts");
/* harmony import */ var _Main_masters_CAM_cam_assign_list_cam_assign_list_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ../Main_masters/CAM/cam-assign-list/cam-assign-list.component */ "./src/app/Main_masters/CAM/cam-assign-list/cam-assign-list.component.ts");
/* harmony import */ var _Main_masters_WSM_wsm_add_wsm_add_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ../Main_masters/WSM/wsm-add/wsm-add.component */ "./src/app/Main_masters/WSM/wsm-add/wsm-add.component.ts");
/* harmony import */ var _Main_masters_WSM_wsm_edit_list_wsm_edit_list_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ../Main_masters/WSM/wsm-edit-list/wsm-edit-list.component */ "./src/app/Main_masters/WSM/wsm-edit-list/wsm-edit-list.component.ts");
/* harmony import */ var _Main_masters_CMM_add_cmm_add_cmm_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ../Main_masters/CMM/add-cmm/add-cmm.component */ "./src/app/Main_masters/CMM/add-cmm/add-cmm.component.ts");
/* harmony import */ var _Main_masters_CMM_edit_cmm_edit_cmm_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ../Main_masters/CMM/edit-cmm/edit-cmm.component */ "./src/app/Main_masters/CMM/edit-cmm/edit-cmm.component.ts");
/* harmony import */ var _Main_masters_DPM_add_dpm_add_dpm_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ../Main_masters/DPM/add-dpm/add-dpm.component */ "./src/app/Main_masters/DPM/add-dpm/add-dpm.component.ts");
/* harmony import */ var _Main_masters_DPM_edit_dpm_edit_dpm_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ../Main_masters/DPM/edit-dpm/edit-dpm.component */ "./src/app/Main_masters/DPM/edit-dpm/edit-dpm.component.ts");
/* harmony import */ var _Main_masters_DMSM_add_dmsm_add_dmsm_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ../Main_masters/DMSM/add-dmsm/add-dmsm.component */ "./src/app/Main_masters/DMSM/add-dmsm/add-dmsm.component.ts");
/* harmony import */ var _Main_masters_DMSM_edit_dmsm_edit_dmsm_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ../Main_masters/DMSM/edit-dmsm/edit-dmsm.component */ "./src/app/Main_masters/DMSM/edit-dmsm/edit-dmsm.component.ts");
/* harmony import */ var _Main_masters_UQDM_add_uqdm_add_uqdm_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ../Main_masters/UQDM/add-uqdm/add-uqdm.component */ "./src/app/Main_masters/UQDM/add-uqdm/add-uqdm.component.ts");
/* harmony import */ var _Main_masters_UQDM_edit_uqdm_edit_uqdm_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ../Main_masters/UQDM/edit-uqdm/edit-uqdm.component */ "./src/app/Main_masters/UQDM/edit-uqdm/edit-uqdm.component.ts");
/* harmony import */ var _Main_masters_FQM_add_fqm_add_fqm_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ../Main_masters/FQM/add-fqm/add-fqm.component */ "./src/app/Main_masters/FQM/add-fqm/add-fqm.component.ts");
/* harmony import */ var _Main_masters_FQM_edit_fqm_edit_fqm_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ../Main_masters/FQM/edit-fqm/edit-fqm.component */ "./src/app/Main_masters/FQM/edit-fqm/edit-fqm.component.ts");
/* harmony import */ var _Main_masters_TEM_add_tem_add_tem_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ../Main_masters/TEM/add-tem/add-tem.component */ "./src/app/Main_masters/TEM/add-tem/add-tem.component.ts");
/* harmony import */ var _Main_masters_TEM_edit_tem_edit_tem_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ../Main_masters/TEM/edit-tem/edit-tem.component */ "./src/app/Main_masters/TEM/edit-tem/edit-tem.component.ts");
/* harmony import */ var _Main_masters_CMM_import_cmm_import_cmm_component__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ../Main_masters/CMM/import-cmm/import-cmm.component */ "./src/app/Main_masters/CMM/import-cmm/import-cmm.component.ts");
/* harmony import */ var _Main_masters_DPMPM_add_dpmpm_add_dpmpm_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ../Main_masters/DPMPM/add-dpmpm/add-dpmpm.component */ "./src/app/Main_masters/DPMPM/add-dpmpm/add-dpmpm.component.ts");
/* harmony import */ var _Main_masters_DPMPM_edit_dpmpm_edit_dpmpm_component__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ../Main_masters/DPMPM/edit-dpmpm/edit-dpmpm.component */ "./src/app/Main_masters/DPMPM/edit-dpmpm/edit-dpmpm.component.ts");
/* harmony import */ var _Main_masters_DPMPM_import_dpmpm_import_dpmpm_component__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ../Main_masters/DPMPM/import-dpmpm/import-dpmpm.component */ "./src/app/Main_masters/DPMPM/import-dpmpm/import-dpmpm.component.ts");
/* harmony import */ var _Main_masters_DMSM_import_dmsm_import_dmsm_component__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ../Main_masters/DMSM/import-dmsm/import-dmsm.component */ "./src/app/Main_masters/DMSM/import-dmsm/import-dmsm.component.ts");
/* harmony import */ var _Main_masters_BBM_import_table_import_table_component__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ../Main_masters/BBM/import-table/import-table.component */ "./src/app/Main_masters/BBM/import-table/import-table.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












// import { MenusGmmComponent } from '../Main_masters/GMM/menus-gmm/menus-gmm.component';



// import { BbmMenusComponent } from '../Main_masters/BBM/bbm-menus/bbm-menus.component';



// import { WpmMenusComponent } from '../Main_masters/WPM/wpm-menus/wpm-menus.component';


// import { GimMenusComponent } from '../Main_masters/GIM/gim-menus/gim-menus.component';


// import { CadmMenusComponent } from '../Main_masters/CADM/cadm-menus/cadm-menus.component';



// import { CpmMenusComponent } from '../Main_masters/CPM/cpm-menus/cpm-menus.component';



// import { CamMenusComponent } from '../Main_masters/CAM/cam-menus/cam-menus.component';


// import { WsmMenusComponent } from '../Main_masters/WSM/wsm-menus/wsm-menus.component';




















var routes = [
    {
        path: '', component: _signin_signin_component__WEBPACK_IMPORTED_MODULE_2__["SigninComponent"]
    },
    {
        path: 'change_password', component: _reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_3__["ResetPasswordComponent"]
    },
    {
        path: 'client_login', component: _customer_login_customer_login_component__WEBPACK_IMPORTED_MODULE_4__["CustomerLoginComponent"]
    },
    {
        path: 'sub_department', component: _sub_department_sub_department_component__WEBPACK_IMPORTED_MODULE_5__["SubDepartmentComponent"]
    },
    {
        path: 'sub_department_list', component: _sub_dep_list_sub_dep_list_component__WEBPACK_IMPORTED_MODULE_6__["SubDepListComponent"]
    },
    {
        path: 'edit_sub_department', component: _edit_sub_dep_edit_sub_dep_component__WEBPACK_IMPORTED_MODULE_7__["EditSubDepComponent"]
    },
    {
        path: 'updated_sub_department', component: _updated_sub_dep_updated_sub_dep_component__WEBPACK_IMPORTED_MODULE_8__["UpdatedSubDepComponent"]
    },
    {
        path: 'logout', component: _logout_logout_component__WEBPACK_IMPORTED_MODULE_9__["LogoutComponent"]
    },
    {
        path: 'getdata', component: _getdata_getdata_component__WEBPACK_IMPORTED_MODULE_10__["GetdataComponent"]
    },
    {
        path: 'main-masters', component: _Main_masters_main_master_main_master_component__WEBPACK_IMPORTED_MODULE_11__["MainMasterComponent"]
    },
    // garment master
    // {
    //   path: 'garment-manufacturer-master-menus', component: MenusGmmComponent
    // },
    {
        path: 'add-garment-manufacturer-master', component: _Main_masters_GMM_add_gmm_add_gmm_component__WEBPACK_IMPORTED_MODULE_12__["AddGmmComponent"]
    },
    {
        path: 'edit-garmment-manufacturer', component: _Main_masters_GMM_table_gmm_table_gmm_component__WEBPACK_IMPORTED_MODULE_13__["TableGmmComponent"]
    },
    {
        path: 'garment-manufacturer-import', component: _Main_masters_GMM_import_gmm_import_gmm_component__WEBPACK_IMPORTED_MODULE_14__["ImportGmmComponent"]
    },
    // Buyer/Brand master
    // {
    //   path: 'buyer-brand-menus', component: BbmMenusComponent
    // },
    {
        path: 'add-buyser-brand', component: _Main_masters_BBM_add_bbm_add_bbm_component__WEBPACK_IMPORTED_MODULE_15__["AddBbmComponent"]
    },
    {
        path: 'edit-buyer-brand', component: _Main_masters_BBM_edit_bbm_edit_bbm_component__WEBPACK_IMPORTED_MODULE_16__["EditBbmComponent"]
    },
    {
        path: 'buyer-brand-import', component: _Main_masters_BBM_import_bbm_import_bbm_component__WEBPACK_IMPORTED_MODULE_17__["ImportBbmComponent"]
    },
    {
        path: 'import-table', component: _Main_masters_BBM_import_table_import_table_component__WEBPACK_IMPORTED_MODULE_49__["ImportTableComponent"]
    },
    // Wash Process Master
    // {
    //   path: 'wash-process-menus', component: WpmMenusComponent
    // },
    {
        path: 'assign-wash-process', component: _Main_masters_WPM_wpm_assign_wpm_assign_component__WEBPACK_IMPORTED_MODULE_18__["WpmAssignComponent"]
    },
    {
        path: 'edit-wash-process', component: _Main_masters_WPM_wpm_edit_wpm_edit_component__WEBPACK_IMPORTED_MODULE_19__["WpmEditComponent"]
    },
    // Garment Item Master
    // {
    //   path: 'garment-item-menus', component: GimMenusComponent
    // },
    {
        path: 'add-garment-item', component: _Main_masters_GIM_gim_add_gim_add_component__WEBPACK_IMPORTED_MODULE_20__["GimAddComponent"]
    },
    {
        path: 'edit-garment-item', component: _Main_masters_GIM_gim_list_gim_list_component__WEBPACK_IMPORTED_MODULE_21__["GimListComponent"]
    },
    // chemical agent distributor master
    // {
    //   path: 'chemical-agent-distributor-menus', component: CadmMenusComponent
    // },
    {
        path: 'add-chemical-agent-distributor', component: _Main_masters_CADM_cadm_add_cadm_add_component__WEBPACK_IMPORTED_MODULE_22__["CadmAddComponent"]
    },
    {
        path: 'edit-chemical-agent-distributor', component: _Main_masters_CADM_cadm_edit_cadm_edit_component__WEBPACK_IMPORTED_MODULE_23__["CadmEditComponent"]
    },
    {
        path: 'import-chemical-agent-distributor', component: _Main_masters_CADM_cadm_import_cadm_import_component__WEBPACK_IMPORTED_MODULE_24__["CadmImportComponent"]
    },
    // chemical product master
    // {
    //   path: 'chemical-product-menus', component: CpmMenusComponent
    // },
    {
        path: 'add-chemical-product', component: _Main_masters_CPM_cpm_add_cpm_add_component__WEBPACK_IMPORTED_MODULE_25__["CpmAddComponent"]
    },
    {
        path: 'edit-chemical-product', component: _Main_masters_CPM_cpm_edit_cpm_edit_component__WEBPACK_IMPORTED_MODULE_26__["CpmEditComponent"]
    },
    {
        path: 'import-chemical-product', component: _Main_masters_CPM_cpm_import_cpm_import_component__WEBPACK_IMPORTED_MODULE_27__["CpmImportComponent"]
    },
    // Chemical Assign Master
    // {
    //   path: 'chemical-assign-master-menus', component: CamMenusComponent
    // },
    {
        path: 'add-chemical-assign-master', component: _Main_masters_CAM_cam_add_cam_add_component__WEBPACK_IMPORTED_MODULE_28__["CamAddComponent"]
    },
    {
        path: 'chemical-assign-master-assign-list', component: _Main_masters_CAM_cam_assign_list_cam_assign_list_component__WEBPACK_IMPORTED_MODULE_29__["CamAssignListComponent"]
    },
    // Wash step Master
    // {
    //   path: 'wash-step-master-menus', component: WsmMenusComponent
    // },
    {
        path: 'add-wash-step-master', component: _Main_masters_WSM_wsm_add_wsm_add_component__WEBPACK_IMPORTED_MODULE_30__["WsmAddComponent"]
    },
    {
        path: 'wash-step-master-list-edit', component: _Main_masters_WSM_wsm_edit_list_wsm_edit_list_component__WEBPACK_IMPORTED_MODULE_31__["WsmEditListComponent"]
    },
    //Chemical Manufacturer Master.
    {
        path: 'add-chemical-master', component: _Main_masters_CMM_add_cmm_add_cmm_component__WEBPACK_IMPORTED_MODULE_32__["AddCmmComponent"]
    },
    {
        path: 'edit-chemical-master', component: _Main_masters_CMM_edit_cmm_edit_cmm_component__WEBPACK_IMPORTED_MODULE_33__["EditCmmComponent"]
    },
    {
        path: 'import-chemical-manufacturer', component: _Main_masters_CMM_import_cmm_import_cmm_component__WEBPACK_IMPORTED_MODULE_44__["ImportCmmComponent"]
    },
    //Dry Process Master
    {
        path: 'add-supplier-master', component: _Main_masters_DPM_add_dpm_add_dpm_component__WEBPACK_IMPORTED_MODULE_34__["AddDpmComponent"]
    },
    {
        path: 'edit-supplier-master', component: _Main_masters_DPM_edit_dpm_edit_dpm_component__WEBPACK_IMPORTED_MODULE_35__["EditDpmComponent"]
    },
    // Dry material Supplier master
    {
        path: 'add-dry-material', component: _Main_masters_DMSM_add_dmsm_add_dmsm_component__WEBPACK_IMPORTED_MODULE_36__["AddDmsmComponent"]
    },
    {
        path: 'edit-dry-material', component: _Main_masters_DMSM_edit_dmsm_edit_dmsm_component__WEBPACK_IMPORTED_MODULE_37__["EditDmsmComponent"]
    },
    {
        path: 'import-dry-material', component: _Main_masters_DMSM_import_dmsm_import_dmsm_component__WEBPACK_IMPORTED_MODULE_48__["ImportDmsmComponent"]
    },
    // unwash quality defect master
    {
        path: 'add-unwash', component: _Main_masters_UQDM_add_uqdm_add_uqdm_component__WEBPACK_IMPORTED_MODULE_38__["AddUqdmComponent"]
    },
    {
        path: 'edit-unwash', component: _Main_masters_UQDM_edit_uqdm_edit_uqdm_component__WEBPACK_IMPORTED_MODULE_39__["EditUqdmComponent"]
    },
    // finalcial master
    {
        path: 'add-financial', component: _Main_masters_FQM_add_fqm_add_fqm_component__WEBPACK_IMPORTED_MODULE_40__["AddFqmComponent"]
    },
    {
        path: 'edit-financial', component: _Main_masters_FQM_edit_fqm_edit_fqm_component__WEBPACK_IMPORTED_MODULE_41__["EditFqmComponent"]
    },
    // Transit master
    {
        path: 'add-transit', component: _Main_masters_TEM_add_tem_add_tem_component__WEBPACK_IMPORTED_MODULE_42__["AddTemComponent"]
    },
    {
        path: 'edit-transit', component: _Main_masters_TEM_edit_tem_edit_tem_component__WEBPACK_IMPORTED_MODULE_43__["EditTemComponent"]
    },
    // DPMPM
    {
        path: 'add-Dry-Process-Material-Product-Master', component: _Main_masters_DPMPM_add_dpmpm_add_dpmpm_component__WEBPACK_IMPORTED_MODULE_45__["AddDpmpmComponent"]
    },
    {
        path: 'edit-Dry-Process-Material-Product-Master', component: _Main_masters_DPMPM_edit_dpmpm_edit_dpmpm_component__WEBPACK_IMPORTED_MODULE_46__["EditDpmpmComponent"]
    },
    {
        path: 'import-Dry-Process-Material-Product-Master', component: _Main_masters_DPMPM_import_dpmpm_import_dpmpm_component__WEBPACK_IMPORTED_MODULE_47__["ImportDpmpmComponent"]
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [],
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".main\r\n{\r\n    height: 100vh;\r\n    width: 100%;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0lBRUksYUFBYTtJQUNiLFdBQVc7QUFDZiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1haW5cclxue1xyXG4gICAgaGVpZ2h0OiAxMDB2aDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main\">\r\n    <app-header></app-header>\r\n    <particles [params]=\"myParams\" [style]=\"myStyle\" [width]=\"width\" [height]=\"height\"></particles>\r\n    <router-outlet></router-outlet>\r\n\r\n\r\n</div>\r\n<!-- <app-file-upload></app-file-upload> -->"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.myStyle = {};
        this.myParams = {};
        this.width = 100;
        this.height = 100;
    }
    AppComponent.prototype.ngOnInit = function () {
        this.myStyle = {
            'background-color': '#24419b',
            'position': 'fixed',
            'width': '100%',
            'height': '100%',
            'z-index': -1,
            'top': 0,
            'left': 0,
            'right': 0,
            'bottom': 0,
        };
        this.myParams = {
            particles: {
                // number: {
                //     value: 200,
                // },
                // color: {
                //     value: '#ff0000'
                // },
                number: {
                    "value": 130,
                    "density": {
                        "enable": true,
                    }
                },
                color: {
                    "value": "#3ece98"
                },
                shape: {
                    "type": "circle",
                    "stroke": {
                        "width": 0,
                        "color": "#000000"
                    },
                    "polygon": {
                        "nb_sides": 5
                    },
                    "image": {
                        "src": "",
                        "width": 100,
                        "height": 100
                    }
                },
                opacity: {
                    "value": 10,
                    "random": false,
                    "anim": {
                        "enable": false,
                        "speed": 2,
                        "opacity_min": 0.1,
                        "sync": false
                    }
                },
                size: {
                    "value": 6,
                    "random": true,
                    "anim": {
                        "enable": false,
                        "speed": 40,
                        "size_min": 0.1,
                        "sync": false
                    }
                },
                line_linked: {
                    "enable": true,
                    "distance": 150,
                    "color": "#ffffff",
                    "opacity": 0.9,
                    "width": 1
                },
                move: {
                    "enable": true,
                    "speed": 6,
                    "direction": "none",
                    "random": false,
                    "straight": false,
                    "out_mode": "out",
                    "attract": {
                        "enable": false,
                        "rotateX": 600,
                        "rotateY": 1200
                    }
                }
            },
            interactivity: {
                "detect_on": "canvas",
                "events": {
                    "onhover": {
                        "enable": true,
                        "mode": "grab"
                    },
                    onclick: {
                        "enable": true,
                        "mode": "push"
                    },
                    "resize": true
                },
                modes: {
                    "grab": {
                        "distance": 300,
                        "line_linked": {
                            "opacity": 1
                        }
                    },
                    bubble: {
                        "distance": 400,
                        "size": 40,
                        "duration": 2,
                        "opacity": 8,
                        "speed": 3
                    },
                    repulse: {
                        "distance": 200
                    },
                    push: {
                        "particles_nb": 4
                    },
                    remove: {
                        "particles_nb": 2
                    }
                }
            },
            // image:{
            //   "background-image":("/src/assets/images/main.jpg"),
            // },
            retina_detect: true,
            config_demo: {
                "hide_card": false,
                //"background_color": "#000000",
                // "background-image":("/src/assets/images/main.jpg"),
                "background_position": "50% 50%",
                "background_repeat": "no-repeat",
                "background_size": "cover"
            }
        };
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_cdk_table__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/cdk/table */ "./node_modules/@angular/cdk/esm5/table.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var angular_particle__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angular-particle */ "./node_modules/angular-particle/index.js");
/* harmony import */ var _signin_signin_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./signin/signin.component */ "./src/app/signin/signin.component.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _app_routing_app_routing_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./app-routing/app-routing.module */ "./src/app/app-routing/app-routing.module.ts");
/* harmony import */ var _reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./reset-password/reset-password.component */ "./src/app/reset-password/reset-password.component.ts");
/* harmony import */ var _sub_department_sub_department_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./sub-department/sub-department.component */ "./src/app/sub-department/sub-department.component.ts");
/* harmony import */ var _edit_sub_dep_edit_sub_dep_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./edit-sub-dep/edit-sub-dep.component */ "./src/app/edit-sub-dep/edit-sub-dep.component.ts");
/* harmony import */ var _customer_login_customer_login_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./customer-login/customer-login.component */ "./src/app/customer-login/customer-login.component.ts");
/* harmony import */ var _logout_logout_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./logout/logout.component */ "./src/app/logout/logout.component.ts");
/* harmony import */ var _updated_sub_dep_updated_sub_dep_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./updated-sub-dep/updated-sub-dep.component */ "./src/app/updated-sub-dep/updated-sub-dep.component.ts");
/* harmony import */ var _sub_dep_list_sub_dep_list_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./sub-dep-list/sub-dep-list.component */ "./src/app/sub-dep-list/sub-dep-list.component.ts");
/* harmony import */ var _Main_masters_main_master_main_master_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./Main_masters/main-master/main-master.component */ "./src/app/Main_masters/main-master/main-master.component.ts");
/* harmony import */ var _getdata_getdata_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./getdata/getdata.component */ "./src/app/getdata/getdata.component.ts");
/* harmony import */ var ag_grid_angular__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ag-grid-angular */ "./node_modules/ag-grid-angular/main.js");
/* harmony import */ var ag_grid_angular__WEBPACK_IMPORTED_MODULE_21___default = /*#__PURE__*/__webpack_require__.n(ag_grid_angular__WEBPACK_IMPORTED_MODULE_21__);
/* harmony import */ var ngx_order_pipe__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ngx-order-pipe */ "./node_modules/ngx-order-pipe/ngx-order-pipe.es5.js");
/* harmony import */ var _Services_search_pipe__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./Services/search.pipe */ "./src/app/Services/search.pipe.ts");
/* harmony import */ var _Main_masters_GMM_add_gmm_add_gmm_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./Main_masters/GMM/add-gmm/add-gmm.component */ "./src/app/Main_masters/GMM/add-gmm/add-gmm.component.ts");
/* harmony import */ var _Main_masters_GMM_table_gmm_table_gmm_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./Main_masters/GMM/table-gmm/table-gmm.component */ "./src/app/Main_masters/GMM/table-gmm/table-gmm.component.ts");
/* harmony import */ var _Main_masters_GMM_import_gmm_import_gmm_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./Main_masters/GMM/import-gmm/import-gmm.component */ "./src/app/Main_masters/GMM/import-gmm/import-gmm.component.ts");
/* harmony import */ var _Main_masters_BBM_add_bbm_add_bbm_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./Main_masters/BBM/add-bbm/add-bbm.component */ "./src/app/Main_masters/BBM/add-bbm/add-bbm.component.ts");
/* harmony import */ var _Main_masters_BBM_edit_bbm_edit_bbm_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./Main_masters/BBM/edit-bbm/edit-bbm.component */ "./src/app/Main_masters/BBM/edit-bbm/edit-bbm.component.ts");
/* harmony import */ var _Main_masters_BBM_import_bbm_import_bbm_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./Main_masters/BBM/import-bbm/import-bbm.component */ "./src/app/Main_masters/BBM/import-bbm/import-bbm.component.ts");
/* harmony import */ var _Main_masters_WPM_wpm_assign_wpm_assign_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./Main_masters/WPM/wpm-assign/wpm-assign.component */ "./src/app/Main_masters/WPM/wpm-assign/wpm-assign.component.ts");
/* harmony import */ var _Main_masters_WPM_wpm_edit_wpm_edit_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./Main_masters/WPM/wpm-edit/wpm-edit.component */ "./src/app/Main_masters/WPM/wpm-edit/wpm-edit.component.ts");
/* harmony import */ var _Main_masters_GIM_gim_add_gim_add_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./Main_masters/GIM/gim-add/gim-add.component */ "./src/app/Main_masters/GIM/gim-add/gim-add.component.ts");
/* harmony import */ var _Main_masters_GIM_gim_list_gim_list_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./Main_masters/GIM/gim-list/gim-list.component */ "./src/app/Main_masters/GIM/gim-list/gim-list.component.ts");
/* harmony import */ var _Main_masters_CADM_cadm_add_cadm_add_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./Main_masters/CADM/cadm-add/cadm-add.component */ "./src/app/Main_masters/CADM/cadm-add/cadm-add.component.ts");
/* harmony import */ var _Main_masters_CADM_cadm_edit_cadm_edit_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./Main_masters/CADM/cadm-edit/cadm-edit.component */ "./src/app/Main_masters/CADM/cadm-edit/cadm-edit.component.ts");
/* harmony import */ var _Main_masters_CADM_cadm_import_cadm_import_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./Main_masters/CADM/cadm-import/cadm-import.component */ "./src/app/Main_masters/CADM/cadm-import/cadm-import.component.ts");
/* harmony import */ var _Main_masters_CPM_cpm_add_cpm_add_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./Main_masters/CPM/cpm-add/cpm-add.component */ "./src/app/Main_masters/CPM/cpm-add/cpm-add.component.ts");
/* harmony import */ var _Main_masters_CPM_cpm_edit_cpm_edit_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./Main_masters/CPM/cpm-edit/cpm-edit.component */ "./src/app/Main_masters/CPM/cpm-edit/cpm-edit.component.ts");
/* harmony import */ var _Main_masters_CPM_cpm_import_cpm_import_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./Main_masters/CPM/cpm-import/cpm-import.component */ "./src/app/Main_masters/CPM/cpm-import/cpm-import.component.ts");
/* harmony import */ var _Main_masters_CAM_cam_add_cam_add_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./Main_masters/CAM/cam-add/cam-add.component */ "./src/app/Main_masters/CAM/cam-add/cam-add.component.ts");
/* harmony import */ var _Main_masters_CAM_cam_assign_list_cam_assign_list_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./Main_masters/CAM/cam-assign-list/cam-assign-list.component */ "./src/app/Main_masters/CAM/cam-assign-list/cam-assign-list.component.ts");
/* harmony import */ var _Main_masters_WSM_wsm_add_wsm_add_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./Main_masters/WSM/wsm-add/wsm-add.component */ "./src/app/Main_masters/WSM/wsm-add/wsm-add.component.ts");
/* harmony import */ var _Main_masters_WSM_wsm_edit_list_wsm_edit_list_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./Main_masters/WSM/wsm-edit-list/wsm-edit-list.component */ "./src/app/Main_masters/WSM/wsm-edit-list/wsm-edit-list.component.ts");
/* harmony import */ var _Services_mydata_service__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./Services/mydata.service */ "./src/app/Services/mydata.service.ts");
/* harmony import */ var _Services_cadm_service__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./Services/cadm.service */ "./src/app/Services/cadm.service.ts");
/* harmony import */ var _Services_bbm_service__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ./Services/bbm.service */ "./src/app/Services/bbm.service.ts");
/* harmony import */ var _Services_wpm_service__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ./Services/wpm.service */ "./src/app/Services/wpm.service.ts");
/* harmony import */ var _Services_gmm_service__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ./Services/gmm.service */ "./src/app/Services/gmm.service.ts");
/* harmony import */ var _Services_wsm_service__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ./Services/wsm.service */ "./src/app/Services/wsm.service.ts");
/* harmony import */ var _Services_cpm_service__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ./Services/cpm.service */ "./src/app/Services/cpm.service.ts");
/* harmony import */ var _Services_cam_service__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! ./Services/cam.service */ "./src/app/Services/cam.service.ts");
/* harmony import */ var _Services_gim_service__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! ./Services/gim.service */ "./src/app/Services/gim.service.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _Main_masters_CMM_add_cmm_add_cmm_component__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(/*! ./Main_masters/CMM/add-cmm/add-cmm.component */ "./src/app/Main_masters/CMM/add-cmm/add-cmm.component.ts");
/* harmony import */ var _Main_masters_CMM_edit_cmm_edit_cmm_component__WEBPACK_IMPORTED_MODULE_55__ = __webpack_require__(/*! ./Main_masters/CMM/edit-cmm/edit-cmm.component */ "./src/app/Main_masters/CMM/edit-cmm/edit-cmm.component.ts");
/* harmony import */ var _Main_masters_DPM_add_dpm_add_dpm_component__WEBPACK_IMPORTED_MODULE_56__ = __webpack_require__(/*! ./Main_masters/DPM/add-dpm/add-dpm.component */ "./src/app/Main_masters/DPM/add-dpm/add-dpm.component.ts");
/* harmony import */ var _Main_masters_DPM_edit_dpm_edit_dpm_component__WEBPACK_IMPORTED_MODULE_57__ = __webpack_require__(/*! ./Main_masters/DPM/edit-dpm/edit-dpm.component */ "./src/app/Main_masters/DPM/edit-dpm/edit-dpm.component.ts");
/* harmony import */ var _Services_cmm_service__WEBPACK_IMPORTED_MODULE_58__ = __webpack_require__(/*! ./Services/cmm.service */ "./src/app/Services/cmm.service.ts");
/* harmony import */ var _Services_dpm_service__WEBPACK_IMPORTED_MODULE_59__ = __webpack_require__(/*! ./Services/dpm.service */ "./src/app/Services/dpm.service.ts");
/* harmony import */ var _Main_masters_DMSM_add_dmsm_add_dmsm_component__WEBPACK_IMPORTED_MODULE_60__ = __webpack_require__(/*! ./Main_masters/DMSM/add-dmsm/add-dmsm.component */ "./src/app/Main_masters/DMSM/add-dmsm/add-dmsm.component.ts");
/* harmony import */ var _Main_masters_DMSM_edit_dmsm_edit_dmsm_component__WEBPACK_IMPORTED_MODULE_61__ = __webpack_require__(/*! ./Main_masters/DMSM/edit-dmsm/edit-dmsm.component */ "./src/app/Main_masters/DMSM/edit-dmsm/edit-dmsm.component.ts");
/* harmony import */ var _Main_masters_UQDM_add_uqdm_add_uqdm_component__WEBPACK_IMPORTED_MODULE_62__ = __webpack_require__(/*! ./Main_masters/UQDM/add-uqdm/add-uqdm.component */ "./src/app/Main_masters/UQDM/add-uqdm/add-uqdm.component.ts");
/* harmony import */ var _Main_masters_UQDM_edit_uqdm_edit_uqdm_component__WEBPACK_IMPORTED_MODULE_63__ = __webpack_require__(/*! ./Main_masters/UQDM/edit-uqdm/edit-uqdm.component */ "./src/app/Main_masters/UQDM/edit-uqdm/edit-uqdm.component.ts");
/* harmony import */ var _Main_masters_FQM_edit_fqm_edit_fqm_component__WEBPACK_IMPORTED_MODULE_64__ = __webpack_require__(/*! ./Main_masters/FQM/edit-fqm/edit-fqm.component */ "./src/app/Main_masters/FQM/edit-fqm/edit-fqm.component.ts");
/* harmony import */ var _Main_masters_FQM_add_fqm_add_fqm_component__WEBPACK_IMPORTED_MODULE_65__ = __webpack_require__(/*! ./Main_masters/FQM/add-fqm/add-fqm.component */ "./src/app/Main_masters/FQM/add-fqm/add-fqm.component.ts");
/* harmony import */ var _Main_masters_TEM_add_tem_add_tem_component__WEBPACK_IMPORTED_MODULE_66__ = __webpack_require__(/*! ./Main_masters/TEM/add-tem/add-tem.component */ "./src/app/Main_masters/TEM/add-tem/add-tem.component.ts");
/* harmony import */ var _Main_masters_TEM_edit_tem_edit_tem_component__WEBPACK_IMPORTED_MODULE_67__ = __webpack_require__(/*! ./Main_masters/TEM/edit-tem/edit-tem.component */ "./src/app/Main_masters/TEM/edit-tem/edit-tem.component.ts");
/* harmony import */ var _Services_dmsm_service__WEBPACK_IMPORTED_MODULE_68__ = __webpack_require__(/*! ./Services/dmsm.service */ "./src/app/Services/dmsm.service.ts");
/* harmony import */ var _Services_uqdm_service__WEBPACK_IMPORTED_MODULE_69__ = __webpack_require__(/*! ./Services/uqdm.service */ "./src/app/Services/uqdm.service.ts");
/* harmony import */ var _Services_fqm_service__WEBPACK_IMPORTED_MODULE_70__ = __webpack_require__(/*! ./Services/fqm.service */ "./src/app/Services/fqm.service.ts");
/* harmony import */ var _Services_tem_service__WEBPACK_IMPORTED_MODULE_71__ = __webpack_require__(/*! ./Services/tem.service */ "./src/app/Services/tem.service.ts");
/* harmony import */ var _Services_header_title_service__WEBPACK_IMPORTED_MODULE_72__ = __webpack_require__(/*! ./Services/header-title.service */ "./src/app/Services/header-title.service.ts");
/* harmony import */ var _Main_masters_CMM_import_cmm_import_cmm_component__WEBPACK_IMPORTED_MODULE_73__ = __webpack_require__(/*! ./Main_masters/CMM/import-cmm/import-cmm.component */ "./src/app/Main_masters/CMM/import-cmm/import-cmm.component.ts");
/* harmony import */ var _Main_masters_DPMPM_add_dpmpm_add_dpmpm_component__WEBPACK_IMPORTED_MODULE_74__ = __webpack_require__(/*! ./Main_masters/DPMPM/add-dpmpm/add-dpmpm.component */ "./src/app/Main_masters/DPMPM/add-dpmpm/add-dpmpm.component.ts");
/* harmony import */ var _Main_masters_DPMPM_edit_dpmpm_edit_dpmpm_component__WEBPACK_IMPORTED_MODULE_75__ = __webpack_require__(/*! ./Main_masters/DPMPM/edit-dpmpm/edit-dpmpm.component */ "./src/app/Main_masters/DPMPM/edit-dpmpm/edit-dpmpm.component.ts");
/* harmony import */ var _Main_masters_DPMPM_import_dpmpm_import_dpmpm_component__WEBPACK_IMPORTED_MODULE_76__ = __webpack_require__(/*! ./Main_masters/DPMPM/import-dpmpm/import-dpmpm.component */ "./src/app/Main_masters/DPMPM/import-dpmpm/import-dpmpm.component.ts");
/* harmony import */ var _Services_dpmpm_service__WEBPACK_IMPORTED_MODULE_77__ = __webpack_require__(/*! ./Services/dpmpm.service */ "./src/app/Services/dpmpm.service.ts");
/* harmony import */ var _Main_masters_DMSM_import_dmsm_import_dmsm_component__WEBPACK_IMPORTED_MODULE_78__ = __webpack_require__(/*! ./Main_masters/DMSM/import-dmsm/import-dmsm.component */ "./src/app/Main_masters/DMSM/import-dmsm/import-dmsm.component.ts");
/* harmony import */ var _Services_excel_service__WEBPACK_IMPORTED_MODULE_79__ = __webpack_require__(/*! ./Services/excel.service */ "./src/app/Services/excel.service.ts");
/* harmony import */ var _file_upload_file_upload_component__WEBPACK_IMPORTED_MODULE_80__ = __webpack_require__(/*! ./file-upload/file-upload.component */ "./src/app/file-upload/file-upload.component.ts");
/* harmony import */ var _Services_file_upload_service__WEBPACK_IMPORTED_MODULE_81__ = __webpack_require__(/*! ./Services/file-upload.service */ "./src/app/Services/file-upload.service.ts");
/* harmony import */ var _Main_masters_BBM_import_table_import_table_component__WEBPACK_IMPORTED_MODULE_82__ = __webpack_require__(/*! ./Main_masters/BBM/import-table/import-table.component */ "./src/app/Main_masters/BBM/import-table/import-table.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





//angular material



















// import { MenusGmmComponent } from './Main_masters/GMM/menus-gmm/menus-gmm.component';



// import { BbmMenusComponent } from './Main_masters/BBM/bbm-menus/bbm-menus.component';



// import { WpmMenusComponent } from './Main_masters/WPM/wpm-menus/wpm-menus.component';


// import { GimMenusComponent } from './Main_masters/GIM/gim-menus/gim-menus.component';


// import { CadmMenusComponent } from './Main_masters/CADM/cadm-menus/cadm-menus.component';



// import { CpmMenusComponent } from './Main_masters/CPM/cpm-menus/cpm-menus.component';



// import { CamMenusComponent } from './Main_masters/CAM/cam-menus/cam-menus.component';


// import { WsmMenusComponent } from './Main_masters/WSM/wsm-menus/wsm-menus.component';









































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _signin_signin_component__WEBPACK_IMPORTED_MODULE_9__["SigninComponent"],
                _reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_12__["ResetPasswordComponent"],
                _sub_department_sub_department_component__WEBPACK_IMPORTED_MODULE_13__["SubDepartmentComponent"],
                _edit_sub_dep_edit_sub_dep_component__WEBPACK_IMPORTED_MODULE_14__["EditSubDepComponent"],
                _customer_login_customer_login_component__WEBPACK_IMPORTED_MODULE_15__["CustomerLoginComponent"],
                _logout_logout_component__WEBPACK_IMPORTED_MODULE_16__["LogoutComponent"],
                _updated_sub_dep_updated_sub_dep_component__WEBPACK_IMPORTED_MODULE_17__["UpdatedSubDepComponent"],
                _sub_dep_list_sub_dep_list_component__WEBPACK_IMPORTED_MODULE_18__["SubDepListComponent"],
                _Main_masters_main_master_main_master_component__WEBPACK_IMPORTED_MODULE_19__["MainMasterComponent"],
                _getdata_getdata_component__WEBPACK_IMPORTED_MODULE_20__["GetdataComponent"],
                _Services_search_pipe__WEBPACK_IMPORTED_MODULE_23__["SearchPipe"],
                // MenusGmmComponent,
                _Main_masters_GMM_add_gmm_add_gmm_component__WEBPACK_IMPORTED_MODULE_24__["AddGmmComponent"],
                _Main_masters_GMM_table_gmm_table_gmm_component__WEBPACK_IMPORTED_MODULE_25__["TableGmmComponent"],
                _Main_masters_GMM_import_gmm_import_gmm_component__WEBPACK_IMPORTED_MODULE_26__["ImportGmmComponent"],
                // BbmMenusComponent,
                _Main_masters_BBM_add_bbm_add_bbm_component__WEBPACK_IMPORTED_MODULE_27__["AddBbmComponent"],
                _Main_masters_BBM_edit_bbm_edit_bbm_component__WEBPACK_IMPORTED_MODULE_28__["EditBbmComponent"],
                _Main_masters_BBM_import_bbm_import_bbm_component__WEBPACK_IMPORTED_MODULE_29__["ImportBbmComponent"],
                // WpmMenusComponent,
                _Main_masters_WPM_wpm_assign_wpm_assign_component__WEBPACK_IMPORTED_MODULE_30__["WpmAssignComponent"],
                _Main_masters_WPM_wpm_edit_wpm_edit_component__WEBPACK_IMPORTED_MODULE_31__["WpmEditComponent"],
                // GimMenusComponent,
                _Main_masters_GIM_gim_add_gim_add_component__WEBPACK_IMPORTED_MODULE_32__["GimAddComponent"],
                _Main_masters_GIM_gim_list_gim_list_component__WEBPACK_IMPORTED_MODULE_33__["GimListComponent"],
                // CadmMenusComponent,
                _Main_masters_CADM_cadm_add_cadm_add_component__WEBPACK_IMPORTED_MODULE_34__["CadmAddComponent"],
                _Main_masters_CADM_cadm_edit_cadm_edit_component__WEBPACK_IMPORTED_MODULE_35__["CadmEditComponent"],
                _Main_masters_CADM_cadm_import_cadm_import_component__WEBPACK_IMPORTED_MODULE_36__["CadmImportComponent"],
                // CpmMenusComponent,
                _Main_masters_CPM_cpm_add_cpm_add_component__WEBPACK_IMPORTED_MODULE_37__["CpmAddComponent"],
                _Main_masters_CPM_cpm_edit_cpm_edit_component__WEBPACK_IMPORTED_MODULE_38__["CpmEditComponent"],
                _Main_masters_CPM_cpm_import_cpm_import_component__WEBPACK_IMPORTED_MODULE_39__["CpmImportComponent"],
                // CamMenusComponent,
                _Main_masters_CAM_cam_add_cam_add_component__WEBPACK_IMPORTED_MODULE_40__["CamAddComponent"],
                _Main_masters_CAM_cam_assign_list_cam_assign_list_component__WEBPACK_IMPORTED_MODULE_41__["CamAssignListComponent"],
                // WsmMenusComponent,
                _Main_masters_WSM_wsm_add_wsm_add_component__WEBPACK_IMPORTED_MODULE_42__["WsmAddComponent"],
                _Main_masters_WSM_wsm_edit_list_wsm_edit_list_component__WEBPACK_IMPORTED_MODULE_43__["WsmEditListComponent"],
                _header_header_component__WEBPACK_IMPORTED_MODULE_53__["HeaderComponent"],
                _Main_masters_CMM_add_cmm_add_cmm_component__WEBPACK_IMPORTED_MODULE_54__["AddCmmComponent"],
                _Main_masters_CMM_edit_cmm_edit_cmm_component__WEBPACK_IMPORTED_MODULE_55__["EditCmmComponent"],
                _Main_masters_DPM_add_dpm_add_dpm_component__WEBPACK_IMPORTED_MODULE_56__["AddDpmComponent"],
                _Main_masters_DPM_edit_dpm_edit_dpm_component__WEBPACK_IMPORTED_MODULE_57__["EditDpmComponent"],
                _Main_masters_DMSM_add_dmsm_add_dmsm_component__WEBPACK_IMPORTED_MODULE_60__["AddDmsmComponent"],
                _Main_masters_DMSM_edit_dmsm_edit_dmsm_component__WEBPACK_IMPORTED_MODULE_61__["EditDmsmComponent"],
                _Main_masters_UQDM_add_uqdm_add_uqdm_component__WEBPACK_IMPORTED_MODULE_62__["AddUqdmComponent"],
                _Main_masters_UQDM_edit_uqdm_edit_uqdm_component__WEBPACK_IMPORTED_MODULE_63__["EditUqdmComponent"],
                _Main_masters_FQM_edit_fqm_edit_fqm_component__WEBPACK_IMPORTED_MODULE_64__["EditFqmComponent"],
                _Main_masters_FQM_add_fqm_add_fqm_component__WEBPACK_IMPORTED_MODULE_65__["AddFqmComponent"],
                _Main_masters_TEM_add_tem_add_tem_component__WEBPACK_IMPORTED_MODULE_66__["AddTemComponent"],
                _Main_masters_TEM_edit_tem_edit_tem_component__WEBPACK_IMPORTED_MODULE_67__["EditTemComponent"],
                _Main_masters_CMM_import_cmm_import_cmm_component__WEBPACK_IMPORTED_MODULE_73__["ImportCmmComponent"],
                _Main_masters_DPMPM_add_dpmpm_add_dpmpm_component__WEBPACK_IMPORTED_MODULE_74__["AddDpmpmComponent"],
                _Main_masters_DPMPM_edit_dpmpm_edit_dpmpm_component__WEBPACK_IMPORTED_MODULE_75__["EditDpmpmComponent"],
                _Main_masters_DPMPM_import_dpmpm_import_dpmpm_component__WEBPACK_IMPORTED_MODULE_76__["ImportDpmpmComponent"],
                _Main_masters_DMSM_import_dmsm_import_dmsm_component__WEBPACK_IMPORTED_MODULE_78__["ImportDmsmComponent"],
                _file_upload_file_upload_component__WEBPACK_IMPORTED_MODULE_80__["FileUploadComponent"],
                _Main_masters_BBM_import_table_import_table_component__WEBPACK_IMPORTED_MODULE_82__["ImportTableComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                ngx_order_pipe__WEBPACK_IMPORTED_MODULE_22__["OrderModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_10__["HttpModule"],
                _app_routing_app_routing_module__WEBPACK_IMPORTED_MODULE_11__["AppRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                angular_particle__WEBPACK_IMPORTED_MODULE_8__["ParticlesModule"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot([]),
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"],
                _angular_cdk_table__WEBPACK_IMPORTED_MODULE_6__["CdkTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatAutocompleteModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatButtonToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatStepperModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatRippleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSliderModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatTooltipModule"],
                ag_grid_angular__WEBPACK_IMPORTED_MODULE_21__["AgGridModule"],
            ],
            providers: [_Services_mydata_service__WEBPACK_IMPORTED_MODULE_44__["MydataService"],
                _Services_wpm_service__WEBPACK_IMPORTED_MODULE_47__["WpmService"],
                _Services_cam_service__WEBPACK_IMPORTED_MODULE_51__["CamService"],
                _Services_cadm_service__WEBPACK_IMPORTED_MODULE_45__["CadmService"],
                _Services_bbm_service__WEBPACK_IMPORTED_MODULE_46__["BbmService"],
                _Services_gmm_service__WEBPACK_IMPORTED_MODULE_48__["GmmService"],
                _Services_wsm_service__WEBPACK_IMPORTED_MODULE_49__["WsmService"],
                _Services_gim_service__WEBPACK_IMPORTED_MODULE_52__["GimService"],
                _Services_cpm_service__WEBPACK_IMPORTED_MODULE_50__["CpmService"],
                _Services_cmm_service__WEBPACK_IMPORTED_MODULE_58__["CmmService"],
                _Services_dpm_service__WEBPACK_IMPORTED_MODULE_59__["DpmService"],
                _Services_dmsm_service__WEBPACK_IMPORTED_MODULE_68__["DMSMService"],
                _Services_uqdm_service__WEBPACK_IMPORTED_MODULE_69__["UQDMService"],
                _Services_fqm_service__WEBPACK_IMPORTED_MODULE_70__["FQMService"],
                _Services_tem_service__WEBPACK_IMPORTED_MODULE_71__["TEMService"],
                _Services_header_title_service__WEBPACK_IMPORTED_MODULE_72__["HeaderTitleService"],
                _Services_dpmpm_service__WEBPACK_IMPORTED_MODULE_77__["DpmpmService"],
                _Services_excel_service__WEBPACK_IMPORTED_MODULE_79__["ExcelService"],
                _Services_file_upload_service__WEBPACK_IMPORTED_MODULE_81__["FileUploadService"],
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/customer-login/customer-login.component.css":
/*!*************************************************************!*\
  !*** ./src/app/customer-login/customer-login.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n@import url('https://fonts.googleapis.com/css?family=Numans');\n/* Made with love by Mutiullah Samim*/\nhtml,body{\n\nbackground-size: cover;\nbackground-repeat: no-repeat;\nheight: 100%;\nfont-family: 'Numans', sans-serif;\n}\n.container{\nheight: 100%;\nbackground-size: cover;\nwidth:100%;\nbackground-repeat: no-repeat;\n/* align-content: center; */\n}\n.card{\nheight: 370px;\nmargin-top: auto;\nmargin-bottom: auto;\nwidth: 400px;\nbackground-color: rgba(0,0,0,0.5) !important;\n}\n.social_icon span{\nfont-size: 60px;\nmargin-left: 10px;\ncolor: #FFC312;\n}\n.social_icon span:hover{\ncolor: white;\ncursor: pointer;\n}\n.card-header h3{\ncolor: white;\n}\n.social_icon{\nposition: absolute;\nright: 20px;\ntop: -45px;\n}\n.input-group-prepend span{\nwidth: 50px;\nbackground-color: #FFC312;\ncolor: black;\nborder:0 !important;\n}\ninput:focus{\noutline: 0 0 0 0  !important;\nbox-shadow: 0 0 0 0 !important;\n\n}\n.remember{\ncolor: white;\n}\n.remember input\n{\nwidth: 20px;\nheight: 20px;\nmargin-left: 15px;\nmargin-right: 5px;\n}\n.login_btn{\ncolor: black;\nbackground-color: #FFC312;\nwidth: 100px;\n}\n.login_btn:hover{\ncolor: black;\nbackground-color: white;\n}\n.links{\ncolor: white;\n}\n.links a{\nmargin-left: 4px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY3VzdG9tZXItbG9naW4vY3VzdG9tZXItbG9naW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLDZEQUE2RDtBQUY3RCxxQ0FBcUM7QUFJckM7O0FBRUEsc0JBQXNCO0FBQ3RCLDRCQUE0QjtBQUM1QixZQUFZO0FBQ1osaUNBQWlDO0FBQ2pDO0FBRUE7QUFDQSxZQUFZO0FBQ1osc0JBQXNCO0FBQ3RCLFVBQVU7QUFDViw0QkFBNEI7QUFDNUIsMkJBQTJCO0FBQzNCO0FBRUE7QUFDQSxhQUFhO0FBQ2IsZ0JBQWdCO0FBQ2hCLG1CQUFtQjtBQUNuQixZQUFZO0FBQ1osNENBQTRDO0FBQzVDO0FBRUE7QUFDQSxlQUFlO0FBQ2YsaUJBQWlCO0FBQ2pCLGNBQWM7QUFDZDtBQUVBO0FBQ0EsWUFBWTtBQUNaLGVBQWU7QUFDZjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxrQkFBa0I7QUFDbEIsV0FBVztBQUNYLFVBQVU7QUFDVjtBQUVBO0FBQ0EsV0FBVztBQUNYLHlCQUF5QjtBQUN6QixZQUFZO0FBQ1osbUJBQW1CO0FBQ25CO0FBRUE7QUFDQSw0QkFBNEI7QUFDNUIsOEJBQThCOztBQUU5QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7O0FBRUEsV0FBVztBQUNYLFlBQVk7QUFDWixpQkFBaUI7QUFDakIsaUJBQWlCO0FBQ2pCO0FBRUE7QUFDQSxZQUFZO0FBQ1oseUJBQXlCO0FBQ3pCLFlBQVk7QUFDWjtBQUVBO0FBQ0EsWUFBWTtBQUNaLHVCQUF1QjtBQUN2QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxnQkFBZ0I7QUFDaEIiLCJmaWxlIjoic3JjL2FwcC9jdXN0b21lci1sb2dpbi9jdXN0b21lci1sb2dpbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogTWFkZSB3aXRoIGxvdmUgYnkgTXV0aXVsbGFoIFNhbWltKi9cblxuQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1OdW1hbnMnKTtcblxuaHRtbCxib2R5e1xuXG5iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbmhlaWdodDogMTAwJTtcbmZvbnQtZmFtaWx5OiAnTnVtYW5zJywgc2Fucy1zZXJpZjtcbn1cblxuLmNvbnRhaW5lcntcbmhlaWdodDogMTAwJTtcbmJhY2tncm91bmQtc2l6ZTogY292ZXI7XG53aWR0aDoxMDAlO1xuYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbi8qIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjsgKi9cbn1cblxuLmNhcmR7XG5oZWlnaHQ6IDM3MHB4O1xubWFyZ2luLXRvcDogYXV0bztcbm1hcmdpbi1ib3R0b206IGF1dG87XG53aWR0aDogNDAwcHg7XG5iYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuNSkgIWltcG9ydGFudDtcbn1cblxuLnNvY2lhbF9pY29uIHNwYW57XG5mb250LXNpemU6IDYwcHg7XG5tYXJnaW4tbGVmdDogMTBweDtcbmNvbG9yOiAjRkZDMzEyO1xufVxuXG4uc29jaWFsX2ljb24gc3Bhbjpob3ZlcntcbmNvbG9yOiB3aGl0ZTtcbmN1cnNvcjogcG9pbnRlcjtcbn1cblxuLmNhcmQtaGVhZGVyIGgze1xuY29sb3I6IHdoaXRlO1xufVxuXG4uc29jaWFsX2ljb257XG5wb3NpdGlvbjogYWJzb2x1dGU7XG5yaWdodDogMjBweDtcbnRvcDogLTQ1cHg7XG59XG5cbi5pbnB1dC1ncm91cC1wcmVwZW5kIHNwYW57XG53aWR0aDogNTBweDtcbmJhY2tncm91bmQtY29sb3I6ICNGRkMzMTI7XG5jb2xvcjogYmxhY2s7XG5ib3JkZXI6MCAhaW1wb3J0YW50O1xufVxuXG5pbnB1dDpmb2N1c3tcbm91dGxpbmU6IDAgMCAwIDAgICFpbXBvcnRhbnQ7XG5ib3gtc2hhZG93OiAwIDAgMCAwICFpbXBvcnRhbnQ7XG5cbn1cblxuLnJlbWVtYmVye1xuY29sb3I6IHdoaXRlO1xufVxuXG4ucmVtZW1iZXIgaW5wdXRcbntcbndpZHRoOiAyMHB4O1xuaGVpZ2h0OiAyMHB4O1xubWFyZ2luLWxlZnQ6IDE1cHg7XG5tYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLmxvZ2luX2J0bntcbmNvbG9yOiBibGFjaztcbmJhY2tncm91bmQtY29sb3I6ICNGRkMzMTI7XG53aWR0aDogMTAwcHg7XG59XG5cbi5sb2dpbl9idG46aG92ZXJ7XG5jb2xvcjogYmxhY2s7XG5iYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cblxuLmxpbmtze1xuY29sb3I6IHdoaXRlO1xufVxuXG4ubGlua3MgYXtcbm1hcmdpbi1sZWZ0OiA0cHg7XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/customer-login/customer-login.component.html":
/*!**************************************************************!*\
  !*** ./src/app/customer-login/customer-login.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"d-flex justify-content-center h-100\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        <h3 style=\"text-align:center\">Login</h3>\n\n      </div>\n      <div class=\"card-body\">\n        <form class=\"form-login\" #clientRef='ngForm' (ngSubmit)='onSubmit(clientRef.value)'>\n          <div class=\"input-group form-group\">\n            <div class=\"input-group-prepend\">\n              <span class=\"input-group-text\"><i class=\"fas fa-user\"></i></span>\n            </div>\n            <input type=\"text\" class=\"form-control\" placeholder=\"username\" #usernameRef='ngModel' name=\"username\"\n              required ngModel>\n          </div>\n          <div class=\"input-group form-group\">\n            <div class=\"input-group-prepend\">\n              <span class=\"input-group-text\"><i class=\"fas fa-key\"></i></span>\n            </div>\n            <input type=\"password\" class=\"form-control\" placeholder=\"password\" #passwordRef='ngModel' name=\"password\"\n              required ngModel>\n          </div>\n          <div class=\"row align-items-center remember \">\n            <input type=\"checkbox\" style=\"zoom:.7;\">Remember Me\n          </div>\n          <div class=\"form-group\" style=\"text-align:center\">\n            <input type=\"submit\" value=\"Login\" class=\"btn  login_btn\">\n          </div>\n        </form>\n      </div>\n      <div class=\"card-footer\">\n        <div class=\"d-flex justify-content-center\">\n          <a href=\"#\">Forgot your password?</a>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/customer-login/customer-login.component.ts":
/*!************************************************************!*\
  !*** ./src/app/customer-login/customer-login.component.ts ***!
  \************************************************************/
/*! exports provided: CustomerLoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerLoginComponent", function() { return CustomerLoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CustomerLoginComponent = /** @class */ (function () {
    function CustomerLoginComponent(router, _http) {
        this.router = router;
        this._http = _http;
    }
    CustomerLoginComponent.prototype.ngOnInit = function () {
    };
    CustomerLoginComponent.prototype.onSubmit = function (a) {
        var _this = this;
        this.username = a.username;
        this.password = a.password;
        console.log(a.username + "   " + a.password);
        console.log(a);
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.post('api/client_login', a, options)
            .subscribe(function (data) {
            alert("success");
            _this.router.navigate(['/sub_department']);
        }, function (error) {
            alert("invalid user");
            //console.log(JSON.stringify(error.json()));
        });
    };
    CustomerLoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-customer-login',
            template: __webpack_require__(/*! ./customer-login.component.html */ "./src/app/customer-login/customer-login.component.html"),
            styles: [__webpack_require__(/*! ./customer-login.component.css */ "./src/app/customer-login/customer-login.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], CustomerLoginComponent);
    return CustomerLoginComponent;
}());



/***/ }),

/***/ "./src/app/edit-sub-dep/edit-sub-dep.component.css":
/*!*********************************************************!*\
  !*** ./src/app/edit-sub-dep/edit-sub-dep.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n@import url('https://fonts.googleapis.com/css?family=Numans');\n/* Made with love by Mutiullah Samim*/\nhtml,body{\n\nbackground-size: cover;\nbackground-repeat: no-repeat;\nheight: 100%;\nfont-family: 'Numans', sans-serif;\n}\nlabel\n{\n    color: #fff;\n}\n.container{\nheight: 100%;\nbackground-size: cover;\nwidth:100%;\nbackground-repeat: no-repeat;\n/* align-content: center; */\n}\n.card{\nheight: 490px;\nmargin-top: auto;\nmargin-bottom: auto;\nwidth: 400px;\nbackground-color: rgba(0,0,0,0.5) !important;\n}\n.social_icon span{\nfont-size: 60px;\nmargin-left: 10px;\ncolor: #FFC312;\n}\n.social_icon span:hover{\ncolor: white;\ncursor: pointer;\n}\n.card-header h3{\ncolor: white;\n}\n.social_icon{\nposition: absolute;\nright: 20px;\ntop: -45px;\n}\n.input-group-prepend span{\nwidth: 50px;\nbackground-color: #FFC312;\ncolor: black;\nborder:0 !important;\n}\ninput:focus{\noutline: 0 0 0 0  !important;\nbox-shadow: 0 0 0 0 !important;\n\n}\n.remember{\ncolor: white;\n}\n.remember input\n{\nwidth: 20px;\nheight: 20px;\nmargin-left: 15px;\nmargin-right: 5px;\n}\n.login_btn{\ncolor: black;\nbackground-color: #FFC312;\nwidth: 100px;\n}\n.login_btn:hover{\ncolor: black;\nbackground-color: white;\n}\n.links{\ncolor: white;\n}\n.links a{\nmargin-left: 4px;\n}\noption\n{\n    margin-bottom: 10px!important;\n\n}\n/* Hamburger Menu css */\na\n{\n  text-decoration: none;\n  color: #232323;\n  \n  transition: color 0.3s ease;\n}\na:hover\n{\n  color: tomato;\n}\n#menuToggle\n{\n  display: block;\n  position: absolute;\n  top: 20px;\n  right: 50px;\n  \n  z-index: 1;\n  \n  -webkit-user-select: none;\n  -moz-user-select: none;\n   -ms-user-select: none;\n       user-select: none;\n}\n#menuToggle input\n{\n  display: block;\n  width: 40px;\n  height: 32px;\n  position: absolute;\n  top: -7px;\n  left: -5px;\n  \n  cursor: pointer;\n  \n  opacity: 0; /* hide this */\n  z-index: 2; /* and place it over the hamburger */\n  \n  -webkit-touch-callout: none;\n}\n/*\n * Just a quick hamburger\n */\n#menuToggle span\n{\n  display: block;\n  width: 33px;\n  height: 4px;\n  margin-bottom: 5px;\n  position: relative;\n  \n  background: #cdcdcd;\n  border-radius: 3px;\n  \n  z-index: 1;\n  \n  -webkit-transform-origin: 4px 0px;\n  \n          transform-origin: 4px 0px;\n  \n  transition: background 0.5s cubic-bezier(0.77,0.2,0.05,1.0),\n              opacity 0.55s ease,\n              -webkit-transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0);\n  \n  transition: transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0),\n              background 0.5s cubic-bezier(0.77,0.2,0.05,1.0),\n              opacity 0.55s ease;\n  \n  transition: transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0),\n              background 0.5s cubic-bezier(0.77,0.2,0.05,1.0),\n              opacity 0.55s ease,\n              -webkit-transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0);\n}\n#menuToggle span:first-child\n{\n  -webkit-transform-origin: 0% 0%;\n          transform-origin: 0% 0%;\n}\n#menuToggle span:nth-last-child(2)\n{\n  -webkit-transform-origin: 0% 100%;\n          transform-origin: 0% 100%;\n}\n/* \n * Transform all the slices of hamburger\n * into a crossmark.\n */\n#menuToggle input:checked ~ span\n{\n  opacity: 1;\n  -webkit-transform: rotate(45deg) translate(-2px, -1px);\n          transform: rotate(45deg) translate(-2px, -1px);\n  background: #232323;\n}\n/*\n * But let's hide the middle one.\n */\n#menuToggle input:checked ~ span:nth-last-child(3)\n{\n  opacity: 0;\n  -webkit-transform: rotate(0deg) scale(0.2, 0.2);\n          transform: rotate(0deg) scale(0.2, 0.2);\n}\n/*\n * Ohyeah and the last one should go the other direction\n */\n#menuToggle input:checked ~ span:nth-last-child(2)\n{\n  opacity: 1;\n  -webkit-transform: rotate(-45deg) translate(0, -1px);\n          transform: rotate(-45deg) translate(0, -1px);\n}\n/*\n * Make this absolute positioned\n * at the top left of the screen\n */\n#menu\n{\n  position: absolute;\n  width: 300px;\n  margin: -100px 0 0 0;\n  padding: 20px;\n  padding-top: 100px;\n  right: -100px;\n  \n  background: #ededed;\n  list-style-type: none;\n  -webkit-font-smoothing: antialiased;\n  /* to stop flickering of text in safari */\n  \n  -webkit-transform-origin: 0% 0%;\n  \n          transform-origin: 0% 0%;\n  -webkit-transform: translate(100%, 0);\n          transform: translate(100%, 0);\n  \n  transition: -webkit-transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0);\n  \n  transition: transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0);\n  \n  transition: transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0), -webkit-transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0);\n}\n#menu li\n{\n  padding: 10px 0;\n \n  font-size: 22px;\n}\n/*\n * And let's fade it in from the left\n */\n#menuToggle input:checked ~ ul\n{\n  -webkit-transform: scale(1.0, 1.0);\n          transform: scale(1.0, 1.0);\n  opacity: 1;\n}\n/* End of Hamburger Menu css */\n/* *************************************************************************\n**********************  Media query for Mobile screen  */\n@media only screen and (max-width : 480px) {\n    .card{\n        height: 750px;\n        margin-top: auto;\n        margin-bottom: auto;\n        width: 400px;\n        background-color: rgba(0,0,0,0.5) !important;\n        }\n\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZWRpdC1zdWItZGVwL2VkaXQtc3ViLWRlcC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUEsNkRBQTZEO0FBRjdELHFDQUFxQztBQUlyQzs7QUFFQSxzQkFBc0I7QUFDdEIsNEJBQTRCO0FBQzVCLFlBQVk7QUFDWixpQ0FBaUM7QUFDakM7QUFDQTs7SUFFSSxXQUFXO0FBQ2Y7QUFDQTtBQUNBLFlBQVk7QUFDWixzQkFBc0I7QUFDdEIsVUFBVTtBQUNWLDRCQUE0QjtBQUM1QiwyQkFBMkI7QUFDM0I7QUFFQTtBQUNBLGFBQWE7QUFDYixnQkFBZ0I7QUFDaEIsbUJBQW1CO0FBQ25CLFlBQVk7QUFDWiw0Q0FBNEM7QUFDNUM7QUFFQTtBQUNBLGVBQWU7QUFDZixpQkFBaUI7QUFDakIsY0FBYztBQUNkO0FBRUE7QUFDQSxZQUFZO0FBQ1osZUFBZTtBQUNmO0FBRUE7QUFDQSxZQUFZO0FBQ1o7QUFFQTtBQUNBLGtCQUFrQjtBQUNsQixXQUFXO0FBQ1gsVUFBVTtBQUNWO0FBRUE7QUFDQSxXQUFXO0FBQ1gseUJBQXlCO0FBQ3pCLFlBQVk7QUFDWixtQkFBbUI7QUFDbkI7QUFFQTtBQUNBLDRCQUE0QjtBQUM1Qiw4QkFBOEI7O0FBRTlCO0FBRUE7QUFDQSxZQUFZO0FBQ1o7QUFFQTs7QUFFQSxXQUFXO0FBQ1gsWUFBWTtBQUNaLGlCQUFpQjtBQUNqQixpQkFBaUI7QUFDakI7QUFFQTtBQUNBLFlBQVk7QUFDWix5QkFBeUI7QUFDekIsWUFBWTtBQUNaO0FBRUE7QUFDQSxZQUFZO0FBQ1osdUJBQXVCO0FBQ3ZCO0FBRUE7QUFDQSxZQUFZO0FBQ1o7QUFFQTtBQUNBLGdCQUFnQjtBQUNoQjtBQUNBOztJQUVJLDZCQUE2Qjs7QUFFakM7QUFFQSx1QkFBdUI7QUFDdkI7O0VBRUUscUJBQXFCO0VBQ3JCLGNBQWM7O0VBRWQsMkJBQTJCO0FBQzdCO0FBRUE7O0VBRUUsYUFBYTtBQUNmO0FBRUE7O0VBRUUsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsV0FBVzs7RUFFWCxVQUFVOztFQUVWLHlCQUF5QjtFQUN6QixzQkFBaUI7R0FBakIscUJBQWlCO09BQWpCLGlCQUFpQjtBQUNuQjtBQUVBOztFQUVFLGNBQWM7RUFDZCxXQUFXO0VBQ1gsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsVUFBVTs7RUFFVixlQUFlOztFQUVmLFVBQVUsRUFBRSxjQUFjO0VBQzFCLFVBQVUsRUFBRSxvQ0FBb0M7O0VBRWhELDJCQUEyQjtBQUM3QjtBQUVBOztFQUVFO0FBQ0Y7O0VBRUUsY0FBYztFQUNkLFdBQVc7RUFDWCxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGtCQUFrQjs7RUFFbEIsbUJBQW1CO0VBQ25CLGtCQUFrQjs7RUFFbEIsVUFBVTs7RUFFVixpQ0FBeUI7O1VBQXpCLHlCQUF5Qjs7RUFFekI7O29FQUU4Qjs7RUFGOUI7O2dDQUU4Qjs7RUFGOUI7OztvRUFFOEI7QUFDaEM7QUFFQTs7RUFFRSwrQkFBdUI7VUFBdkIsdUJBQXVCO0FBQ3pCO0FBRUE7O0VBRUUsaUNBQXlCO1VBQXpCLHlCQUF5QjtBQUMzQjtBQUVBOzs7RUFHRTtBQUNGOztFQUVFLFVBQVU7RUFDVixzREFBOEM7VUFBOUMsOENBQThDO0VBQzlDLG1CQUFtQjtBQUNyQjtBQUVBOztFQUVFO0FBQ0Y7O0VBRUUsVUFBVTtFQUNWLCtDQUF1QztVQUF2Qyx1Q0FBdUM7QUFDekM7QUFFQTs7RUFFRTtBQUNGOztFQUVFLFVBQVU7RUFDVixvREFBNEM7VUFBNUMsNENBQTRDO0FBQzlDO0FBRUE7OztFQUdFO0FBQ0Y7O0VBRUUsa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixvQkFBb0I7RUFDcEIsYUFBYTtFQUNiLGtCQUFrQjtFQUNsQixhQUFhOztFQUViLG1CQUFtQjtFQUNuQixxQkFBcUI7RUFDckIsbUNBQW1DO0VBQ25DLHlDQUF5Qzs7RUFFekMsK0JBQXVCOztVQUF2Qix1QkFBdUI7RUFDdkIscUNBQTZCO1VBQTdCLDZCQUE2Qjs7RUFFN0Isa0VBQTBEOztFQUExRCwwREFBMEQ7O0VBQTFELGtIQUEwRDtBQUM1RDtBQUVBOztFQUVFLGVBQWU7O0VBRWYsZUFBZTtBQUNqQjtBQUVBOztFQUVFO0FBQ0Y7O0VBRUUsa0NBQTBCO1VBQTFCLDBCQUEwQjtFQUMxQixVQUFVO0FBQ1o7QUFDQSw4QkFBOEI7QUFJOUI7d0RBQ3dEO0FBRXhEO0lBQ0k7UUFDSSxhQUFhO1FBQ2IsZ0JBQWdCO1FBQ2hCLG1CQUFtQjtRQUNuQixZQUFZO1FBQ1osNENBQTRDO1FBQzVDOztBQUVSIiwiZmlsZSI6InNyYy9hcHAvZWRpdC1zdWItZGVwL2VkaXQtc3ViLWRlcC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogTWFkZSB3aXRoIGxvdmUgYnkgTXV0aXVsbGFoIFNhbWltKi9cblxuQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1OdW1hbnMnKTtcblxuaHRtbCxib2R5e1xuXG5iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbmhlaWdodDogMTAwJTtcbmZvbnQtZmFtaWx5OiAnTnVtYW5zJywgc2Fucy1zZXJpZjtcbn1cbmxhYmVsXG57XG4gICAgY29sb3I6ICNmZmY7XG59XG4uY29udGFpbmVye1xuaGVpZ2h0OiAxMDAlO1xuYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbndpZHRoOjEwMCU7XG5iYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuLyogYWxpZ24tY29udGVudDogY2VudGVyOyAqL1xufVxuXG4uY2FyZHtcbmhlaWdodDogNDkwcHg7XG5tYXJnaW4tdG9wOiBhdXRvO1xubWFyZ2luLWJvdHRvbTogYXV0bztcbndpZHRoOiA0MDBweDtcbmJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O1xufVxuXG4uc29jaWFsX2ljb24gc3BhbntcbmZvbnQtc2l6ZTogNjBweDtcbm1hcmdpbi1sZWZ0OiAxMHB4O1xuY29sb3I6ICNGRkMzMTI7XG59XG5cbi5zb2NpYWxfaWNvbiBzcGFuOmhvdmVye1xuY29sb3I6IHdoaXRlO1xuY3Vyc29yOiBwb2ludGVyO1xufVxuXG4uY2FyZC1oZWFkZXIgaDN7XG5jb2xvcjogd2hpdGU7XG59XG5cbi5zb2NpYWxfaWNvbntcbnBvc2l0aW9uOiBhYnNvbHV0ZTtcbnJpZ2h0OiAyMHB4O1xudG9wOiAtNDVweDtcbn1cblxuLmlucHV0LWdyb3VwLXByZXBlbmQgc3BhbntcbndpZHRoOiA1MHB4O1xuYmFja2dyb3VuZC1jb2xvcjogI0ZGQzMxMjtcbmNvbG9yOiBibGFjaztcbmJvcmRlcjowICFpbXBvcnRhbnQ7XG59XG5cbmlucHV0OmZvY3Vze1xub3V0bGluZTogMCAwIDAgMCAgIWltcG9ydGFudDtcbmJveC1zaGFkb3c6IDAgMCAwIDAgIWltcG9ydGFudDtcblxufVxuXG4ucmVtZW1iZXJ7XG5jb2xvcjogd2hpdGU7XG59XG5cbi5yZW1lbWJlciBpbnB1dFxue1xud2lkdGg6IDIwcHg7XG5oZWlnaHQ6IDIwcHg7XG5tYXJnaW4tbGVmdDogMTVweDtcbm1hcmdpbi1yaWdodDogNXB4O1xufVxuXG4ubG9naW5fYnRue1xuY29sb3I6IGJsYWNrO1xuYmFja2dyb3VuZC1jb2xvcjogI0ZGQzMxMjtcbndpZHRoOiAxMDBweDtcbn1cblxuLmxvZ2luX2J0bjpob3ZlcntcbmNvbG9yOiBibGFjaztcbmJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuXG4ubGlua3N7XG5jb2xvcjogd2hpdGU7XG59XG5cbi5saW5rcyBhe1xubWFyZ2luLWxlZnQ6IDRweDtcbn1cbm9wdGlvblxue1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHghaW1wb3J0YW50O1xuXG59XG5cbi8qIEhhbWJ1cmdlciBNZW51IGNzcyAqL1xuYVxue1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIGNvbG9yOiAjMjMyMzIzO1xuICBcbiAgdHJhbnNpdGlvbjogY29sb3IgMC4zcyBlYXNlO1xufVxuXG5hOmhvdmVyXG57XG4gIGNvbG9yOiB0b21hdG87XG59XG5cbiNtZW51VG9nZ2xlXG57XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMjBweDtcbiAgcmlnaHQ6IDUwcHg7XG4gIFxuICB6LWluZGV4OiAxO1xuICBcbiAgLXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTtcbiAgdXNlci1zZWxlY3Q6IG5vbmU7XG59XG5cbiNtZW51VG9nZ2xlIGlucHV0XG57XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB3aWR0aDogNDBweDtcbiAgaGVpZ2h0OiAzMnB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogLTdweDtcbiAgbGVmdDogLTVweDtcbiAgXG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgXG4gIG9wYWNpdHk6IDA7IC8qIGhpZGUgdGhpcyAqL1xuICB6LWluZGV4OiAyOyAvKiBhbmQgcGxhY2UgaXQgb3ZlciB0aGUgaGFtYnVyZ2VyICovXG4gIFxuICAtd2Via2l0LXRvdWNoLWNhbGxvdXQ6IG5vbmU7XG59XG5cbi8qXG4gKiBKdXN0IGEgcXVpY2sgaGFtYnVyZ2VyXG4gKi9cbiNtZW51VG9nZ2xlIHNwYW5cbntcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiAzM3B4O1xuICBoZWlnaHQ6IDRweDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIFxuICBiYWNrZ3JvdW5kOiAjY2RjZGNkO1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG4gIFxuICB6LWluZGV4OiAxO1xuICBcbiAgdHJhbnNmb3JtLW9yaWdpbjogNHB4IDBweDtcbiAgXG4gIHRyYW5zaXRpb246IHRyYW5zZm9ybSAwLjVzIGN1YmljLWJlemllcigwLjc3LDAuMiwwLjA1LDEuMCksXG4gICAgICAgICAgICAgIGJhY2tncm91bmQgMC41cyBjdWJpYy1iZXppZXIoMC43NywwLjIsMC4wNSwxLjApLFxuICAgICAgICAgICAgICBvcGFjaXR5IDAuNTVzIGVhc2U7XG59XG5cbiNtZW51VG9nZ2xlIHNwYW46Zmlyc3QtY2hpbGRcbntcbiAgdHJhbnNmb3JtLW9yaWdpbjogMCUgMCU7XG59XG5cbiNtZW51VG9nZ2xlIHNwYW46bnRoLWxhc3QtY2hpbGQoMilcbntcbiAgdHJhbnNmb3JtLW9yaWdpbjogMCUgMTAwJTtcbn1cblxuLyogXG4gKiBUcmFuc2Zvcm0gYWxsIHRoZSBzbGljZXMgb2YgaGFtYnVyZ2VyXG4gKiBpbnRvIGEgY3Jvc3NtYXJrLlxuICovXG4jbWVudVRvZ2dsZSBpbnB1dDpjaGVja2VkIH4gc3Bhblxue1xuICBvcGFjaXR5OiAxO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZykgdHJhbnNsYXRlKC0ycHgsIC0xcHgpO1xuICBiYWNrZ3JvdW5kOiAjMjMyMzIzO1xufVxuXG4vKlxuICogQnV0IGxldCdzIGhpZGUgdGhlIG1pZGRsZSBvbmUuXG4gKi9cbiNtZW51VG9nZ2xlIGlucHV0OmNoZWNrZWQgfiBzcGFuOm50aC1sYXN0LWNoaWxkKDMpXG57XG4gIG9wYWNpdHk6IDA7XG4gIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpIHNjYWxlKDAuMiwgMC4yKTtcbn1cblxuLypcbiAqIE9oeWVhaCBhbmQgdGhlIGxhc3Qgb25lIHNob3VsZCBnbyB0aGUgb3RoZXIgZGlyZWN0aW9uXG4gKi9cbiNtZW51VG9nZ2xlIGlucHV0OmNoZWNrZWQgfiBzcGFuOm50aC1sYXN0LWNoaWxkKDIpXG57XG4gIG9wYWNpdHk6IDE7XG4gIHRyYW5zZm9ybTogcm90YXRlKC00NWRlZykgdHJhbnNsYXRlKDAsIC0xcHgpO1xufVxuXG4vKlxuICogTWFrZSB0aGlzIGFic29sdXRlIHBvc2l0aW9uZWRcbiAqIGF0IHRoZSB0b3AgbGVmdCBvZiB0aGUgc2NyZWVuXG4gKi9cbiNtZW51XG57XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgd2lkdGg6IDMwMHB4O1xuICBtYXJnaW46IC0xMDBweCAwIDAgMDtcbiAgcGFkZGluZzogMjBweDtcbiAgcGFkZGluZy10b3A6IDEwMHB4O1xuICByaWdodDogLTEwMHB4O1xuICBcbiAgYmFja2dyb3VuZDogI2VkZWRlZDtcbiAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xuICAtd2Via2l0LWZvbnQtc21vb3RoaW5nOiBhbnRpYWxpYXNlZDtcbiAgLyogdG8gc3RvcCBmbGlja2VyaW5nIG9mIHRleHQgaW4gc2FmYXJpICovXG4gIFxuICB0cmFuc2Zvcm0tb3JpZ2luOiAwJSAwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMTAwJSwgMCk7XG4gIFxuICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMC41cyBjdWJpYy1iZXppZXIoMC43NywwLjIsMC4wNSwxLjApO1xufVxuXG4jbWVudSBsaVxue1xuICBwYWRkaW5nOiAxMHB4IDA7XG4gXG4gIGZvbnQtc2l6ZTogMjJweDtcbn1cblxuLypcbiAqIEFuZCBsZXQncyBmYWRlIGl0IGluIGZyb20gdGhlIGxlZnRcbiAqL1xuI21lbnVUb2dnbGUgaW5wdXQ6Y2hlY2tlZCB+IHVsXG57XG4gIHRyYW5zZm9ybTogc2NhbGUoMS4wLCAxLjApO1xuICBvcGFjaXR5OiAxO1xufVxuLyogRW5kIG9mIEhhbWJ1cmdlciBNZW51IGNzcyAqL1xuXG5cblxuLyogKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuKioqKioqKioqKioqKioqKioqKioqKiAgTWVkaWEgcXVlcnkgZm9yIE1vYmlsZSBzY3JlZW4gICovXG5cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aCA6IDQ4MHB4KSB7XG4gICAgLmNhcmR7XG4gICAgICAgIGhlaWdodDogNzUwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6IGF1dG87XG4gICAgICAgIG1hcmdpbi1ib3R0b206IGF1dG87XG4gICAgICAgIHdpZHRoOiA0MDBweDtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLDAsMCwwLjUpICFpbXBvcnRhbnQ7XG4gICAgICAgIH1cblxufSJdfQ== */"

/***/ }),

/***/ "./src/app/edit-sub-dep/edit-sub-dep.component.html":
/*!**********************************************************!*\
  !*** ./src/app/edit-sub-dep/edit-sub-dep.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n\n  <!-- Hamburger Menu -->\n  <nav role='navigation'>\n    <div id=\"menuToggle\">\n\n      <input type=\"checkbox\" />\n\n      <span></span>\n      <span></span>\n      <span></span>\n      <ul id=\"menu\">\n        <a href=\"#\">\n          <li>Add Sub Department</li>\n        </a>\n        <a href=\"#\">\n          <li>Sub Department List</li>\n        </a>\n        <a routerLink=\"/logout\">\n          <li>Logout</li>\n        </a>\n      </ul>\n    </div>\n  </nav>\n  <!-- End of Hamburger Menu -->\n\n  <div class=\"d-flex justify-content-center h-100\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        <h3 style=\"text-align:center\">Edit Sub Department Details</h3>\n\n      </div>\n      <div class=\"card-body\">\n        <form class=\"form-login\">\n\n          <div class=\"form-group row\">\n            <label for=\"seldep\" class=\"col-sm-5 col-form-label\">Department</label>\n            <div class=\"col-sm-7\">\n              <select class=\"form-control\" id=\"seldep\" name=\"seldep\">\n                <option>Select Department</option>\n                <option selected>Inward</option>\n                <option>Accountant</option>\n                <option>Washing</option>\n                <option>Outward</option>\n              </select>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDep\" class=\"col-sm-5 col-form-label\">Sub Department</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDep\" value=\"Inward 2\">\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepPassword\" class=\"col-sm-5 col-form-label\"> Old Password</label>\n            <div class=\"col-sm-7\">\n              <input type=\"password\" value=\"***********\" class=\"form-control\" id=\"subDepPassword\">\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepPassword\" class=\"col-sm-5 col-form-label\"> New Password</label>\n            <div class=\"col-sm-7\">\n              <input type=\"password\" class=\"form-control\" id=\"subDepPassword\">\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepcfPassword\" class=\"col-sm-5 col-form-label\">Confirm Password</label>\n            <div class=\"col-sm-7\">\n              <input type=\"password\" class=\"form-control\" id=\"subDepcfPassword\">\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Remarks</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" value=\"Ramerks1, Remarks2\" id=\"subDepRemark\">\n            </div>\n          </div>\n\n          <div class=\"form-group\" style=\"text-align:center\">\n            <button style=\"padding: 0;\n            border: none;\n            background: none;\" type=\"submit\">\n              <!-- <i class=\"far fa-check-circle\"> -->\n              <i class=\"fas fa-check-circle\" (click)=\"onUpdatedSubDep()\" style=\"font-size:60px;color: yellow;\"></i>\n\n            </button>\n          </div>\n\n        </form>\n      </div>\n\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/edit-sub-dep/edit-sub-dep.component.ts":
/*!********************************************************!*\
  !*** ./src/app/edit-sub-dep/edit-sub-dep.component.ts ***!
  \********************************************************/
/*! exports provided: EditSubDepComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditSubDepComponent", function() { return EditSubDepComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EditSubDepComponent = /** @class */ (function () {
    function EditSubDepComponent(router) {
        this.router = router;
    }
    EditSubDepComponent.prototype.ngOnInit = function () {
    };
    EditSubDepComponent.prototype.onUpdatedSubDep = function () {
        this.router.navigate(['/updated_sub_department']);
    };
    EditSubDepComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-sub-dep',
            template: __webpack_require__(/*! ./edit-sub-dep.component.html */ "./src/app/edit-sub-dep/edit-sub-dep.component.html"),
            styles: [__webpack_require__(/*! ./edit-sub-dep.component.css */ "./src/app/edit-sub-dep/edit-sub-dep.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], EditSubDepComponent);
    return EditSubDepComponent;
}());



/***/ }),

/***/ "./src/app/file-upload/file-upload.component.css":
/*!*******************************************************!*\
  !*** ./src/app/file-upload/file-upload.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZpbGUtdXBsb2FkL2ZpbGUtdXBsb2FkLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/file-upload/file-upload.component.html":
/*!********************************************************!*\
  !*** ./src/app/file-upload/file-upload.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2>\n  CSV File Data\n</h2>\n\n<input type=\"file\" name=\"files\" class=\"form-control\" #uploads (change)=\"onChange(uploads.files)\" multiple value=\"process\" />\n\n<div>\n  <table style=\"width:100%\">\n    <thead>\n      <tr>\n        <td> Parent Identifier</td>\n        <td> Student Identifier</td>\n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor=\"let record of data\">\n        <td> {{record.Parent_Identifier}} </td>\n        <td> {{record.Student_Identifier}} </td>\n      </tr>\n\n    </tbody>\n  </table>\n\n</div>"

/***/ }),

/***/ "./src/app/file-upload/file-upload.component.ts":
/*!******************************************************!*\
  !*** ./src/app/file-upload/file-upload.component.ts ***!
  \******************************************************/
/*! exports provided: FileUploadComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileUploadComponent", function() { return FileUploadComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var papaparse__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! papaparse */ "./node_modules/papaparse/papaparse.min.js");
/* harmony import */ var papaparse__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(papaparse__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _Services_file_upload_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Services/file-upload.service */ "./src/app/Services/file-upload.service.ts");
// import { Component, OnInit } from '@angular/core';
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// @Component({
//   selector: 'app-file-upload',
//   templateUrl: './file-upload.component.html',
//   styleUrls: ['./file-upload.component.css']
// })
// export class FileUploadComponent implements OnInit {
//   constructor() { }
//   ngOnInit() {
//   }
// }




// import { FileUploadService } from '../file-upload.service';
var obj;
var FileUploadComponent = /** @class */ (function () {
    function FileUploadComponent(http, fileUploadService) {
        this.http = http;
        this.fileUploadService = fileUploadService;
        this.name = 'Angular 5 csv file parser example';
    }
    FileUploadComponent.prototype.ngOnInit = function () {
        var _this = this;
        obj = this;
        this.fileUploadService.getData().subscribe(function (FUData) { return _this.data = FUData; });
    };
    FileUploadComponent.prototype.onChange = function (files) {
        var _this = this;
        if (files[0]) {
            console.log(files[0]);
            papaparse__WEBPACK_IMPORTED_MODULE_1__["parse"](files[0], {
                header: true,
                skipEmptyLines: true,
                complete: function (result, file) {
                    console.log(result);
                    _this.dataList = result.data;
                    console.log(_this.dataList);
                    console.log(file);
                    var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
                    headers.append('Content-Type', 'application/json');
                    var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
                    _this.http.post('api/fileUpload', _this.dataList, options)
                        .subscribe(function (data) {
                        console.log("added successfully");
                        // this.router.navigate(['/main-masters']);
                    }, function (error) {
                        console.log(JSON.stringify(error.json()));
                    });
                }
            });
        }
    };
    FileUploadComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-file-upload',
            template: __webpack_require__(/*! ./file-upload.component.html */ "./src/app/file-upload/file-upload.component.html"),
            styles: [__webpack_require__(/*! ./file-upload.component.css */ "./src/app/file-upload/file-upload.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"], _Services_file_upload_service__WEBPACK_IMPORTED_MODULE_3__["FileUploadService"]])
    ], FileUploadComponent);
    return FileUploadComponent;
}());



/***/ }),

/***/ "./src/app/getdata/getdata.component.css":
/*!***********************************************!*\
  !*** ./src/app/getdata/getdata.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".main-table\n{\n    width:800px;\n    height:100%;\n    margin: 0 auto;\n    text-align: center;\n    margin-top:20px;\n    margin-bottom: 20px;\n    /* background-color: rgba(0,0,0,0.5) !important; */\n    /* color: #fff; */\n}\n.text-color{\n    color: cornsilk;\n    text-align: -webkit-center;\n}\n.success{\n    color: cornsilk;\n    text-align: -webkit-center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZ2V0ZGF0YS9nZXRkYXRhLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0lBRUksV0FBVztJQUNYLFdBQVc7SUFDWCxjQUFjO0lBQ2Qsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixtQkFBbUI7SUFDbkIsa0RBQWtEO0lBQ2xELGlCQUFpQjtBQUNyQjtBQUNBO0lBQ0ksZUFBZTtJQUNmLDBCQUEwQjtBQUM5QjtBQUNBO0lBQ0ksZUFBZTtJQUNmLDBCQUEwQjtBQUM5QiIsImZpbGUiOiJzcmMvYXBwL2dldGRhdGEvZ2V0ZGF0YS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1haW4tdGFibGVcbntcbiAgICB3aWR0aDo4MDBweDtcbiAgICBoZWlnaHQ6MTAwJTtcbiAgICBtYXJnaW46IDAgYXV0bztcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luLXRvcDoyMHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gICAgLyogYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLDAsMCwwLjUpICFpbXBvcnRhbnQ7ICovXG4gICAgLyogY29sb3I6ICNmZmY7ICovXG59XG4udGV4dC1jb2xvcntcbiAgICBjb2xvcjogY29ybnNpbGs7XG4gICAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XG59XG4uc3VjY2Vzc3tcbiAgICBjb2xvcjogY29ybnNpbGs7XG4gICAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XG59Il19 */"

/***/ }),

/***/ "./src/app/getdata/getdata.component.html":
/*!************************************************!*\
  !*** ./src/app/getdata/getdata.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\" style=\"text-align: -webkit-center;\">\n  <div *ngIf=\"update\" >\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.subDepPassword >\n    <input type=\"text\" name=\"name\" [(ngModel)]=datatobeedited.subDepRemark>\n    <button (click)='saveUpdate(datatobeedited)' type=\"button\" class=\"btn btn-sm btn-info\" style=\"margin-top: -5px;font-size: 13.9px;\">save</button>\n  </div>\n     \n<br>\n<br>\n  <table class=\"table\">\n    <thead>\n      <tr class=\"text-color\">\n        <th>subDepSelect</th>\n        <th>subDepartment</th>\n        <th>subDepPassword</th>\n        <th>subDepRemark </th>\n        <th>Edit</th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor='let i of test; let in=index'  class=\"success\">\n        <td>{{i.subDepSelect}}</td>\n         <td>{{i.subDepartment}}</td>\n         <td>{{i.subDepPassword}}</td>\n         <td>{{i.subDepRemark}}</td>\n         <td><button (click)='editData(i,in)' type=\"button\" class=\"btn btn-large btn-info\">Edit</button></td>  \n      </tr>\n    </tbody>\n  </table>\n</div>\n \n\n"

/***/ }),

/***/ "./src/app/getdata/getdata.component.ts":
/*!**********************************************!*\
  !*** ./src/app/getdata/getdata.component.ts ***!
  \**********************************************/
/*! exports provided: GetdataComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetdataComponent", function() { return GetdataComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Services_mydata_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Services/mydata.service */ "./src/app/Services/mydata.service.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GetdataComponent = /** @class */ (function () {
    function GetdataComponent(mydataService, _http) {
        this.mydataService = mydataService;
        this._http = _http;
        this.update = false;
    }
    GetdataComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.mydataService.getData().subscribe(function (testData) { return _this.test = testData; });
    };
    GetdataComponent.prototype.editData = function (i) {
        this.update = true;
        this.datatobeedited = i;
        console.log("obj is" + this.datatobeedited.subDepSelect);
    };
    GetdataComponent.prototype.saveUpdate = function (datasaved) {
        console.log("datatobesaved" + datasaved.subDepSelect);
        console.log("datatobesaved" + '' + datasaved.subDepPassword + '' + '' + datasaved.subDepRemark);
        var _url = "http://localhost:3000/api/data" + "/" + datasaved._id;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.put(_url, datasaved, options)
            .subscribe(function (data) {
            alert('saved Successfully');
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    GetdataComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-getdata',
            template: __webpack_require__(/*! ./getdata.component.html */ "./src/app/getdata/getdata.component.html"),
            providers: [_Services_mydata_service__WEBPACK_IMPORTED_MODULE_1__["MydataService"]],
            styles: [__webpack_require__(/*! ./getdata.component.css */ "./src/app/getdata/getdata.component.css")]
        }),
        __metadata("design:paramtypes", [_Services_mydata_service__WEBPACK_IMPORTED_MODULE_1__["MydataService"], _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], GetdataComponent);
    return GetdataComponent;
}());



/***/ }),

/***/ "./src/app/header/header.component.css":
/*!*********************************************!*\
  !*** ./src/app/header/header.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "i\r\n{\r\n    font-size: 2em;\r\n    color: white;\r\n}\r\n.line\r\n{\r\n    border-left: 1px solid white;\r\n    height: 48px;\r\n    margin-top: -8px;\r\n    margin-bottom: -8px;\r\n}\r\n.fa-arrow-left\r\n{\r\n    font-size:20px;\r\n}\r\n/* .mr-auto\r\n{\r\n    margin-left: 1500px;\r\n}\r\n\r\n.fas fa-arrow-left\r\n{\r\n    margin-right: 1500px;\r\n} */\r\nnav \r\n{\r\n    padding: 0%;\r\n}\r\n/* .right-menu\r\n{\r\n    margin-left: auto!important;\r\n} */\r\n/* .mr-auto, .mx-auto {\r\n    margin-left: auto!important;\r\n} */\r\n/* Full width inout fields */\r\ninput[type=text], input[type=password]\r\n{\r\n    width: 90%;\r\n    padding: 12px 20px;\r\n    margin: 8px 20px;\r\n    display: inline-block;\r\n    border: 1px solid #ccc;\r\n    box-sizing: border-box;\r\n    font-size: 10px;\r\n}\r\n/* Set a style for all buttons */\r\nbutton\r\n{\r\n    background-color:transparent;\r\n    color: white;\r\n    /* padding-left: 5px;\r\n    padding-right: 5px;     */\r\n    margin-top: 8px; \r\n    border: none;\r\n    font-size: 15px;\r\n   \r\n}\r\nbutton:hover\r\n{\r\n    opacity: 0.8;\r\n}\r\n/* Pop-up Button */\r\n.button\r\n{\r\n    background-color: darkgrey;\r\n    color: black;\r\n    font: 18px \"Tahoma\", Arial, Serif;\r\n    padding: 5px;\r\n}\r\n/* Center the image and position the close button */\r\n.imgcontainer\r\n{ \r\n    text-align: center;\r\n    /* margin: 50px; */\r\n    position: relative;\r\n    /* margin-left: 1300px; */\r\n    margin-top: 200px;\r\n    margin-left: 200px;\r\n}\r\n.avatar\r\n{\r\n    width: 200px;\r\n    height: 200px;\r\n    border-radius: 50%;\r\n}\r\n/* .card {\r\n    position: relative;\r\n    width: 60px;\r\n    background-color: #404040;\r\n  }\r\n   \r\n  .element {\r\n    position: absolute;\r\n    top: 10px;\r\n    right: 10px;\r\n  } */\r\n#modal-head\r\n {\r\n     position: relative;\r\n     /* margin-left:99%!important; */\r\n }\r\n/* the Modal (background) */\r\n.modal\r\n{\r\n    /* display: none;\r\n    position:absolute;\r\n    top:0;\r\n    right:0;\r\n    \r\n    \r\n    width: 15%;\r\n    height: 30%;\r\n    margin-top: 80px;\r\n    background-color: white;\r\n    border-radius: 5px; */\r\n    margin-left:83%!important;\r\n    width:230px;\r\n    height:230px;\r\n    top:0!important;\r\n    right:0!important;\r\n \r\n    \r\n    position: fixed!important;\r\n    display: none;\r\n    margin-top: 38px;\r\n    background-color: white;\r\n}\r\n/*Modal Content Box*/\r\n.modal-content\r\n{\r\n    background-color: #cacacacc;\r\n    margin: 4% auto 15% auto;\r\n    border: 1px solid #888;\r\n    width: 40%;\r\n    \r\n    padding-bottom: 30px;\r\n}\r\n/* The Closse Buttn (x) */\r\n.close\r\n{\r\n    position: absolute;\r\n    right: 10px;\r\n    /* left: 1550px; */\r\n    /* top: 30px; */\r\n    color: #000;\r\n    font-size: 35px;\r\n    font-weight: bold;\r\n    margin-top: 10px; \r\n}\r\n.close:hover,.close focus\r\n{\r\n    color: red;\r\n    cursor: pointer;\r\n}\r\n/* Add Zoom Animation */\r\n.animate\r\n{\r\n    -webkit-animation: zoom 0.6s;\r\n            animation: zoom 0.6s;\r\n}\r\n@-webkit-keyframes zoom\r\n{\r\n    from {-webkit-transform: scale(0);transform: scale(0)}\r\n    to {-webkit-transform: scale(1);transform: scale(1)}\r\n}\r\n@keyframes zoom\r\n{\r\n    from {-webkit-transform: scale(0);transform: scale(0)}\r\n    to {-webkit-transform: scale(1);transform: scale(1)}\r\n}\r\n.account\r\n{\r\n    background-color: transparent;\r\n    color: black;\r\n    font: 18px \"Tahoma\", Arial, Serif;\r\n    padding: 5px;\r\n    margin-left: 20px;\r\n    margin-top: -20px;\r\n}\r\n.sign-out\r\n{\r\n    background-color: transparent;\r\n    color: black;\r\n    font: 18px \"Tahoma\", Arial, Serif;\r\n    padding: 5px;\r\n    margin-left:100px;\r\n}\r\n.container\r\n{\r\n    padding-left: 60px;\r\n    /* padding-right: 130px; */\r\n\r\n}\r\n/*Button */\r\n/* Popup */\r\n/* \r\n.circle_container1\r\n{\r\n    width: 130px;\r\n    height: 130px;\r\n    margin: 0 auto;\r\n    padding: 0;\r\n    text-align: center;\r\n    padding: 5px;\r\n    font-size: 15px;\r\n    margin-left: 1500px;\r\n    margin-top: 80px;\r\n\t\r\n\r\n}\r\n.circle_container1 a{\r\n\ttext-decoration: none;\r\n}\r\n\r\n\r\n.circle_main1\r\n{\r\n\twidth : 100%;\r\n\tbackground-color: #3ece98;\r\n\theight : 100%;\r\n\tborder-radius : 50%;\r\n\tborder : 6px solid #ffffff;\t\r\n\tmargin : 0 auto;\r\n    padding : 0;\r\n\ttext-align: center;\r\n\toverflow: hidden;\r\n}\r\n\r\n\r\n.circle_text_container1\r\n{\r\n\t\r\n\twidth : 70%;\r\n\theight : 70%;\r\n\tmax-width : 70%;\r\n\tmax-height : 70%;\r\n\tmargin : 0;\r\n\tpadding : 0;\r\n\t\r\n\tposition : relative;\r\n\tleft : 15%;\r\n\ttop : 15%;\r\n\t\r\n\t\r\n\ttransform-style : preserve-3d;\r\n\r\n}\r\n\r\n\r\n.circle_text1\r\n{\r\n\r\n\tfont: 18px \"Tahoma\", Arial, Serif;\t\r\n    text-align : center;\r\n    color:#fff;\r\n\t\r\n\t\r\n\tposition : relative;\r\n\ttop : 50%;\r\n\ttransform : translateY(-50%);\r\n}\r\n */\r\n/* Profile  - Gmail */\r\n.profile-image\r\n {\r\n    width: 96px;\r\n    height: 96px;\r\n    margin: 0 auto;\r\n    display: block;\r\n    border-radius: 50%;\r\n }\r\n.profile-name\r\n {\r\n     font-size: 16px;\r\n     font-weight: bold;\r\n     text-align: center;\r\n     margin: 10px 0 0;\r\n     /* height: 10rem; */\r\n\r\n }\r\n.profile-email\r\n {\r\n     display: block;\r\n     padding: 0px 8px;\r\n     font-size: 14px;\r\n     color: #404040;\r\n     line-height: 2;\r\n     text-align: center;\r\n     overflow: hidden;\r\n     text-overflow: ellipsis;\r\n     white-space: nowrap;\r\n     box-sizing: border-box;\r\n }\r\n.mr-auto, .mx-auto {\r\n    margin-right: auto !important;\r\n}\r\n.ml-auto, .mx-auto {\r\n    margin-left: auto !important;\r\n}\r\n@media only screen and (max-width: 992px) \r\n{\r\n    .line \r\n    {\r\n        display: none;\r\n    }\r\n\r\n    i\r\n    {\r\n        font-size: 1em;\r\n        color: white;\r\n    }   \r\n}\r\n@media only screen and (max-width : 768px) {\r\n\r\n   .nav-item\r\n   {\r\n       right:10px;\r\n   }\r\n        \r\n\r\n    }\r\n@media only screen and (max-width : 768px) {\r\n\r\n        .modal\r\n{\r\n    /* display: none;\r\n    position:absolute;\r\n    top:0;\r\n    right:0;\r\n    \r\n    \r\n    width: 15%;\r\n    height: 30%;\r\n    margin-top: 80px;\r\n    background-color: white;\r\n    border-radius: 5px; */\r\n    margin-left:70%!important;\r\n    width:230px;\r\n    height:230px;\r\n    top:0!important;\r\n    right:0!important;\r\n \r\n    \r\n    position: fixed!important;\r\n    display: none;\r\n    margin-top: 58px;\r\n    background-color: white;\r\n}\r\n\r\n    }\r\n@media only screen and (max-width :480px) {\r\n\r\n        .modal\r\n{\r\n    /* display: none;\r\n    position:absolute;\r\n    top:0;\r\n    right:0;\r\n    \r\n    \r\n    width: 15%;\r\n    height: 30%;\r\n    margin-top: 80px;\r\n    background-color: white;\r\n    border-radius: 5px; */\r\n    margin-left:40%!important;\r\n    width:230px;\r\n    height:230px;\r\n    top:0!important;\r\n    right:0!important;\r\n \r\n    \r\n    position: fixed!important;\r\n    display: none;\r\n    margin-top: 68px;\r\n    background-color: white;\r\n}\r\n\r\n    }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztJQUVJLGNBQWM7SUFDZCxZQUFZO0FBQ2hCO0FBQ0E7O0lBRUksNEJBQTRCO0lBQzVCLFlBQVk7SUFDWixnQkFBZ0I7SUFDaEIsbUJBQW1CO0FBQ3ZCO0FBQ0E7O0lBRUksY0FBYztBQUNsQjtBQUVBOzs7Ozs7OztHQVFHO0FBQ0g7O0lBRUksV0FBVztBQUNmO0FBRUE7OztHQUdHO0FBRUg7O0dBRUc7QUFHSCw0QkFBNEI7QUFDNUI7O0lBRUksVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIscUJBQXFCO0lBQ3JCLHNCQUFzQjtJQUN0QixzQkFBc0I7SUFDdEIsZUFBZTtBQUNuQjtBQUVBLGdDQUFnQztBQUNoQzs7SUFFSSw0QkFBNEI7SUFDNUIsWUFBWTtJQUNaOzZCQUN5QjtJQUN6QixlQUFlO0lBQ2YsWUFBWTtJQUNaLGVBQWU7O0FBRW5CO0FBRUE7O0lBRUksWUFBWTtBQUNoQjtBQUVBLGtCQUFrQjtBQUNsQjs7SUFFSSwwQkFBMEI7SUFDMUIsWUFBWTtJQUNaLGlDQUFpQztJQUNqQyxZQUFZO0FBQ2hCO0FBR0EsbURBQW1EO0FBQ25EOztJQUVJLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLHlCQUF5QjtJQUN6QixpQkFBaUI7SUFDakIsa0JBQWtCO0FBQ3RCO0FBRUE7O0lBRUksWUFBWTtJQUNaLGFBQWE7SUFDYixrQkFBa0I7QUFDdEI7QUFDQTs7Ozs7Ozs7OztLQVVLO0FBRUo7O0tBRUksa0JBQWtCO0tBQ2xCLCtCQUErQjtDQUNuQztBQUNELDJCQUEyQjtBQUMzQjs7SUFFSTs7Ozs7Ozs7Ozt5QkFVcUI7SUFDckIseUJBQXlCO0lBQ3pCLFdBQVc7SUFDWCxZQUFZO0lBQ1osZUFBZTtJQUNmLGlCQUFpQjs7O0lBR2pCLHlCQUF5QjtJQUN6QixhQUFhO0lBQ2IsZ0JBQWdCO0lBQ2hCLHVCQUF1QjtBQUMzQjtBQUVBLG9CQUFvQjtBQUNwQjs7SUFFSSwyQkFBMkI7SUFDM0Isd0JBQXdCO0lBQ3hCLHNCQUFzQjtJQUN0QixVQUFVOztJQUVWLG9CQUFvQjtBQUN4QjtBQUVBLHlCQUF5QjtBQUV6Qjs7SUFFSSxrQkFBa0I7SUFDbEIsV0FBVztJQUNYLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsV0FBVztJQUNYLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsZ0JBQWdCO0FBQ3BCO0FBRUE7O0lBRUksVUFBVTtJQUNWLGVBQWU7QUFDbkI7QUFFQSx1QkFBdUI7QUFDdkI7O0lBRUksNEJBQW9CO1lBQXBCLG9CQUFvQjtBQUN4QjtBQUNBOztJQUVJLE1BQU0sMkJBQWtCLENBQWxCLG1CQUFtQjtJQUN6QixJQUFJLDJCQUFrQixDQUFsQixtQkFBbUI7QUFDM0I7QUFKQTs7SUFFSSxNQUFNLDJCQUFrQixDQUFsQixtQkFBbUI7SUFDekIsSUFBSSwyQkFBa0IsQ0FBbEIsbUJBQW1CO0FBQzNCO0FBRUE7O0lBRUksNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixpQ0FBaUM7SUFDakMsWUFBWTtJQUNaLGlCQUFpQjtJQUNqQixpQkFBaUI7QUFDckI7QUFDQTs7SUFFSSw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGlDQUFpQztJQUNqQyxZQUFZO0lBQ1osaUJBQWlCO0FBQ3JCO0FBRUE7O0lBRUksa0JBQWtCO0lBQ2xCLDBCQUEwQjs7QUFFOUI7QUFFQyxVQUFVO0FBR1gsVUFBVTtBQUNWOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7RUFrRUU7QUFFRCxxQkFBcUI7QUFDckI7O0lBRUcsV0FBVztJQUNYLFlBQVk7SUFDWixjQUFjO0lBQ2QsY0FBYztJQUNkLGtCQUFrQjtDQUNyQjtBQUVBOztLQUVJLGVBQWU7S0FDZixpQkFBaUI7S0FDakIsa0JBQWtCO0tBQ2xCLGdCQUFnQjtLQUNoQixtQkFBbUI7O0NBRXZCO0FBRUE7O0tBRUksY0FBYztLQUNkLGdCQUFnQjtLQUNoQixlQUFlO0tBQ2YsY0FBYztLQUNkLGNBQWM7S0FDZCxrQkFBa0I7S0FDbEIsZ0JBQWdCO0tBQ2hCLHVCQUF1QjtLQUN2QixtQkFBbUI7S0FDbkIsc0JBQXNCO0NBQzFCO0FBR0Q7SUFDSSw2QkFBNkI7QUFDakM7QUFFQTtJQUNJLDRCQUE0QjtBQUNoQztBQUNBOztJQUVJOztRQUVJLGFBQWE7SUFDakI7O0lBRUE7O1FBRUksY0FBYztRQUNkLFlBQVk7SUFDaEI7QUFDSjtBQUVJOztHQUVEOztPQUVJLFVBQVU7R0FDZDs7O0lBR0M7QUFFQTs7UUFFSTs7SUFFSjs7Ozs7Ozs7Ozt5QkFVcUI7SUFDckIseUJBQXlCO0lBQ3pCLFdBQVc7SUFDWCxZQUFZO0lBQ1osZUFBZTtJQUNmLGlCQUFpQjs7O0lBR2pCLHlCQUF5QjtJQUN6QixhQUFhO0lBQ2IsZ0JBQWdCO0lBQ2hCLHVCQUF1QjtBQUMzQjs7SUFFSTtBQUdBOztRQUVJOztJQUVKOzs7Ozs7Ozs7O3lCQVVxQjtJQUNyQix5QkFBeUI7SUFDekIsV0FBVztJQUNYLFlBQVk7SUFDWixlQUFlO0lBQ2YsaUJBQWlCOzs7SUFHakIseUJBQXlCO0lBQ3pCLGFBQWE7SUFDYixnQkFBZ0I7SUFDaEIsdUJBQXVCO0FBQzNCOztJQUVJIiwiZmlsZSI6InNyYy9hcHAvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaVxyXG57XHJcbiAgICBmb250LXNpemU6IDJlbTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG4ubGluZVxyXG57XHJcbiAgICBib3JkZXItbGVmdDogMXB4IHNvbGlkIHdoaXRlO1xyXG4gICAgaGVpZ2h0OiA0OHB4O1xyXG4gICAgbWFyZ2luLXRvcDogLThweDtcclxuICAgIG1hcmdpbi1ib3R0b206IC04cHg7XHJcbn1cclxuLmZhLWFycm93LWxlZnRcclxue1xyXG4gICAgZm9udC1zaXplOjIwcHg7XHJcbn1cclxuXHJcbi8qIC5tci1hdXRvXHJcbntcclxuICAgIG1hcmdpbi1sZWZ0OiAxNTAwcHg7XHJcbn1cclxuXHJcbi5mYXMgZmEtYXJyb3ctbGVmdFxyXG57XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDE1MDBweDtcclxufSAqL1xyXG5uYXYgXHJcbntcclxuICAgIHBhZGRpbmc6IDAlO1xyXG59XHJcblxyXG4vKiAucmlnaHQtbWVudVxyXG57XHJcbiAgICBtYXJnaW4tbGVmdDogYXV0byFpbXBvcnRhbnQ7XHJcbn0gKi9cclxuXHJcbi8qIC5tci1hdXRvLCAubXgtYXV0byB7XHJcbiAgICBtYXJnaW4tbGVmdDogYXV0byFpbXBvcnRhbnQ7XHJcbn0gKi9cclxuXHJcblxyXG4vKiBGdWxsIHdpZHRoIGlub3V0IGZpZWxkcyAqL1xyXG5pbnB1dFt0eXBlPXRleHRdLCBpbnB1dFt0eXBlPXBhc3N3b3JkXVxyXG57XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgcGFkZGluZzogMTJweCAyMHB4O1xyXG4gICAgbWFyZ2luOiA4cHggMjBweDtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgZm9udC1zaXplOiAxMHB4O1xyXG59XHJcblxyXG4vKiBTZXQgYSBzdHlsZSBmb3IgYWxsIGJ1dHRvbnMgKi9cclxuYnV0dG9uXHJcbntcclxuICAgIGJhY2tncm91bmQtY29sb3I6dHJhbnNwYXJlbnQ7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAvKiBwYWRkaW5nLWxlZnQ6IDVweDtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDVweDsgICAgICovXHJcbiAgICBtYXJnaW4tdG9wOiA4cHg7IFxyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICBcclxufVxyXG5cclxuYnV0dG9uOmhvdmVyXHJcbntcclxuICAgIG9wYWNpdHk6IDAuODtcclxufVxyXG5cclxuLyogUG9wLXVwIEJ1dHRvbiAqL1xyXG4uYnV0dG9uXHJcbntcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGRhcmtncmV5O1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgZm9udDogMThweCBcIlRhaG9tYVwiLCBBcmlhbCwgU2VyaWY7XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbn1cclxuXHJcblxyXG4vKiBDZW50ZXIgdGhlIGltYWdlIGFuZCBwb3NpdGlvbiB0aGUgY2xvc2UgYnV0dG9uICovXHJcbi5pbWdjb250YWluZXJcclxueyBcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIC8qIG1hcmdpbjogNTBweDsgKi9cclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIC8qIG1hcmdpbi1sZWZ0OiAxMzAwcHg7ICovXHJcbiAgICBtYXJnaW4tdG9wOiAyMDBweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAyMDBweDtcclxufVxyXG5cclxuLmF2YXRhclxyXG57XHJcbiAgICB3aWR0aDogMjAwcHg7XHJcbiAgICBoZWlnaHQ6IDIwMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG59XHJcbi8qIC5jYXJkIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHdpZHRoOiA2MHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzQwNDA0MDtcclxuICB9XHJcbiAgIFxyXG4gIC5lbGVtZW50IHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMTBweDtcclxuICAgIHJpZ2h0OiAxMHB4O1xyXG4gIH0gKi9cclxuICAgXHJcbiAjbW9kYWwtaGVhZFxyXG4ge1xyXG4gICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAvKiBtYXJnaW4tbGVmdDo5OSUhaW1wb3J0YW50OyAqL1xyXG4gfVxyXG4vKiB0aGUgTW9kYWwgKGJhY2tncm91bmQpICovXHJcbi5tb2RhbFxyXG57XHJcbiAgICAvKiBkaXNwbGF5OiBub25lO1xyXG4gICAgcG9zaXRpb246YWJzb2x1dGU7XHJcbiAgICB0b3A6MDtcclxuICAgIHJpZ2h0OjA7XHJcbiAgICBcclxuICAgIFxyXG4gICAgd2lkdGg6IDE1JTtcclxuICAgIGhlaWdodDogMzAlO1xyXG4gICAgbWFyZ2luLXRvcDogODBweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4OyAqL1xyXG4gICAgbWFyZ2luLWxlZnQ6ODMlIWltcG9ydGFudDtcclxuICAgIHdpZHRoOjIzMHB4O1xyXG4gICAgaGVpZ2h0OjIzMHB4O1xyXG4gICAgdG9wOjAhaW1wb3J0YW50O1xyXG4gICAgcmlnaHQ6MCFpbXBvcnRhbnQ7XHJcbiBcclxuICAgIFxyXG4gICAgcG9zaXRpb246IGZpeGVkIWltcG9ydGFudDtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICBtYXJnaW4tdG9wOiAzOHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi8qTW9kYWwgQ29udGVudCBCb3gqL1xyXG4ubW9kYWwtY29udGVudFxyXG57XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2FjYWNhY2M7XHJcbiAgICBtYXJnaW46IDQlIGF1dG8gMTUlIGF1dG87XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjODg4O1xyXG4gICAgd2lkdGg6IDQwJTtcclxuICAgIFxyXG4gICAgcGFkZGluZy1ib3R0b206IDMwcHg7XHJcbn1cclxuXHJcbi8qIFRoZSBDbG9zc2UgQnV0dG4gKHgpICovXHJcblxyXG4uY2xvc2Vcclxue1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgcmlnaHQ6IDEwcHg7XHJcbiAgICAvKiBsZWZ0OiAxNTUwcHg7ICovXHJcbiAgICAvKiB0b3A6IDMwcHg7ICovXHJcbiAgICBjb2xvcjogIzAwMDtcclxuICAgIGZvbnQtc2l6ZTogMzVweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDsgXHJcbn1cclxuXHJcbi5jbG9zZTpob3ZlciwuY2xvc2UgZm9jdXNcclxue1xyXG4gICAgY29sb3I6IHJlZDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuLyogQWRkIFpvb20gQW5pbWF0aW9uICovXHJcbi5hbmltYXRlXHJcbntcclxuICAgIGFuaW1hdGlvbjogem9vbSAwLjZzO1xyXG59XHJcbkBrZXlmcmFtZXMgem9vbVxyXG57XHJcbiAgICBmcm9tIHt0cmFuc2Zvcm06IHNjYWxlKDApfVxyXG4gICAgdG8ge3RyYW5zZm9ybTogc2NhbGUoMSl9XHJcbn1cclxuXHJcbi5hY2NvdW50XHJcbntcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgZm9udDogMThweCBcIlRhaG9tYVwiLCBBcmlhbCwgU2VyaWY7XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMjBweDtcclxuICAgIG1hcmdpbi10b3A6IC0yMHB4O1xyXG59XHJcbi5zaWduLW91dFxyXG57XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIGZvbnQ6IDE4cHggXCJUYWhvbWFcIiwgQXJpYWwsIFNlcmlmO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6MTAwcHg7XHJcbn1cclxuXHJcbi5jb250YWluZXJcclxue1xyXG4gICAgcGFkZGluZy1sZWZ0OiA2MHB4O1xyXG4gICAgLyogcGFkZGluZy1yaWdodDogMTMwcHg7ICovXHJcblxyXG59XHJcblxyXG4gLypCdXR0b24gKi9cclxuXHJcblxyXG4vKiBQb3B1cCAqL1xyXG4vKiBcclxuLmNpcmNsZV9jb250YWluZXIxXHJcbntcclxuICAgIHdpZHRoOiAxMzBweDtcclxuICAgIGhlaWdodDogMTMwcHg7XHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMTUwMHB4O1xyXG4gICAgbWFyZ2luLXRvcDogODBweDtcclxuXHRcclxuXHJcbn1cclxuLmNpcmNsZV9jb250YWluZXIxIGF7XHJcblx0dGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG59XHJcblxyXG5cclxuLmNpcmNsZV9tYWluMVxyXG57XHJcblx0d2lkdGggOiAxMDAlO1xyXG5cdGJhY2tncm91bmQtY29sb3I6ICMzZWNlOTg7XHJcblx0aGVpZ2h0IDogMTAwJTtcclxuXHRib3JkZXItcmFkaXVzIDogNTAlO1xyXG5cdGJvcmRlciA6IDZweCBzb2xpZCAjZmZmZmZmO1x0XHJcblx0bWFyZ2luIDogMCBhdXRvO1xyXG4gICAgcGFkZGluZyA6IDA7XHJcblx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdG92ZXJmbG93OiBoaWRkZW47XHJcbn1cclxuXHJcblxyXG4uY2lyY2xlX3RleHRfY29udGFpbmVyMVxyXG57XHJcblx0XHJcblx0d2lkdGggOiA3MCU7XHJcblx0aGVpZ2h0IDogNzAlO1xyXG5cdG1heC13aWR0aCA6IDcwJTtcclxuXHRtYXgtaGVpZ2h0IDogNzAlO1xyXG5cdG1hcmdpbiA6IDA7XHJcblx0cGFkZGluZyA6IDA7XHJcblx0XHJcblx0cG9zaXRpb24gOiByZWxhdGl2ZTtcclxuXHRsZWZ0IDogMTUlO1xyXG5cdHRvcCA6IDE1JTtcclxuXHRcclxuXHRcclxuXHR0cmFuc2Zvcm0tc3R5bGUgOiBwcmVzZXJ2ZS0zZDtcclxuXHJcbn1cclxuXHJcblxyXG4uY2lyY2xlX3RleHQxXHJcbntcclxuXHJcblx0Zm9udDogMThweCBcIlRhaG9tYVwiLCBBcmlhbCwgU2VyaWY7XHRcclxuICAgIHRleHQtYWxpZ24gOiBjZW50ZXI7XHJcbiAgICBjb2xvcjojZmZmO1xyXG5cdFxyXG5cdFxyXG5cdHBvc2l0aW9uIDogcmVsYXRpdmU7XHJcblx0dG9wIDogNTAlO1xyXG5cdHRyYW5zZm9ybSA6IHRyYW5zbGF0ZVkoLTUwJSk7XHJcbn1cclxuICovXHJcblxyXG4gLyogUHJvZmlsZSAgLSBHbWFpbCAqL1xyXG4gLnByb2ZpbGUtaW1hZ2VcclxuIHtcclxuICAgIHdpZHRoOiA5NnB4O1xyXG4gICAgaGVpZ2h0OiA5NnB4O1xyXG4gICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuIH1cclxuXHJcbiAucHJvZmlsZS1uYW1lXHJcbiB7XHJcbiAgICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICBtYXJnaW46IDEwcHggMCAwO1xyXG4gICAgIC8qIGhlaWdodDogMTByZW07ICovXHJcblxyXG4gfVxyXG5cclxuIC5wcm9maWxlLWVtYWlsXHJcbiB7XHJcbiAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgcGFkZGluZzogMHB4IDhweDtcclxuICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgY29sb3I6ICM0MDQwNDA7XHJcbiAgICAgbGluZS1oZWlnaHQ6IDI7XHJcbiAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gfVxyXG5cclxuXHJcbi5tci1hdXRvLCAubXgtYXV0byB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG8gIWltcG9ydGFudDtcclxufVxyXG5cclxuLm1sLWF1dG8sIC5teC1hdXRvIHtcclxuICAgIG1hcmdpbi1sZWZ0OiBhdXRvICFpbXBvcnRhbnQ7XHJcbn1cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTJweCkgXHJcbntcclxuICAgIC5saW5lIFxyXG4gICAge1xyXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICB9XHJcblxyXG4gICAgaVxyXG4gICAge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMWVtO1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIH0gICBcclxufVxyXG5cclxuICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aCA6IDc2OHB4KSB7XHJcblxyXG4gICAubmF2LWl0ZW1cclxuICAge1xyXG4gICAgICAgcmlnaHQ6MTBweDtcclxuICAgfVxyXG4gICAgICAgIFxyXG5cclxuICAgIH1cclxuXHJcbiAgICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGggOiA3NjhweCkge1xyXG5cclxuICAgICAgICAubW9kYWxcclxue1xyXG4gICAgLyogZGlzcGxheTogbm9uZTtcclxuICAgIHBvc2l0aW9uOmFic29sdXRlO1xyXG4gICAgdG9wOjA7XHJcbiAgICByaWdodDowO1xyXG4gICAgXHJcbiAgICBcclxuICAgIHdpZHRoOiAxNSU7XHJcbiAgICBoZWlnaHQ6IDMwJTtcclxuICAgIG1hcmdpbi10b3A6IDgwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDsgKi9cclxuICAgIG1hcmdpbi1sZWZ0OjcwJSFpbXBvcnRhbnQ7XHJcbiAgICB3aWR0aDoyMzBweDtcclxuICAgIGhlaWdodDoyMzBweDtcclxuICAgIHRvcDowIWltcG9ydGFudDtcclxuICAgIHJpZ2h0OjAhaW1wb3J0YW50O1xyXG4gXHJcbiAgICBcclxuICAgIHBvc2l0aW9uOiBmaXhlZCFpbXBvcnRhbnQ7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgbWFyZ2luLXRvcDogNThweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4gICAgfVxyXG4gICAgXHJcblxyXG4gICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoIDo0ODBweCkge1xyXG5cclxuICAgICAgICAubW9kYWxcclxue1xyXG4gICAgLyogZGlzcGxheTogbm9uZTtcclxuICAgIHBvc2l0aW9uOmFic29sdXRlO1xyXG4gICAgdG9wOjA7XHJcbiAgICByaWdodDowO1xyXG4gICAgXHJcbiAgICBcclxuICAgIHdpZHRoOiAxNSU7XHJcbiAgICBoZWlnaHQ6IDMwJTtcclxuICAgIG1hcmdpbi10b3A6IDgwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDsgKi9cclxuICAgIG1hcmdpbi1sZWZ0OjQwJSFpbXBvcnRhbnQ7XHJcbiAgICB3aWR0aDoyMzBweDtcclxuICAgIGhlaWdodDoyMzBweDtcclxuICAgIHRvcDowIWltcG9ydGFudDtcclxuICAgIHJpZ2h0OjAhaW1wb3J0YW50O1xyXG4gXHJcbiAgICBcclxuICAgIHBvc2l0aW9uOiBmaXhlZCFpbXBvcnRhbnQ7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgbWFyZ2luLXRvcDogNjhweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4gICAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/header/header.component.html":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Top Navbar -->\r\n<nav class=\"navbar navbar-expand-lg \" style=\"background:  rgba(0,0,0,0.5);\">\r\n\r\n    <button class=\"navbar-toggler d-inline-block d-lg-none ml-auto\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\"\r\n        aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n        <span class=\"navbar-toggler-icon\" style=\"color:black;\">\r\n            <i class=\"fa fa-bars\" aria-hidden=\"true\"></i>\r\n        </span>\r\n    </button>\r\n\r\n\r\n\r\n    <div class=\"collapse navbar-collapse mx-auto\" id=\"navbarSupportedContent\">\r\n\r\n        <ul class=\"navbar-nav ml-auto  text-right\">\r\n\r\n            <div class=\"line\"></div>\r\n\r\n\r\n            <li class=\"nav-item\">\r\n                <a class=\"nav-link\" style=\"color: white;\">\r\n                    {{title}}\r\n                </a>\r\n            </li>\r\n            <div class=\"line\"></div>\r\n\r\n            <!-- <button class=\"button\" onclick=\"document.getElementById('modal-wrapper').style.display='block'\">\r\n                            {{title}} Login\r\n                    </button> -->\r\n            <li class=\"nav-item \">\r\n                <button onclick=\"document.getElementById('modal-wrapper').style.display='block'\">{{logout}}</button>\r\n\r\n            </li>\r\n\r\n        </ul>\r\n    </div>\r\n</nav>\r\n\r\n\r\n\r\n\r\n<!-- End Top Navbar -->\r\n\r\n<div id=\"modal-head\">\r\n    <div id=\"modal-wrapper\" class=\"modal\">\r\n\r\n        <div class=\"container\">\r\n            <div class=\"row\">\r\n                <div>\r\n                    <span onclick=\"document.getElementById('modal-wrapper').style.display='none'\" class=\"close\" title=\"Close Popup\">&times;</span>\r\n\r\n                    <div class=\"account-wall\">\r\n                        <img src=\"https://i2.wp.com/www.naturallysensible.com/wp-content/uploads/2017/10/login-user-icon.png?resize=200%2C200&ssl=1\"\r\n                            alt=\"\" class=\"profile-image\">\r\n                        <p class=\"profile-name\">Parallel Reality</p>\r\n                        <span class=\"profile-emil\">info@parallelreality.co.uk</span>\r\n\r\n\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n\r\n        </div>\r\n        <hr>\r\n        <div>\r\n\r\n            <span><button routerLink=\"/\" class=\"sign-out\" type=\"submit\">Sign out</button></span>\r\n        </div>\r\n\r\n    </div>\r\n</div>\r\n\r\n<script>\r\n    //If User clicks outside of the modal. Modal will close\r\n\r\n    var modal = document.getElementById('modal-wrapper');\r\n    window.onclick = function (event) {\r\n        if (event.target == modal) {\r\n            modal.style.display = \"none\";\r\n        }\r\n    }\r\n</script>\r\n\r\n<!-- \r\n<head>\r\n\r\n<meta charset=\"utf-8\">\r\n<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\r\n\r\n\r\n<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css\" integrity=\"sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS\" crossorigin=\"anonymous\">\r\n<link rel=\"stylesheet\" href=\"header.component.css\">\r\n\r\n<title>Logout</title>\r\n</head>\r\n<body>\r\n\r\n\r\n<div class=\"container\">\r\n    <div class=\"row\">\r\n        <div class=\"col-am-6 col-md-4 col-md-offset-4\">\r\n            <h1 class=\"text-center\">Sign In</h1>\r\n            <div class=\"account-wall\">\r\n                <img src=\"https://i2.wp.com/www.naturallysensible.com/wp-content/uploads/2017/10/login-user-icon.png?resize=200%2C200&ssl=1\" alt=\"\" class=\"profile-image\">\r\n                <p class=\"profile-name\">Parallel Reality</p>\r\n                <span class=\"profile-emil\">info@parallelreality.co.uk</span>\r\n                <form action=\"#\" class=\"form-signin\">\r\n                    <input type=\"password\" class=\"form-control\" placeholder=\"password\" required=\"true\" autofocus=\"true\">\r\n                    <button class=\"btn btn-lg btn-primary btn-block\">Sign Up</button>\r\n                    <a href=\"#\" class=\"need-help\">Need Help?</a><span class=\"clearfix\"></span>\r\n                </form>\r\n            </div>\r\n        </div>\r\n        <a href=\"#\" class=\"new-account\">Sign in with a different Account</a>\r\n    </div>\r\n</div>\r\n\r\n\r\n<script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>\r\n<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js\" integrity=\"sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut\" crossorigin=\"anonymous\"></script>\r\n<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js\" integrity=\"sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k\" crossorigin=\"anonymous\"></script>\r\n</body> -->"

/***/ }),

/***/ "./src/app/header/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Services_header_title_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(headerTitleService) {
        this.headerTitleService = headerTitleService;
        this.title = '';
        this.logout = '';
    }
    HeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.headerTitleService.title.subscribe(function (updatedTitle) {
            _this.title = updatedTitle;
        });
        this.headerTitleService.logout.subscribe(function (logoutTitle) {
            _this.logout = logoutTitle;
        });
    };
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/header/header.component.css")]
        }),
        __metadata("design:paramtypes", [_Services_header_title_service__WEBPACK_IMPORTED_MODULE_1__["HeaderTitleService"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/logout/logout.component.css":
/*!*********************************************!*\
  !*** ./src/app/logout/logout.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n@import url('https://fonts.googleapis.com/css?family=Numans');\n/* Made with love by Mutiullah Samim*/\nhtml,body{\n\nbackground-size: cover;\nbackground-repeat: no-repeat;\nheight: 100%;\nfont-family: 'Numans', sans-serif;\n}\n.container{\nheight: 100%;\nbackground-size: cover;\nwidth:100%;\nbackground-repeat: no-repeat;\n/* align-content: center; */\n}\n.card{\nheight: 370px;\nmargin-top: auto;\nmargin-bottom: auto;\nwidth: 400px;\nbackground-color: rgba(0,0,0,0.5) !important;\n}\n.social_icon span{\nfont-size: 60px;\nmargin-left: 10px;\ncolor: #FFC312;\n}\n.social_icon span:hover{\ncolor: white;\ncursor: pointer;\n}\n.card-header h3{\ncolor: white;\n}\n.social_icon{\nposition: absolute;\nright: 20px;\ntop: -45px;\n}\n.input-group-prepend span{\nwidth: 50px;\nbackground-color: #FFC312;\ncolor: black;\nborder:0 !important;\n}\ninput:focus{\noutline: 0 0 0 0  !important;\nbox-shadow: 0 0 0 0 !important;\n\n}\n.remember{\ncolor: white;\n}\n.remember input\n{\nwidth: 20px;\nheight: 20px;\nmargin-left: 15px;\nmargin-right: 5px;\n}\n.login_btn{\ncolor: black;\nbackground-color: #FFC312;\nwidth: 100px;\n}\n.login_btn:hover{\ncolor: black;\nbackground-color: white;\n}\n.links{\ncolor: white;\n}\n.links a{\nmargin-left: 4px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9nb3V0L2xvZ291dC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUEsNkRBQTZEO0FBRjdELHFDQUFxQztBQUlyQzs7QUFFQSxzQkFBc0I7QUFDdEIsNEJBQTRCO0FBQzVCLFlBQVk7QUFDWixpQ0FBaUM7QUFDakM7QUFFQTtBQUNBLFlBQVk7QUFDWixzQkFBc0I7QUFDdEIsVUFBVTtBQUNWLDRCQUE0QjtBQUM1QiwyQkFBMkI7QUFDM0I7QUFFQTtBQUNBLGFBQWE7QUFDYixnQkFBZ0I7QUFDaEIsbUJBQW1CO0FBQ25CLFlBQVk7QUFDWiw0Q0FBNEM7QUFDNUM7QUFFQTtBQUNBLGVBQWU7QUFDZixpQkFBaUI7QUFDakIsY0FBYztBQUNkO0FBRUE7QUFDQSxZQUFZO0FBQ1osZUFBZTtBQUNmO0FBRUE7QUFDQSxZQUFZO0FBQ1o7QUFFQTtBQUNBLGtCQUFrQjtBQUNsQixXQUFXO0FBQ1gsVUFBVTtBQUNWO0FBRUE7QUFDQSxXQUFXO0FBQ1gseUJBQXlCO0FBQ3pCLFlBQVk7QUFDWixtQkFBbUI7QUFDbkI7QUFFQTtBQUNBLDRCQUE0QjtBQUM1Qiw4QkFBOEI7O0FBRTlCO0FBRUE7QUFDQSxZQUFZO0FBQ1o7QUFFQTs7QUFFQSxXQUFXO0FBQ1gsWUFBWTtBQUNaLGlCQUFpQjtBQUNqQixpQkFBaUI7QUFDakI7QUFFQTtBQUNBLFlBQVk7QUFDWix5QkFBeUI7QUFDekIsWUFBWTtBQUNaO0FBRUE7QUFDQSxZQUFZO0FBQ1osdUJBQXVCO0FBQ3ZCO0FBRUE7QUFDQSxZQUFZO0FBQ1o7QUFFQTtBQUNBLGdCQUFnQjtBQUNoQiIsImZpbGUiOiJzcmMvYXBwL2xvZ291dC9sb2dvdXQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIE1hZGUgd2l0aCBsb3ZlIGJ5IE11dGl1bGxhaCBTYW1pbSovXG5cbkBpbXBvcnQgdXJsKCdodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2Nzcz9mYW1pbHk9TnVtYW5zJyk7XG5cbmh0bWwsYm9keXtcblxuYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbmJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG5oZWlnaHQ6IDEwMCU7XG5mb250LWZhbWlseTogJ051bWFucycsIHNhbnMtc2VyaWY7XG59XG5cbi5jb250YWluZXJ7XG5oZWlnaHQ6IDEwMCU7XG5iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xud2lkdGg6MTAwJTtcbmJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4vKiBhbGlnbi1jb250ZW50OiBjZW50ZXI7ICovXG59XG5cbi5jYXJke1xuaGVpZ2h0OiAzNzBweDtcbm1hcmdpbi10b3A6IGF1dG87XG5tYXJnaW4tYm90dG9tOiBhdXRvO1xud2lkdGg6IDQwMHB4O1xuYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLDAsMCwwLjUpICFpbXBvcnRhbnQ7XG59XG5cbi5zb2NpYWxfaWNvbiBzcGFue1xuZm9udC1zaXplOiA2MHB4O1xubWFyZ2luLWxlZnQ6IDEwcHg7XG5jb2xvcjogI0ZGQzMxMjtcbn1cblxuLnNvY2lhbF9pY29uIHNwYW46aG92ZXJ7XG5jb2xvcjogd2hpdGU7XG5jdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5jYXJkLWhlYWRlciBoM3tcbmNvbG9yOiB3aGl0ZTtcbn1cblxuLnNvY2lhbF9pY29ue1xucG9zaXRpb246IGFic29sdXRlO1xucmlnaHQ6IDIwcHg7XG50b3A6IC00NXB4O1xufVxuXG4uaW5wdXQtZ3JvdXAtcHJlcGVuZCBzcGFue1xud2lkdGg6IDUwcHg7XG5iYWNrZ3JvdW5kLWNvbG9yOiAjRkZDMzEyO1xuY29sb3I6IGJsYWNrO1xuYm9yZGVyOjAgIWltcG9ydGFudDtcbn1cblxuaW5wdXQ6Zm9jdXN7XG5vdXRsaW5lOiAwIDAgMCAwICAhaW1wb3J0YW50O1xuYm94LXNoYWRvdzogMCAwIDAgMCAhaW1wb3J0YW50O1xuXG59XG5cbi5yZW1lbWJlcntcbmNvbG9yOiB3aGl0ZTtcbn1cblxuLnJlbWVtYmVyIGlucHV0XG57XG53aWR0aDogMjBweDtcbmhlaWdodDogMjBweDtcbm1hcmdpbi1sZWZ0OiAxNXB4O1xubWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5sb2dpbl9idG57XG5jb2xvcjogYmxhY2s7XG5iYWNrZ3JvdW5kLWNvbG9yOiAjRkZDMzEyO1xud2lkdGg6IDEwMHB4O1xufVxuXG4ubG9naW5fYnRuOmhvdmVye1xuY29sb3I6IGJsYWNrO1xuYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG5cbi5saW5rc3tcbmNvbG9yOiB3aGl0ZTtcbn1cblxuLmxpbmtzIGF7XG5tYXJnaW4tbGVmdDogNHB4O1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/logout/logout.component.html":
/*!**********************************************!*\
  !*** ./src/app/logout/logout.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"d-flex justify-content-center h-100\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        <h3 style=\"text-align:center\">Login</h3>\n\n      </div>\n      <div class=\"card-body\">\n        <form class=\"form-login\">\n          <div class=\"input-group form-group\">\n            <div class=\"input-group-prepend\">\n              <span class=\"input-group-text\"><i class=\"fas fa-user\"></i></span>\n            </div>\n            <input type=\"text\" class=\"form-control\" placeholder=\"username\">\n          </div>\n          <div class=\"input-group form-group\">\n            <div class=\"input-group-prepend\">\n              <span class=\"input-group-text\"><i class=\"fas fa-key\"></i></span>\n            </div>\n            <input type=\"password\" class=\"form-control\" placeholder=\"password\">\n          </div>\n          <!-- <div class=\"row align-items-center remember \">\n            <input type=\"checkbox\" style=\"zoom:.7;\">Remember Me\n          </div> -->\n          <div class=\"form-group\" style=\"text-align:center\">\n            <input type=\"submit\" (click)=\"onClick()\" value=\"Login\" class=\"btn  login_btn\">\n          </div>\n        </form>\n      </div>\n      <div class=\"card-footer\">\n        <div class=\"d-flex justify-content-center\">\n          <a href=\"#\">Forgot your password?</a>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/logout/logout.component.ts":
/*!********************************************!*\
  !*** ./src/app/logout/logout.component.ts ***!
  \********************************************/
/*! exports provided: LogoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogoutComponent", function() { return LogoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LogoutComponent = /** @class */ (function () {
    function LogoutComponent() {
    }
    LogoutComponent.prototype.ngOnInit = function () {
    };
    LogoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-logout',
            template: __webpack_require__(/*! ./logout.component.html */ "./src/app/logout/logout.component.html"),
            styles: [__webpack_require__(/*! ./logout.component.css */ "./src/app/logout/logout.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], LogoutComponent);
    return LogoutComponent;
}());



/***/ }),

/***/ "./src/app/reset-password/reset-password.component.css":
/*!*************************************************************!*\
  !*** ./src/app/reset-password/reset-password.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n@import url('https://fonts.googleapis.com/css?family=Numans');\n/* Made with love by Mutiullah Samim*/\nhtml,body{\n\nbackground-size: cover;\nbackground-repeat: no-repeat;\nheight: 100%;\nfont-family: 'Numans', sans-serif;\n}\nlabel\n{\n    color: #fff;\n}\n.container{\nheight: 100%;\nbackground-size: cover;\nwidth:100%;\nbackground-repeat: no-repeat;\n/* align-content: center; */\n}\n.card{\nheight: 370px;\nmargin-top: auto;\nmargin-bottom: auto;\nwidth: 400px;\nbackground-color: rgba(0,0,0,0.5) !important;\n}\n.social_icon span{\nfont-size: 60px;\nmargin-left: 10px;\ncolor: #FFC312;\n}\n.social_icon span:hover{\ncolor: white;\ncursor: pointer;\n}\n.card-header h3{\ncolor: white;\n}\n.social_icon{\nposition: absolute;\nright: 20px;\ntop: -45px;\n}\n.input-group-prepend span{\nwidth: 50px;\nbackground-color: #FFC312;\ncolor: black;\nborder:0 !important;\n}\ninput:focus{\noutline: 0 0 0 0  !important;\nbox-shadow: 0 0 0 0 !important;\n\n}\n.remember{\ncolor: white;\n}\n.remember input\n{\nwidth: 20px;\nheight: 20px;\nmargin-left: 15px;\nmargin-right: 5px;\n}\n.login_btn{\ncolor: black;\nbackground-color: #FFC312;\nwidth: 100px;\n}\n.login_btn:hover{\ncolor: black;\nbackground-color: white;\n}\n.links{\ncolor: white;\n}\n.links a{\nmargin-left: 4px;\n}\n/* .field-icon {\n    float: right;\n    margin-left: -18px;\n    margin-top: 15px;\n  \n    position: relative;\n    z-index: 2;\n  } */\n/* *************************************************************************\n**********************  Media query for Mobile screen  */\n@media only screen and (max-width : 480px) {\n    .card{\n        height: 500px;\n        margin-top: auto;\n        margin-bottom: auto;\n        width: 400px;\n        background-color: rgba(0,0,0,0.5) !important;\n        }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVzZXQtcGFzc3dvcmQvcmVzZXQtcGFzc3dvcmQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLDZEQUE2RDtBQUY3RCxxQ0FBcUM7QUFJckM7O0FBRUEsc0JBQXNCO0FBQ3RCLDRCQUE0QjtBQUM1QixZQUFZO0FBQ1osaUNBQWlDO0FBQ2pDO0FBQ0E7O0lBRUksV0FBVztBQUNmO0FBQ0E7QUFDQSxZQUFZO0FBQ1osc0JBQXNCO0FBQ3RCLFVBQVU7QUFDViw0QkFBNEI7QUFDNUIsMkJBQTJCO0FBQzNCO0FBRUE7QUFDQSxhQUFhO0FBQ2IsZ0JBQWdCO0FBQ2hCLG1CQUFtQjtBQUNuQixZQUFZO0FBQ1osNENBQTRDO0FBQzVDO0FBRUE7QUFDQSxlQUFlO0FBQ2YsaUJBQWlCO0FBQ2pCLGNBQWM7QUFDZDtBQUVBO0FBQ0EsWUFBWTtBQUNaLGVBQWU7QUFDZjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxrQkFBa0I7QUFDbEIsV0FBVztBQUNYLFVBQVU7QUFDVjtBQUVBO0FBQ0EsV0FBVztBQUNYLHlCQUF5QjtBQUN6QixZQUFZO0FBQ1osbUJBQW1CO0FBQ25CO0FBRUE7QUFDQSw0QkFBNEI7QUFDNUIsOEJBQThCOztBQUU5QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7O0FBRUEsV0FBVztBQUNYLFlBQVk7QUFDWixpQkFBaUI7QUFDakIsaUJBQWlCO0FBQ2pCO0FBRUE7QUFDQSxZQUFZO0FBQ1oseUJBQXlCO0FBQ3pCLFlBQVk7QUFDWjtBQUVBO0FBQ0EsWUFBWTtBQUNaLHVCQUF1QjtBQUN2QjtBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBRUE7QUFDQSxnQkFBZ0I7QUFDaEI7QUFJQTs7Ozs7OztLQU9LO0FBQ0w7d0RBQ3dEO0FBRXhEO0lBQ0k7UUFDSSxhQUFhO1FBQ2IsZ0JBQWdCO1FBQ2hCLG1CQUFtQjtRQUNuQixZQUFZO1FBQ1osNENBQTRDO1FBQzVDO0FBQ1IiLCJmaWxlIjoic3JjL2FwcC9yZXNldC1wYXNzd29yZC9yZXNldC1wYXNzd29yZC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogTWFkZSB3aXRoIGxvdmUgYnkgTXV0aXVsbGFoIFNhbWltKi9cblxuQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1OdW1hbnMnKTtcblxuaHRtbCxib2R5e1xuXG5iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbmhlaWdodDogMTAwJTtcbmZvbnQtZmFtaWx5OiAnTnVtYW5zJywgc2Fucy1zZXJpZjtcbn1cbmxhYmVsXG57XG4gICAgY29sb3I6ICNmZmY7XG59XG4uY29udGFpbmVye1xuaGVpZ2h0OiAxMDAlO1xuYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbndpZHRoOjEwMCU7XG5iYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuLyogYWxpZ24tY29udGVudDogY2VudGVyOyAqL1xufVxuXG4uY2FyZHtcbmhlaWdodDogMzcwcHg7XG5tYXJnaW4tdG9wOiBhdXRvO1xubWFyZ2luLWJvdHRvbTogYXV0bztcbndpZHRoOiA0MDBweDtcbmJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O1xufVxuXG4uc29jaWFsX2ljb24gc3BhbntcbmZvbnQtc2l6ZTogNjBweDtcbm1hcmdpbi1sZWZ0OiAxMHB4O1xuY29sb3I6ICNGRkMzMTI7XG59XG5cbi5zb2NpYWxfaWNvbiBzcGFuOmhvdmVye1xuY29sb3I6IHdoaXRlO1xuY3Vyc29yOiBwb2ludGVyO1xufVxuXG4uY2FyZC1oZWFkZXIgaDN7XG5jb2xvcjogd2hpdGU7XG59XG5cbi5zb2NpYWxfaWNvbntcbnBvc2l0aW9uOiBhYnNvbHV0ZTtcbnJpZ2h0OiAyMHB4O1xudG9wOiAtNDVweDtcbn1cblxuLmlucHV0LWdyb3VwLXByZXBlbmQgc3BhbntcbndpZHRoOiA1MHB4O1xuYmFja2dyb3VuZC1jb2xvcjogI0ZGQzMxMjtcbmNvbG9yOiBibGFjaztcbmJvcmRlcjowICFpbXBvcnRhbnQ7XG59XG5cbmlucHV0OmZvY3Vze1xub3V0bGluZTogMCAwIDAgMCAgIWltcG9ydGFudDtcbmJveC1zaGFkb3c6IDAgMCAwIDAgIWltcG9ydGFudDtcblxufVxuXG4ucmVtZW1iZXJ7XG5jb2xvcjogd2hpdGU7XG59XG5cbi5yZW1lbWJlciBpbnB1dFxue1xud2lkdGg6IDIwcHg7XG5oZWlnaHQ6IDIwcHg7XG5tYXJnaW4tbGVmdDogMTVweDtcbm1hcmdpbi1yaWdodDogNXB4O1xufVxuXG4ubG9naW5fYnRue1xuY29sb3I6IGJsYWNrO1xuYmFja2dyb3VuZC1jb2xvcjogI0ZGQzMxMjtcbndpZHRoOiAxMDBweDtcbn1cblxuLmxvZ2luX2J0bjpob3ZlcntcbmNvbG9yOiBibGFjaztcbmJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuXG4ubGlua3N7XG5jb2xvcjogd2hpdGU7XG59XG5cbi5saW5rcyBhe1xubWFyZ2luLWxlZnQ6IDRweDtcbn1cblxuXG5cbi8qIC5maWVsZC1pY29uIHtcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgbWFyZ2luLWxlZnQ6IC0xOHB4O1xuICAgIG1hcmdpbi10b3A6IDE1cHg7XG4gIFxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB6LWluZGV4OiAyO1xuICB9ICovXG4vKiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4qKioqKioqKioqKioqKioqKioqKioqICBNZWRpYSBxdWVyeSBmb3IgTW9iaWxlIHNjcmVlbiAgKi9cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoIDogNDgwcHgpIHtcbiAgICAuY2FyZHtcbiAgICAgICAgaGVpZ2h0OiA1MDBweDtcbiAgICAgICAgbWFyZ2luLXRvcDogYXV0bztcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogYXV0bztcbiAgICAgICAgd2lkdGg6IDQwMHB4O1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuNSkgIWltcG9ydGFudDtcbiAgICAgICAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/reset-password/reset-password.component.html":
/*!**************************************************************!*\
  !*** ./src/app/reset-password/reset-password.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"d-flex justify-content-center h-100\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        <h3 style=\"text-align:center\">Change Password</h3>\n\n      </div>\n      <div class=\"card-body\">\n        <form class=\"form-login\" #resetRef='ngForm' (ngSubmit)='onClient(resetRef.value)'>\n\n          <div class=\"form-group row\">\n            <label for=\"oldPassword\" class=\"col-sm-5 col-form-label\">Old Password</label>\n            <div class=\"col-sm-7\">\n              <input type=\"password\" class=\"form-control\" id=\"oldPassword\" #oldPassword='ngModel' name=\"oldpassword\"\n                required ngModel>\n            </div>\n            <!-- <span toggle=\"#password-field\" class=\"fa fa-fw fa-eye field-icon toggle-password\"></span> -->\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"newPassword\" class=\"col-sm-5 col-form-label\"> New Password</label>\n            <div class=\"col-sm-7\">\n              <input type=\"password\" class=\"form-control\" id=\"newPassword\" #newPassword='ngModel' name=\"newpassword\"\n                required ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"confirmPassword\" class=\"col-sm-5 col-form-label\">Confirm Password</label>\n            <div class=\"col-sm-7\">\n              <input type=\"password\" class=\"form-control\" id=\"confirmPassword\" #confirmPassword='ngModel' name=\"confirmpassword\"\n                required ngModel>\n            </div>\n          </div>\n\n          <div class=\"form-group\" style=\"text-align:center\">\n            <input type=\"submit\" value=\"Confirm\" class=\"btn  login_btn\">\n          </div>\n        </form>\n      </div>\n      <div class=\"card-footer\">\n        <div class=\"d-flex justify-content-center\">\n          <a href=\"#\">Forgot your password?</a>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/reset-password/reset-password.component.ts":
/*!************************************************************!*\
  !*** ./src/app/reset-password/reset-password.component.ts ***!
  \************************************************************/
/*! exports provided: ResetPasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetPasswordComponent", function() { return ResetPasswordComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ResetPasswordComponent = /** @class */ (function () {
    function ResetPasswordComponent(router, _http) {
        this.router = router;
        this._http = _http;
    }
    ResetPasswordComponent.prototype.ngOnInit = function () {
    };
    ResetPasswordComponent.prototype.onClient = function (a) {
        var _this = this;
        this.oldpassword = a.oldpassword;
        this.newpassword = a.newpassword;
        this.confirmpassword = a.confirmpassword;
        console.log('posted');
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.post('api/signup', a, options)
            .subscribe(function (data) {
            alert("successfully posted");
            _this.router.navigate(['/client_login']);
        }, function (error) {
            alert("failed to add");
            //console.log(JSON.stringify(error.json()));
        });
    };
    ResetPasswordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-reset-password',
            template: __webpack_require__(/*! ./reset-password.component.html */ "./src/app/reset-password/reset-password.component.html"),
            styles: [__webpack_require__(/*! ./reset-password.component.css */ "./src/app/reset-password/reset-password.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], ResetPasswordComponent);
    return ResetPasswordComponent;
}());



/***/ }),

/***/ "./src/app/signin/signin.component.css":
/*!*********************************************!*\
  !*** ./src/app/signin/signin.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* \r\n\tbody {\r\n        color: #434343;\r\n\t\tbackground: #dfe7e9;\r\n\t\tfont-family: 'Varela Round', sans-serif;\r\n\t} */\r\n    .form-control {\r\n\t\tfont-size: 16px;\r\n\t\ttransition: all 0.4s;\r\n\t\tbox-shadow: none;\r\n\t}\r\n    .form-control:focus {\r\n        border-color: #5cb85c;\r\n    }\r\n    .form-control, .btn {\r\n        border-radius: 50px;\r\n\t\toutline: none !important;\r\n    }\r\n    .signin-form {\r\n\t\twidth: 400px;\r\n    \tmargin: 0 auto;\r\n\t\tpadding: 30px 0;\r\n\t}\r\n    .signin-form form {\r\n\t\tborder-radius: 5px;\r\n    \tmargin-bottom: 20px;\r\n        background-color: rgba(0,0,0,0.5) !important;\r\n        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);\r\n\t\tpadding: 40px;\r\n\t\tcolor:#fff;\r\n    }\r\n    .signin-form a {\r\n\t\tcolor: #5cb85c;\r\n\t}\r\n    .signin-form h2 {\r\n\t\ttext-align: center;\r\n\t\tfont-size: 34px;\r\n\t\tmargin: 10px 0 15px;\r\n\t}\r\n    .signin-form .hint-text {\r\n\t\tcolor: #999;\r\n\t\ttext-align: center;\r\n\t\tmargin-bottom: 20px;\r\n\t}\r\n    .signin-form .form-group {\r\n\t\tmargin-bottom: 20px;\r\n\t}\r\n    .signin-form .btn {        \r\n        font-size: 18px;\r\n\t\tline-height: 26px;        \r\n        font-weight: bold;\r\n\t\ttext-align: center;\r\n    }\r\n    .signin-form .small {\r\n        font-size: 13px;\r\n    }\r\n    .signup-btn {\r\n\t\ttext-align: center;\r\n\t\tborder-color: #5cb85c;\r\n\t\ttransition: all 0.4s;\r\n\t}\r\n    .signup-btn:hover {\r\n\t\tbackground: #5cb85c;\r\n\t\topacity: 0.8;\r\n\t}\r\n    .or-seperator {\r\n        margin: 50px 0 15px;\r\n        text-align: center;\r\n        border-top: 1px solid #e0e0e0;\r\n    }\r\n    .or-seperator b {\r\n        padding: 0 10px;\r\n\t\twidth: 40px;\r\n\t\theight: 40px;\r\n\t\tfont-size: 16px;\r\n\t\ttext-align: center;\r\n\t\tline-height: 40px;\r\n\t\tbackground: #fff;\r\n\t\tdisplay: inline-block;\r\n        border: 1px solid #e0e0e0;\r\n\t\tborder-radius: 50%;\r\n        position: relative;\r\n        top: -22px;\r\n        z-index: 1;\r\n    }\r\n    .social-btn .btn {\r\n\t\tcolor: #fff;\r\n        margin: 10px 0 0 30px;\r\n\t\tfont-size: 15px;\r\n        width: 55px;\r\n        height: 55px;\r\n        line-height: 38px;\r\n        border-radius: 50%;\r\n\t\tfont-weight: normal;\r\n        text-align: center;\r\n\t\tborder: none;\r\n\t\ttransition: all 0.4s;\r\n    }\r\n    .social-btn .btn:first-child {\r\n\t\tmargin-left: 0;\r\n\t}\r\n    .social-btn .btn:hover {\r\n\t\topacity: 0.8;\r\n\t}\r\n    .social-btn .btn-primary {\r\n\t\tbackground: #507cc0;\r\n\t}\r\n    .social-btn .btn-info {\r\n\t\tbackground: #64ccf1;\r\n\t}\r\n    .social-btn .btn-danger {\r\n\t\tbackground: #df4930;\r\n\t}\r\n    .social-btn .btn i {\r\n\t\tfont-size: 20px;\r\n\t}\r\n    .navbar-inverse .navbar-nav>.active>a, .navbar-inverse .navbar-nav>.active>a:hover, .navbar-inverse .navbar-nav>.active>a:focus {\r\n\t\tcolor: black !important;\r\n\t\tbackground-color: #080808;\r\n\t}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2lnbmluL3NpZ25pbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7OztJQUtJO0lBQ0E7RUFDRixlQUFlO0VBQ2Ysb0JBQW9CO0VBQ3BCLGdCQUFnQjtDQUNqQjtJQUNBO1FBQ08scUJBQXFCO0lBQ3pCO0lBQ0E7UUFDSSxtQkFBbUI7RUFDekIsd0JBQXdCO0lBQ3RCO0lBQ0g7RUFDQyxZQUFZO0tBQ1QsY0FBYztFQUNqQixlQUFlO0NBQ2hCO0lBQ0c7RUFDRixrQkFBa0I7S0FDZixtQkFBbUI7UUFDaEIsNENBQTRDO1FBQzVDLDBDQUEwQztFQUNoRCxhQUFhO0VBQ2IsVUFBVTtJQUNSO0lBQ0g7RUFDQyxjQUFjO0NBQ2Y7SUFDQTtFQUNDLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsbUJBQW1CO0NBQ3BCO0lBQ0E7RUFDQyxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLG1CQUFtQjtDQUNwQjtJQUNBO0VBQ0MsbUJBQW1CO0NBQ3BCO0lBQ0c7UUFDSSxlQUFlO0VBQ3JCLGlCQUFpQjtRQUNYLGlCQUFpQjtFQUN2QixrQkFBa0I7SUFDaEI7SUFDQTtRQUNJLGVBQWU7SUFDbkI7SUFDSDtFQUNDLGtCQUFrQjtFQUNsQixxQkFBcUI7RUFDckIsb0JBQW9CO0NBQ3JCO0lBQ0E7RUFDQyxtQkFBbUI7RUFDbkIsWUFBWTtDQUNiO0lBQ0c7UUFDSSxtQkFBbUI7UUFDbkIsa0JBQWtCO1FBQ2xCLDZCQUE2QjtJQUNqQztJQUNBO1FBQ0ksZUFBZTtFQUNyQixXQUFXO0VBQ1gsWUFBWTtFQUNaLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQixxQkFBcUI7UUFDZix5QkFBeUI7RUFDL0Isa0JBQWtCO1FBQ1osa0JBQWtCO1FBQ2xCLFVBQVU7UUFDVixVQUFVO0lBQ2Q7SUFDQTtFQUNGLFdBQVc7UUFDTCxxQkFBcUI7RUFDM0IsZUFBZTtRQUNULFdBQVc7UUFDWCxZQUFZO1FBQ1osaUJBQWlCO1FBQ2pCLGtCQUFrQjtFQUN4QixtQkFBbUI7UUFDYixrQkFBa0I7RUFDeEIsWUFBWTtFQUNaLG9CQUFvQjtJQUNsQjtJQUNIO0VBQ0MsY0FBYztDQUNmO0lBQ0E7RUFDQyxZQUFZO0NBQ2I7SUFDQTtFQUNDLG1CQUFtQjtDQUNwQjtJQUNBO0VBQ0MsbUJBQW1CO0NBQ3BCO0lBQ0E7RUFDQyxtQkFBbUI7Q0FDcEI7SUFDQTtFQUNDLGVBQWU7Q0FDaEI7SUFDQTtFQUNDLHVCQUF1QjtFQUN2Qix5QkFBeUI7Q0FDMUIiLCJmaWxlIjoic3JjL2FwcC9zaWduaW4vc2lnbmluLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBcclxuXHRib2R5IHtcclxuICAgICAgICBjb2xvcjogIzQzNDM0MztcclxuXHRcdGJhY2tncm91bmQ6ICNkZmU3ZTk7XHJcblx0XHRmb250LWZhbWlseTogJ1ZhcmVsYSBSb3VuZCcsIHNhbnMtc2VyaWY7XHJcblx0fSAqL1xyXG4gICAgLmZvcm0tY29udHJvbCB7XHJcblx0XHRmb250LXNpemU6IDE2cHg7XHJcblx0XHR0cmFuc2l0aW9uOiBhbGwgMC40cztcclxuXHRcdGJveC1zaGFkb3c6IG5vbmU7XHJcblx0fVxyXG5cdC5mb3JtLWNvbnRyb2w6Zm9jdXMge1xyXG4gICAgICAgIGJvcmRlci1jb2xvcjogIzVjYjg1YztcclxuICAgIH1cclxuICAgIC5mb3JtLWNvbnRyb2wsIC5idG4ge1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcblx0XHRvdXRsaW5lOiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcblx0LnNpZ25pbi1mb3JtIHtcclxuXHRcdHdpZHRoOiA0MDBweDtcclxuICAgIFx0bWFyZ2luOiAwIGF1dG87XHJcblx0XHRwYWRkaW5nOiAzMHB4IDA7XHJcblx0fVxyXG4gICAgLnNpZ25pbi1mb3JtIGZvcm0ge1xyXG5cdFx0Ym9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgXHRtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KSAhaW1wb3J0YW50O1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDBweCAycHggMnB4IHJnYmEoMCwgMCwgMCwgMC4zKTtcclxuXHRcdHBhZGRpbmc6IDQwcHg7XHJcblx0XHRjb2xvcjojZmZmO1xyXG4gICAgfVxyXG5cdC5zaWduaW4tZm9ybSBhIHtcclxuXHRcdGNvbG9yOiAjNWNiODVjO1xyXG5cdH0gICAgXHJcblx0LnNpZ25pbi1mb3JtIGgyIHtcclxuXHRcdHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHRcdGZvbnQtc2l6ZTogMzRweDtcclxuXHRcdG1hcmdpbjogMTBweCAwIDE1cHg7XHJcblx0fVxyXG5cdC5zaWduaW4tZm9ybSAuaGludC10ZXh0IHtcclxuXHRcdGNvbG9yOiAjOTk5O1xyXG5cdFx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdFx0bWFyZ2luLWJvdHRvbTogMjBweDtcclxuXHR9XHJcblx0LnNpZ25pbi1mb3JtIC5mb3JtLWdyb3VwIHtcclxuXHRcdG1hcmdpbi1ib3R0b206IDIwcHg7XHJcblx0fVxyXG4gICAgLnNpZ25pbi1mb3JtIC5idG4geyAgICAgICAgXHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG5cdFx0bGluZS1oZWlnaHQ6IDI2cHg7ICAgICAgICBcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuXHRcdHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuICAgIC5zaWduaW4tZm9ybSAuc21hbGwge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIH1cclxuXHQuc2lnbnVwLWJ0biB7XHJcblx0XHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblx0XHRib3JkZXItY29sb3I6ICM1Y2I4NWM7XHJcblx0XHR0cmFuc2l0aW9uOiBhbGwgMC40cztcclxuXHR9XHJcblx0LnNpZ251cC1idG46aG92ZXIge1xyXG5cdFx0YmFja2dyb3VuZDogIzVjYjg1YztcclxuXHRcdG9wYWNpdHk6IDAuODtcclxuXHR9XHJcbiAgICAub3Itc2VwZXJhdG9yIHtcclxuICAgICAgICBtYXJnaW46IDUwcHggMCAxNXB4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBib3JkZXItdG9wOiAxcHggc29saWQgI2UwZTBlMDtcclxuICAgIH1cclxuICAgIC5vci1zZXBlcmF0b3IgYiB7XHJcbiAgICAgICAgcGFkZGluZzogMCAxMHB4O1xyXG5cdFx0d2lkdGg6IDQwcHg7XHJcblx0XHRoZWlnaHQ6IDQwcHg7XHJcblx0XHRmb250LXNpemU6IDE2cHg7XHJcblx0XHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblx0XHRsaW5lLWhlaWdodDogNDBweDtcclxuXHRcdGJhY2tncm91bmQ6ICNmZmY7XHJcblx0XHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2UwZTBlMDtcclxuXHRcdGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgdG9wOiAtMjJweDtcclxuICAgICAgICB6LWluZGV4OiAxO1xyXG4gICAgfVxyXG4gICAgLnNvY2lhbC1idG4gLmJ0biB7XHJcblx0XHRjb2xvcjogI2ZmZjtcclxuICAgICAgICBtYXJnaW46IDEwcHggMCAwIDMwcHg7XHJcblx0XHRmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgd2lkdGg6IDU1cHg7XHJcbiAgICAgICAgaGVpZ2h0OiA1NXB4O1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAzOHB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuXHRcdGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdFx0Ym9yZGVyOiBub25lO1xyXG5cdFx0dHJhbnNpdGlvbjogYWxsIDAuNHM7XHJcbiAgICB9XHRcclxuXHQuc29jaWFsLWJ0biAuYnRuOmZpcnN0LWNoaWxkIHtcclxuXHRcdG1hcmdpbi1sZWZ0OiAwO1xyXG5cdH1cclxuXHQuc29jaWFsLWJ0biAuYnRuOmhvdmVyIHtcclxuXHRcdG9wYWNpdHk6IDAuODtcclxuXHR9XHJcblx0LnNvY2lhbC1idG4gLmJ0bi1wcmltYXJ5IHtcclxuXHRcdGJhY2tncm91bmQ6ICM1MDdjYzA7XHJcblx0fVxyXG5cdC5zb2NpYWwtYnRuIC5idG4taW5mbyB7XHJcblx0XHRiYWNrZ3JvdW5kOiAjNjRjY2YxO1xyXG5cdH1cclxuXHQuc29jaWFsLWJ0biAuYnRuLWRhbmdlciB7XHJcblx0XHRiYWNrZ3JvdW5kOiAjZGY0OTMwO1xyXG5cdH1cclxuXHQuc29jaWFsLWJ0biAuYnRuIGkge1xyXG5cdFx0Zm9udC1zaXplOiAyMHB4O1xyXG5cdH1cclxuXHQubmF2YmFyLWludmVyc2UgLm5hdmJhci1uYXY+LmFjdGl2ZT5hLCAubmF2YmFyLWludmVyc2UgLm5hdmJhci1uYXY+LmFjdGl2ZT5hOmhvdmVyLCAubmF2YmFyLWludmVyc2UgLm5hdmJhci1uYXY+LmFjdGl2ZT5hOmZvY3VzIHtcclxuXHRcdGNvbG9yOiBibGFjayAhaW1wb3J0YW50O1xyXG5cdFx0YmFja2dyb3VuZC1jb2xvcjogIzA4MDgwODtcclxuXHR9Il19 */"

/***/ }),

/***/ "./src/app/signin/signin.component.html":
/*!**********************************************!*\
  !*** ./src/app/signin/signin.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"signin-form\">\n    <form  #formRef='ngForm' (ngSubmit)='onSubmit(formRef.value)'>\n\t\t<h2>Sign in</h2>\n        <div class=\"form-group\">\n        \t<input type=\"text\" class=\"form-control input-lg\"placeholder=\"Enter Username\" #usernameRef='ngModel' name=\"username\" required ngModel>\n        </div>\n\t\t<div class=\"form-group\">\n            <input type=\"password\" class=\"form-control input-lg\" placeholder=\"Enter Password\" #passwordRef='ngModel' name=\"password\" required ngModel>\n        </div>  \n        <div class=\"form-group\">\n            <button type=\"submit\" class=\"btn btn-success btn-lg btn-block signup-btn\" value=\"SignIn\">Sign in</button>\n        </div>\n        \n\n                           "

/***/ }),

/***/ "./src/app/signin/signin.component.ts":
/*!********************************************!*\
  !*** ./src/app/signin/signin.component.ts ***!
  \********************************************/
/*! exports provided: SigninComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SigninComponent", function() { return SigninComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Services/header-title.service */ "./src/app/Services/header-title.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SigninComponent = /** @class */ (function () {
    function SigninComponent(_http, router, headerTitleService) {
        this._http = _http;
        this.router = router;
        this.headerTitleService = headerTitleService;
    }
    SigninComponent.prototype.ngOnInit = function () {
        this.headerTitleService.LogTitle('');
    };
    SigninComponent.prototype.onSubmit = function (a) {
        var _this = this;
        this.username = a.username;
        this.password = a.password;
        console.log(a.username + "   " + a.password);
        console.log(a);
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.post('api/signin', a, options)
            .subscribe(function (data) {
            _this.router.navigate(['/main-masters']);
        }, function (error) {
            alert("Username and Password is Wrong");
            //console.log(JSON.stringify(error.json()));
        });
    };
    SigninComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-signin',
            template: __webpack_require__(/*! ./signin.component.html */ "./src/app/signin/signin.component.html"),
            styles: [__webpack_require__(/*! ./signin.component.css */ "./src/app/signin/signin.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _Services_header_title_service__WEBPACK_IMPORTED_MODULE_3__["HeaderTitleService"]])
    ], SigninComponent);
    return SigninComponent;
}());



/***/ }),

/***/ "./src/app/sub-dep-list/sub-dep-list.component.css":
/*!*********************************************************!*\
  !*** ./src/app/sub-dep-list/sub-dep-list.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n@import url('https://fonts.googleapis.com/css?family=Numans');\n/* Made with love by Mutiullah Samim*/\n.container{\nheight: 100%;\nbackground-size: cover;\nwidth:100%;\nbackground-repeat: no-repeat;\ncolor:#fff;\n/* align-content: center; */\n}\n.card{\nheight: 550px;\nmargin-top: 50px;\nmargin-bottom: 50px;\nwidth: 850px;\nbackground-color: rgba(0,0,0,0.5) !important;\n\n}\ni\n{\n    font-size: 22px;\n}\ntable, th, td{\n    border: 1px solid white;\n    border-collapse: collapse;\n  }\nth, td \n  {\n      text-align: center;\n      padding: 10px;\n\t}\n/* Hamburger Menu css */\na\n{\n  text-decoration: none;\n  color: #232323;\n  \n  transition: color 0.3s ease;\n}\na:hover\n{\n  color: tomato;\n}\n#menuToggle\n{\n  display: block;\n  position: absolute;\n  top: 20px;\n  right: 50px;\n  \n  z-index: 1;\n  \n  -webkit-user-select: none;\n  -moz-user-select: none;\n   -ms-user-select: none;\n       user-select: none;\n}\n#menuToggle input\n{\n  display: block;\n  width: 40px;\n  height: 32px;\n  position: absolute;\n  top: -7px;\n  left: -5px;\n  \n  cursor: pointer;\n  \n  opacity: 0; /* hide this */\n  z-index: 2; /* and place it over the hamburger */\n  \n  -webkit-touch-callout: none;\n}\n/*\n * Just a quick hamburger\n */\n#menuToggle span\n{\n  display: block;\n  width: 33px;\n  height: 4px;\n  margin-bottom: 5px;\n  position: relative;\n  \n  background: #cdcdcd;\n  border-radius: 3px;\n  \n  z-index: 1;\n  \n  -webkit-transform-origin: 4px 0px;\n  \n          transform-origin: 4px 0px;\n  \n  transition: background 0.5s cubic-bezier(0.77,0.2,0.05,1.0),\n              opacity 0.55s ease,\n              -webkit-transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0);\n  \n  transition: transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0),\n              background 0.5s cubic-bezier(0.77,0.2,0.05,1.0),\n              opacity 0.55s ease;\n  \n  transition: transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0),\n              background 0.5s cubic-bezier(0.77,0.2,0.05,1.0),\n              opacity 0.55s ease,\n              -webkit-transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0);\n}\n#menuToggle span:first-child\n{\n  -webkit-transform-origin: 0% 0%;\n          transform-origin: 0% 0%;\n}\n#menuToggle span:nth-last-child(2)\n{\n  -webkit-transform-origin: 0% 100%;\n          transform-origin: 0% 100%;\n}\n/* \n * Transform all the slices of hamburger\n * into a crossmark.\n */\n#menuToggle input:checked ~ span\n{\n  opacity: 1;\n  -webkit-transform: rotate(45deg) translate(-2px, -1px);\n          transform: rotate(45deg) translate(-2px, -1px);\n  background: #232323;\n}\n/*\n * But let's hide the middle one.\n */\n#menuToggle input:checked ~ span:nth-last-child(3)\n{\n  opacity: 0;\n  -webkit-transform: rotate(0deg) scale(0.2, 0.2);\n          transform: rotate(0deg) scale(0.2, 0.2);\n}\n/*\n * Ohyeah and the last one should go the other direction\n */\n#menuToggle input:checked ~ span:nth-last-child(2)\n{\n  opacity: 1;\n  -webkit-transform: rotate(-45deg) translate(0, -1px);\n          transform: rotate(-45deg) translate(0, -1px);\n}\n/*\n * Make this absolute positioned\n * at the top left of the screen\n */\n#menu\n{\n  position: absolute;\n  width: 300px;\n  margin: -100px 0 0 0;\n  padding: 20px;\n  padding-top: 100px;\n  right: -100px;\n  \n  background: #ededed;\n  list-style-type: none;\n  -webkit-font-smoothing: antialiased;\n  /* to stop flickering of text in safari */\n  \n  -webkit-transform-origin: 0% 0%;\n  \n          transform-origin: 0% 0%;\n  -webkit-transform: translate(100%, 0);\n          transform: translate(100%, 0);\n  \n  transition: -webkit-transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0);\n  \n  transition: transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0);\n  \n  transition: transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0), -webkit-transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0);\n}\n#menu li\n{\n  padding: 10px 0;\n \n  font-size: 22px;\n}\n/*\n * And let's fade it in from the left\n */\n#menuToggle input:checked ~ ul\n{\n  -webkit-transform: scale(1.0, 1.0);\n          transform: scale(1.0, 1.0);\n  opacity: 1;\n}\n/* End of Hamburger Menu css */\n/*\n\tMax width before this PARTICULAR table gets nasty. This query will take effect for any screen smaller than 760px and also iPads specifically.\n\t*/\n@media\n\t  only screen \n    and (max-width: 760px), (min-device-width: 768px) \n    and (max-device-width: 1024px)  {\n\n\n\t\t/* Force table to not be like tables anymore */\n\t\ttable, thead, tbody, th, td, tr {\n            display: block;\n            \n        }\n\n\n\t\t/* Hide table headers (but not display: none;, for accessibility) */\n\t\tthead tr {\n\t\t\tposition: absolute;\n\t\t\ttop: -9999px;\n\t\t\tleft: -9999px;\n\t\t}\n\n    tr {\n      margin: 0 0 1rem 0;\n    }\n      \n    tr:nth-child(odd) {\n      background: #ccc;\n    }\n    \n\t\ttd {\n\t\t\t/* Behave  like a \"row\" */\n\t\t\tborder: none;\n\t\t\tborder-bottom: 1px solid #eee;\n\t\t\tposition: relative;\n\t\t\tpadding-left: 50%;\n\t\t}\n\n\t\ttd:before {\n\t\t\t/* Now like a table header */\n\t\t\tposition: absolute;\n\t\t\t/* Top/left values mimic padding */\n\t\t\ttop: 0;\n\t\t\tleft: 6px;\n\t\t\twidth: 45%;\n\t\t\tpadding-right: 10px;\n\t\t\twhite-space: nowrap;\n\t\t}\n\n\t\t/*\n\t\tLabel the data\n    You could also use a data-* attribute and content for this. That way \"bloats\" the HTML, this way means you need to keep HTML and CSS in sync. Lea Verou has a clever way to handle with text-shadow.\n\t\t*/\n\t\ttd:nth-of-type(1):before { content: \"First Name\"; }\n\t\ttd:nth-of-type(2):before { content: \"Last Name\"; }\n\t\ttd:nth-of-type(3):before { content: \"Job Title\"; }\n\t\ttd:nth-of-type(4):before { content: \"Favorite Color\"; }\n\t\ttd:nth-of-type(5):before { content: \"Wars of Trek?\"; }\n\t\ttd:nth-of-type(6):before { content: \"Secret Alias\"; }\n\t\ttd:nth-of-type(7):before { content: \"Date of Birth\"; }\n\t\ttd:nth-of-type(8):before { content: \"Dream Vacation City\"; }\n\t\ttd:nth-of-type(9):before { content: \"GPA\"; }\n\t\ttd:nth-of-type(10):before { content: \"Arbitrary Data\"; }\n\t}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3ViLWRlcC1saXN0L3N1Yi1kZXAtbGlzdC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUEsNkRBQTZEO0FBRjdELHFDQUFxQztBQUtyQztBQUNBLFlBQVk7QUFDWixzQkFBc0I7QUFDdEIsVUFBVTtBQUNWLDRCQUE0QjtBQUM1QixVQUFVO0FBQ1YsMkJBQTJCO0FBQzNCO0FBRUE7QUFDQSxhQUFhO0FBQ2IsZ0JBQWdCO0FBQ2hCLG1CQUFtQjtBQUNuQixZQUFZO0FBQ1osNENBQTRDOztBQUU1QztBQUNBOztJQUVJLGVBQWU7QUFDbkI7QUFFQTtJQUNJLHVCQUF1QjtJQUN2Qix5QkFBeUI7RUFDM0I7QUFDQTs7TUFFSSxrQkFBa0I7TUFDbEIsYUFBYTtDQUNsQjtBQUVBLHVCQUF1QjtBQUN4Qjs7RUFFRSxxQkFBcUI7RUFDckIsY0FBYzs7RUFFZCwyQkFBMkI7QUFDN0I7QUFFQTs7RUFFRSxhQUFhO0FBQ2Y7QUFFQTs7RUFFRSxjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxXQUFXOztFQUVYLFVBQVU7O0VBRVYseUJBQXlCO0VBQ3pCLHNCQUFpQjtHQUFqQixxQkFBaUI7T0FBakIsaUJBQWlCO0FBQ25CO0FBRUE7O0VBRUUsY0FBYztFQUNkLFdBQVc7RUFDWCxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxVQUFVOztFQUVWLGVBQWU7O0VBRWYsVUFBVSxFQUFFLGNBQWM7RUFDMUIsVUFBVSxFQUFFLG9DQUFvQzs7RUFFaEQsMkJBQTJCO0FBQzdCO0FBRUE7O0VBRUU7QUFDRjs7RUFFRSxjQUFjO0VBQ2QsV0FBVztFQUNYLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsa0JBQWtCOztFQUVsQixtQkFBbUI7RUFDbkIsa0JBQWtCOztFQUVsQixVQUFVOztFQUVWLGlDQUF5Qjs7VUFBekIseUJBQXlCOztFQUV6Qjs7b0VBRThCOztFQUY5Qjs7Z0NBRThCOztFQUY5Qjs7O29FQUU4QjtBQUNoQztBQUVBOztFQUVFLCtCQUF1QjtVQUF2Qix1QkFBdUI7QUFDekI7QUFFQTs7RUFFRSxpQ0FBeUI7VUFBekIseUJBQXlCO0FBQzNCO0FBRUE7OztFQUdFO0FBQ0Y7O0VBRUUsVUFBVTtFQUNWLHNEQUE4QztVQUE5Qyw4Q0FBOEM7RUFDOUMsbUJBQW1CO0FBQ3JCO0FBRUE7O0VBRUU7QUFDRjs7RUFFRSxVQUFVO0VBQ1YsK0NBQXVDO1VBQXZDLHVDQUF1QztBQUN6QztBQUVBOztFQUVFO0FBQ0Y7O0VBRUUsVUFBVTtFQUNWLG9EQUE0QztVQUE1Qyw0Q0FBNEM7QUFDOUM7QUFFQTs7O0VBR0U7QUFDRjs7RUFFRSxrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLG9CQUFvQjtFQUNwQixhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLGFBQWE7O0VBRWIsbUJBQW1CO0VBQ25CLHFCQUFxQjtFQUNyQixtQ0FBbUM7RUFDbkMseUNBQXlDOztFQUV6QywrQkFBdUI7O1VBQXZCLHVCQUF1QjtFQUN2QixxQ0FBNkI7VUFBN0IsNkJBQTZCOztFQUU3QixrRUFBMEQ7O0VBQTFELDBEQUEwRDs7RUFBMUQsa0hBQTBEO0FBQzVEO0FBRUE7O0VBRUUsZUFBZTs7RUFFZixlQUFlO0FBQ2pCO0FBRUE7O0VBRUU7QUFDRjs7RUFFRSxrQ0FBMEI7VUFBMUIsMEJBQTBCO0VBQzFCLFVBQVU7QUFDWjtBQUNBLDhCQUE4QjtBQUk5Qjs7RUFFRTtBQUNEOzs7Ozs7RUFNQyw4Q0FBOEM7RUFDOUM7WUFDVSxjQUFjOztRQUVsQjs7O0VBR04sbUVBQW1FO0VBQ25FO0dBQ0Msa0JBQWtCO0dBQ2xCLFlBQVk7R0FDWixhQUFhO0VBQ2Q7O0lBRUU7TUFDRSxrQkFBa0I7SUFDcEI7O0lBRUE7TUFDRSxnQkFBZ0I7SUFDbEI7O0VBRUY7R0FDQyx5QkFBeUI7R0FDekIsWUFBWTtHQUNaLDZCQUE2QjtHQUM3QixrQkFBa0I7R0FDbEIsaUJBQWlCO0VBQ2xCOztFQUVBO0dBQ0MsNEJBQTRCO0dBQzVCLGtCQUFrQjtHQUNsQixrQ0FBa0M7R0FDbEMsTUFBTTtHQUNOLFNBQVM7R0FDVCxVQUFVO0dBQ1YsbUJBQW1CO0dBQ25CLG1CQUFtQjtFQUNwQjs7RUFFQTs7O0dBR0M7RUFDRCwyQkFBMkIscUJBQXFCLEVBQUU7RUFDbEQsMkJBQTJCLG9CQUFvQixFQUFFO0VBQ2pELDJCQUEyQixvQkFBb0IsRUFBRTtFQUNqRCwyQkFBMkIseUJBQXlCLEVBQUU7RUFDdEQsMkJBQTJCLHdCQUF3QixFQUFFO0VBQ3JELDJCQUEyQix1QkFBdUIsRUFBRTtFQUNwRCwyQkFBMkIsd0JBQXdCLEVBQUU7RUFDckQsMkJBQTJCLDhCQUE4QixFQUFFO0VBQzNELDJCQUEyQixjQUFjLEVBQUU7RUFDM0MsNEJBQTRCLHlCQUF5QixFQUFFO0NBQ3hEIiwiZmlsZSI6InNyYy9hcHAvc3ViLWRlcC1saXN0L3N1Yi1kZXAtbGlzdC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogTWFkZSB3aXRoIGxvdmUgYnkgTXV0aXVsbGFoIFNhbWltKi9cblxuQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1OdW1hbnMnKTtcblxuXG4uY29udGFpbmVye1xuaGVpZ2h0OiAxMDAlO1xuYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbndpZHRoOjEwMCU7XG5iYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuY29sb3I6I2ZmZjtcbi8qIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjsgKi9cbn1cblxuLmNhcmR7XG5oZWlnaHQ6IDU1MHB4O1xubWFyZ2luLXRvcDogNTBweDtcbm1hcmdpbi1ib3R0b206IDUwcHg7XG53aWR0aDogODUwcHg7XG5iYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuNSkgIWltcG9ydGFudDtcblxufVxuaVxue1xuICAgIGZvbnQtc2l6ZTogMjJweDtcbn1cblxudGFibGUsIHRoLCB0ZHtcbiAgICBib3JkZXI6IDFweCBzb2xpZCB3aGl0ZTtcbiAgICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xuICB9XG4gIHRoLCB0ZCBcbiAge1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgcGFkZGluZzogMTBweDtcblx0fVxuXG5cdC8qIEhhbWJ1cmdlciBNZW51IGNzcyAqL1xuYVxue1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIGNvbG9yOiAjMjMyMzIzO1xuICBcbiAgdHJhbnNpdGlvbjogY29sb3IgMC4zcyBlYXNlO1xufVxuXG5hOmhvdmVyXG57XG4gIGNvbG9yOiB0b21hdG87XG59XG5cbiNtZW51VG9nZ2xlXG57XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMjBweDtcbiAgcmlnaHQ6IDUwcHg7XG4gIFxuICB6LWluZGV4OiAxO1xuICBcbiAgLXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTtcbiAgdXNlci1zZWxlY3Q6IG5vbmU7XG59XG5cbiNtZW51VG9nZ2xlIGlucHV0XG57XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB3aWR0aDogNDBweDtcbiAgaGVpZ2h0OiAzMnB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogLTdweDtcbiAgbGVmdDogLTVweDtcbiAgXG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgXG4gIG9wYWNpdHk6IDA7IC8qIGhpZGUgdGhpcyAqL1xuICB6LWluZGV4OiAyOyAvKiBhbmQgcGxhY2UgaXQgb3ZlciB0aGUgaGFtYnVyZ2VyICovXG4gIFxuICAtd2Via2l0LXRvdWNoLWNhbGxvdXQ6IG5vbmU7XG59XG5cbi8qXG4gKiBKdXN0IGEgcXVpY2sgaGFtYnVyZ2VyXG4gKi9cbiNtZW51VG9nZ2xlIHNwYW5cbntcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiAzM3B4O1xuICBoZWlnaHQ6IDRweDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIFxuICBiYWNrZ3JvdW5kOiAjY2RjZGNkO1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG4gIFxuICB6LWluZGV4OiAxO1xuICBcbiAgdHJhbnNmb3JtLW9yaWdpbjogNHB4IDBweDtcbiAgXG4gIHRyYW5zaXRpb246IHRyYW5zZm9ybSAwLjVzIGN1YmljLWJlemllcigwLjc3LDAuMiwwLjA1LDEuMCksXG4gICAgICAgICAgICAgIGJhY2tncm91bmQgMC41cyBjdWJpYy1iZXppZXIoMC43NywwLjIsMC4wNSwxLjApLFxuICAgICAgICAgICAgICBvcGFjaXR5IDAuNTVzIGVhc2U7XG59XG5cbiNtZW51VG9nZ2xlIHNwYW46Zmlyc3QtY2hpbGRcbntcbiAgdHJhbnNmb3JtLW9yaWdpbjogMCUgMCU7XG59XG5cbiNtZW51VG9nZ2xlIHNwYW46bnRoLWxhc3QtY2hpbGQoMilcbntcbiAgdHJhbnNmb3JtLW9yaWdpbjogMCUgMTAwJTtcbn1cblxuLyogXG4gKiBUcmFuc2Zvcm0gYWxsIHRoZSBzbGljZXMgb2YgaGFtYnVyZ2VyXG4gKiBpbnRvIGEgY3Jvc3NtYXJrLlxuICovXG4jbWVudVRvZ2dsZSBpbnB1dDpjaGVja2VkIH4gc3Bhblxue1xuICBvcGFjaXR5OiAxO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZykgdHJhbnNsYXRlKC0ycHgsIC0xcHgpO1xuICBiYWNrZ3JvdW5kOiAjMjMyMzIzO1xufVxuXG4vKlxuICogQnV0IGxldCdzIGhpZGUgdGhlIG1pZGRsZSBvbmUuXG4gKi9cbiNtZW51VG9nZ2xlIGlucHV0OmNoZWNrZWQgfiBzcGFuOm50aC1sYXN0LWNoaWxkKDMpXG57XG4gIG9wYWNpdHk6IDA7XG4gIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpIHNjYWxlKDAuMiwgMC4yKTtcbn1cblxuLypcbiAqIE9oeWVhaCBhbmQgdGhlIGxhc3Qgb25lIHNob3VsZCBnbyB0aGUgb3RoZXIgZGlyZWN0aW9uXG4gKi9cbiNtZW51VG9nZ2xlIGlucHV0OmNoZWNrZWQgfiBzcGFuOm50aC1sYXN0LWNoaWxkKDIpXG57XG4gIG9wYWNpdHk6IDE7XG4gIHRyYW5zZm9ybTogcm90YXRlKC00NWRlZykgdHJhbnNsYXRlKDAsIC0xcHgpO1xufVxuXG4vKlxuICogTWFrZSB0aGlzIGFic29sdXRlIHBvc2l0aW9uZWRcbiAqIGF0IHRoZSB0b3AgbGVmdCBvZiB0aGUgc2NyZWVuXG4gKi9cbiNtZW51XG57XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgd2lkdGg6IDMwMHB4O1xuICBtYXJnaW46IC0xMDBweCAwIDAgMDtcbiAgcGFkZGluZzogMjBweDtcbiAgcGFkZGluZy10b3A6IDEwMHB4O1xuICByaWdodDogLTEwMHB4O1xuICBcbiAgYmFja2dyb3VuZDogI2VkZWRlZDtcbiAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xuICAtd2Via2l0LWZvbnQtc21vb3RoaW5nOiBhbnRpYWxpYXNlZDtcbiAgLyogdG8gc3RvcCBmbGlja2VyaW5nIG9mIHRleHQgaW4gc2FmYXJpICovXG4gIFxuICB0cmFuc2Zvcm0tb3JpZ2luOiAwJSAwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMTAwJSwgMCk7XG4gIFxuICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMC41cyBjdWJpYy1iZXppZXIoMC43NywwLjIsMC4wNSwxLjApO1xufVxuXG4jbWVudSBsaVxue1xuICBwYWRkaW5nOiAxMHB4IDA7XG4gXG4gIGZvbnQtc2l6ZTogMjJweDtcbn1cblxuLypcbiAqIEFuZCBsZXQncyBmYWRlIGl0IGluIGZyb20gdGhlIGxlZnRcbiAqL1xuI21lbnVUb2dnbGUgaW5wdXQ6Y2hlY2tlZCB+IHVsXG57XG4gIHRyYW5zZm9ybTogc2NhbGUoMS4wLCAxLjApO1xuICBvcGFjaXR5OiAxO1xufVxuLyogRW5kIG9mIEhhbWJ1cmdlciBNZW51IGNzcyAqL1xuXG5cblxuLypcblx0TWF4IHdpZHRoIGJlZm9yZSB0aGlzIFBBUlRJQ1VMQVIgdGFibGUgZ2V0cyBuYXN0eS4gVGhpcyBxdWVyeSB3aWxsIHRha2UgZWZmZWN0IGZvciBhbnkgc2NyZWVuIHNtYWxsZXIgdGhhbiA3NjBweCBhbmQgYWxzbyBpUGFkcyBzcGVjaWZpY2FsbHkuXG5cdCovXG5cdEBtZWRpYVxuXHQgIG9ubHkgc2NyZWVuIFxuICAgIGFuZCAobWF4LXdpZHRoOiA3NjBweCksIChtaW4tZGV2aWNlLXdpZHRoOiA3NjhweCkgXG4gICAgYW5kIChtYXgtZGV2aWNlLXdpZHRoOiAxMDI0cHgpICB7XG5cblxuXHRcdC8qIEZvcmNlIHRhYmxlIHRvIG5vdCBiZSBsaWtlIHRhYmxlcyBhbnltb3JlICovXG5cdFx0dGFibGUsIHRoZWFkLCB0Ym9keSwgdGgsIHRkLCB0ciB7XG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgIFxuICAgICAgICB9XG5cblxuXHRcdC8qIEhpZGUgdGFibGUgaGVhZGVycyAoYnV0IG5vdCBkaXNwbGF5OiBub25lOywgZm9yIGFjY2Vzc2liaWxpdHkpICovXG5cdFx0dGhlYWQgdHIge1xuXHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xuXHRcdFx0dG9wOiAtOTk5OXB4O1xuXHRcdFx0bGVmdDogLTk5OTlweDtcblx0XHR9XG5cbiAgICB0ciB7XG4gICAgICBtYXJnaW46IDAgMCAxcmVtIDA7XG4gICAgfVxuICAgICAgXG4gICAgdHI6bnRoLWNoaWxkKG9kZCkge1xuICAgICAgYmFja2dyb3VuZDogI2NjYztcbiAgICB9XG4gICAgXG5cdFx0dGQge1xuXHRcdFx0LyogQmVoYXZlICBsaWtlIGEgXCJyb3dcIiAqL1xuXHRcdFx0Ym9yZGVyOiBub25lO1xuXHRcdFx0Ym9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlZWU7XG5cdFx0XHRwb3NpdGlvbjogcmVsYXRpdmU7XG5cdFx0XHRwYWRkaW5nLWxlZnQ6IDUwJTtcblx0XHR9XG5cblx0XHR0ZDpiZWZvcmUge1xuXHRcdFx0LyogTm93IGxpa2UgYSB0YWJsZSBoZWFkZXIgKi9cblx0XHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcblx0XHRcdC8qIFRvcC9sZWZ0IHZhbHVlcyBtaW1pYyBwYWRkaW5nICovXG5cdFx0XHR0b3A6IDA7XG5cdFx0XHRsZWZ0OiA2cHg7XG5cdFx0XHR3aWR0aDogNDUlO1xuXHRcdFx0cGFkZGluZy1yaWdodDogMTBweDtcblx0XHRcdHdoaXRlLXNwYWNlOiBub3dyYXA7XG5cdFx0fVxuXG5cdFx0Lypcblx0XHRMYWJlbCB0aGUgZGF0YVxuICAgIFlvdSBjb3VsZCBhbHNvIHVzZSBhIGRhdGEtKiBhdHRyaWJ1dGUgYW5kIGNvbnRlbnQgZm9yIHRoaXMuIFRoYXQgd2F5IFwiYmxvYXRzXCIgdGhlIEhUTUwsIHRoaXMgd2F5IG1lYW5zIHlvdSBuZWVkIHRvIGtlZXAgSFRNTCBhbmQgQ1NTIGluIHN5bmMuIExlYSBWZXJvdSBoYXMgYSBjbGV2ZXIgd2F5IHRvIGhhbmRsZSB3aXRoIHRleHQtc2hhZG93LlxuXHRcdCovXG5cdFx0dGQ6bnRoLW9mLXR5cGUoMSk6YmVmb3JlIHsgY29udGVudDogXCJGaXJzdCBOYW1lXCI7IH1cblx0XHR0ZDpudGgtb2YtdHlwZSgyKTpiZWZvcmUgeyBjb250ZW50OiBcIkxhc3QgTmFtZVwiOyB9XG5cdFx0dGQ6bnRoLW9mLXR5cGUoMyk6YmVmb3JlIHsgY29udGVudDogXCJKb2IgVGl0bGVcIjsgfVxuXHRcdHRkOm50aC1vZi10eXBlKDQpOmJlZm9yZSB7IGNvbnRlbnQ6IFwiRmF2b3JpdGUgQ29sb3JcIjsgfVxuXHRcdHRkOm50aC1vZi10eXBlKDUpOmJlZm9yZSB7IGNvbnRlbnQ6IFwiV2FycyBvZiBUcmVrP1wiOyB9XG5cdFx0dGQ6bnRoLW9mLXR5cGUoNik6YmVmb3JlIHsgY29udGVudDogXCJTZWNyZXQgQWxpYXNcIjsgfVxuXHRcdHRkOm50aC1vZi10eXBlKDcpOmJlZm9yZSB7IGNvbnRlbnQ6IFwiRGF0ZSBvZiBCaXJ0aFwiOyB9XG5cdFx0dGQ6bnRoLW9mLXR5cGUoOCk6YmVmb3JlIHsgY29udGVudDogXCJEcmVhbSBWYWNhdGlvbiBDaXR5XCI7IH1cblx0XHR0ZDpudGgtb2YtdHlwZSg5KTpiZWZvcmUgeyBjb250ZW50OiBcIkdQQVwiOyB9XG5cdFx0dGQ6bnRoLW9mLXR5cGUoMTApOmJlZm9yZSB7IGNvbnRlbnQ6IFwiQXJiaXRyYXJ5IERhdGFcIjsgfVxuXHR9Il19 */"

/***/ }),

/***/ "./src/app/sub-dep-list/sub-dep-list.component.html":
/*!**********************************************************!*\
  !*** ./src/app/sub-dep-list/sub-dep-list.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n\n  <!-- Hamburger Menu -->\n  <nav role='navigation'>\n    <div id=\"menuToggle\">\n\n      <input type=\"checkbox\" />\n\n      <span></span>\n      <span></span>\n      <span></span>\n      <ul id=\"menu\">\n        <a href=\"#\">\n          <li>Add Sub Department</li>\n        </a>\n        <a href=\"#\">\n          <li>Sub Department List</li>\n        </a>\n        <a routerLink=\"/logout\">\n          <li>Logout</li>\n        </a>\n      </ul>\n    </div>\n  </nav>\n  <!-- End of Hamburger Menu -->\n\n  <div class=\"d-flex justify-content-center h-100\">\n    <div class=\"card\">\n\n      <table role=\"table\">\n\n\n\n        <thead role=\"rowgroup\">\n          <tr>\n            <th colspan=\"9\" style=\"background-color:#5E1D55;color:white;\">\n              <h3>Sub Department List</h3>\n            </th>\n          </tr>\n\n          <tr role=\"row\">\n            <th role=\"columnheader\">Department</th>\n            <th role=\"columnheader\">Sub Department</th>\n            <th role=\"columnheader\">Password</th>\n            <th role=\"columnheader\">Remarks</th>\n            <td role=\"cell\">Edit</td>\n\n          </tr>\n        </thead>\n        <tbody role=\"rowgroup\">\n          <tr role=\"row\" *ngFor=\"let data of datas\">\n            <td role=\"cell\">{{data.dep}}</td>\n            <td role=\"cell\">{{data.sub_dep}}</td>\n            <td role=\"cell\">{{data.password}}</td>\n            <td role=\"cell\">{{data.remarks}}</td>\n            <td role=\"cell\"><i class=\"far fa-edit \" (click)=\"onEditSubDep()\"></i></td>\n\n          </tr>\n          <!-- <tr role=\"row\">\n                      <td role=\"cell\">The</td>\n                      <td role=\"cell\">Tick</td>\n                      <td role=\"cell\">Crimefighter Sorta</td>\n                      <td role=\"cell\">Blue</td>\n                      <td role=\"cell\"><i class=\"far fa-edit\"></i></td>\n\n                  </tr>\n                  <tr role=\"row\">\n                      <td role=\"cell\">Jokey</td>\n                      <td role=\"cell\">Smurf</td>\n                      <td role=\"cell\">Giving Exploding Presents</td>\n                      <td role=\"cell\">Smurflow</td>\n                      <td role=\"cell\"><i class=\"far fa-edit\"></i></td>\n\n                  </tr>\n                  <tr role=\"row\">\n                      <td role=\"cell\">Cindy</td>\n                      <td role=\"cell\">Beyler</td>\n                      <td role=\"cell\">Sales Representative</td>\n                      <td role=\"cell\">Red</td>\n                      <td role=\"cell\"><i class=\"far fa-edit\"></i></td>\n\n                  </tr>\n                  <tr role=\"row\">\n                      <td role=\"cell\">Captain</td>\n                      <td role=\"cell\">Cool</td>\n                      <td role=\"cell\">Tree Crusher</td>\n                      <td role=\"cell\">Blue</td>\n                      <td role=\"cell\"><i class=\"far fa-edit\"></i></td>\n\n                  </tr>\n                  <tr role=\"row\">\n                      <td role=\"cell\">Captain</td>\n                      <td role=\"cell\">Cool</td>\n                      <td role=\"cell\">Tree Crusher</td>\n                      <td role=\"cell\">Blue</td>\n                      <td role=\"cell\"><i class=\"far fa-edit\"></i></td>\n\n                  </tr>\n                  <tr role=\"row\">\n                      <td role=\"cell\">Captain</td>\n                      <td role=\"cell\">Cool</td>\n                      <td role=\"cell\">Tree Crusher</td>\n                      <td role=\"cell\">Blue</td>\n                      <td role=\"cell\"><i class=\"far fa-edit\"></i></td>\n\n                  </tr>\n                  <tr role=\"row\">\n                      <td role=\"cell\">Captain</td>\n                      <td role=\"cell\">Cool</td>\n                      <td role=\"cell\">Tree Crusher</td>\n                      <td role=\"cell\">Blue</td>\n                      <td role=\"cell\"><i class=\"far fa-edit\"></i></td>\n\n                  </tr>\n                  <tr role=\"row\">\n                      <td role=\"cell\">Captain</td>\n                      <td role=\"cell\">Cool</td>\n                      <td role=\"cell\">Tree Crusher</td>\n                      <td role=\"cell\">Blue</td>\n                      <td role=\"cell\"><i class=\"far fa-edit\"></i></td>\n\n\n                  </tr> -->\n          <tr>\n            <th colspan=\"9\" style=\"background-color:#5E1D55;color:white;\">\n\n              <ul class=\"pagination\" style=\"float:right\">\n                <li class=\"page-item\"><a class=\"page-link\" href=\"#\">Previous</a></li>\n                <li class=\"page-item\"><a class=\"page-link\" href=\"#\">1</a></li>\n                <li class=\"page-item\"><a class=\"page-link\" href=\"#\">2</a></li>\n                <li class=\"page-item\"><a class=\"page-link\" href=\"#\">3</a></li>\n                <li class=\"page-item\"><a class=\"page-link\" href=\"#\">4</a></li>\n                <li class=\"page-item\"><a class=\"page-link\" href=\"#\">5</a></li>\n                <li class=\"page-item\"><a class=\"page-link\" href=\"#\">Next</a></li>\n              </ul>\n            </th>\n\n          </tr>\n\n        </tbody>\n      </table>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/sub-dep-list/sub-dep-list.component.ts":
/*!********************************************************!*\
  !*** ./src/app/sub-dep-list/sub-dep-list.component.ts ***!
  \********************************************************/
/*! exports provided: SubDepListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubDepListComponent", function() { return SubDepListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubDepListComponent = /** @class */ (function () {
    function SubDepListComponent(router) {
        this.router = router;
        this.datas = [
            {
                dep: 'Inward',
                sub_dep: 'Inawrd 1',
                password: '********',
                remarks: 'Remarks1, Remarks2',
            },
            {
                dep: 'Inward',
                sub_dep: 'Inawrd 2',
                password: '********',
                remarks: 'Remarks1, Remarks2',
            },
            {
                dep: 'Inward',
                sub_dep: 'Inawrd 3',
                password: '********',
                remarks: 'Remarks1, Remarks2',
            },
            {
                dep: 'Inward',
                sub_dep: 'Inawrd 4',
                password: '********',
                remarks: 'Remarks1, Remarks2',
            },
            {
                dep: 'Inward',
                sub_dep: 'Inawrd 5',
                password: '********',
                remarks: 'Remarks1, Remarks2',
            },
            {
                dep: 'Inward',
                sub_dep: 'Inawrd 6',
                password: '********',
                remarks: 'Remarks1, Remarks2',
            },
            {
                dep: 'Inward',
                sub_dep: 'Inawrd 7',
                password: '********',
                remarks: 'Remarks1, Remarks2',
            },
            {
                dep: 'Inward',
                sub_dep: 'Inawrd 8',
                password: '********',
                remarks: 'Remarks1, Remarks2',
            }
        ];
    }
    SubDepListComponent.prototype.onEditSubDep = function () {
        this.router.navigate(['/edit_sub_department']);
    };
    SubDepListComponent.prototype.ngOnInit = function () {
    };
    SubDepListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sub-dep-list',
            template: __webpack_require__(/*! ./sub-dep-list.component.html */ "./src/app/sub-dep-list/sub-dep-list.component.html"),
            styles: [__webpack_require__(/*! ./sub-dep-list.component.css */ "./src/app/sub-dep-list/sub-dep-list.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], SubDepListComponent);
    return SubDepListComponent;
}());



/***/ }),

/***/ "./src/app/sub-department/sub-department.component.css":
/*!*************************************************************!*\
  !*** ./src/app/sub-department/sub-department.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n@import url('https://fonts.googleapis.com/css?family=Numans');\n/* Made with love by Mutiullah Samim*/\nhtml,body{\n\nbackground-size: cover;\nbackground-repeat: no-repeat;\nheight: 100%;\nfont-family: 'Numans', sans-serif;\n}\nlabel\n{\n    color: #fff;\n}\n.container{\nheight: 100%;\nbackground-size: cover;\noverflow-x: hidden;\nwidth:100%;\nbackground-repeat: no-repeat;\n/* align-content: center; */\n}\n.card{\nheight: 440px;\nmargin-top: auto;\nmargin-bottom: auto;\nwidth: 400px;\nbackground-color: rgba(0,0,0,0.5) !important;\n}\n.social_icon span{\nfont-size: 60px;\nmargin-left: 10px;\ncolor: #FFC312;\n}\n.social_icon span:hover{\ncolor: white;\ncursor: pointer;\n}\n.card-header h3{\ncolor: white;\n}\n.social_icon{\nposition: absolute;\nright: 20px;\ntop: -45px;\n}\n.input-group-prepend span{\nwidth: 50px;\nbackground-color: #FFC312;\ncolor: black;\nborder:0 !important;\n}\ninput:focus{\noutline: 0 0 0 0  !important;\nbox-shadow: 0 0 0 0 !important;\n\n}\n.remember{\ncolor: white;\n}\n.remember input\n{\nwidth: 20px;\nheight: 20px;\nmargin-left: 15px;\nmargin-right: 5px;\n}\n.login_btn{\ncolor: black;\nbackground-color: #FFC312;\nwidth: 100px;\n}\n.login_btn:hover{\ncolor: black;\nbackground-color: white;\n}\n.links{\ncolor: white;\n}\n.links a{\nmargin-left: 4px;\n}\noption\n{\n    margin-bottom: 10px!important;\n\n}\n/* Hamburger Menu css */\na\n{\n  text-decoration: none;\n  color: #232323;\n  \n  transition: color 0.3s ease;\n}\na:hover\n{\n  color: tomato;\n}\n#menuToggle\n{\n  display: block;\n  position: absolute;\n  top: 20px;\n  right: 50px;\n  \n  /* z-index: 1; */\n  \n  -webkit-user-select: none;\n  -moz-user-select: none;\n   -ms-user-select: none;\n       user-select: none;\n  \n}\n#menuToggle input\n{\n  display: block;\n  width: 40px;\n  height: 32px;\n  position: absolute;\n  top: -7px;\n  left: -5px;\n  \n  cursor: pointer;\n  \n  opacity: 0; /* hide this */\n  z-index: 2; /* and place it over the hamburger */\n  \n  -webkit-touch-callout: none;\n}\n/*\n * Just a quick hamburger\n */\n#menuToggle span\n{\n  display: block;\n  width: 33px;\n  height: 4px;\n  margin-bottom: 5px;\n  position: relative;\n  \n  background: #cdcdcd;\n  border-radius: 3px;\n  \n  z-index: 1;\n  \n  -webkit-transform-origin: 4px 0px;\n  \n          transform-origin: 4px 0px;\n  \n  transition: background 0.5s cubic-bezier(0.77,0.2,0.05,1.0),\n              opacity 0.55s ease,\n              -webkit-transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0);\n  \n  transition: transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0),\n              background 0.5s cubic-bezier(0.77,0.2,0.05,1.0),\n              opacity 0.55s ease;\n  \n  transition: transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0),\n              background 0.5s cubic-bezier(0.77,0.2,0.05,1.0),\n              opacity 0.55s ease,\n              -webkit-transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0);\n}\n#menuToggle span:first-child\n{\n  -webkit-transform-origin: 0% 0%;\n          transform-origin: 0% 0%;\n}\n#menuToggle span:nth-last-child(2)\n{\n  -webkit-transform-origin: 0% 100%;\n          transform-origin: 0% 100%;\n}\n/* \n * Transform all the slices of hamburger\n * into a crossmark.\n */\n#menuToggle input:checked ~ span\n{\n  opacity: 1;\n  -webkit-transform: rotate(45deg) translate(-2px, -1px);\n          transform: rotate(45deg) translate(-2px, -1px);\n  background: #232323;\n}\n/*\n * But let's hide the middle one.\n */\n#menuToggle input:checked ~ span:nth-last-child(3)\n{\n  opacity: 0;\n  -webkit-transform: rotate(0deg) scale(0.2, 0.2);\n          transform: rotate(0deg) scale(0.2, 0.2);\n}\n/*\n * Ohyeah and the last one should go the other direction\n */\n#menuToggle input:checked ~ span:nth-last-child(2)\n{\n  opacity: 1;\n  -webkit-transform: rotate(-45deg) translate(0, -1px);\n          transform: rotate(-45deg) translate(0, -1px);\n}\n/*\n * Make this absolute positioned\n * at the top left of the screen\n */\n#menu\n{\n  position: absolute;\n  width: 300px;\n  margin: -100px 0 0 0;\n  padding: 20px;\n  padding-top: 100px;\n  right: -100px;\n  \n  background: #ededed;\n  list-style-type: none;\n  -webkit-font-smoothing: antialiased;\n  /* to stop flickering of text in safari */\n  \n  -webkit-transform-origin: 0% 0%;\n  \n          transform-origin: 0% 0%;\n  -webkit-transform: translate(100%, 0);\n          transform: translate(100%, 0);\n  \n  transition: -webkit-transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0);\n  \n  transition: transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0);\n  \n  transition: transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0), -webkit-transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0);\n}\n#menu li\n{\n  padding: 10px 0;\n \n  font-size: 22px;\n}\n/*\n * And let's fade it in from the left\n */\n#menuToggle input:checked ~ ul\n{\n  -webkit-transform: scale(1.0, 1.0);\n          transform: scale(1.0, 1.0);\n  opacity: 1;\n}\n/* End of Hamburger Menu css */\n/* *************************************************************************\n**********************  Media query for Mobile screen  */\n@media only screen and (max-width : 480px) {\n    .card{\n        height: 630px;\n        margin-top: auto;\n        margin-bottom: auto;\n        width: 400px;\n        background-color: rgba(0,0,0,0.5) !important;\n        }\n\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3ViLWRlcGFydG1lbnQvc3ViLWRlcGFydG1lbnQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLDZEQUE2RDtBQUY3RCxxQ0FBcUM7QUFJckM7O0FBRUEsc0JBQXNCO0FBQ3RCLDRCQUE0QjtBQUM1QixZQUFZO0FBQ1osaUNBQWlDO0FBQ2pDO0FBQ0E7O0lBRUksV0FBVztBQUNmO0FBQ0E7QUFDQSxZQUFZO0FBQ1osc0JBQXNCO0FBQ3RCLGtCQUFrQjtBQUNsQixVQUFVO0FBQ1YsNEJBQTRCO0FBQzVCLDJCQUEyQjtBQUMzQjtBQUVBO0FBQ0EsYUFBYTtBQUNiLGdCQUFnQjtBQUNoQixtQkFBbUI7QUFDbkIsWUFBWTtBQUNaLDRDQUE0QztBQUM1QztBQUVBO0FBQ0EsZUFBZTtBQUNmLGlCQUFpQjtBQUNqQixjQUFjO0FBQ2Q7QUFFQTtBQUNBLFlBQVk7QUFDWixlQUFlO0FBQ2Y7QUFFQTtBQUNBLFlBQVk7QUFDWjtBQUVBO0FBQ0Esa0JBQWtCO0FBQ2xCLFdBQVc7QUFDWCxVQUFVO0FBQ1Y7QUFFQTtBQUNBLFdBQVc7QUFDWCx5QkFBeUI7QUFDekIsWUFBWTtBQUNaLG1CQUFtQjtBQUNuQjtBQUVBO0FBQ0EsNEJBQTRCO0FBQzVCLDhCQUE4Qjs7QUFFOUI7QUFFQTtBQUNBLFlBQVk7QUFDWjtBQUVBOztBQUVBLFdBQVc7QUFDWCxZQUFZO0FBQ1osaUJBQWlCO0FBQ2pCLGlCQUFpQjtBQUNqQjtBQUVBO0FBQ0EsWUFBWTtBQUNaLHlCQUF5QjtBQUN6QixZQUFZO0FBQ1o7QUFFQTtBQUNBLFlBQVk7QUFDWix1QkFBdUI7QUFDdkI7QUFFQTtBQUNBLFlBQVk7QUFDWjtBQUVBO0FBQ0EsZ0JBQWdCO0FBQ2hCO0FBQ0E7O0lBRUksNkJBQTZCOztBQUVqQztBQUVBLHVCQUF1QjtBQUN2Qjs7RUFFRSxxQkFBcUI7RUFDckIsY0FBYzs7RUFFZCwyQkFBMkI7QUFDN0I7QUFFQTs7RUFFRSxhQUFhO0FBQ2Y7QUFFQTs7RUFFRSxjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxXQUFXOztFQUVYLGdCQUFnQjs7RUFFaEIseUJBQXlCO0VBQ3pCLHNCQUFpQjtHQUFqQixxQkFBaUI7T0FBakIsaUJBQWlCOztBQUVuQjtBQUVBOztFQUVFLGNBQWM7RUFDZCxXQUFXO0VBQ1gsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsVUFBVTs7RUFFVixlQUFlOztFQUVmLFVBQVUsRUFBRSxjQUFjO0VBQzFCLFVBQVUsRUFBRSxvQ0FBb0M7O0VBRWhELDJCQUEyQjtBQUM3QjtBQUVBOztFQUVFO0FBQ0Y7O0VBRUUsY0FBYztFQUNkLFdBQVc7RUFDWCxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGtCQUFrQjs7RUFFbEIsbUJBQW1CO0VBQ25CLGtCQUFrQjs7RUFFbEIsVUFBVTs7RUFFVixpQ0FBeUI7O1VBQXpCLHlCQUF5Qjs7RUFFekI7O29FQUU4Qjs7RUFGOUI7O2dDQUU4Qjs7RUFGOUI7OztvRUFFOEI7QUFDaEM7QUFFQTs7RUFFRSwrQkFBdUI7VUFBdkIsdUJBQXVCO0FBQ3pCO0FBRUE7O0VBRUUsaUNBQXlCO1VBQXpCLHlCQUF5QjtBQUMzQjtBQUVBOzs7RUFHRTtBQUNGOztFQUVFLFVBQVU7RUFDVixzREFBOEM7VUFBOUMsOENBQThDO0VBQzlDLG1CQUFtQjtBQUNyQjtBQUVBOztFQUVFO0FBQ0Y7O0VBRUUsVUFBVTtFQUNWLCtDQUF1QztVQUF2Qyx1Q0FBdUM7QUFDekM7QUFFQTs7RUFFRTtBQUNGOztFQUVFLFVBQVU7RUFDVixvREFBNEM7VUFBNUMsNENBQTRDO0FBQzlDO0FBRUE7OztFQUdFO0FBQ0Y7O0VBRUUsa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixvQkFBb0I7RUFDcEIsYUFBYTtFQUNiLGtCQUFrQjtFQUNsQixhQUFhOztFQUViLG1CQUFtQjtFQUNuQixxQkFBcUI7RUFDckIsbUNBQW1DO0VBQ25DLHlDQUF5Qzs7RUFFekMsK0JBQXVCOztVQUF2Qix1QkFBdUI7RUFDdkIscUNBQTZCO1VBQTdCLDZCQUE2Qjs7RUFFN0Isa0VBQTBEOztFQUExRCwwREFBMEQ7O0VBQTFELGtIQUEwRDtBQUM1RDtBQUVBOztFQUVFLGVBQWU7O0VBRWYsZUFBZTtBQUNqQjtBQUVBOztFQUVFO0FBQ0Y7O0VBRUUsa0NBQTBCO1VBQTFCLDBCQUEwQjtFQUMxQixVQUFVO0FBQ1o7QUFDQSw4QkFBOEI7QUFHOUI7d0RBQ3dEO0FBRXhEO0lBQ0k7UUFDSSxhQUFhO1FBQ2IsZ0JBQWdCO1FBQ2hCLG1CQUFtQjtRQUNuQixZQUFZO1FBQ1osNENBQTRDO1FBQzVDOztBQUVSIiwiZmlsZSI6InNyYy9hcHAvc3ViLWRlcGFydG1lbnQvc3ViLWRlcGFydG1lbnQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIE1hZGUgd2l0aCBsb3ZlIGJ5IE11dGl1bGxhaCBTYW1pbSovXG5cbkBpbXBvcnQgdXJsKCdodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2Nzcz9mYW1pbHk9TnVtYW5zJyk7XG5cbmh0bWwsYm9keXtcblxuYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbmJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG5oZWlnaHQ6IDEwMCU7XG5mb250LWZhbWlseTogJ051bWFucycsIHNhbnMtc2VyaWY7XG59XG5sYWJlbFxue1xuICAgIGNvbG9yOiAjZmZmO1xufVxuLmNvbnRhaW5lcntcbmhlaWdodDogMTAwJTtcbmJhY2tncm91bmQtc2l6ZTogY292ZXI7XG5vdmVyZmxvdy14OiBoaWRkZW47XG53aWR0aDoxMDAlO1xuYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbi8qIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjsgKi9cbn1cblxuLmNhcmR7XG5oZWlnaHQ6IDQ0MHB4O1xubWFyZ2luLXRvcDogYXV0bztcbm1hcmdpbi1ib3R0b206IGF1dG87XG53aWR0aDogNDAwcHg7XG5iYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuNSkgIWltcG9ydGFudDtcbn1cblxuLnNvY2lhbF9pY29uIHNwYW57XG5mb250LXNpemU6IDYwcHg7XG5tYXJnaW4tbGVmdDogMTBweDtcbmNvbG9yOiAjRkZDMzEyO1xufVxuXG4uc29jaWFsX2ljb24gc3Bhbjpob3ZlcntcbmNvbG9yOiB3aGl0ZTtcbmN1cnNvcjogcG9pbnRlcjtcbn1cblxuLmNhcmQtaGVhZGVyIGgze1xuY29sb3I6IHdoaXRlO1xufVxuXG4uc29jaWFsX2ljb257XG5wb3NpdGlvbjogYWJzb2x1dGU7XG5yaWdodDogMjBweDtcbnRvcDogLTQ1cHg7XG59XG5cbi5pbnB1dC1ncm91cC1wcmVwZW5kIHNwYW57XG53aWR0aDogNTBweDtcbmJhY2tncm91bmQtY29sb3I6ICNGRkMzMTI7XG5jb2xvcjogYmxhY2s7XG5ib3JkZXI6MCAhaW1wb3J0YW50O1xufVxuXG5pbnB1dDpmb2N1c3tcbm91dGxpbmU6IDAgMCAwIDAgICFpbXBvcnRhbnQ7XG5ib3gtc2hhZG93OiAwIDAgMCAwICFpbXBvcnRhbnQ7XG5cbn1cblxuLnJlbWVtYmVye1xuY29sb3I6IHdoaXRlO1xufVxuXG4ucmVtZW1iZXIgaW5wdXRcbntcbndpZHRoOiAyMHB4O1xuaGVpZ2h0OiAyMHB4O1xubWFyZ2luLWxlZnQ6IDE1cHg7XG5tYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLmxvZ2luX2J0bntcbmNvbG9yOiBibGFjaztcbmJhY2tncm91bmQtY29sb3I6ICNGRkMzMTI7XG53aWR0aDogMTAwcHg7XG59XG5cbi5sb2dpbl9idG46aG92ZXJ7XG5jb2xvcjogYmxhY2s7XG5iYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cblxuLmxpbmtze1xuY29sb3I6IHdoaXRlO1xufVxuXG4ubGlua3MgYXtcbm1hcmdpbi1sZWZ0OiA0cHg7XG59XG5vcHRpb25cbntcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4IWltcG9ydGFudDtcblxufVxuXG4vKiBIYW1idXJnZXIgTWVudSBjc3MgKi9cbmFcbntcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBjb2xvcjogIzIzMjMyMztcbiAgXG4gIHRyYW5zaXRpb246IGNvbG9yIDAuM3MgZWFzZTtcbn1cblxuYTpob3Zlclxue1xuICBjb2xvcjogdG9tYXRvO1xufVxuXG4jbWVudVRvZ2dsZVxue1xuICBkaXNwbGF5OiBibG9jaztcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDIwcHg7XG4gIHJpZ2h0OiA1MHB4O1xuICBcbiAgLyogei1pbmRleDogMTsgKi9cbiAgXG4gIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7XG4gIHVzZXItc2VsZWN0OiBub25lO1xuICBcbn1cblxuI21lbnVUb2dnbGUgaW5wdXRcbntcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiA0MHB4O1xuICBoZWlnaHQ6IDMycHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAtN3B4O1xuICBsZWZ0OiAtNXB4O1xuICBcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBcbiAgb3BhY2l0eTogMDsgLyogaGlkZSB0aGlzICovXG4gIHotaW5kZXg6IDI7IC8qIGFuZCBwbGFjZSBpdCBvdmVyIHRoZSBoYW1idXJnZXIgKi9cbiAgXG4gIC13ZWJraXQtdG91Y2gtY2FsbG91dDogbm9uZTtcbn1cblxuLypcbiAqIEp1c3QgYSBxdWljayBoYW1idXJnZXJcbiAqL1xuI21lbnVUb2dnbGUgc3Bhblxue1xuICBkaXNwbGF5OiBibG9jaztcbiAgd2lkdGg6IDMzcHg7XG4gIGhlaWdodDogNHB4O1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgXG4gIGJhY2tncm91bmQ6ICNjZGNkY2Q7XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgXG4gIHotaW5kZXg6IDE7XG4gIFxuICB0cmFuc2Zvcm0tb3JpZ2luOiA0cHggMHB4O1xuICBcbiAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDAuNXMgY3ViaWMtYmV6aWVyKDAuNzcsMC4yLDAuMDUsMS4wKSxcbiAgICAgICAgICAgICAgYmFja2dyb3VuZCAwLjVzIGN1YmljLWJlemllcigwLjc3LDAuMiwwLjA1LDEuMCksXG4gICAgICAgICAgICAgIG9wYWNpdHkgMC41NXMgZWFzZTtcbn1cblxuI21lbnVUb2dnbGUgc3BhbjpmaXJzdC1jaGlsZFxue1xuICB0cmFuc2Zvcm0tb3JpZ2luOiAwJSAwJTtcbn1cblxuI21lbnVUb2dnbGUgc3BhbjpudGgtbGFzdC1jaGlsZCgyKVxue1xuICB0cmFuc2Zvcm0tb3JpZ2luOiAwJSAxMDAlO1xufVxuXG4vKiBcbiAqIFRyYW5zZm9ybSBhbGwgdGhlIHNsaWNlcyBvZiBoYW1idXJnZXJcbiAqIGludG8gYSBjcm9zc21hcmsuXG4gKi9cbiNtZW51VG9nZ2xlIGlucHV0OmNoZWNrZWQgfiBzcGFuXG57XG4gIG9wYWNpdHk6IDE7XG4gIHRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKSB0cmFuc2xhdGUoLTJweCwgLTFweCk7XG4gIGJhY2tncm91bmQ6ICMyMzIzMjM7XG59XG5cbi8qXG4gKiBCdXQgbGV0J3MgaGlkZSB0aGUgbWlkZGxlIG9uZS5cbiAqL1xuI21lbnVUb2dnbGUgaW5wdXQ6Y2hlY2tlZCB+IHNwYW46bnRoLWxhc3QtY2hpbGQoMylcbntcbiAgb3BhY2l0eTogMDtcbiAgdHJhbnNmb3JtOiByb3RhdGUoMGRlZykgc2NhbGUoMC4yLCAwLjIpO1xufVxuXG4vKlxuICogT2h5ZWFoIGFuZCB0aGUgbGFzdCBvbmUgc2hvdWxkIGdvIHRoZSBvdGhlciBkaXJlY3Rpb25cbiAqL1xuI21lbnVUb2dnbGUgaW5wdXQ6Y2hlY2tlZCB+IHNwYW46bnRoLWxhc3QtY2hpbGQoMilcbntcbiAgb3BhY2l0eTogMTtcbiAgdHJhbnNmb3JtOiByb3RhdGUoLTQ1ZGVnKSB0cmFuc2xhdGUoMCwgLTFweCk7XG59XG5cbi8qXG4gKiBNYWtlIHRoaXMgYWJzb2x1dGUgcG9zaXRpb25lZFxuICogYXQgdGhlIHRvcCBsZWZ0IG9mIHRoZSBzY3JlZW5cbiAqL1xuI21lbnVcbntcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogMzAwcHg7XG4gIG1hcmdpbjogLTEwMHB4IDAgMCAwO1xuICBwYWRkaW5nOiAyMHB4O1xuICBwYWRkaW5nLXRvcDogMTAwcHg7XG4gIHJpZ2h0OiAtMTAwcHg7XG4gIFxuICBiYWNrZ3JvdW5kOiAjZWRlZGVkO1xuICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XG4gIC13ZWJraXQtZm9udC1zbW9vdGhpbmc6IGFudGlhbGlhc2VkO1xuICAvKiB0byBzdG9wIGZsaWNrZXJpbmcgb2YgdGV4dCBpbiBzYWZhcmkgKi9cbiAgXG4gIHRyYW5zZm9ybS1vcmlnaW46IDAlIDAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgxMDAlLCAwKTtcbiAgXG4gIHRyYW5zaXRpb246IHRyYW5zZm9ybSAwLjVzIGN1YmljLWJlemllcigwLjc3LDAuMiwwLjA1LDEuMCk7XG59XG5cbiNtZW51IGxpXG57XG4gIHBhZGRpbmc6IDEwcHggMDtcbiBcbiAgZm9udC1zaXplOiAyMnB4O1xufVxuXG4vKlxuICogQW5kIGxldCdzIGZhZGUgaXQgaW4gZnJvbSB0aGUgbGVmdFxuICovXG4jbWVudVRvZ2dsZSBpbnB1dDpjaGVja2VkIH4gdWxcbntcbiAgdHJhbnNmb3JtOiBzY2FsZSgxLjAsIDEuMCk7XG4gIG9wYWNpdHk6IDE7XG59XG4vKiBFbmQgb2YgSGFtYnVyZ2VyIE1lbnUgY3NzICovXG5cblxuLyogKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuKioqKioqKioqKioqKioqKioqKioqKiAgTWVkaWEgcXVlcnkgZm9yIE1vYmlsZSBzY3JlZW4gICovXG5cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aCA6IDQ4MHB4KSB7XG4gICAgLmNhcmR7XG4gICAgICAgIGhlaWdodDogNjMwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6IGF1dG87XG4gICAgICAgIG1hcmdpbi1ib3R0b206IGF1dG87XG4gICAgICAgIHdpZHRoOiA0MDBweDtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLDAsMCwwLjUpICFpbXBvcnRhbnQ7XG4gICAgICAgIH1cblxufSJdfQ== */"

/***/ }),

/***/ "./src/app/sub-department/sub-department.component.html":
/*!**************************************************************!*\
  !*** ./src/app/sub-department/sub-department.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <!-- Hamburger Menu -->\n  <nav role='navigation'>\n    <div id=\"menuToggle\">\n      <input type=\"checkbox\" />\n      <span></span>\n      <span></span>\n      <span></span>\n      <ul id=\"menu\">\n        <a href=\"#\">\n          <li>Add Sub Department</li>\n        </a>\n        <a href=\"#\">\n          <li>Sub Department List</li>\n        </a>\n        <a routerLink=\"/logout\">\n          <li>Logout</li>\n        </a>\n      </ul>\n    </div>\n  </nav>\n  <!-- End of Hamburger Menu -->\n  <div class=\"d-flex justify-content-center h-100\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        <h3 style=\"text-align:center\">Add Sub Department</h3>\n      </div>\n      <div class=\"card-body\">\n        <form class=\"form-login\" role=\"form\" #formRef='ngForm' (ngSubmit)='onSubmit(formRef.value)'>\n\n          <div class=\"form-group row\">\n            <label for=\"seldep\" class=\"col-sm-5 col-form-label\">Department</label>\n            <div class=\"col-sm-7\">\n              <select class=\"form-control\" id=\"selectdep\" name=\"subDepSelect\" #selectRef='ngModel' required ngModel>\n                <option>Select Department</option>\n                <option>Inward</option>\n                <option>Accountant</option>\n                <option>Washing</option>\n                <option>Outward</option>\n              </select>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDep\" class=\"col-sm-5 col-form-label\">Sub Department</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDep\" #subdeportmentRef='ngModel' name=\"subDepartment\" required ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepPassword\" class=\"col-sm-5 col-form-label\">Password</label>\n            <div class=\"col-sm-7\">\n              <input type=\"password\" class=\"form-control\" id=\"subDepPassword\" #subPasswordRef='ngModel' name=\"subDepPassword\"\n                required ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepcfPassword\" class=\"col-sm-5 col-form-label\">Confirm Password</label>\n            <div class=\"col-sm-7\">\n              <input type=\"password\" class=\"form-control\" id=\"subDepcfPassword\" #subDepCFPasswordRef='ngModel' name=\"subDepCFPassword\"\n                required ngModel>\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"subDepRemark\" class=\"col-sm-5 col-form-label\">Remarks</label>\n            <div class=\"col-sm-7\">\n              <input type=\"text\" class=\"form-control\" id=\"subDepRemark\" subDepRemarkRef='ngModel' name=\"subDepRemark\" required ngModel>\n            </div>\n          </div>\n\n          <div class=\"form-group\" style=\"text-align:center\">\n            <button style=\"padding: 0;\n            border: none;\n            background: none;\" type=\"submit\">\n              <!-- <i class=\"far fa-check-circle\"> -->\n              <i class=\"fas fa-check-circle\" style=\"font-size:60px;color: yellow;\"></i>\n\n            </button>\n          </div>\n\n        </form>\n      </div>\n\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/sub-department/sub-department.component.ts":
/*!************************************************************!*\
  !*** ./src/app/sub-department/sub-department.component.ts ***!
  \************************************************************/
/*! exports provided: SubDepartmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubDepartmentComponent", function() { return SubDepartmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SubDepartmentComponent = /** @class */ (function () {
    function SubDepartmentComponent(router, _http) {
        this.router = router;
        this._http = _http;
    }
    SubDepartmentComponent.prototype.ngOnInit = function () {
    };
    SubDepartmentComponent.prototype.onSubmit = function (a) {
        var _this = this;
        this.subDepSelect = a.subDepSelect;
        this.subDepartment = a.subDepartment;
        this.subDepPassword = a.subDepPassword;
        this.subDepCFPassword = a.subDepCFPassword;
        this.subDepRemark = a.subDepRemark;
        console.log(a);
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.post('/api/sub_department', a, options)
            .subscribe(function (data) {
            alert('Registered Successfully');
            _this.router.navigate(['/getdata']);
        }, function (error) {
            console.log(JSON.stringify(error.json()));
        });
    };
    SubDepartmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sub-department',
            template: __webpack_require__(/*! ./sub-department.component.html */ "./src/app/sub-department/sub-department.component.html"),
            styles: [__webpack_require__(/*! ./sub-department.component.css */ "./src/app/sub-department/sub-department.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], SubDepartmentComponent);
    return SubDepartmentComponent;
}());



/***/ }),

/***/ "./src/app/updated-sub-dep/updated-sub-dep.component.css":
/*!***************************************************************!*\
  !*** ./src/app/updated-sub-dep/updated-sub-dep.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n@import url('https://fonts.googleapis.com/css?family=Numans');\n/* Made with love by Mutiullah Samim*/\n.container{\nheight: 100%;\nbackground-size: cover;\nwidth:100%;\nbackground-repeat: no-repeat;\ncolor:#fff;\n/* align-content: center; */\n}\n.card{\nheight: 550px;\nmargin-top: 50px;\nmargin-bottom: 50px;\nwidth: 850px;\nbackground-color: rgba(0,0,0,0.5) !important;\n\n}\ni\n{\n    font-size: 22px;\n}\ntable, th, td{\n    border: 1px solid white;\n    border-collapse: collapse;\n  }\nth, td \n  {\n      text-align: center;\n      padding: 10px;\n\t}\n/* Hamburger Menu css */\na\n{\n  text-decoration: none;\n  color: #232323;\n  \n  transition: color 0.3s ease;\n}\na:hover\n{\n  color: tomato;\n}\n#menuToggle\n{\n  display: block;\n  position: absolute;\n  top: 20px;\n  right: 50px;\n  \n  z-index: 1;\n  \n  -webkit-user-select: none;\n  -moz-user-select: none;\n   -ms-user-select: none;\n       user-select: none;\n}\n#menuToggle input\n{\n  display: block;\n  width: 40px;\n  height: 32px;\n  position: absolute;\n  top: -7px;\n  left: -5px;\n  \n  cursor: pointer;\n  \n  opacity: 0; /* hide this */\n  z-index: 2; /* and place it over the hamburger */\n  \n  -webkit-touch-callout: none;\n}\n/*\n * Just a quick hamburger\n */\n#menuToggle span\n{\n  display: block;\n  width: 33px;\n  height: 4px;\n  margin-bottom: 5px;\n  position: relative;\n  \n  background: #cdcdcd;\n  border-radius: 3px;\n  \n  z-index: 1;\n  \n  -webkit-transform-origin: 4px 0px;\n  \n          transform-origin: 4px 0px;\n  \n  transition: background 0.5s cubic-bezier(0.77,0.2,0.05,1.0),\n              opacity 0.55s ease,\n              -webkit-transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0);\n  \n  transition: transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0),\n              background 0.5s cubic-bezier(0.77,0.2,0.05,1.0),\n              opacity 0.55s ease;\n  \n  transition: transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0),\n              background 0.5s cubic-bezier(0.77,0.2,0.05,1.0),\n              opacity 0.55s ease,\n              -webkit-transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0);\n}\n#menuToggle span:first-child\n{\n  -webkit-transform-origin: 0% 0%;\n          transform-origin: 0% 0%;\n}\n#menuToggle span:nth-last-child(2)\n{\n  -webkit-transform-origin: 0% 100%;\n          transform-origin: 0% 100%;\n}\n/* \n * Transform all the slices of hamburger\n * into a crossmark.\n */\n#menuToggle input:checked ~ span\n{\n  opacity: 1;\n  -webkit-transform: rotate(45deg) translate(-2px, -1px);\n          transform: rotate(45deg) translate(-2px, -1px);\n  background: #232323;\n}\n/*\n * But let's hide the middle one.\n */\n#menuToggle input:checked ~ span:nth-last-child(3)\n{\n  opacity: 0;\n  -webkit-transform: rotate(0deg) scale(0.2, 0.2);\n          transform: rotate(0deg) scale(0.2, 0.2);\n}\n/*\n * Ohyeah and the last one should go the other direction\n */\n#menuToggle input:checked ~ span:nth-last-child(2)\n{\n  opacity: 1;\n  -webkit-transform: rotate(-45deg) translate(0, -1px);\n          transform: rotate(-45deg) translate(0, -1px);\n}\n/*\n * Make this absolute positioned\n * at the top left of the screen\n */\n#menu\n{\n  position: absolute;\n  width: 300px;\n  margin: -100px 0 0 0;\n  padding: 20px;\n  padding-top: 100px;\n  right: -100px;\n  \n  background: #ededed;\n  list-style-type: none;\n  -webkit-font-smoothing: antialiased;\n  /* to stop flickering of text in safari */\n  \n  -webkit-transform-origin: 0% 0%;\n  \n          transform-origin: 0% 0%;\n  -webkit-transform: translate(100%, 0);\n          transform: translate(100%, 0);\n  \n  transition: -webkit-transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0);\n  \n  transition: transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0);\n  \n  transition: transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0), -webkit-transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0);\n}\n#menu li\n{\n  padding: 10px 0;\n \n  font-size: 22px;\n}\n/*\n * And let's fade it in from the left\n */\n#menuToggle input:checked ~ ul\n{\n  -webkit-transform: scale(1.0, 1.0);\n          transform: scale(1.0, 1.0);\n  opacity: 1;\n}\n/* End of Hamburger Menu css */\n/*\n\tMax width before this PARTICULAR table gets nasty. This query will take effect for any screen smaller than 760px and also iPads specifically.\n\t*/\n@media\n\t  only screen \n    and (max-width: 760px), (min-device-width: 768px) \n    and (max-device-width: 1024px)  {\n\n\n\t\t/* Force table to not be like tables anymore */\n\t\ttable, thead, tbody, th, td, tr {\n            display: block;\n            \n        }\n\n\n\t\t/* Hide table headers (but not display: none;, for accessibility) */\n\t\tthead tr {\n\t\t\tposition: absolute;\n\t\t\ttop: -9999px;\n\t\t\tleft: -9999px;\n\t\t}\n\n    tr {\n      margin: 0 0 1rem 0;\n    }\n      \n    tr:nth-child(odd) {\n      background: #ccc;\n    }\n    \n\t\ttd {\n\t\t\t/* Behave  like a \"row\" */\n\t\t\tborder: none;\n\t\t\tborder-bottom: 1px solid #eee;\n\t\t\tposition: relative;\n\t\t\tpadding-left: 50%;\n\t\t}\n\n\t\ttd:before {\n\t\t\t/* Now like a table header */\n\t\t\tposition: absolute;\n\t\t\t/* Top/left values mimic padding */\n\t\t\ttop: 0;\n\t\t\tleft: 6px;\n\t\t\twidth: 45%;\n\t\t\tpadding-right: 10px;\n\t\t\twhite-space: nowrap;\n\t\t}\n\n\t\t/*\n\t\tLabel the data\n    You could also use a data-* attribute and content for this. That way \"bloats\" the HTML, this way means you need to keep HTML and CSS in sync. Lea Verou has a clever way to handle with text-shadow.\n\t\t*/\n\t\ttd:nth-of-type(1):before { content: \"First Name\"; }\n\t\ttd:nth-of-type(2):before { content: \"Last Name\"; }\n\t\ttd:nth-of-type(3):before { content: \"Job Title\"; }\n\t\ttd:nth-of-type(4):before { content: \"Favorite Color\"; }\n\t\ttd:nth-of-type(5):before { content: \"Wars of Trek?\"; }\n\t\ttd:nth-of-type(6):before { content: \"Secret Alias\"; }\n\t\ttd:nth-of-type(7):before { content: \"Date of Birth\"; }\n\t\ttd:nth-of-type(8):before { content: \"Dream Vacation City\"; }\n\t\ttd:nth-of-type(9):before { content: \"GPA\"; }\n\t\ttd:nth-of-type(10):before { content: \"Arbitrary Data\"; }\n\t}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXBkYXRlZC1zdWItZGVwL3VwZGF0ZWQtc3ViLWRlcC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUEsNkRBQTZEO0FBRjdELHFDQUFxQztBQUtyQztBQUNBLFlBQVk7QUFDWixzQkFBc0I7QUFDdEIsVUFBVTtBQUNWLDRCQUE0QjtBQUM1QixVQUFVO0FBQ1YsMkJBQTJCO0FBQzNCO0FBRUE7QUFDQSxhQUFhO0FBQ2IsZ0JBQWdCO0FBQ2hCLG1CQUFtQjtBQUNuQixZQUFZO0FBQ1osNENBQTRDOztBQUU1QztBQUNBOztJQUVJLGVBQWU7QUFDbkI7QUFFQTtJQUNJLHVCQUF1QjtJQUN2Qix5QkFBeUI7RUFDM0I7QUFDQTs7TUFFSSxrQkFBa0I7TUFDbEIsYUFBYTtDQUNsQjtBQUVBLHVCQUF1QjtBQUN4Qjs7RUFFRSxxQkFBcUI7RUFDckIsY0FBYzs7RUFFZCwyQkFBMkI7QUFDN0I7QUFFQTs7RUFFRSxhQUFhO0FBQ2Y7QUFFQTs7RUFFRSxjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxXQUFXOztFQUVYLFVBQVU7O0VBRVYseUJBQXlCO0VBQ3pCLHNCQUFpQjtHQUFqQixxQkFBaUI7T0FBakIsaUJBQWlCO0FBQ25CO0FBRUE7O0VBRUUsY0FBYztFQUNkLFdBQVc7RUFDWCxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxVQUFVOztFQUVWLGVBQWU7O0VBRWYsVUFBVSxFQUFFLGNBQWM7RUFDMUIsVUFBVSxFQUFFLG9DQUFvQzs7RUFFaEQsMkJBQTJCO0FBQzdCO0FBRUE7O0VBRUU7QUFDRjs7RUFFRSxjQUFjO0VBQ2QsV0FBVztFQUNYLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsa0JBQWtCOztFQUVsQixtQkFBbUI7RUFDbkIsa0JBQWtCOztFQUVsQixVQUFVOztFQUVWLGlDQUF5Qjs7VUFBekIseUJBQXlCOztFQUV6Qjs7b0VBRThCOztFQUY5Qjs7Z0NBRThCOztFQUY5Qjs7O29FQUU4QjtBQUNoQztBQUVBOztFQUVFLCtCQUF1QjtVQUF2Qix1QkFBdUI7QUFDekI7QUFFQTs7RUFFRSxpQ0FBeUI7VUFBekIseUJBQXlCO0FBQzNCO0FBRUE7OztFQUdFO0FBQ0Y7O0VBRUUsVUFBVTtFQUNWLHNEQUE4QztVQUE5Qyw4Q0FBOEM7RUFDOUMsbUJBQW1CO0FBQ3JCO0FBRUE7O0VBRUU7QUFDRjs7RUFFRSxVQUFVO0VBQ1YsK0NBQXVDO1VBQXZDLHVDQUF1QztBQUN6QztBQUVBOztFQUVFO0FBQ0Y7O0VBRUUsVUFBVTtFQUNWLG9EQUE0QztVQUE1Qyw0Q0FBNEM7QUFDOUM7QUFFQTs7O0VBR0U7QUFDRjs7RUFFRSxrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLG9CQUFvQjtFQUNwQixhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLGFBQWE7O0VBRWIsbUJBQW1CO0VBQ25CLHFCQUFxQjtFQUNyQixtQ0FBbUM7RUFDbkMseUNBQXlDOztFQUV6QywrQkFBdUI7O1VBQXZCLHVCQUF1QjtFQUN2QixxQ0FBNkI7VUFBN0IsNkJBQTZCOztFQUU3QixrRUFBMEQ7O0VBQTFELDBEQUEwRDs7RUFBMUQsa0hBQTBEO0FBQzVEO0FBRUE7O0VBRUUsZUFBZTs7RUFFZixlQUFlO0FBQ2pCO0FBRUE7O0VBRUU7QUFDRjs7RUFFRSxrQ0FBMEI7VUFBMUIsMEJBQTBCO0VBQzFCLFVBQVU7QUFDWjtBQUNBLDhCQUE4QjtBQUk5Qjs7RUFFRTtBQUNEOzs7Ozs7RUFNQyw4Q0FBOEM7RUFDOUM7WUFDVSxjQUFjOztRQUVsQjs7O0VBR04sbUVBQW1FO0VBQ25FO0dBQ0Msa0JBQWtCO0dBQ2xCLFlBQVk7R0FDWixhQUFhO0VBQ2Q7O0lBRUU7TUFDRSxrQkFBa0I7SUFDcEI7O0lBRUE7TUFDRSxnQkFBZ0I7SUFDbEI7O0VBRUY7R0FDQyx5QkFBeUI7R0FDekIsWUFBWTtHQUNaLDZCQUE2QjtHQUM3QixrQkFBa0I7R0FDbEIsaUJBQWlCO0VBQ2xCOztFQUVBO0dBQ0MsNEJBQTRCO0dBQzVCLGtCQUFrQjtHQUNsQixrQ0FBa0M7R0FDbEMsTUFBTTtHQUNOLFNBQVM7R0FDVCxVQUFVO0dBQ1YsbUJBQW1CO0dBQ25CLG1CQUFtQjtFQUNwQjs7RUFFQTs7O0dBR0M7RUFDRCwyQkFBMkIscUJBQXFCLEVBQUU7RUFDbEQsMkJBQTJCLG9CQUFvQixFQUFFO0VBQ2pELDJCQUEyQixvQkFBb0IsRUFBRTtFQUNqRCwyQkFBMkIseUJBQXlCLEVBQUU7RUFDdEQsMkJBQTJCLHdCQUF3QixFQUFFO0VBQ3JELDJCQUEyQix1QkFBdUIsRUFBRTtFQUNwRCwyQkFBMkIsd0JBQXdCLEVBQUU7RUFDckQsMkJBQTJCLDhCQUE4QixFQUFFO0VBQzNELDJCQUEyQixjQUFjLEVBQUU7RUFDM0MsNEJBQTRCLHlCQUF5QixFQUFFO0NBQ3hEIiwiZmlsZSI6InNyYy9hcHAvdXBkYXRlZC1zdWItZGVwL3VwZGF0ZWQtc3ViLWRlcC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogTWFkZSB3aXRoIGxvdmUgYnkgTXV0aXVsbGFoIFNhbWltKi9cblxuQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1OdW1hbnMnKTtcblxuXG4uY29udGFpbmVye1xuaGVpZ2h0OiAxMDAlO1xuYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbndpZHRoOjEwMCU7XG5iYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuY29sb3I6I2ZmZjtcbi8qIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjsgKi9cbn1cblxuLmNhcmR7XG5oZWlnaHQ6IDU1MHB4O1xubWFyZ2luLXRvcDogNTBweDtcbm1hcmdpbi1ib3R0b206IDUwcHg7XG53aWR0aDogODUwcHg7XG5iYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuNSkgIWltcG9ydGFudDtcblxufVxuaVxue1xuICAgIGZvbnQtc2l6ZTogMjJweDtcbn1cblxudGFibGUsIHRoLCB0ZHtcbiAgICBib3JkZXI6IDFweCBzb2xpZCB3aGl0ZTtcbiAgICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xuICB9XG4gIHRoLCB0ZCBcbiAge1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgcGFkZGluZzogMTBweDtcblx0fVxuXG5cdC8qIEhhbWJ1cmdlciBNZW51IGNzcyAqL1xuYVxue1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIGNvbG9yOiAjMjMyMzIzO1xuICBcbiAgdHJhbnNpdGlvbjogY29sb3IgMC4zcyBlYXNlO1xufVxuXG5hOmhvdmVyXG57XG4gIGNvbG9yOiB0b21hdG87XG59XG5cbiNtZW51VG9nZ2xlXG57XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMjBweDtcbiAgcmlnaHQ6IDUwcHg7XG4gIFxuICB6LWluZGV4OiAxO1xuICBcbiAgLXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTtcbiAgdXNlci1zZWxlY3Q6IG5vbmU7XG59XG5cbiNtZW51VG9nZ2xlIGlucHV0XG57XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB3aWR0aDogNDBweDtcbiAgaGVpZ2h0OiAzMnB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogLTdweDtcbiAgbGVmdDogLTVweDtcbiAgXG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgXG4gIG9wYWNpdHk6IDA7IC8qIGhpZGUgdGhpcyAqL1xuICB6LWluZGV4OiAyOyAvKiBhbmQgcGxhY2UgaXQgb3ZlciB0aGUgaGFtYnVyZ2VyICovXG4gIFxuICAtd2Via2l0LXRvdWNoLWNhbGxvdXQ6IG5vbmU7XG59XG5cbi8qXG4gKiBKdXN0IGEgcXVpY2sgaGFtYnVyZ2VyXG4gKi9cbiNtZW51VG9nZ2xlIHNwYW5cbntcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiAzM3B4O1xuICBoZWlnaHQ6IDRweDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIFxuICBiYWNrZ3JvdW5kOiAjY2RjZGNkO1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG4gIFxuICB6LWluZGV4OiAxO1xuICBcbiAgdHJhbnNmb3JtLW9yaWdpbjogNHB4IDBweDtcbiAgXG4gIHRyYW5zaXRpb246IHRyYW5zZm9ybSAwLjVzIGN1YmljLWJlemllcigwLjc3LDAuMiwwLjA1LDEuMCksXG4gICAgICAgICAgICAgIGJhY2tncm91bmQgMC41cyBjdWJpYy1iZXppZXIoMC43NywwLjIsMC4wNSwxLjApLFxuICAgICAgICAgICAgICBvcGFjaXR5IDAuNTVzIGVhc2U7XG59XG5cbiNtZW51VG9nZ2xlIHNwYW46Zmlyc3QtY2hpbGRcbntcbiAgdHJhbnNmb3JtLW9yaWdpbjogMCUgMCU7XG59XG5cbiNtZW51VG9nZ2xlIHNwYW46bnRoLWxhc3QtY2hpbGQoMilcbntcbiAgdHJhbnNmb3JtLW9yaWdpbjogMCUgMTAwJTtcbn1cblxuLyogXG4gKiBUcmFuc2Zvcm0gYWxsIHRoZSBzbGljZXMgb2YgaGFtYnVyZ2VyXG4gKiBpbnRvIGEgY3Jvc3NtYXJrLlxuICovXG4jbWVudVRvZ2dsZSBpbnB1dDpjaGVja2VkIH4gc3Bhblxue1xuICBvcGFjaXR5OiAxO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZykgdHJhbnNsYXRlKC0ycHgsIC0xcHgpO1xuICBiYWNrZ3JvdW5kOiAjMjMyMzIzO1xufVxuXG4vKlxuICogQnV0IGxldCdzIGhpZGUgdGhlIG1pZGRsZSBvbmUuXG4gKi9cbiNtZW51VG9nZ2xlIGlucHV0OmNoZWNrZWQgfiBzcGFuOm50aC1sYXN0LWNoaWxkKDMpXG57XG4gIG9wYWNpdHk6IDA7XG4gIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpIHNjYWxlKDAuMiwgMC4yKTtcbn1cblxuLypcbiAqIE9oeWVhaCBhbmQgdGhlIGxhc3Qgb25lIHNob3VsZCBnbyB0aGUgb3RoZXIgZGlyZWN0aW9uXG4gKi9cbiNtZW51VG9nZ2xlIGlucHV0OmNoZWNrZWQgfiBzcGFuOm50aC1sYXN0LWNoaWxkKDIpXG57XG4gIG9wYWNpdHk6IDE7XG4gIHRyYW5zZm9ybTogcm90YXRlKC00NWRlZykgdHJhbnNsYXRlKDAsIC0xcHgpO1xufVxuXG4vKlxuICogTWFrZSB0aGlzIGFic29sdXRlIHBvc2l0aW9uZWRcbiAqIGF0IHRoZSB0b3AgbGVmdCBvZiB0aGUgc2NyZWVuXG4gKi9cbiNtZW51XG57XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgd2lkdGg6IDMwMHB4O1xuICBtYXJnaW46IC0xMDBweCAwIDAgMDtcbiAgcGFkZGluZzogMjBweDtcbiAgcGFkZGluZy10b3A6IDEwMHB4O1xuICByaWdodDogLTEwMHB4O1xuICBcbiAgYmFja2dyb3VuZDogI2VkZWRlZDtcbiAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xuICAtd2Via2l0LWZvbnQtc21vb3RoaW5nOiBhbnRpYWxpYXNlZDtcbiAgLyogdG8gc3RvcCBmbGlja2VyaW5nIG9mIHRleHQgaW4gc2FmYXJpICovXG4gIFxuICB0cmFuc2Zvcm0tb3JpZ2luOiAwJSAwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMTAwJSwgMCk7XG4gIFxuICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMC41cyBjdWJpYy1iZXppZXIoMC43NywwLjIsMC4wNSwxLjApO1xufVxuXG4jbWVudSBsaVxue1xuICBwYWRkaW5nOiAxMHB4IDA7XG4gXG4gIGZvbnQtc2l6ZTogMjJweDtcbn1cblxuLypcbiAqIEFuZCBsZXQncyBmYWRlIGl0IGluIGZyb20gdGhlIGxlZnRcbiAqL1xuI21lbnVUb2dnbGUgaW5wdXQ6Y2hlY2tlZCB+IHVsXG57XG4gIHRyYW5zZm9ybTogc2NhbGUoMS4wLCAxLjApO1xuICBvcGFjaXR5OiAxO1xufVxuLyogRW5kIG9mIEhhbWJ1cmdlciBNZW51IGNzcyAqL1xuXG5cblxuLypcblx0TWF4IHdpZHRoIGJlZm9yZSB0aGlzIFBBUlRJQ1VMQVIgdGFibGUgZ2V0cyBuYXN0eS4gVGhpcyBxdWVyeSB3aWxsIHRha2UgZWZmZWN0IGZvciBhbnkgc2NyZWVuIHNtYWxsZXIgdGhhbiA3NjBweCBhbmQgYWxzbyBpUGFkcyBzcGVjaWZpY2FsbHkuXG5cdCovXG5cdEBtZWRpYVxuXHQgIG9ubHkgc2NyZWVuIFxuICAgIGFuZCAobWF4LXdpZHRoOiA3NjBweCksIChtaW4tZGV2aWNlLXdpZHRoOiA3NjhweCkgXG4gICAgYW5kIChtYXgtZGV2aWNlLXdpZHRoOiAxMDI0cHgpICB7XG5cblxuXHRcdC8qIEZvcmNlIHRhYmxlIHRvIG5vdCBiZSBsaWtlIHRhYmxlcyBhbnltb3JlICovXG5cdFx0dGFibGUsIHRoZWFkLCB0Ym9keSwgdGgsIHRkLCB0ciB7XG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgIFxuICAgICAgICB9XG5cblxuXHRcdC8qIEhpZGUgdGFibGUgaGVhZGVycyAoYnV0IG5vdCBkaXNwbGF5OiBub25lOywgZm9yIGFjY2Vzc2liaWxpdHkpICovXG5cdFx0dGhlYWQgdHIge1xuXHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xuXHRcdFx0dG9wOiAtOTk5OXB4O1xuXHRcdFx0bGVmdDogLTk5OTlweDtcblx0XHR9XG5cbiAgICB0ciB7XG4gICAgICBtYXJnaW46IDAgMCAxcmVtIDA7XG4gICAgfVxuICAgICAgXG4gICAgdHI6bnRoLWNoaWxkKG9kZCkge1xuICAgICAgYmFja2dyb3VuZDogI2NjYztcbiAgICB9XG4gICAgXG5cdFx0dGQge1xuXHRcdFx0LyogQmVoYXZlICBsaWtlIGEgXCJyb3dcIiAqL1xuXHRcdFx0Ym9yZGVyOiBub25lO1xuXHRcdFx0Ym9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlZWU7XG5cdFx0XHRwb3NpdGlvbjogcmVsYXRpdmU7XG5cdFx0XHRwYWRkaW5nLWxlZnQ6IDUwJTtcblx0XHR9XG5cblx0XHR0ZDpiZWZvcmUge1xuXHRcdFx0LyogTm93IGxpa2UgYSB0YWJsZSBoZWFkZXIgKi9cblx0XHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcblx0XHRcdC8qIFRvcC9sZWZ0IHZhbHVlcyBtaW1pYyBwYWRkaW5nICovXG5cdFx0XHR0b3A6IDA7XG5cdFx0XHRsZWZ0OiA2cHg7XG5cdFx0XHR3aWR0aDogNDUlO1xuXHRcdFx0cGFkZGluZy1yaWdodDogMTBweDtcblx0XHRcdHdoaXRlLXNwYWNlOiBub3dyYXA7XG5cdFx0fVxuXG5cdFx0Lypcblx0XHRMYWJlbCB0aGUgZGF0YVxuICAgIFlvdSBjb3VsZCBhbHNvIHVzZSBhIGRhdGEtKiBhdHRyaWJ1dGUgYW5kIGNvbnRlbnQgZm9yIHRoaXMuIFRoYXQgd2F5IFwiYmxvYXRzXCIgdGhlIEhUTUwsIHRoaXMgd2F5IG1lYW5zIHlvdSBuZWVkIHRvIGtlZXAgSFRNTCBhbmQgQ1NTIGluIHN5bmMuIExlYSBWZXJvdSBoYXMgYSBjbGV2ZXIgd2F5IHRvIGhhbmRsZSB3aXRoIHRleHQtc2hhZG93LlxuXHRcdCovXG5cdFx0dGQ6bnRoLW9mLXR5cGUoMSk6YmVmb3JlIHsgY29udGVudDogXCJGaXJzdCBOYW1lXCI7IH1cblx0XHR0ZDpudGgtb2YtdHlwZSgyKTpiZWZvcmUgeyBjb250ZW50OiBcIkxhc3QgTmFtZVwiOyB9XG5cdFx0dGQ6bnRoLW9mLXR5cGUoMyk6YmVmb3JlIHsgY29udGVudDogXCJKb2IgVGl0bGVcIjsgfVxuXHRcdHRkOm50aC1vZi10eXBlKDQpOmJlZm9yZSB7IGNvbnRlbnQ6IFwiRmF2b3JpdGUgQ29sb3JcIjsgfVxuXHRcdHRkOm50aC1vZi10eXBlKDUpOmJlZm9yZSB7IGNvbnRlbnQ6IFwiV2FycyBvZiBUcmVrP1wiOyB9XG5cdFx0dGQ6bnRoLW9mLXR5cGUoNik6YmVmb3JlIHsgY29udGVudDogXCJTZWNyZXQgQWxpYXNcIjsgfVxuXHRcdHRkOm50aC1vZi10eXBlKDcpOmJlZm9yZSB7IGNvbnRlbnQ6IFwiRGF0ZSBvZiBCaXJ0aFwiOyB9XG5cdFx0dGQ6bnRoLW9mLXR5cGUoOCk6YmVmb3JlIHsgY29udGVudDogXCJEcmVhbSBWYWNhdGlvbiBDaXR5XCI7IH1cblx0XHR0ZDpudGgtb2YtdHlwZSg5KTpiZWZvcmUgeyBjb250ZW50OiBcIkdQQVwiOyB9XG5cdFx0dGQ6bnRoLW9mLXR5cGUoMTApOmJlZm9yZSB7IGNvbnRlbnQ6IFwiQXJiaXRyYXJ5IERhdGFcIjsgfVxuXHR9Il19 */"

/***/ }),

/***/ "./src/app/updated-sub-dep/updated-sub-dep.component.html":
/*!****************************************************************!*\
  !*** ./src/app/updated-sub-dep/updated-sub-dep.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n\n  <!-- Hamburger Menu -->\n  <nav role='navigation'>\n    <div id=\"menuToggle\">\n\n      <input type=\"checkbox\" />\n\n      <span></span>\n      <span></span>\n      <span></span>\n      <ul id=\"menu\">\n        <a href=\"#\">\n          <li>Add Sub Department</li>\n        </a>\n        <a href=\"#\">\n          <li>Sub Department List</li>\n        </a>\n        <a routerLink=\"/logout\">\n          <li>Logout</li>\n        </a>\n      </ul>\n    </div>\n  </nav>\n  <!-- End of Hamburger Menu -->\n\n  <div class=\"d-flex justify-content-center h-100\">\n    <div class=\"card\">\n\n      <table role=\"table\">\n\n\n\n        <thead role=\"rowgroup\">\n          <tr>\n            <th colspan=\"9\" style=\"background-color:#5E1D55;color:white;\">\n              <h3>Sub Department List</h3>\n            </th>\n          </tr>\n\n          <tr role=\"row\">\n            <th role=\"columnheader\">Department</th>\n            <th role=\"columnheader\">Sub Department</th>\n            <th role=\"columnheader\">Password</th>\n            <th role=\"columnheader\">Remarks</th>\n            <td role=\"cell\">Edit</td>\n\n          </tr>\n        </thead>\n        <tbody role=\"rowgroup\">\n          <tr role=\"row\" *ngFor=\"let data of datas\">\n            <td role=\"cell\">{{data.dep}}</td>\n            <td role=\"cell\">{{data.sub_dep}}</td>\n            <td role=\"cell\">{{data.password}}</td>\n            <td role=\"cell\">{{data.remarks}}</td>\n            <td role=\"cell\"><i class=\"far fa-edit \" (click)=\"onEditSubDep()\"></i></td>\n\n          </tr>\n          <!-- <tr role=\"row\">\n                        <td role=\"cell\">The</td>\n                        <td role=\"cell\">Tick</td>\n                        <td role=\"cell\">Crimefighter Sorta</td>\n                        <td role=\"cell\">Blue</td>\n                        <td role=\"cell\"><i class=\"far fa-edit\"></i></td>\n\n                    </tr>\n                    <tr role=\"row\">\n                        <td role=\"cell\">Jokey</td>\n                        <td role=\"cell\">Smurf</td>\n                        <td role=\"cell\">Giving Exploding Presents</td>\n                        <td role=\"cell\">Smurflow</td>\n                        <td role=\"cell\"><i class=\"far fa-edit\"></i></td>\n\n                    </tr>\n                    <tr role=\"row\">\n                        <td role=\"cell\">Cindy</td>\n                        <td role=\"cell\">Beyler</td>\n                        <td role=\"cell\">Sales Representative</td>\n                        <td role=\"cell\">Red</td>\n                        <td role=\"cell\"><i class=\"far fa-edit\"></i></td>\n\n                    </tr>\n                    <tr role=\"row\">\n                        <td role=\"cell\">Captain</td>\n                        <td role=\"cell\">Cool</td>\n                        <td role=\"cell\">Tree Crusher</td>\n                        <td role=\"cell\">Blue</td>\n                        <td role=\"cell\"><i class=\"far fa-edit\"></i></td>\n\n                    </tr>\n                    <tr role=\"row\">\n                        <td role=\"cell\">Captain</td>\n                        <td role=\"cell\">Cool</td>\n                        <td role=\"cell\">Tree Crusher</td>\n                        <td role=\"cell\">Blue</td>\n                        <td role=\"cell\"><i class=\"far fa-edit\"></i></td>\n\n                    </tr>\n                    <tr role=\"row\">\n                        <td role=\"cell\">Captain</td>\n                        <td role=\"cell\">Cool</td>\n                        <td role=\"cell\">Tree Crusher</td>\n                        <td role=\"cell\">Blue</td>\n                        <td role=\"cell\"><i class=\"far fa-edit\"></i></td>\n\n                    </tr>\n                    <tr role=\"row\">\n                        <td role=\"cell\">Captain</td>\n                        <td role=\"cell\">Cool</td>\n                        <td role=\"cell\">Tree Crusher</td>\n                        <td role=\"cell\">Blue</td>\n                        <td role=\"cell\"><i class=\"far fa-edit\"></i></td>\n\n                    </tr>\n                    <tr role=\"row\">\n                        <td role=\"cell\">Captain</td>\n                        <td role=\"cell\">Cool</td>\n                        <td role=\"cell\">Tree Crusher</td>\n                        <td role=\"cell\">Blue</td>\n                        <td role=\"cell\"><i class=\"far fa-edit\"></i></td>\n\n\n                    </tr> -->\n          <tr>\n            <th colspan=\"9\" style=\"background-color:#5E1D55;color:white;\">\n\n              <p style=\"float:left\">**** Updated Successfully</p>\n\n              <ul class=\"pagination\" style=\"float:right\">\n                <li class=\"page-item\"><a class=\"page-link\" href=\"#\">Previous</a></li>\n                <li class=\"page-item\"><a class=\"page-link\" href=\"#\">1</a></li>\n                <li class=\"page-item\"><a class=\"page-link\" href=\"#\">2</a></li>\n                <li class=\"page-item\"><a class=\"page-link\" href=\"#\">3</a></li>\n                <li class=\"page-item\"><a class=\"page-link\" href=\"#\">4</a></li>\n                <li class=\"page-item\"><a class=\"page-link\" href=\"#\">5</a></li>\n                <li class=\"page-item\"><a class=\"page-link\" href=\"#\">Next</a></li>\n              </ul>\n            </th>\n\n          </tr>\n\n        </tbody>\n      </table>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/updated-sub-dep/updated-sub-dep.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/updated-sub-dep/updated-sub-dep.component.ts ***!
  \**************************************************************/
/*! exports provided: UpdatedSubDepComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdatedSubDepComponent", function() { return UpdatedSubDepComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UpdatedSubDepComponent = /** @class */ (function () {
    function UpdatedSubDepComponent() {
        this.datas = [
            {
                dep: 'Inward',
                sub_dep: 'Inawrd 1',
                password: '********',
                remarks: 'Remarks1, Remarks2',
            },
            {
                dep: 'Inward',
                sub_dep: 'Inawrd 2',
                password: '********',
                remarks: 'Remarks1, Remarks2',
            },
            {
                dep: 'Inward',
                sub_dep: 'Inawrd 3',
                password: '********',
                remarks: 'Remarks1, Remarks2',
            },
            {
                dep: 'Inward',
                sub_dep: 'Inawrd 4',
                password: '********',
                remarks: 'Remarks1, Remarks2',
            },
            {
                dep: 'Inward',
                sub_dep: 'Inawrd 5',
                password: '********',
                remarks: 'Remarks1, Remarks2',
            },
            {
                dep: 'Inward',
                sub_dep: 'Inawrd 6',
                password: '********',
                remarks: 'Remarks1, Remarks2',
            },
            {
                dep: 'Inward',
                sub_dep: 'Inawrd 7',
                password: '********',
                remarks: 'Remarks1, Remarks2',
            },
            {
                dep: 'Inward',
                sub_dep: 'Inawrd 8',
                password: '********',
                remarks: 'Remarks1, Remarks2',
            }
        ];
    }
    UpdatedSubDepComponent.prototype.ngOnInit = function () {
    };
    UpdatedSubDepComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-updated-sub-dep',
            template: __webpack_require__(/*! ./updated-sub-dep.component.html */ "./src/app/updated-sub-dep/updated-sub-dep.component.html"),
            styles: [__webpack_require__(/*! ./updated-sub-dep.component.css */ "./src/app/updated-sub-dep/updated-sub-dep.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], UpdatedSubDepComponent);
    return UpdatedSubDepComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_4__);





if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\kalpana\Downloads\February\21st of feb\importv1.0\src\main.ts */"./src/main.ts");


/***/ }),

/***/ 1:
/*!********************!*\
  !*** fs (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 2:
/*!************************!*\
  !*** crypto (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 3:
/*!************************!*\
  !*** stream (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map