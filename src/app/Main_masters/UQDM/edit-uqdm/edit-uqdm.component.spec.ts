import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditUqdmComponent } from './edit-uqdm.component';

describe('EditUqdmComponent', () => {
  let component: EditUqdmComponent;
  let fixture: ComponentFixture<EditUqdmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditUqdmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditUqdmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
