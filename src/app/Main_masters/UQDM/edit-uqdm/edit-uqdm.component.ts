import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import { UQDMService } from 'src/app/Services/uqdm.service';
import { HeaderTitleService } from 'src/app/Services/header-title.service';
@Component({
  selector: 'app-edit-uqdm',
  templateUrl: './edit-uqdm.component.html',
  styleUrls: ['./edit-uqdm.component.css']
})
export class EditUqdmComponent implements OnInit {
  sign: any;
  test: any[];
  datatobeedited: any;
  update: boolean = false;
  chname: string;
  chage: string;
  constructor(
    private UqdmService: UQDMService,
    private _http: Http,
    private headerTitleService: HeaderTitleService) { }

  ngOnInit() {
    this.UqdmService.getData().subscribe((DpmData) => this.test = DpmData);
    this.headerTitleService.setTitle('Unwash Quality Defect Master');

  }

  editData(i: any) {

    this.update = true;
    this.datatobeedited = i;
    console.log("obj is" + this.datatobeedited.DpmName);
  }
  saveUpdate(datasaved: any) {

    console.log("datatobesaved" + datasaved.DpmName);
    console.log("datatobesaved" + '' + datasaved.DpmName);

    let _url = "http://localhost:3000/api/add-unwash" + "/" + datasaved._id;

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.put(_url, datasaved, options)
      .subscribe(data => {
        alert('saved Successfully');
      }, error => {
        console.log(JSON.stringify(error.json()));
      })
  }
}
