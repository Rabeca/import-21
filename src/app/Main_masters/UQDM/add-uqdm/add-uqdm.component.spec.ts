import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUqdmComponent } from './add-uqdm.component';

describe('AddUqdmComponent', () => {
  let component: AddUqdmComponent;
  let fixture: ComponentFixture<AddUqdmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUqdmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUqdmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
