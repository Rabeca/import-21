import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDmsmComponent } from './add-dmsm.component';

describe('AddDmsmComponent', () => {
  let component: AddDmsmComponent;
  let fixture: ComponentFixture<AddDmsmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDmsmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDmsmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
