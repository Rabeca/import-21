import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HeaderTitleService } from 'src/app/Services/header-title.service';
@Component({
  selector: 'app-add-dmsm',
  templateUrl: './add-dmsm.component.html',
  styleUrls: ['./add-dmsm.component.css']
})
export class AddDmsmComponent implements OnInit {
  newSupplierName: string;
  address: String;
  Pincode: number;
  Cperson: String;
  mobile: number;
  landline: String;
  Email: String;
  tin: String;
  remarks: String;

  constructor(
    private router: Router,
    private _http: Http,
    private headerTitleService: HeaderTitleService) { }

  ngOnInit() {
    this.headerTitleService.setTitle('Dry Material Supplier Master');
  }


  onSubmit(a) {
    this.newSupplierName = a.newSupplierName;
    this.Cperson = a.Cperson;
    this.address = a.address;
    this.Pincode = a.Pincode;
    this.mobile = a.mobile;
    this.landline = a.altermobile;
    this.Email = a.Email;
    this.tin = a.tin;
    this.remarks = a.remarks;

    console.log(a);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.post('api/add-dry-material', a, options)
      .subscribe(data => {
        alert('Added Successfully');
        this.router.navigate(['/main-masters']);

      }, error => {
        console.log(JSON.stringify(error.json()));
      })
  }
}
