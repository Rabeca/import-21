import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportDmsmComponent } from './import-dmsm.component';

describe('ImportDmsmComponent', () => {
  let component: ImportDmsmComponent;
  let fixture: ComponentFixture<ImportDmsmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportDmsmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportDmsmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
