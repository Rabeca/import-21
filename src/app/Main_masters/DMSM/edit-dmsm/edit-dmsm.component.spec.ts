import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDmsmComponent } from './edit-dmsm.component';

describe('EditDmsmComponent', () => {
  let component: EditDmsmComponent;
  let fixture: ComponentFixture<EditDmsmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDmsmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDmsmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
