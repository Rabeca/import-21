import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import { DMSMService } from 'src/app/Services/dmsm.service';
import { HeaderTitleService } from 'src/app/Services/header-title.service';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
@Component({
  selector: 'app-edit-dmsm',
  templateUrl: './edit-dmsm.component.html',
  styleUrls: ['./edit-dmsm.component.css']
})
export class EditDmsmComponent implements OnInit {

  sign: any;
  test: any[];
  datatobeedited: any;
  update: boolean = false;
  chname: string;
  chage: string;

  test1 = [{}];


  dataedit: any;

  constructor(
    private DmsmService: DMSMService,
    private _http: Http,
    private router: Router,
    private headerTitleService: HeaderTitleService) { }

  ngOnInit() {
    this.DmsmService.getData().subscribe((DpmData) => this.test = DpmData);
    this.headerTitleService.setTitle('Dry Material Supplier Master');
  }

  editData(i: any) {
    this.update = true;
    this.dataedit = i;
    console.log("obj is" + this.dataedit.newSupplierName);
  }
  saveUpdate(datasave: any) {

    console.log("datasaved" + datasave.newSupplierName);
    console.log("datasaved" + '' + datasave.newSupplierName + '' +
      '' + datasave.address + '' +
      '' + datasave.Pincode + '' +
      '' + datasave.Cperson + '' +
      '' + datasave.mobile);


    let url = "http://localhost:3000/api/add-dry-material" + "/" + datasave._id;

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.put(url, datasave, options)
      .subscribe(data => {
        alert('saved Successfully');
      }, error => {
        console.log(JSON.stringify(error.json()));
      })
  }

  exportExcel() {

    var head = [
      'newSupplierName',
      'Cperson',
      'address',
      'Pincode',
      'mobile',
      'landline',
      'Email',
      'tin',
      'remarks'
    ];
    new Angular2Csv(this.test1, 'Dry Process Material Supplier Master', { headers: (head) });
    alert("Please Give the Input Details");
    this.router.navigate(['/import-dry-material']);

  }

}
