import { Component, OnInit } from '@angular/core';
import { WsmService } from '../../../Services/wsm.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import { HeaderTitleService } from 'src/app/Services/header-title.service';



@Component({
  selector: 'app-wsm-edit-list',
  templateUrl: './wsm-edit-list.component.html',
  styleUrls: ['./wsm-edit-list.component.css']
})
export class WsmEditListComponent implements OnInit {
  sign: any;
  test: any[];
  datatobeedited: any;
  update: boolean = false;
  chname: string;
  chage: string;
  constructor(
    private WsmService: WsmService,
    private _http: Http,
    private headerTitleService: HeaderTitleService) { }


  ngOnInit() {
    this.WsmService.getData().subscribe((testData) => this.test = testData);
    this.headerTitleService.setTitle('Wash Step Master');

  }

  editData(i: any) {

    this.update = true;
    this.datatobeedited = i;
    console.log("obj is" + this.datatobeedited.AWS);
  }
  saveUpdate(datasaved: any) {

    console.log("datatobesaved" + datasaved.AWS);
    console.log("datatobesaved" + '' + datasaved.AWS);

    let _url = "http://localhost:3000/api/wash-step-master-list-edit" + "/" + datasaved._id;

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.put(_url, datasaved, options)
      .subscribe(data => {
        alert('saved Successfully');
      }, error => {
        console.log(JSON.stringify(error.json()));
      })
  }
}



