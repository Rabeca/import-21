import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WsmEditListComponent } from './wsm-edit-list.component';

describe('WsmEditListComponent', () => {
  let component: WsmEditListComponent;
  let fixture: ComponentFixture<WsmEditListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WsmEditListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WsmEditListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
