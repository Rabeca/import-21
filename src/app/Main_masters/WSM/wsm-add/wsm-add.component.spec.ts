import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WsmAddComponent } from './wsm-add.component';

describe('WsmAddComponent', () => {
  let component: WsmAddComponent;
  let fixture: ComponentFixture<WsmAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WsmAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WsmAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
