import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HeaderTitleService } from 'src/app/Services/header-title.service';

@Component({
  selector: 'app-wsm-add',
  templateUrl: './wsm-add.component.html',
  styleUrls: ['./wsm-add.component.css']
})
export class WsmAddComponent implements OnInit {
  AWS: string;

  constructor(
    private router: Router,
    private _http: Http,
    private headerTitleService: HeaderTitleService) { }

  ngOnInit() {
    this.headerTitleService.setTitle('Wash Step Master');


  }
  onSubmit(a) {

    this.AWS = a.AWS,

      console.log(a);

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.post('/api/wash-step-master-list-edit', a, options)
      .subscribe(data => {
        alert('added Successfully');
        this.router.navigate(['/main-masters']);

      }, error => {
        console.log(JSON.stringify(error.json()));
      })

  }
}
