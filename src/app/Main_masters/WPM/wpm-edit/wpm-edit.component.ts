import { Component, OnInit } from '@angular/core';
import { WpmService } from '../../../Services/wpm.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HeaderTitleService } from 'src/app/Services/header-title.service';


@Component({
  selector: 'app-wpm-edit',
  templateUrl: './wpm-edit.component.html',
  styleUrls: ['./wpm-edit.component.css']
})

export class WpmEditComponent implements OnInit {
  sign: any;
  test: any[];
  datatobeedited: any;
  update: boolean = false;
  chname: string;
  chage: string;
  constructor(
    private WpmService: WpmService,
    private _http: Http,
    private headerTitleService: HeaderTitleService) { }

  ngOnInit() {
    this.WpmService.getData().subscribe((testData) => this.test = testData);
    this.headerTitleService.setTitle('Wash Process Master');

  }

  editData(i: any) {

    this.update = true;
    this.datatobeedited = i;
    console.log("obj is" + this.datatobeedited.subDepSelect);
  }
  saveUpdate(datasaved: any) {

    console.log("datatobesaved" + datasaved.subDepSelect);
    console.log("datatobesaved" + '' + datasaved.subDepPassword + '' + '' + datasaved.subDepRemark);

    let _url = "http://localhost:3000/api/assign-wash-process" + "/" + datasaved._id;

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.put(_url, datasaved, options)
      .subscribe(data => {
        alert('saved Successfully');
      }, error => {
        console.log(JSON.stringify(error.json()));
      })
  }
}



