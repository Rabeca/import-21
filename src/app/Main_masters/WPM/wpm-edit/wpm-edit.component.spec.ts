import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WpmEditComponent } from './wpm-edit.component';

describe('WpmEditComponent', () => {
  let component: WpmEditComponent;
  let fixture: ComponentFixture<WpmEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WpmEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WpmEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
