import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HeaderTitleService } from 'src/app/Services/header-title.service';


@Component({
  selector: 'app-wpm-assign',
  templateUrl: './wpm-assign.component.html',
  styleUrls: ['./wpm-assign.component.css']
})
export class WpmAssignComponent implements OnInit {
  seldep: string;
  subDep1: string;

  constructor(
    private router: Router,
    private _http: Http,
    private headerTitleService: HeaderTitleService) { }

  ngOnInit() {
    this.headerTitleService.setTitle('Wash Process Master');

  }
  onSubmit(a) {

    this.seldep = a.seldep,
      this.subDep1 = a.subDep1,


      //console.log(a.seldep + "   " + a.subDep1);
      console.log(a);

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.post('/api/assign-wash-process', a, options)
      .subscribe(data => {
        alert('assigned Successfully');
        this.router.navigate(['/main-masters']);

      }, error => {
        console.log(JSON.stringify(error.json()));
      })

  }
}
