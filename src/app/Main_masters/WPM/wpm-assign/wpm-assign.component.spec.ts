import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WpmAssignComponent } from './wpm-assign.component';

describe('WpmAssignComponent', () => {
  let component: WpmAssignComponent;
  let fixture: ComponentFixture<WpmAssignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WpmAssignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WpmAssignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
