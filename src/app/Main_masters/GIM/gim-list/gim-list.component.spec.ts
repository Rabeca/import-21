import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GimListComponent } from './gim-list.component';

describe('GimListComponent', () => {
  let component: GimListComponent;
  let fixture: ComponentFixture<GimListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GimListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GimListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
