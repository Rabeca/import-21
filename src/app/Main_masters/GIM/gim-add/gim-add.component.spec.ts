import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GimAddComponent } from './gim-add.component';

describe('GimAddComponent', () => {
  let component: GimAddComponent;
  let fixture: ComponentFixture<GimAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GimAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GimAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
