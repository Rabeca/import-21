import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HeaderTitleService } from 'src/app/Services/header-title.service';

@Component({
  selector: 'app-gim-add',
  templateUrl: './gim-add.component.html',
  styleUrls: ['./gim-add.component.css']
})
export class GimAddComponent implements OnInit {

  AddGarment: string;

  constructor(
    private router: Router,
    private _http: Http,
    private headerTitleService: HeaderTitleService) { }

  ngOnInit() {
    this.headerTitleService.setTitle('Garment Item Master');
  }
  onSubmit(a) {

    this.AddGarment = a.AddGarment,

      console.log(a);

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.post('/api/add-garment-item', a, options)
      .subscribe(data => {
        alert('added Successfully');
        this.router.navigate(['/main-masters']);
      }, error => {
        console.log(JSON.stringify(error.json()));
      })

  }
}