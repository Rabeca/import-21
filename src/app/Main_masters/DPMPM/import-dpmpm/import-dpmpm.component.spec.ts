import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportDpmpmComponent } from './import-dpmpm.component';

describe('ImportDpmpmComponent', () => {
  let component: ImportDpmpmComponent;
  let fixture: ComponentFixture<ImportDpmpmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportDpmpmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportDpmpmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
