import { Component, OnInit } from '@angular/core';
import * as Papa from 'papaparse';
import { HeaderTitleService } from 'src/app/Services/header-title.service';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

@Component({
  selector: 'app-import-dpmpm',
  templateUrl: './import-dpmpm.component.html',
  styleUrls: ['./import-dpmpm.component.css']
})
export class ImportDpmpmComponent implements OnInit {
  dataList: any[];
  files: any[];
  data: any[];
  fileName: string;

  constructor(private headerTitleService: HeaderTitleService,
    private http: Http,
    private router: Router) { }

  ngOnInit() {
    this.headerTitleService.setTitle('Chemical Product Master');
  }
  onChange(files: File[]) {

    if (files[0]) {
      console.log(files[0]);
      Papa.parse(files[0], {
        header: true,
        skipEmptyLines: true,
        complete: (result, file) => {
          console.log(result);

          this.dataList = result.data;
          this.fileName = file.name;
          console.log(this.fileName);
          console.log(this.dataList);

          var headers = new Headers();
          headers.append('Content-Type', 'application/json');
          let options = new RequestOptions({ headers: headers });

          this.http.post('api/dpmpmFileUpload', this.dataList, options)
            .subscribe(data => {
              console.log("added successfully");


            }, error => {
              console.log(JSON.stringify(error.json()));
            })



        }


      });
    }
  }

  onUpload() {

    this.router.navigate(['/edit-Dry-Process-Material-Product-Master']);
  }

}
