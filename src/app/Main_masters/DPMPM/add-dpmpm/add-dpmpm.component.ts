import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HeaderTitleService } from 'src/app/Services/header-title.service';
import { DMSMService } from 'src/app/Services/dmsm.service';

@Component({
  selector: 'app-add-dpmpm',
  templateUrl: './add-dpmpm.component.html',
  styleUrls: ['./add-dpmpm.component.css']
})
export class AddDpmpmComponent implements OnInit {

  data: any[];

  dryproductname: string;
  dryDistributorName: string;

  dryConsumption: String;
  dryQuantity: String;
  dryReorderLevel: String;
  dryRemark: String;
  constructor(
    private router: Router,
    private _http: Http,
    private dmsmService: DMSMService,
    private headerTitleService: HeaderTitleService
  ) {

  }

  ngOnInit() {
    this.headerTitleService.setTitle('Dry Process Material Product Master');
    this.dmsmService.getData().subscribe((DmsmData) => this.data = DmsmData);
  }

  onSubmit(a) {
    this.dryproductname = a.dryproductname;
    this.dryDistributorName = a.dryDistributorName;

    this.dryConsumption = a.dryConsumption;
    this.dryQuantity = a.dryQuantity;
    this.dryReorderLevel = a.dryReorderLevel;
    this.dryRemark = a.dryRemark;
    console.log(a);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.post('/api/add-Dry-Process-Material-Product-Master', a, options)
      .subscribe(data => {
        alert('Registered Successfully');
        this.router.navigate(['/main-masters']);

      }, error => {
        console.log(JSON.stringify(error.json()));
      })
  }
}
