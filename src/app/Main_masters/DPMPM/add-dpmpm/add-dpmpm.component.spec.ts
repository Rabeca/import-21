import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDpmpmComponent } from './add-dpmpm.component';

describe('AddDpmpmComponent', () => {
  let component: AddDpmpmComponent;
  let fixture: ComponentFixture<AddDpmpmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDpmpmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDpmpmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
