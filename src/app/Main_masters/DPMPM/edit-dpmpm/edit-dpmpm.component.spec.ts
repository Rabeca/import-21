import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDpmpmComponent } from './edit-dpmpm.component';

describe('EditDpmpmComponent', () => {
  let component: EditDpmpmComponent;
  let fixture: ComponentFixture<EditDpmpmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDpmpmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDpmpmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
