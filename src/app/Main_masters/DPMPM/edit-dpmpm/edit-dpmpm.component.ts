import { Component, OnInit } from '@angular/core';

import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HeaderTitleService } from 'src/app/Services/header-title.service';
import { DpmpmService } from 'src/app/Services/dpmpm.service';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-dpmpm',
  templateUrl: './edit-dpmpm.component.html',
  styleUrls: ['./edit-dpmpm.component.css']
})
export class EditDpmpmComponent implements OnInit {
  sign: any;
  test1;
  datatobeedited: any;
  update: boolean = false;
  chname: string;
  chage: string;
  test = [{}];
  constructor(
    private dpmpmService: DpmpmService,
    private _http: Http,
    private router: Router,
    private headerTitleService: HeaderTitleService) { }
  ngOnInit() {
    this.dpmpmService.getData().subscribe((DpmpmData) => this.test1 = DpmpmData);
    this.headerTitleService.setTitle('Dry Process Material Product Master');
  }

  editData(i: any) {

    this.update = true;
    this.datatobeedited = i;
    console.log("obj is" + this.datatobeedited.dryproductname);
  }
  saveUpdate(datasaved: any) {

    console.log("datatobesaved" + datasaved.dryproductname);
    console.log("datatobesaved" + '' + datasaved.dryproductname +

      '' + '' + datasaved.dryConsumption +
      '' + '' + datasaved.dryQuantity +
      '' + '' + datasaved.dryReorderLevel +
      '' + '' + datasaved.dryRemark);

    let _url = "http://localhost:3000/api/add-Dry-Process-Material-Product-Master" + "/" + datasaved._id;

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.put(_url, datasaved, options)
      .subscribe(data => {
        alert('saved Successfully');
      }, error => {
        console.log(JSON.stringify(error.json()));
      })
  }

  exportExcel() {

    var head = [
      'dryproductname',
      'dryDistributorName',
      'dryConsumption',
      'dryQuantity',
      'dryReorderLevel',
      'dryRemark',

    ];
    new Angular2Csv(this.test, 'Dry Process Material Product Master', { headers: (head) });
    alert("Please Give the Input Details");
    this.router.navigate(['/import-Dry-Process-Material-Product-Master']);

  }

}
