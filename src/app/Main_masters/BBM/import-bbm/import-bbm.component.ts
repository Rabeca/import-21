import { Component, OnInit } from '@angular/core';
import { HeaderTitleService } from 'src/app/Services/header-title.service';
import * as Papa from 'papaparse';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';


@Component({
  selector: 'app-import-bbm',
  templateUrl: './import-bbm.component.html',
  styleUrls: ['./import-bbm.component.css']
})
export class ImportBbmComponent implements OnInit {
  dataList: any[];
  files: any[];
  data: any[];
  test = [
    {

    }
  ];
  fileName: String;
  constructor(private headerTitleService: HeaderTitleService,
    private http: Http,
    private router: Router) { }

  ngOnInit() {
    this.headerTitleService.setTitle('Buyer/Brand');
  }
  onChange(files: File[]) {

    if (files[0]) {
      console.log(files[0]);
      Papa.parse(files[0], {
        header: true,
        skipEmptyLines: true,
        complete: (result, file) => {
          console.log(result);
          this.dataList = result.data;
          this.fileName = file.name;

          console.log(this.dataList);

          var headers = new Headers();
          headers.append('Content-Type', 'application/json');
          let options = new RequestOptions({ headers: headers });

          this.http.post('api/fileUpload', this.dataList, options)
            .subscribe(data => {
              console.log("added successfully");
            }, error => {
              console.log(JSON.stringify(error.json()));
            })
        }
      });
    }
  }

  // Navigating Table
  onUpload() {
    this.router.navigate(['/import-table']);
  }
  exportExcel() {

    var head = ['newBuyerBrand', 'address', 'Pincode', 'Cperson', 'mobile', 'altermobile', 'Email', 'website', 'remarks']
    new Angular2Csv(this.test, 'Buyer Master', { headers: (head) });
    alert("Please Give the Input Details");
    this.router.navigate(['/buyer-brand-import']);

  }

}
