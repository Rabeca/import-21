import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportBbmComponent } from './import-bbm.component';

describe('ImportBbmComponent', () => {
  let component: ImportBbmComponent;
  let fixture: ComponentFixture<ImportBbmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportBbmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportBbmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
