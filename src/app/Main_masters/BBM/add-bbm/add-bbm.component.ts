import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HeaderTitleService } from 'src/app/Services/header-title.service';

@Component({
  selector: 'app-add-bbm',
  templateUrl: './add-bbm.component.html',
  styleUrls: ['./add-bbm.component.css']
})
export class AddBbmComponent implements OnInit {
  newBuyerBrand: string;
  address: String;
  Pincode: number;
  Cperson: String;
  mobile: number;
  altermobile: String;
  Email: String;
  website: String;
  remarks: String;

  constructor(
    private router: Router,
    private _http: Http,
    private headerTitleService: HeaderTitleService
  ) { }

  ngOnInit() {
    this.headerTitleService.setTitle('Buyer/Brand');
  }


  onSubmit(a) {
    this.newBuyerBrand = a.newBuyerBrand;
    this.address = a.address;
    this.Pincode = a.Pincode;
    this.Cperson = a.Cperson;
    this.mobile = a.mobile;
    this.altermobile = a.altermobile;
    this.Email = a.Email;
    this.website = a.website;
    this.remarks = a.remarks;

    console.log(a);
    console.log(typeof (a));
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.post('api/add-buyser-brand', a, options)
      .subscribe(data => {
        alert('Added Successfully');
        this.router.navigate(['/main-masters']);

      }, error => {
        console.log(JSON.stringify(error.json()));
      })
  }
}
