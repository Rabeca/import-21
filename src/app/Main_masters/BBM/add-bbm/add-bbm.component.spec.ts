import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBbmComponent } from './add-bbm.component';

describe('AddBbmComponent', () => {
  let component: AddBbmComponent;
  let fixture: ComponentFixture<AddBbmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBbmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBbmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
