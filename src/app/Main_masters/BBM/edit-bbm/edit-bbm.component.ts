import { Component, OnInit } from '@angular/core';

import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import { HeaderTitleService } from 'src/app/Services/header-title.service';

import { FileUploadService } from 'src/app/Services/file-upload.service';


import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { BbmService } from 'src/app/Services/bbm.service';

@Component({
  selector: 'app-edit-bbm',
  templateUrl: './edit-bbm.component.html',
  styleUrls: ['./edit-bbm.component.css']
})
export class EditBbmComponent implements OnInit {
  dataedit: [];
  test = [
    {

    }
  ];

  data: any[];
  update: boolean = false;
  constructor(

    private _http: Http,
    private BBMSrvice: BbmService,
    private headerTitleService: HeaderTitleService,
    // private fileUploadService: FileUploadService,
    private router: Router,
  ) { }

  ngOnInit() {

    // this.fileUploadService.getData().subscribe((fileData) => this.data = fileData);
    this.BBMSrvice.getData().subscribe((BBMData) => this.data = BBMData);

    this.headerTitleService.setTitle('Buyer/Brand');
  }

  editData(i: any) {
    this.update = true;
    this.dataedit = i;

  }
  saveUpdate(datasave: any) {

    console.log("datasaved" + datasave.newBuyerBrand);
    console.log("datasaved" + '' + datasave.newBuyerBrand + '' +
      '' + datasave.address + '' +
      '' + datasave.Pincode + '' +
      '' + datasave.Cperson + '' +
      '' + datasave.mobile);


    let url = "http://localhost:3000/api/fileUpload" + "/" + datasave._id;

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.put(url, datasave, options)
      .subscribe(data => {
        alert('saved Successfully');
      }, error => {
        console.log(JSON.stringify(error.json()));
      })
  }

  // Export Function Call
  // exportExcel() {

  //   var head = ['newBuyerBrand', 'address', 'Pincode', 'Cperson', 'mobile', 'altermobile', 'Email', 'website', 'remarks']
  //   new Angular2Csv(this.test, 'Buyer Master', { headers: (head) });
  //   alert("Please Give the Input Details");
  //   this.router.navigate(['/buyer-brand-import']);

  // }



}

