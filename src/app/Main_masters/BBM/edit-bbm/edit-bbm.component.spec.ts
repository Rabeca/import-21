import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBbmComponent } from './edit-bbm.component';

describe('EditBbmComponent', () => {
  let component: EditBbmComponent;
  let fixture: ComponentFixture<EditBbmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditBbmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditBbmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
