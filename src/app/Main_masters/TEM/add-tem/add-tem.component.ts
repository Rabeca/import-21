import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HeaderTitleService } from 'src/app/Services/header-title.service';
@Component({
  selector: 'app-add-tem',
  templateUrl: './add-tem.component.html',
  styleUrls: ['./add-tem.component.css']
})
export class AddTemComponent implements OnInit {

  DpmName: string;

  constructor(
    private router: Router,
    private _http: Http,
    private headerTitleService: HeaderTitleService) { }

  ngOnInit() {
    this.headerTitleService.setTitle('Transit Error Master');
  }


  onSubmit(a) {

    this.DpmName = a.DpmName,

      console.log(a);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.post('api/add-transit', a, options)
      .subscribe(data => {
        alert('Added Successfully');
        this.router.navigate(['/main-masters']);

      }, error => {
        console.log(JSON.stringify(error.json()));
      })
  }

}
