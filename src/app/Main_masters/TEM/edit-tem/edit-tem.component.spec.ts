import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTemComponent } from './edit-tem.component';

describe('EditTemComponent', () => {
  let component: EditTemComponent;
  let fixture: ComponentFixture<EditTemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
