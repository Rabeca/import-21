import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportGmmComponent } from './import-gmm.component';

describe('ImportGmmComponent', () => {
  let component: ImportGmmComponent;
  let fixture: ComponentFixture<ImportGmmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportGmmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportGmmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
