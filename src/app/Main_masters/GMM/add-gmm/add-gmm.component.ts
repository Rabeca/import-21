import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HeaderTitleService } from 'src/app/Services/header-title.service';

@Component({
  selector: 'app-add-gmm',
  templateUrl: './add-gmm.component.html',
  styleUrls: ['./add-gmm.component.css']
})
export class AddGmmComponent implements OnInit {
  newGarManf: string;
  address: String;
  Pincode: number;
  Cperson: String;
  mobile: number;
  altermobile: String;
  Email: String;
  website: String;
  remarks: String;

  constructor(
    private router: Router,
    private _http: Http,
    private headerTitleService: HeaderTitleService) { }

  ngOnInit() {
    this.headerTitleService.setTitle('Garment Manufacturer Master');
  }


  onSubmit(a) {
    this.newGarManf = a.newGarManf;
    this.address = a.address;
    this.Pincode = a.Pincode;
    this.Cperson = a.Cperson;
    this.mobile = a.mobile;
    this.altermobile = a.altermobile;
    this.Email = a.Email;
    this.website = a.website;
    this.remarks = a.remarks;

    console.log(a);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.post('api/add-garment-manufacturer-master', a, options)
      .subscribe(data => {
        alert('Added Successfully');
        this.router.navigate(['/main-masters']);

      }, error => {
        console.log(JSON.stringify(error.json()));
      })
  }
}
