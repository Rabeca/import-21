import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddGmmComponent } from './add-gmm.component';

describe('AddGmmComponent', () => {
  let component: AddGmmComponent;
  let fixture: ComponentFixture<AddGmmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddGmmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddGmmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
