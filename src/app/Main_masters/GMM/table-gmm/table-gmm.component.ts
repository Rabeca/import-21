import { Component, OnInit } from '@angular/core';
import { GmmService } from '../../../Services/gmm.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import { HeaderTitleService } from 'src/app/Services/header-title.service';

import { Angular2Csv } from 'angular2-csv/Angular2-csv';

@Component({
  selector: 'app-table-gmm',
  templateUrl: './table-gmm.component.html',
  styleUrls: ['./table-gmm.component.css']
})
export class TableGmmComponent implements OnInit {
  data: any[];
  dataedit: any;
  update: boolean = false;
  test = [{}];
  constructor(
    private GmmService: GmmService,
    private _http: Http,
    private router: Router,
    private headerTitleService: HeaderTitleService) { }

  ngOnInit() {
    this.GmmService.getData().subscribe((GmmData) => this.data = GmmData);
    this.headerTitleService.setTitle('Garment Manufacturer Master');
  }

  editData(i: any) {
    this.update = true;
    this.dataedit = i;
    console.log("obj is" + this.dataedit.newGarManf);
  }
  saveUpdate(datasave: any) {

    console.log("datasaved" + datasave.newGarManf);
    console.log("datasaved" + '' + datasave.newGarManf);

    let url = "http://localhost:3000/api/add-garment-manufacturer-master" + "/" + datasave._id;

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.put(url, datasave, options)
      .subscribe(data => {
        alert('saved Successfully');
      }, error => {
        console.log(JSON.stringify(error.json()));
      })
  }

  exportExcel() {

    var head = [
      'newGarManf',
      'address',
      'Pincode',
      'Cperson',
      'mobile',
      'altermobile',
      'Email',
      'website',
      'remarks'
    ];
    new Angular2Csv(this.test, 'Garment-Manufacturer-Master', { headers: (head) });
    alert("Please Give the Input Details");
    this.router.navigate(['/garment-manufacturer-import']);

  }
}
