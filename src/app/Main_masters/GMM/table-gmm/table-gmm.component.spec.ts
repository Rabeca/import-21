import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableGmmComponent } from './table-gmm.component';

describe('TableGmmComponent', () => {
  let component: TableGmmComponent;
  let fixture: ComponentFixture<TableGmmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableGmmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableGmmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
