import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CamAssignListComponent } from './cam-assign-list.component';

describe('CamAssignListComponent', () => {
  let component: CamAssignListComponent;
  let fixture: ComponentFixture<CamAssignListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CamAssignListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CamAssignListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
