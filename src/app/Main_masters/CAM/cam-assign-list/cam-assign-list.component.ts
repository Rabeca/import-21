import { Component, OnInit } from '@angular/core';
import { CamService } from '../../../Services/cam.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HeaderTitleService } from 'src/app/Services/header-title.service';

@Component({
  selector: 'app-cam-assign-list',
  templateUrl: './cam-assign-list.component.html',
  styleUrls: ['./cam-assign-list.component.css']
})
export class CamAssignListComponent implements OnInit {

  sign: any;
  test: any[];
  datatobeedited: any;
  update: boolean = false;
  chname: string;
  chage: string;
  constructor(
    private CamService: CamService,
    private _http: Http,
    private headerTitleService: HeaderTitleService) { }
  ngOnInit() {
    this.CamService.getData().subscribe((CamData) => this.test = CamData);
    this.headerTitleService.setTitle('chemical Assign Master');
  }

  editData(i: any) {

    this.update = true;
    this.datatobeedited = i;
    console.log("obj is" + this.datatobeedited.choice);
  }
  saveUpdate(datasaved: any) {

    console.log("datatobesaved" + datasaved.choice);
    console.log("datatobesaved" + '' + datasaved.choice + '' + '' + datasaved.choice1 + '' + datasaved.choice2);

    let _url = "http://localhost:3000/api/add-chemical-assign-master" + "/" + datasaved._id;

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.put(_url, datasaved, options)
      .subscribe(data => {
        alert('saved Successfully');
      }, error => {
        console.log(JSON.stringify(error.json()));
      })
  }
}
