import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { CadmService } from 'src/app/Services/cadm.service';
import { CmmService } from 'src/app/Services/cmm.service';
import { HeaderTitleService } from 'src/app/Services/header-title.service';
import { CpmService } from 'src/app/Services/cpm.service';

@Component({
  selector: 'app-cam-add',
  templateUrl: './cam-add.component.html',
  styleUrls: ['./cam-add.component.css']
})
export class CamAddComponent implements OnInit {
  choice: string;
  choice1: string;
  choice2: string;
  dropdown: any[];
  dropdown1: any[];
  dropdown2: any[];

  constructor(private router: Router,
    private _http: Http,
    private Cadmservice: CadmService,
    private CmmService: CmmService,
    private CpmService: CpmService,
    private headerTitleService: HeaderTitleService) { }

  ngOnInit() {
    this.Cadmservice.getData().subscribe((CadmData) => this.dropdown = CadmData);
    this.CmmService.getData().subscribe((CmmData) => this.dropdown1 = CmmData);
    this.CpmService.getData().subscribe((CpmData) => this.dropdown2 = CpmData)

    // header title change
    this.headerTitleService.setTitle('Chemical Assign Master');
  }

  onSubmit(a) {
    this.choice = a.choice;
    this.choice1 = a.choice1;
    this.choice2 = a.choice2;
    console.log(a);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.post('/api/add-chemical-assign-master', a, options)
      .subscribe(data => {
        alert('Registered Successfully');
        this.router.navigate(['/main-masters']);

      }, error => {
        console.log(JSON.stringify(error.json()));
      })
  }
}