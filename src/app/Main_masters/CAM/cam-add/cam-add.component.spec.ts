import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CamAddComponent } from './cam-add.component';

describe('CamAddComponent', () => {
  let component: CamAddComponent;
  let fixture: ComponentFixture<CamAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CamAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CamAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
