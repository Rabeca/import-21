import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HeaderTitleService } from 'src/app/Services/header-title.service';

@Component({
  selector: 'app-add-cmm',
  templateUrl: './add-cmm.component.html',
  styleUrls: ['./add-cmm.component.css']
})
export class AddCmmComponent implements OnInit {
  newchemicalman: string;
  address: String;
  Pincode: number;
  Cperson: String;
  mobile: number;
  landline: String;
  Email: String;
  gstin: String;
  remarks: String;

  constructor(
    private router: Router,
    private _http: Http,
    private headerTitleService: HeaderTitleService
  ) { }

  ngOnInit() {
    this.headerTitleService.setTitle('Chemical Manufacturer Master');
  }


  onSubmit(a) {
    this.newchemicalman = a.newchemicalman;
    this.address = a.address;
    this.Pincode = a.Pincode;
    this.Cperson = a.Cperson;
    this.mobile = a.mobile;
    this.landline = a.altermobile;
    this.Email = a.Email;
    this.gstin = a.gstin;
    this.remarks = a.remarks;

    console.log(a);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.post('api/add-chemical-master', a, options)
      .subscribe(data => {
        alert('Added Successfully');
        this.router.navigate(['/main-masters']);

      }, error => {
        console.log(JSON.stringify(error.json()));
      })
  }
}
