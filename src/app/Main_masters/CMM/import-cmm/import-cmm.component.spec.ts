import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportCmmComponent } from './import-cmm.component';

describe('ImportCmmComponent', () => {
  let component: ImportCmmComponent;
  let fixture: ComponentFixture<ImportCmmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportCmmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportCmmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
