import { Component, OnInit } from '@angular/core';
import { CmmService } from "../../../Services/cmm.service";
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import { HeaderTitleService } from 'src/app/Services/header-title.service';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';

@Component({
  selector: 'app-edit-cmm',
  templateUrl: './edit-cmm.component.html',
  styleUrls: ['./edit-cmm.component.css']
})
export class EditCmmComponent implements OnInit {

  data: any[];
  dataedit: any;
  update: boolean = false;
  test = [{}];
  constructor(
    private CmmService: CmmService,
    private _http: Http,
    private router: Router,
    private headerTitleService: HeaderTitleService) { }

  ngOnInit() {
    this.CmmService.getData().subscribe((CmmData) => this.data = CmmData);
    this.headerTitleService.setTitle('Chemical Manufacturer Master');
  }

  editData(i: any) {
    this.update = true;
    this.dataedit = i;
    console.log("obj is" + this.dataedit.newchemicalman);
  }
  saveUpdate(datasave: any) {

    console.log("datasaved" + datasave.newchemicalman);
    console.log("datasaved" + '' + datasave.newchemicalman + '' +
      '' + datasave.address + '' +
      '' + datasave.Pincode + '' +
      '' + datasave.Cperson + '' +
      '' + datasave.mobile);


    let url = "http://localhost:3000/api/add-chemical-master" + "/" + datasave._id;

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.put(url, datasave, options)
      .subscribe(data => {
        alert('saved Successfully');
      }, error => {
        console.log(JSON.stringify(error.json()));
      })
  }

  exportExcel() {

    var head = [
      'newchemicalman',
      'Cperson',
      'address',
      'Pincode',
      'mobile',
      'landline',
      'Email',
      'gstin',
      'remarks'
    ];
    new Angular2Csv(this.test, 'Chemical Manufacturer Master', { headers: (head) });
    alert("Please Give the Input Details");
    this.router.navigate(['/import-chemical-manufacturer']);

  }
}

