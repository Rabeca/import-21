import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CpmImportComponent } from './cpm-import.component';

describe('CpmImportComponent', () => {
  let component: CpmImportComponent;
  let fixture: ComponentFixture<CpmImportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CpmImportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CpmImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
