import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CpmEditComponent } from './cpm-edit.component';

describe('CpmEditComponent', () => {
  let component: CpmEditComponent;
  let fixture: ComponentFixture<CpmEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CpmEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CpmEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
