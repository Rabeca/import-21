import { Component, OnInit } from '@angular/core';
import { CpmService } from '../../../Services/cpm.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HeaderTitleService } from 'src/app/Services/header-title.service';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cpm-edit',
  templateUrl: './cpm-edit.component.html',
  styleUrls: ['./cpm-edit.component.css']
})
export class CpmEditComponent implements OnInit {
  sign: any;
  test: any[];
  test1 = [{}];
  datatobeedited: any;
  update: boolean = false;
  chname: string;
  chage: string;
  constructor(
    private CpmService: CpmService,
    private _http: Http,
    private router: Router,
    private headerTitleService: HeaderTitleService) { }

  ngOnInit() {
    this.CpmService.getData().subscribe((testData) => this.test = testData);
    this.headerTitleService.setTitle('Chemical Product Master');
  }

  editData(i: any) {

    this.update = true;
    this.datatobeedited = i;
    console.log("obj is" + this.datatobeedited.newproductname);
  }
  saveUpdate(datasaved: any) {

    console.log("datatobesaved" + datasaved.newproductname);
    console.log("datatobesaved" + '' + datasaved.newproductname +
      '' + '' + datasaved.ChemicalForm +
      '' + '' + datasaved.Consumption +
      '' + '' + datasaved.Quantity +
      '' + '' + datasaved.ReorderLevel +
      '' + '' + datasaved.Remark);

    let _url = "http://localhost:3000/api/add-chemical-product" + "/" + datasaved._id;

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.put(_url, datasaved, options)
      .subscribe(data => {
        alert('saved Successfully');
      }, error => {
        console.log(JSON.stringify(error.json()));
      })
  }

  exportExcel() {

    var head = [
      'newproductname',
      'DistributorName',
      'ManufacturerName',
      'Category',
      'Subcategory',
      'ChemicalForm',
      'Consumption',
      'Quantity',
      'ReorderLevel',
      'Remark'
    ];
    new Angular2Csv(this.test1, 'Chemical Product Master', { headers: (head) });
    alert("Please Give the Input Details");
    this.router.navigate(['/import-chemical-product']);

  }
}



