import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CpmAddComponent } from './cpm-add.component';

describe('CpmAddComponent', () => {
  let component: CpmAddComponent;
  let fixture: ComponentFixture<CpmAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CpmAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CpmAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
