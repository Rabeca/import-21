import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { CadmService } from 'src/app/Services/cadm.service';
import { CmmService } from 'src/app/Services/cmm.service';
import { HeaderTitleService } from 'src/app/Services/header-title.service';

@Component({
  selector: 'app-cpm-add',
  templateUrl: './cpm-add.component.html',
  styleUrls: ['./cpm-add.component.css']
})
export class CpmAddComponent implements OnInit {

  data: any[];
  data1: any[];
  newproductname: string;
  DistributorName: string;
  ManufacturerName: string;
  Category: String;
  Subcategory: String;
  ChemicalForm: string;
  Consumption: String;
  Quantity: String;
  ReorderLevel: String;
  Remark: String;
  constructor(
    private router: Router,
    private _http: Http,
    private Cadmservice: CadmService,
    private CmmService: CmmService,
    private headerTitleService: HeaderTitleService
  ) {

  }

  ngOnInit() {

    this.Cadmservice.getData().subscribe((CadmData) => this.data = CadmData);
    this.CmmService.getData().subscribe((CmmData) => this.data1 = CmmData);
    this.headerTitleService.setTitle('Chemical Product Master');




  }

  onSubmit(a) {
    this.newproductname = a.newproductname;
    this.DistributorName = a.DistributorName;
    this.ManufacturerName = a.ManufacturerName;
    this.Category = a.Category;
    this.Subcategory = a.Subcategory;
    this.ChemicalForm = a.ChemicalForm;
    this.Consumption = a.Consumption;
    this.Quantity = a.Quantity;
    this.ReorderLevel = a.ReorderLevel;
    this.Remark = a.Remark;
    console.log(a);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.post('/api/add-chemical-product', a, options)
      .subscribe(data => {
        alert('Registered Successfully');
        this.router.navigate(['/main-masters']);

      }, error => {
        console.log(JSON.stringify(error.json()));
      })
  }
}