import { Component, OnInit } from '@angular/core';
import { HeaderTitleService } from 'src/app/Services/header-title.service';
import * as Papa from 'papaparse';

import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
@Component({
  selector: 'app-cadm-import',
  templateUrl: './cadm-import.component.html',
  styleUrls: ['./cadm-import.component.css']
})
export class CadmImportComponent implements OnInit {

  dataList: any[];
  files: any[];
  data: any[];
  fileName: string;

  constructor(
    private headerTitleService: HeaderTitleService,
    private http: Http,
    private router: Router
  ) { }

  ngOnInit() {
    this.headerTitleService.setTitle('Chemical Agent Distributor Master');
  }

  onChange(files: File[]) {

    if (files[0]) {
      console.log(files[0]);
      Papa.parse(files[0], {
        header: true,
        skipEmptyLines: true,
        complete: (result, file) => {
          console.log(result);
          this.fileName = file.name;
          this.dataList = result.data;
          console.log(this.dataList);

          //post api
          var headers = new Headers();
          headers.append('Content-Type', 'application/json');
          let options = new RequestOptions({ headers: headers });

          this.http.post('api/cadmFileUpload', this.dataList, options)
            .subscribe(data => {
              console.log("added successfully");

            }, error => {
              console.log(JSON.stringify(error.json()));
            })
        }
      });
    }
  }

  onUpload() {
    this.router.navigate(['/edit-chemical-agent-distributor']);
  }

}
