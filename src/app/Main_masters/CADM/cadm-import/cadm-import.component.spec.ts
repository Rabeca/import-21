import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadmImportComponent } from './cadm-import.component';

describe('CadmImportComponent', () => {
  let component: CadmImportComponent;
  let fixture: ComponentFixture<CadmImportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadmImportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadmImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
