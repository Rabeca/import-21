import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadmAddComponent } from './cadm-add.component';

describe('CadmAddComponent', () => {
  let component: CadmAddComponent;
  let fixture: ComponentFixture<CadmAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadmAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadmAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
