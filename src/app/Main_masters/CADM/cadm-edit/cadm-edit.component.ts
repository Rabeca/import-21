import { Component, OnInit } from '@angular/core';
import { CadmService } from '../../../Services/cadm.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import { HeaderTitleService } from 'src/app/Services/header-title.service';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';

@Component({
  selector: 'app-cadm-edit',
  templateUrl: './cadm-edit.component.html',
  styleUrls: ['./cadm-edit.component.css']
})
export class CadmEditComponent implements OnInit {

  data: any[];
  datatobeedited: any;
  update: boolean = false;
  test = [{}];
  constructor(private CadmService: CadmService,
    private _http: Http,
    private headerTitleService: HeaderTitleService,
    private router: Router) { }

  ngOnInit() {
    this.CadmService.getData().subscribe((CadmData) => this.data = CadmData)
    this.headerTitleService.setTitle('Chemical Agent Distributor  Master');
  }

  editData(i: any) {

    this.update = true;
    this.datatobeedited = i;
    console.log("obj is" + this.datatobeedited.newChemicAgent);
  }
  saveUpdate(datasaved: any) {

    console.log("datasaved" + datasaved.newChemicAgent);
    console.log("datasaved" + '' + datasaved.newChemicAgent + '' +
      '' + datasaved.address + '' +
      '' + datasaved.Pincode + '' +
      '' + datasaved.Cperson + '' +
      '' + datasaved.mobile);

    let _url = "http://localhost:3000/api/add-chemical-agent-distributor" + "/" + datasaved._id;

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.put(_url, datasaved, options)
      .subscribe(data => {
        alert('saved Successfully');
      }, error => {
        console.log(JSON.stringify(error.json()));
      })
  }
  exportExcel() {

    var head = [
      'newChemicAgent',
      'address',
      'Pincode',
      'Cperson',
      'mobile',
      'altermobile',
      'Email',
      'GSTIN',
      'remarks'
    ];
    new Angular2Csv(this.test, 'Chemical Agent Master', { headers: (head) });
    alert("Please Give the Input Details");
    this.router.navigate(['/import-chemical-agent-distributor']);

  }
}
