import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadmEditComponent } from './cadm-edit.component';

describe('CadmEditComponent', () => {
  let component: CadmEditComponent;
  let fixture: ComponentFixture<CadmEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadmEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadmEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
