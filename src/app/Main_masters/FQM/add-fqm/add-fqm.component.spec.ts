import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFqmComponent } from './add-fqm.component';

describe('AddFqmComponent', () => {
  let component: AddFqmComponent;
  let fixture: ComponentFixture<AddFqmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFqmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFqmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
