import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFqmComponent } from './edit-fqm.component';

describe('EditFqmComponent', () => {
  let component: EditFqmComponent;
  let fixture: ComponentFixture<EditFqmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditFqmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFqmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
