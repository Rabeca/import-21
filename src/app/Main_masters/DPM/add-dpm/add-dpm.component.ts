import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HeaderTitleService } from 'src/app/Services/header-title.service';

@Component({
  selector: 'app-add-dpm',
  templateUrl: './add-dpm.component.html',
  styleUrls: ['./add-dpm.component.css']
})
export class AddDpmComponent implements OnInit {
  DpmName: string;

  constructor(
    private router: Router,
    private _http: Http,
    private headerTitleService: HeaderTitleService) { }

  ngOnInit() {
    this.headerTitleService.setTitle('Dry Process Master');
  }


  onSubmit(a) {

    this.DpmName = a.DpmName,

      console.log(a);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.post('api/add-supplier-master', a, options)
      .subscribe(data => {
        alert('Added Successfully');
        this.router.navigate(['/main-masters']);

      }, error => {
        console.log(JSON.stringify(error.json()));
      })
  }
}
