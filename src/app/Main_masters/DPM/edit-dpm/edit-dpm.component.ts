import { Component, OnInit } from '@angular/core';
import { BbmService } from "../../../Services/bbm.service";
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import { DpmService } from 'src/app/Services/dpm.service';
import { HeaderTitleService } from 'src/app/Services/header-title.service';

@Component({
  selector: 'app-edit-dpm',
  templateUrl: './edit-dpm.component.html',
  styleUrls: ['./edit-dpm.component.css']
})
export class EditDpmComponent implements OnInit {

  sign: any;
  test: any[];
  datatobeedited: any;
  update: boolean = false;
  chname: string;
  chage: string;
  constructor(
    private DpmService: DpmService,
    private _http: Http,
    private headerTitleService: HeaderTitleService) { }

  ngOnInit() {
    this.DpmService.getData().subscribe((DpmData) => this.test = DpmData);
    this.headerTitleService.setTitle('Dry Process Master');
  }

  editData(i: any) {

    this.update = true;
    this.datatobeedited = i;
    console.log("obj is" + this.datatobeedited.DpmName);
  }
  saveUpdate(datasaved: any) {

    console.log("datatobesaved" + datasaved.DpmName);
    console.log("datatobesaved" + '' + datasaved.DpmName);

    let _url = "http://localhost:3000/api/add-supplier-master" + "/" + datasaved._id;

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.put(_url, datasaved, options)
      .subscribe(data => {
        alert('saved Successfully');
      }, error => {
        console.log(JSON.stringify(error.json()));
      })
  }
}

