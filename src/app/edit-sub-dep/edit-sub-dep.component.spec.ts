import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSubDepComponent } from './edit-sub-dep.component';

describe('EditSubDepComponent', () => {
  let component: EditSubDepComponent;
  let fixture: ComponentFixture<EditSubDepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSubDepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSubDepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
