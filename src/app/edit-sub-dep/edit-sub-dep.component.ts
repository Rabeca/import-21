import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-sub-dep',
  templateUrl: './edit-sub-dep.component.html',
  styleUrls: ['./edit-sub-dep.component.css']
})
export class EditSubDepComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  onUpdatedSubDep() {
    this.router.navigate(['/updated_sub_department']);

  }

}
