import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-updated-sub-dep',
  templateUrl: './updated-sub-dep.component.html',
  styleUrls: ['./updated-sub-dep.component.css']
})
export class UpdatedSubDepComponent implements OnInit {
  datas: any;
  constructor() {
    this.datas = [
      {
        dep: 'Inward',
        sub_dep: 'Inawrd 1',
        password: '********',
        remarks: 'Remarks1, Remarks2',

      },
      {
        dep: 'Inward',
        sub_dep: 'Inawrd 2',
        password: '********',
        remarks: 'Remarks1, Remarks2',

      },
      {
        dep: 'Inward',
        sub_dep: 'Inawrd 3',
        password: '********',
        remarks: 'Remarks1, Remarks2',

      },
      {
        dep: 'Inward',
        sub_dep: 'Inawrd 4',
        password: '********',
        remarks: 'Remarks1, Remarks2',

      },
      {
        dep: 'Inward',
        sub_dep: 'Inawrd 5',
        password: '********',
        remarks: 'Remarks1, Remarks2',

      },
      {
        dep: 'Inward',
        sub_dep: 'Inawrd 6',
        password: '********',
        remarks: 'Remarks1, Remarks2',

      },
      {
        dep: 'Inward',
        sub_dep: 'Inawrd 7',
        password: '********',
        remarks: 'Remarks1, Remarks2',

      },
      {
        dep: 'Inward',
        sub_dep: 'Inawrd 8',
        password: '********',
        remarks: 'Remarks1, Remarks2',

      }

    ];
  }

  ngOnInit() {
  }

}
