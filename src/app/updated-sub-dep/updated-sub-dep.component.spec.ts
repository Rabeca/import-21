import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatedSubDepComponent } from './updated-sub-dep.component';

describe('UpdatedSubDepComponent', () => {
  let component: UpdatedSubDepComponent;
  let fixture: ComponentFixture<UpdatedSubDepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatedSubDepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatedSubDepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
