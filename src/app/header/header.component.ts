import { Component, OnInit } from '@angular/core';
import { HeaderTitleService } from '../Services/header-title.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  title = '';
  logout = '';
  constructor(private headerTitleService: HeaderTitleService) { }

  ngOnInit() {
    this.headerTitleService.title.subscribe(updatedTitle => {
      this.title = updatedTitle;
    });
    this.headerTitleService.logout.subscribe(logoutTitle => {
      this.logout = logoutTitle;
    })
  }

}
