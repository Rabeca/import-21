import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    myStyle: object = {};
    myParams: object = {};
    width: number = 100;
    height: number = 100;
 
    ngOnInit() {
        this.myStyle = {
          'background-color':'#24419b',
            'position': 'fixed',
            'width': '100%',
            'height': '100%',
            'z-index': -1,
            'top': 0,
            'left': 0,
            'right': 0,
            'bottom': 0,
        };
 
    this.myParams = {
            particles: {
                // number: {
                //     value: 200,
                // },
                // color: {
                //     value: '#ff0000'
                // },

                  number: {
                    "value": 130,
                   "density": {
                  "enable": true,
                //    //"value_area": 1000
                      }
                   },
                   color: {
                      "value": "#3ece98"
                    },
                shape: {
                  "type": "circle",
                  "stroke": {
                    "width": 0,
                    "color": "#000000"
                  },
                  "polygon": {
                    "nb_sides": 5
                  },
                  "image": {
                    "src": "",
                    "width": 100,
                    "height": 100
                  }
                },
                opacity: {
                  "value": 10,
                  "random": false,
                  "anim": {
                    "enable": false,
                    "speed": 2,
                    "opacity_min": 0.1,
                    "sync": false
                  }
                },
                size: {
                  "value": 6,
                  "random": true,
                  "anim": {
                    "enable": false,
                    "speed": 40,
                    "size_min": 0.1,
                    "sync": false
                  }
                },
                line_linked: {
                  "enable": true,
                  "distance": 150,
                  "color": "#ffffff",
                  "opacity": 0.9,
                  "width": 1
                },
                move: {
                  "enable": true,
                  "speed": 6,
                  "direction": "none",
                  "random": false,
                  "straight": false,
                  "out_mode": "out",
                  "attract": {
                    "enable": false,
                    "rotateX": 600,
                    "rotateY": 1200
                  }
                }
              },
              interactivity: {
                "detect_on": "canvas",
                "events": {
                  "onhover": {
                    "enable": true,
                    "mode": "grab"
                  },
                  onclick: {
                    "enable": true,
                    "mode": "push"
                  },
                  "resize": true
                },
                modes: {
                  "grab": {
                    "distance": 300,
                    "line_linked": {
                      "opacity": 1
                    }
                  },
                bubble: {
                    "distance": 400,
                    "size": 40,
                    "duration": 2,
                    "opacity": 8,
                    "speed": 3
                  },
                repulse: {
                    "distance": 200
                  },
                  push: {
                    "particles_nb": 4
                  },
                  remove: {
                    "particles_nb": 2
                  }
                }
              },
              // image:{
              //   "background-image":("/src/assets/images/main.jpg"),
              // },
              retina_detect: true,
              config_demo: {
                "hide_card": false,
               //"background_color": "#000000",
              // "background-image":("/src/assets/images/main.jpg"),
                "background_position": "50% 50%",
                "background_repeat": "no-repeat",
                "background_size": "cover"
              }
            }
          }
        }
