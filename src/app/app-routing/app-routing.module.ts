import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';

import { SigninComponent } from '../signin/signin.component';
import { ResetPasswordComponent } from '../reset-password/reset-password.component';
import { CustomerLoginComponent } from '../customer-login/customer-login.component';
import { SubDepartmentComponent } from '../sub-department/sub-department.component';
import { SubDepListComponent } from '../sub-dep-list/sub-dep-list.component';
import { EditSubDepComponent } from '../edit-sub-dep/edit-sub-dep.component';
import { UpdatedSubDepComponent } from '../updated-sub-dep/updated-sub-dep.component';
import { LogoutComponent } from '../logout/logout.component';
import { GetdataComponent } from '../getdata/getdata.component';
import { MainMasterComponent } from '../Main_masters/main-master/main-master.component';
// import { MenusGmmComponent } from '../Main_masters/GMM/menus-gmm/menus-gmm.component';
import { AddGmmComponent } from '../Main_masters/GMM/add-gmm/add-gmm.component';
import { TableGmmComponent } from '../Main_masters/GMM/table-gmm/table-gmm.component';
import { ImportGmmComponent } from '../Main_masters/GMM/import-gmm/import-gmm.component';
// import { BbmMenusComponent } from '../Main_masters/BBM/bbm-menus/bbm-menus.component';
import { AddBbmComponent } from '../Main_masters/BBM/add-bbm/add-bbm.component';
import { EditBbmComponent } from '../Main_masters/BBM/edit-bbm/edit-bbm.component';
import { ImportBbmComponent } from '../Main_masters/BBM/import-bbm/import-bbm.component';
// import { WpmMenusComponent } from '../Main_masters/WPM/wpm-menus/wpm-menus.component';
import { WpmAssignComponent } from '../Main_masters/WPM/wpm-assign/wpm-assign.component';
import { WpmEditComponent } from '../Main_masters/WPM/wpm-edit/wpm-edit.component';
// import { GimMenusComponent } from '../Main_masters/GIM/gim-menus/gim-menus.component';
import { GimAddComponent } from '../Main_masters/GIM/gim-add/gim-add.component';
import { GimListComponent } from '../Main_masters/GIM/gim-list/gim-list.component';
// import { CadmMenusComponent } from '../Main_masters/CADM/cadm-menus/cadm-menus.component';
import { CadmAddComponent } from '../Main_masters/CADM/cadm-add/cadm-add.component';
import { CadmEditComponent } from '../Main_masters/CADM/cadm-edit/cadm-edit.component';
import { CadmImportComponent } from '../Main_masters/CADM/cadm-import/cadm-import.component';
// import { CpmMenusComponent } from '../Main_masters/CPM/cpm-menus/cpm-menus.component';
import { CpmAddComponent } from '../Main_masters/CPM/cpm-add/cpm-add.component';
import { CpmEditComponent } from '../Main_masters/CPM/cpm-edit/cpm-edit.component';
import { CpmImportComponent } from '../Main_masters/CPM/cpm-import/cpm-import.component';
// import { CamMenusComponent } from '../Main_masters/CAM/cam-menus/cam-menus.component';
import { CamAddComponent } from '../Main_masters/CAM/cam-add/cam-add.component';
import { CamAssignListComponent } from '../Main_masters/CAM/cam-assign-list/cam-assign-list.component';
// import { WsmMenusComponent } from '../Main_masters/WSM/wsm-menus/wsm-menus.component';
import { WsmAddComponent } from '../Main_masters/WSM/wsm-add/wsm-add.component';
import { WsmEditListComponent } from '../Main_masters/WSM/wsm-edit-list/wsm-edit-list.component';
import { AddCmmComponent } from '../Main_masters/CMM/add-cmm/add-cmm.component';
import { EditCmmComponent } from '../Main_masters/CMM/edit-cmm/edit-cmm.component';
import { AddDpmComponent } from '../Main_masters/DPM/add-dpm/add-dpm.component';
import { EditDpmComponent } from '../Main_masters/DPM/edit-dpm/edit-dpm.component';
import { AddDmsmComponent } from '../Main_masters/DMSM/add-dmsm/add-dmsm.component';
import { EditDmsmComponent } from '../Main_masters/DMSM/edit-dmsm/edit-dmsm.component';
import { AddUqdmComponent } from '../Main_masters/UQDM/add-uqdm/add-uqdm.component';
import { EditUqdmComponent } from '../Main_masters/UQDM/edit-uqdm/edit-uqdm.component';
import { AddFqmComponent } from '../Main_masters/FQM/add-fqm/add-fqm.component';
import { EditFqmComponent } from '../Main_masters/FQM/edit-fqm/edit-fqm.component';
import { AddTemComponent } from '../Main_masters/TEM/add-tem/add-tem.component';
import { EditTemComponent } from '../Main_masters/TEM/edit-tem/edit-tem.component';
import { ImportCmmComponent } from '../Main_masters/CMM/import-cmm/import-cmm.component';
import { AddDpmpmComponent } from '../Main_masters/DPMPM/add-dpmpm/add-dpmpm.component';
import { EditDpmpmComponent } from '../Main_masters/DPMPM/edit-dpmpm/edit-dpmpm.component';
import { ImportDpmpmComponent } from '../Main_masters/DPMPM/import-dpmpm/import-dpmpm.component';
import { ImportDmsmComponent } from '../Main_masters/DMSM/import-dmsm/import-dmsm.component';
import { ImportTableComponent } from '../Main_masters/BBM/import-table/import-table.component';


const routes: Routes = [
  {
    path: '', component: SigninComponent
  },
  {
    path: 'change_password', component: ResetPasswordComponent
  },
  {
    path: 'client_login', component: CustomerLoginComponent
  },
  {
    path: 'sub_department', component: SubDepartmentComponent
  },
  {
    path: 'sub_department_list', component: SubDepListComponent
  },
  {
    path: 'edit_sub_department', component: EditSubDepComponent
  },
  {
    path: 'updated_sub_department', component: UpdatedSubDepComponent
  },
  {
    path: 'logout', component: LogoutComponent
  },
  {
    path: 'getdata', component: GetdataComponent
  },

  {
    path: 'main-masters', component: MainMasterComponent
  },


  // garment master
  // {
  //   path: 'garment-manufacturer-master-menus', component: MenusGmmComponent
  // },
  {
    path: 'add-garment-manufacturer-master', component: AddGmmComponent
  },
  {
    path: 'edit-garmment-manufacturer', component: TableGmmComponent
  },
  {
    path: 'garment-manufacturer-import', component: ImportGmmComponent
  },

  // Buyer/Brand master
  // {
  //   path: 'buyer-brand-menus', component: BbmMenusComponent
  // },
  {
    path: 'add-buyser-brand', component: AddBbmComponent
  },
  {
    path: 'edit-buyer-brand', component: EditBbmComponent
  },
  {
    path: 'buyer-brand-import', component: ImportBbmComponent
  },
  {
    path: 'import-table', component: ImportTableComponent
  },
  // Wash Process Master

  // {
  //   path: 'wash-process-menus', component: WpmMenusComponent
  // },
  {
    path: 'assign-wash-process', component: WpmAssignComponent
  },
  {
    path: 'edit-wash-process', component: WpmEditComponent
  },

  // Garment Item Master

  // {
  //   path: 'garment-item-menus', component: GimMenusComponent
  // },
  {
    path: 'add-garment-item', component: GimAddComponent
  },
  {
    path: 'edit-garment-item', component: GimListComponent
  },


  // chemical agent distributor master
  // {
  //   path: 'chemical-agent-distributor-menus', component: CadmMenusComponent
  // },
  {
    path: 'add-chemical-agent-distributor', component: CadmAddComponent
  },
  {
    path: 'edit-chemical-agent-distributor', component: CadmEditComponent
  },
  {
    path: 'import-chemical-agent-distributor', component: CadmImportComponent
  },

  // chemical product master

  // {
  //   path: 'chemical-product-menus', component: CpmMenusComponent
  // },
  {
    path: 'add-chemical-product', component: CpmAddComponent
  },
  {
    path: 'edit-chemical-product', component: CpmEditComponent
  },
  {
    path: 'import-chemical-product', component: CpmImportComponent
  },

  // Chemical Assign Master

  // {
  //   path: 'chemical-assign-master-menus', component: CamMenusComponent
  // },
  {
    path: 'add-chemical-assign-master', component: CamAddComponent
  },
  {
    path: 'chemical-assign-master-assign-list', component: CamAssignListComponent
  },

  // Wash step Master
  // {
  //   path: 'wash-step-master-menus', component: WsmMenusComponent
  // },
  {
    path: 'add-wash-step-master', component: WsmAddComponent
  },
  {
    path: 'wash-step-master-list-edit', component: WsmEditListComponent
  },

  //Chemical Manufacturer Master.
  {
    path: 'add-chemical-master', component: AddCmmComponent
  },

  {
    path: 'edit-chemical-master', component: EditCmmComponent
  },
  {
    path: 'import-chemical-manufacturer', component: ImportCmmComponent
  },

  //Dry Process Master
  {
    path: 'add-supplier-master', component: AddDpmComponent
  },

  {
    path: 'edit-supplier-master', component: EditDpmComponent
  },

  // Dry material Supplier master
  {
    path: 'add-dry-material', component: AddDmsmComponent
  },

  {
    path: 'edit-dry-material', component: EditDmsmComponent
  },
  {
    path: 'import-dry-material', component: ImportDmsmComponent

  },

  // unwash quality defect master
  {
    path: 'add-unwash', component: AddUqdmComponent
  },
  {
    path: 'edit-unwash', component: EditUqdmComponent
  },

  // finalcial master
  {
    path: 'add-financial', component: AddFqmComponent
  },
  {
    path: 'edit-financial', component: EditFqmComponent
  },

  // Transit master
  {
    path: 'add-transit', component: AddTemComponent
  },
  {
    path: 'edit-transit', component: EditTemComponent
  },

  // DPMPM
  {
    path: 'add-Dry-Process-Material-Product-Master', component: AddDpmpmComponent
  },
  {
    path: 'edit-Dry-Process-Material-Product-Master', component: EditDpmpmComponent
  },
  {
    path: 'import-Dry-Process-Material-Product-Master', component: ImportDpmpmComponent
  }


];


@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
