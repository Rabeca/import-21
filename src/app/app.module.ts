import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from "@angular/forms";
import { AppComponent } from './app.component';


//angular material
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CdkTableModule } from '@angular/cdk/table';


import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatStepperModule,


} from '@angular/material';

import { ParticlesModule } from 'angular-particle';
import { SigninComponent } from './signin/signin.component';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { SubDepartmentComponent } from './sub-department/sub-department.component';
import { EditSubDepComponent } from './edit-sub-dep/edit-sub-dep.component';
import { CustomerLoginComponent } from './customer-login/customer-login.component';
import { LogoutComponent } from './logout/logout.component';
import { UpdatedSubDepComponent } from './updated-sub-dep/updated-sub-dep.component';
import { SubDepListComponent } from './sub-dep-list/sub-dep-list.component';
import { MainMasterComponent } from './Main_masters/main-master/main-master.component';
import { GetdataComponent } from './getdata/getdata.component';



import { AgGridModule } from 'ag-grid-angular';

import { OrderModule } from 'ngx-order-pipe';
import { SearchPipe } from './Services/search.pipe';
// import { MenusGmmComponent } from './Main_masters/GMM/menus-gmm/menus-gmm.component';
import { AddGmmComponent } from './Main_masters/GMM/add-gmm/add-gmm.component';
import { TableGmmComponent } from './Main_masters/GMM/table-gmm/table-gmm.component';
import { ImportGmmComponent } from './Main_masters/GMM/import-gmm/import-gmm.component';
// import { BbmMenusComponent } from './Main_masters/BBM/bbm-menus/bbm-menus.component';
import { AddBbmComponent } from './Main_masters/BBM/add-bbm/add-bbm.component';
import { EditBbmComponent } from './Main_masters/BBM/edit-bbm/edit-bbm.component';
import { ImportBbmComponent } from './Main_masters/BBM/import-bbm/import-bbm.component';
// import { WpmMenusComponent } from './Main_masters/WPM/wpm-menus/wpm-menus.component';
import { WpmAssignComponent } from './Main_masters/WPM/wpm-assign/wpm-assign.component';
import { WpmEditComponent } from './Main_masters/WPM/wpm-edit/wpm-edit.component';
// import { GimMenusComponent } from './Main_masters/GIM/gim-menus/gim-menus.component';
import { GimAddComponent } from './Main_masters/GIM/gim-add/gim-add.component';
import { GimListComponent } from './Main_masters/GIM/gim-list/gim-list.component';
// import { CadmMenusComponent } from './Main_masters/CADM/cadm-menus/cadm-menus.component';
import { CadmAddComponent } from './Main_masters/CADM/cadm-add/cadm-add.component';
import { CadmEditComponent } from './Main_masters/CADM/cadm-edit/cadm-edit.component';
import { CadmImportComponent } from './Main_masters/CADM/cadm-import/cadm-import.component';
// import { CpmMenusComponent } from './Main_masters/CPM/cpm-menus/cpm-menus.component';
import { CpmAddComponent } from './Main_masters/CPM/cpm-add/cpm-add.component';
import { CpmEditComponent } from './Main_masters/CPM/cpm-edit/cpm-edit.component';
import { CpmImportComponent } from './Main_masters/CPM/cpm-import/cpm-import.component';
// import { CamMenusComponent } from './Main_masters/CAM/cam-menus/cam-menus.component';
import { CamAddComponent } from './Main_masters/CAM/cam-add/cam-add.component';
import { CamAssignListComponent } from './Main_masters/CAM/cam-assign-list/cam-assign-list.component';
// import { WsmMenusComponent } from './Main_masters/WSM/wsm-menus/wsm-menus.component';
import { WsmAddComponent } from './Main_masters/WSM/wsm-add/wsm-add.component';
import { WsmEditListComponent } from './Main_masters/WSM/wsm-edit-list/wsm-edit-list.component';
import { MydataService } from './Services/mydata.service';
import { CadmService } from "./Services/cadm.service";
import { BbmService } from "./Services/bbm.service";
import { WpmService } from "./Services/wpm.service";
import { GmmService } from "./Services/gmm.service";
import { WsmService } from './Services/wsm.service';
import { CpmService } from "./Services/cpm.service";
import { CamService } from './Services/cam.service';
import { GimService } from "./Services/gim.service";
import { HeaderComponent } from './header/header.component';
import { AddCmmComponent } from './Main_masters/CMM/add-cmm/add-cmm.component';
import { EditCmmComponent } from './Main_masters/CMM/edit-cmm/edit-cmm.component';
import { AddDpmComponent } from './Main_masters/DPM/add-dpm/add-dpm.component';
import { EditDpmComponent } from './Main_masters/DPM/edit-dpm/edit-dpm.component';
import { CmmService } from './Services/cmm.service';
import { DpmService } from './Services/dpm.service';

import { AddDmsmComponent } from './Main_masters/DMSM/add-dmsm/add-dmsm.component';
import { EditDmsmComponent } from './Main_masters/DMSM/edit-dmsm/edit-dmsm.component';
import { AddUqdmComponent } from './Main_masters/UQDM/add-uqdm/add-uqdm.component';
import { EditUqdmComponent } from './Main_masters/UQDM/edit-uqdm/edit-uqdm.component';
import { EditFqmComponent } from './Main_masters/FQM/edit-fqm/edit-fqm.component';
import { AddFqmComponent } from './Main_masters/FQM/add-fqm/add-fqm.component';
import { AddTemComponent } from './Main_masters/TEM/add-tem/add-tem.component';
import { EditTemComponent } from './Main_masters/TEM/edit-tem/edit-tem.component';
import { DMSMService } from './Services/dmsm.service';
import { UQDMService } from './Services/uqdm.service';
import { FQMService } from './Services/fqm.service';
import { TEMService } from './Services/tem.service';
import { HeaderTitleService } from './Services/header-title.service';
import { ImportCmmComponent } from './Main_masters/CMM/import-cmm/import-cmm.component';
import { AddDpmpmComponent } from './Main_masters/DPMPM/add-dpmpm/add-dpmpm.component';
import { EditDpmpmComponent } from './Main_masters/DPMPM/edit-dpmpm/edit-dpmpm.component';
import { ImportDpmpmComponent } from './Main_masters/DPMPM/import-dpmpm/import-dpmpm.component';
import { DpmpmService } from './Services/dpmpm.service';
import { ImportDmsmComponent } from './Main_masters/DMSM/import-dmsm/import-dmsm.component';
import { ExcelService } from './Services/excel.service';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { FileUploadService } from './Services/file-upload.service';
import { ImportTableComponent } from './Main_masters/BBM/import-table/import-table.component';



@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    ResetPasswordComponent,
    SubDepartmentComponent,
    EditSubDepComponent,
    CustomerLoginComponent,
    LogoutComponent,
    UpdatedSubDepComponent,
    SubDepListComponent,
    MainMasterComponent,
    GetdataComponent,

    SearchPipe,
    // MenusGmmComponent,
    AddGmmComponent,
    TableGmmComponent,
    ImportGmmComponent,
    // BbmMenusComponent,
    AddBbmComponent,
    EditBbmComponent,
    ImportBbmComponent,
    // WpmMenusComponent,
    WpmAssignComponent,
    WpmEditComponent,
    // GimMenusComponent,
    GimAddComponent,
    GimListComponent,
    // CadmMenusComponent,
    CadmAddComponent,
    CadmEditComponent,
    CadmImportComponent,
    // CpmMenusComponent,
    CpmAddComponent,
    CpmEditComponent,
    CpmImportComponent,
    // CamMenusComponent,
    CamAddComponent,
    CamAssignListComponent,
    // WsmMenusComponent,
    WsmAddComponent,
    WsmEditListComponent,
    HeaderComponent,
    AddCmmComponent,
    EditCmmComponent,
    AddDpmComponent,
    EditDpmComponent,

    AddDmsmComponent,
    EditDmsmComponent,
    AddUqdmComponent,
    EditUqdmComponent,
    EditFqmComponent,
    AddFqmComponent,
    AddTemComponent,
    EditTemComponent,
    ImportCmmComponent,
    AddDpmpmComponent,
    EditDpmpmComponent,
    ImportDpmpmComponent,
    ImportDmsmComponent,
    FileUploadComponent,
    ImportTableComponent




  ],
  imports: [
    BrowserModule,

    OrderModule,
    HttpModule,
    AppRoutingModule,
    FormsModule,
    ParticlesModule, RouterModule.forRoot([]),

    BrowserAnimationsModule,
    CdkTableModule,
    MatNativeDateModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    AgGridModule,



  ],
  providers: [MydataService,
    WpmService,
    CamService,
    CadmService,
    BbmService,
    GmmService,
    WsmService,
    GimService,
    CpmService,
    CmmService,
    DpmService,
    DMSMService,
    UQDMService,
    FQMService,
    TEMService,
    HeaderTitleService,
    DpmpmService,

    ExcelService,
    FileUploadService,

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
