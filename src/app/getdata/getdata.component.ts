import { Component, OnInit } from '@angular/core';
import { MydataService } from '../Services/mydata.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

@Component({
  selector: 'app-getdata',
  templateUrl: './getdata.component.html',
  styleUrls: ['./getdata.component.css'],
  providers: [MydataService]
})
export class GetdataComponent implements OnInit {
  sign: any;
  test: any[];
  datatobeedited: any;
  update: boolean = false;
  chname: string;
  chage: string;
  constructor(private mydataService: MydataService, private _http: Http) { }
  ngOnInit() {
    this.mydataService.getData().subscribe((testData) => this.test = testData)
  }

  editData(i: any) {

    this.update = true;
    this.datatobeedited = i;
    console.log("obj is" + this.datatobeedited.subDepSelect);
  }
  saveUpdate(datasaved: any) {

    console.log("datatobesaved" + datasaved.subDepSelect);
    console.log("datatobesaved" + '' + datasaved.subDepPassword + '' + '' + datasaved.subDepRemark);

    let _url = "http://localhost:3000/api/data" + "/" + datasaved._id;

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.put(_url, datasaved, options)
      .subscribe(data => {
        alert('saved Successfully');
      }, error => {
        console.log(JSON.stringify(error.json()));
      })
  }
}



