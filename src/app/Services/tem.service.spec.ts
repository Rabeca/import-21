import { TestBed } from '@angular/core/testing';

import { TEMService } from './tem.service';

describe('TEMService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TEMService = TestBed.get(TEMService);
    expect(service).toBeTruthy();
  });
});
