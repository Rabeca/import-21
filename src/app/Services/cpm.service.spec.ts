import { TestBed } from '@angular/core/testing';

import { CpmService } from './cpm.service';

describe('CpmService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CpmService = TestBed.get(CpmService);
    expect(service).toBeTruthy();
  });
});
