import { TestBed } from '@angular/core/testing';

import { DpmpmService } from './dpmpm.service';

describe('DpmpmService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DpmpmService = TestBed.get(DpmpmService);
    expect(service).toBeTruthy();
  });
});
