import { TestBed } from '@angular/core/testing';

import { FQMService } from './fqm.service';

describe('FQMService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FQMService = TestBed.get(FQMService);
    expect(service).toBeTruthy();
  });
});
