import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
@Injectable({
  providedIn: 'root'
})
export class HeaderTitleService {
  title = new BehaviorSubject('Masters');
  logout = new BehaviorSubject('Logout');
  constructor() { }
  setTitle(title: string) {
    this.title.next(title);
  }
  LogTitle(logout: string) {
    this.logout.next(logout);
  }
}
