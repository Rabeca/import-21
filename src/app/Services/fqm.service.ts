import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
@Injectable({
  providedIn: 'root'
})
export class FQMService {

  private url1 = "http://localhost:3000/api/add-financial";

  // get data url
  _url = "http://localhost:3000/api/add-financial";

  constructor(private _http: Http) { }

  getData(): Observable<any[]> {
    return this._http.get(this._url)
      //.map((response:Response)=> <any[]>response.json())
      .map(function (response: Response) { return <any>response.json() })
  }
}
