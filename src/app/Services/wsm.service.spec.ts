import { TestBed } from '@angular/core/testing';

import { WsmService } from './wsm.service';

describe('WsmService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WsmService = TestBed.get(WsmService);
    expect(service).toBeTruthy();
  });
});
