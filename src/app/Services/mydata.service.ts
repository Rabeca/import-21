import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MydataService {
  //post data url
  private url1 = "http://localhost:3000/api/data";
  
 // get data url
  _url = "http://localhost:3000/api/data";


  // url = "http://localhost:3000/api/assign-wash-process";
  // url2 = "http://localhost:3000/api/wash-step-master-list-edit";
  // url3 ="http://localhost:3000/api/add-garment-manufacturer-master";
  // url4 ='http://localhost:3000/api/add-buyser-brand';
  // url5 = "http://localhost:3000/api/add-chemical-agent-distributor";

  constructor(private _http: Http) { }

  getData(): Observable<any[]> {
    return this._http.get(this._url)
      //.map((response:Response)=> <any[]>response.json())
      .map(function (response: Response) { return <any>response.json() })
  }
}
