import { TestBed } from '@angular/core/testing';

import { UQDMService } from './uqdm.service';

describe('UQDMService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UQDMService = TestBed.get(UQDMService);
    expect(service).toBeTruthy();
  });
});
