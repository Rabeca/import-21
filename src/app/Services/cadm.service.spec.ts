import { TestBed } from '@angular/core/testing';

import { CadmService } from './cadm.service';

describe('CadmService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CadmService = TestBed.get(CadmService);
    expect(service).toBeTruthy();
  });
});
