import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class WsmService {
  //post data url
  private url1 = "http://localhost:3000/api/wash-step-master-list-edit";

  // get data url
  _url = "http://localhost:3000/api/wash-step-master-list-edit";

  constructor(private _http: Http) { }

  getData(): Observable<any[]> {
    return this._http.get(this._url)
      //.map((response:Response)=> <any[]>response.json())
      .map(function (response: Response) { return <any>response.json() })
  }
}
