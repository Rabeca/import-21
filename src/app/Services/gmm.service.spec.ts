import { TestBed } from '@angular/core/testing';

import { GmmService } from './gmm.service';

describe('GmmService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GmmService = TestBed.get(GmmService);
    expect(service).toBeTruthy();
  });
});
