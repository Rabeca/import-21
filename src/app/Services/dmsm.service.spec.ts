import { TestBed } from '@angular/core/testing';

import { DMSMService } from './dmsm.service';

describe('DMSMService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DMSMService = TestBed.get(DMSMService);
    expect(service).toBeTruthy();
  });
});
