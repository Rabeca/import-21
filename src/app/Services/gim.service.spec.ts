import { TestBed } from '@angular/core/testing';

import { GimService } from './gim.service';

describe('GimService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GimService = TestBed.get(GimService);
    expect(service).toBeTruthy();
  });
});
