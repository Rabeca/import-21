import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';



@Component({
  selector: 'app-customer-login',
  templateUrl: './customer-login.component.html',
  styleUrls: ['./customer-login.component.css']
})
export class CustomerLoginComponent implements OnInit {
  username: string;
  password: string;

  constructor(private router: Router, private _http: Http) { }

  ngOnInit() {
  }
  onSubmit(a) {
    this.username = a.username;
    this.password = a.password;
    console.log(a.username + "   " + a.password);
    console.log(a);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.post('api/client_login', a, options)
      .subscribe(data => {
        alert("success");
        this.router.navigate(['/sub_department'])
      }, error => {
        alert("invalid user")
        //console.log(JSON.stringify(error.json()));
      })
  }

}
