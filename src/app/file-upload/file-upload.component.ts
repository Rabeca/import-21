// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-file-upload',
//   templateUrl: './file-upload.component.html',
//   styleUrls: ['./file-upload.component.css']
// })
// export class FileUploadComponent implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }
import { Component, OnInit } from '@angular/core';
import * as Papa from 'papaparse';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { FileUploadService } from '../Services/file-upload.service';
// import { FileUploadService } from '../file-upload.service';

let obj: FileUploadComponent;

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {
  Parent_Identifier: string;
  Student_Identifier: string;

  name = 'Angular 5 csv file parser example';
  dataList: any[];
  files: any[];
  data: any[];


  constructor(private http: Http, private fileUploadService: FileUploadService) { }

  ngOnInit() {
    obj = this;
    this.fileUploadService.getData().subscribe((FUData) => this.data = FUData);


  }
  onChange(files: File[]) {

    if (files[0]) {
      console.log(files[0]);
      Papa.parse(files[0], {
        header: true,
        skipEmptyLines: true,
        complete: (result, file) => {
          console.log(result);
          this.dataList = result.data;
          console.log(this.dataList);
          console.log(file);

          var headers = new Headers();
          headers.append('Content-Type', 'application/json');
          let options = new RequestOptions({ headers: headers });

          this.http.post('api/fileUpload', this.dataList, options)
            .subscribe(data => {
              console.log("added successfully");
              // this.router.navigate(['/main-masters']);

            }, error => {
              console.log(JSON.stringify(error.json()));
            })
        }


      });
    }
  }



}

