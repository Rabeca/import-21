import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sub-dep-list',
  templateUrl: './sub-dep-list.component.html',
  styleUrls: ['./sub-dep-list.component.css']
})
export class SubDepListComponent implements OnInit {
  datas: any;
  constructor(private router: Router) {
    this.datas = [
      {
        dep: 'Inward',
        sub_dep: 'Inawrd 1',
        password: '********',
        remarks: 'Remarks1, Remarks2',

      },
      {
        dep: 'Inward',
        sub_dep: 'Inawrd 2',
        password: '********',
        remarks: 'Remarks1, Remarks2',

      },
      {
        dep: 'Inward',
        sub_dep: 'Inawrd 3',
        password: '********',
        remarks: 'Remarks1, Remarks2',

      },
      {
        dep: 'Inward',
        sub_dep: 'Inawrd 4',
        password: '********',
        remarks: 'Remarks1, Remarks2',

      },
      {
        dep: 'Inward',
        sub_dep: 'Inawrd 5',
        password: '********',
        remarks: 'Remarks1, Remarks2',

      },
      {
        dep: 'Inward',
        sub_dep: 'Inawrd 6',
        password: '********',
        remarks: 'Remarks1, Remarks2',

      },
      {
        dep: 'Inward',
        sub_dep: 'Inawrd 7',
        password: '********',
        remarks: 'Remarks1, Remarks2',

      },
      {
        dep: 'Inward',
        sub_dep: 'Inawrd 8',
        password: '********',
        remarks: 'Remarks1, Remarks2',

      }

    ];
  }
  onEditSubDep() {
    this.router.navigate(['/edit_sub_department'])
  }
  ngOnInit() {
  }

}
