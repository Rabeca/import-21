import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubDepListComponent } from './sub-dep-list.component';

describe('SubDepListComponent', () => {
  let component: SubDepListComponent;
  let fixture: ComponentFixture<SubDepListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubDepListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubDepListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
