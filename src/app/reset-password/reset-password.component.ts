import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  oldpassword: string;
  newpassword: string;
  confirmpassword: string;

  constructor(private router: Router, private _http: Http) { }

  ngOnInit() {
  }

  onClient(a) {
    this.oldpassword = a.oldpassword;
    this.newpassword = a.newpassword;
    this.confirmpassword = a.confirmpassword;

    console.log('posted')
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.post('api/signup', a, options)
      .subscribe(data => {
        alert("successfully posted");
        this.router.navigate(['/client_login'])
      }, error => {
        alert("failed to add")
        //console.log(JSON.stringify(error.json()));
      })
  }

}
