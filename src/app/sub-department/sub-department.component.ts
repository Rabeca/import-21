import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

@Component({
  selector: 'app-sub-department',
  templateUrl: './sub-department.component.html',
  styleUrls: ['./sub-department.component.css']
})
export class SubDepartmentComponent implements OnInit {
  subDepSelect: string;
  subDepartment: string;
  subDepPassword: string;
  subDepCFPassword: String;
  subDepRemark: String;

  constructor(private router: Router, private _http: Http) { }

  ngOnInit() {
  }

  onSubmit(a) {
    this.subDepSelect = a.subDepSelect;
    this.subDepartment = a.subDepartment;
    this.subDepPassword = a.subDepPassword;
    this.subDepCFPassword = a.subDepCFPassword;
    this.subDepRemark = a.subDepRemark;
    console.log(a);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    this._http.post('/api/sub_department', a, options)
      .subscribe(data => {
        alert('Registered Successfully');
        this.router.navigate(['/getdata']);

      }, error => {
        console.log(JSON.stringify(error.json()));
      })
  }
  //  validate()
  //   {
  //     this.password;
  //     this.Cpassword ;

  //     if(this.password == this.Cpassword)
  //     {
  //       this.x = true;
  //       this.y = false;
  //     }
  //     else if(this.password != this.Cpassword)
  //     {
  //       this.x = false;
  //       this.y = true;
  //     }
  //   }
  //  }
}
