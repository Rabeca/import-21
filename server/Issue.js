// import mongoose from 'mongoose';
var mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Issue = new Schema({
    Parent_Identifier: {
        type: String
    },
    Student_Identifier: {
        type: String
    },

});

// export default mongoose.model('Issue', Issue);
// module.exports = db.model('Issue', Issue);


// make this available to our users in our Node applications
module.exports = mongoose.model('Issue', Issue);
