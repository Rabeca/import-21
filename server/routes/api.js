var app = require('express')();
var mongo = require('mongodb').MongoClient;
var mongoose = require('mongoose');
// import Issue from '../Issue';
var Issue = require('../Issue');
const multer = require('multer');
var upload = multer({ dest: '../public/uploads/' });

// signin
app.post('/signin', function (req, res) {
    var username = req.body.username;
    var password = req.body.password;
    console.log(username);
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (!err) {
            db.collection('admin1').find({ username: username, password: password }).toArray(function (err, result) {
                // console.log(result);
                if (err || result.length <= 0)
                    console.log('invalid user');
                else
                    res.send(result);
                console.log("successful login");
            })
        }
    })
})

// resetPassword
app.post('/signup', function (req, res) {

    var obj = {
        oldpassword: req.body.oldpassword,
        newpassword: req.body.newpassword,
        confirmpassword: req.body.confirmpassword,

    };
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        db.collection('reset').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully inserted");
        })
        res.send("inserted");
    });
});

// Customer Login
app.post('/client_login', function (req, res) {
    var username = req.body.username;
    var password = req.body.password;
    console.log(username);

    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {

        if (!err) {

            db.collection('clientLogin').find({ username: username, password: password }).toArray(function (err, result) {

                // console.log(result);node
                if (err || result.length <= 0)
                    console.log('invalid user');
                else
                    res.send(result);
                console.log("successful login");
            })
        }
    })
})


// sub department
app.post('/sub_department', function (req, res) {

    var obj = [{
        subDepSelect: req.body.subDepSelect,
        subDepartment: req.body.subDepartment,
        subDepPassword: req.body.subDepPassword,
        subDepCFPassword: req.body.subDepCFPassword,
        subDepRemark: req.body.subDepRemark
    }];
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        db.collection('users').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully inserted");
        })
        res.send("inserted");
    });
});

app.get('/data', function (req, res) {
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        console.log("mongo is connected");
        db.collection('users').find({}).toArray(function (err, sample) {
            if (err) throw err;
            console.log(sample);
            res.send(sample);
        });
    });
});
// sub-deportment update
app.put('/data/:name', function (req, res) {
    console.log("_id in api to update is" + req.body.subDepPassword);
    console.log("_id in api to update is" + req.body.subDepRemark);
    mongoose.connect("mongodb://localhost:27017/gis", { useNewUrlParser: true }, function (err, db) {
        if (err) throw err;
        console.log("connected to db successfully to save update");
        var id = new mongoose.Types.ObjectId(req.params.subDepSelect);
        console.log(id);
        db.collection('users').find(id).toArray(function (err, data) {
            if (err) throw err;
            console.log(data);
            db.collection('users').updateMany({ '_id': id }, {
                $set: {
                    subDepPassword: req.body.subDepPassword,
                    subDepRemark: req.body.subDepRemark,
                }
            });
            db.collection('users').updateMany({ '_id': id }, {
                $set: {
                    subDepPassword: req.body.subDepPassword,
                    subDepRemark: req.body.subDepRemark,
                }
            });
            res.send(data);
        });
        res.send(res);
        res.send({ message: "successfully updated" });
    });
});



// wpm assign matsers posting data
app.post('/assign-wash-process', function (req, res) {
    var obj = {
        seldep: req.body.seldep,
        subDep1: req.body.subDep1,
    };
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        db.collection('wpm').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully assigned");
        })
        res.send("assigned");
    });
});

app.get('/assign-wash-process', function (req, res) {
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        console.log("mongo is connected");
        db.collection('wpm').find({}).toArray(function (err, sample) {
            if (err) throw err;
            console.log(sample);
            res.send(sample);
        });// testvon file upload db
    });
});
//update wpm
app.put('/assign-wash-process/:seldep', function (req, res) {
    console.log("_id in api to update is" + req.body.seldep);
    mongoose.connect("mongodb://localhost:27017/gis", { useNewUrlParser: true }, function (err, db) {
        if (err) throw err;
        console.log("connected to db successfully to save update");
        var id = new mongoose.Types.ObjectId(req.params.seldep);
        console.log(id);
        db.collection('wpm').find(id).toArray(function (err, data) {
            if (err) throw err;
            console.log(data);
            db.collection('wpm').updateMany({ '_id': id }, {
                $set: {
                    seldep: req.body.seldep,
                    subDep1: req.body.subDep1,
                }
            });
            db.collection('wpm').updateMany({ '_id': id }, {
                $set: {
                    seldep: req.body.seldep,
                    subDep1: req.body.subDep1,
                }
            });
            res.send(data);
        });
        res.send(res);
        res.send({ message: "successfully updated" });
    });
});

// wsm master posting data
app.post('/wash-step-master-list-edit', function (req, res) {
    var obj = {
        AWS: req.body.AWS,
    };
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        db.collection('wsm').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully added");
        })
        res.send("added");
    });
});
//get data WSM
app.get('/wash-step-master-list-edit', function (req, res) {
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        console.log("mongo is connected");
        db.collection('wsm').find({}).toArray(function (err, sample) {
            if (err) throw err;
            console.log(sample);
            res.send(sample);
        });
    });
});

//update wsm
app.put('/wash-step-master-list-edit/:AWS', function (req, res) {
    console.log("_id in api to update is" + req.body.AWS);
    mongoose.connect("mongodb://localhost:27017/gis", { useNewUrlParser: true }, function (err, db) {
        if (err) throw err;
        console.log("connected to db successfully to save update");
        var id = new mongoose.Types.ObjectId(req.params.AWS);
        console.log(id);
        db.collection('wsm').find(id).toArray(function (err, data) {
            if (err) throw err;
            console.log(data);
            db.collection('wsm').updateMany({ '_id': id }, {
                $set: {
                    AWS: req.body.AWS,
                }
            });
            db.collection('wsm').updateMany({ '_id': id }, {
                $set: {
                    AWS: req.body.AWS,
                }
            });
            res.send(data);
        });
        res.send(res);
        res.send({ message: "successfully updated" });
    });
});

// GMM posting data

app.post('/add-garment-manufacturer-master', function (req, res) {
    var obj = {
        newGarManf: req.body.newGarManf,
        address: req.body.address,
        Pincode: req.body.Pincode,
        Cperson: req.body.Cperson,
        mobile: req.body.mobile,
        altermobile: req.body.altermobile,
        Email: req.body.Email,
        website: req.body.website,
        remarks: req.body.remarks,
    };
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        db.collection('gmm').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully added");
        })
        res.send("added");
    });
});
//getting data for GMM
app.get('/add-garment-manufacturer-master', function (req, res) {
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        console.log("mongo is connected");
        db.collection('gmm').find({}).toArray(function (err, sample) {
            if (err) throw err;
            console.log(sample);
            res.send(sample);
        });
    });
});

// update  of GMM
app.put('/add-garment-manufacturer-master/:newGarManf', function (req, res) {
    console.log("_id in api to update is" + req.body.newGarManf);
    mongoose.connect("mongodb://localhost:27017/gis", { useNewUrlParser: true }, function (err, db) {
        if (err) throw err;
        console.log("connected to db successfully to save update");
        var id = new mongoose.Types.ObjectId(req.params.newGarManf);
        console.log(id);
        db.collection('gmm').find(id).toArray(function (err, data) {
            if (err) throw err;
            console.log(data);
            db.collection('gmm').updateMany({ '_id': id }, {
                $set: {
                    newGarManf: req.body.newGarManf,
                    address: req.body.address,
                    Pincode: req.body.Pincode,
                    Cperson: req.body.Cperson,
                    mobile: req.body.mobile,
                    altermobile: req.body.altermobile,
                    Email: req.body.Email,
                    website: req.body.website,
                    remarks: req.body.remarks,
                }
            });
            db.collection('gmm').updateMany({ '_id': id }, {
                $set: {
                    newGarManf: req.body.newGarManf,
                    address: req.body.address,
                    Pincode: req.body.Pincode,
                    Cperson: req.body.Cperson,
                    mobile: req.body.mobile,
                    altermobile: req.body.altermobile,
                    Email: req.body.Email,
                    website: req.body.website,
                    remarks: req.body.remarks,
                }
            });
            res.send(data);
        });
        res.send(res);
        res.send({ message: "successfully updated" });
    });
});

//posting the data for BBM
app.post('/add-buyser-brand', function (req, res) {
    var obj = {
        newBuyerBrand: req.body.newBuyerBrand,
        address: req.body.address,
        Pincode: req.body.Pincode,
        Cperson: req.body.Cperson,
        mobile: req.body.mobile,
        altermobile: req.body.altermobile,
        Email: req.body.Email,
        website: req.body.website,
        remarks: req.body.remarks,
    };
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        db.collection('bbm').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully added");
        })
        res.send("added");
    });
});

//getting data for BBM

app.get('/add-buyser-brand', function (req, res) {
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        console.log("mongo is connected");
        db.collection('bbm').find({}).toArray(function (err, sample) {
            if (err) throw err;
            console.log(sample);
            res.send(sample);
        });
    });
});
// update  of BBM
app.put('/add-buyser-brand/:newBuyerBrand', function (req, res) {
    console.log("_id in api to update is" + req.body.newBuyerBrand);
    mongoose.connect("mongodb://localhost:27017/gis", { useNewUrlParser: true }, function (err, db) {
        if (err) throw err;
        console.log("connected to db successfully to save update");
        var id = new mongoose.Types.ObjectId(req.params.newBuyerBrand);
        console.log(id);
        db.collection('bbm').find(id).toArray(function (err, data) {
            if (err) throw err;
            console.log(data);
            db.collection('bbm').updateMany({ '_id': id }, {
                $set: {
                    newBuyerBrand: req.body.newBuyerBrand,
                    address: req.body.address,
                    Pincode: req.body.Pincode,
                    Cperson: req.body.Cperson,
                    mobile: req.body.mobile,
                    altermobile: req.body.altermobile,
                    Email: req.body.Email,
                    website: req.body.website,
                    remarks: req.body.remarks,
                }
            });
            db.collection('bbm').updateMany({ '_id': id }, {
                $set: {
                    newBuyerBrand: req.body.newBuyerBrand,
                    address: req.body.address,
                    Pincode: req.body.Pincode,
                    Cperson: req.body.Cperson,
                    mobile: req.body.mobile,
                    altermobile: req.body.altermobile,
                    Email: req.body.Email,
                    website: req.body.website,
                    remarks: req.body.remarks,
                }
            });
            res.send(data);
        });
        res.send(res);
        res.send({ message: "successfully updated" });
    });
});




// posting data for cadm
app.post('/add-chemical-agent-distributor', function (req, res) {
    var obj = {
        newChemicAgent: req.body.newChemicAgent,
        address: req.body.address,
        Pincode: req.body.Pincode,
        Cperson: req.body.Cperson,
        mobile: req.body.mobile,
        altermobile: req.body.altermobile,
        Email: req.body.Email,
        GSTIN: req.body.GSTIN,
        remarks: req.body.remarks,
    };
    mongo.connect("mongodb://localho\st:27017/gis", function (err, db) {
        if (err) throw err;
        db.collection('cadmm').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully added");
        })
        res.send("added");
    });
});

//getting data for cadm
app.get('/add-chemical-agent-distributor', function (req, res) {
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        console.log("mongo is connected");
        db.collection('cadmm').find({}).toArray(function (err, sample) {
            if (err) throw err;
            console.log(sample);
            res.send(sample);
        });
    });
});


//update cadm
app.put('/add-chemical-agent-distributor/:newChemicAgent', function (req, res) {
    console.log("_id in api to update is" + req.body.newChemicAgent);
    mongoose.connect("mongodb://localhost:27017/gis", { useNewUrlParser: true }, function (err, db) {
        if (err) throw err;
        console.log("connected to db successfully to save update");
        var id = new mongoose.Types.ObjectId(req.params.newChemicAgent);
        console.log(id);
        db.collection('cadmm').find(id).toArray(function (err, data) {
            if (err) throw err;
            console.log(data);
            db.collection('cadmm').updateMany({ '_id': id }, {
                $set: {
                    newChemicAgent: req.body.newChemicAgent,
                    address: req.body.address,
                    Pincode: req.body.Pincode,
                    Cperson: req.body.Cperson,
                    mobile: req.body.mobile,
                    altermobile: req.body.altermobile,
                    Email: req.body.Email,
                    GSTIN: req.body.GSTIN,
                    remarks: req.body.remarks,
                }
            });
            db.collection('bbm').updateMany({ '_id': id }, {
                $set: {
                    newChemicAgent: req.body.newChemicAgent,
                    address: req.body.address,
                    Pincode: req.body.Pincode,
                    Cperson: req.body.Cperson,
                    mobile: req.body.mobile,
                    altermobile: req.body.altermobile,
                    Email: req.body.Email,
                    GSTIN: req.body.GSTIN,
                    remarks: req.body.remarks,
                }
            });
            res.send(data);
        });
        res.send(res);
        res.send({ message: "successfully updated" });
    });
});


// posting data for cam
app.post('/add-chemical-assign-master', function (req, res) {
    var obj = {
        choice: req.body.choice,
        choice1: req.body.choice1,
        choice2: req.body.choice2,
    };
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        db.collection('cam').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully added");
        })
        res.send("added");
    });
});

//getting data for cam
app.get('/add-chemical-assign-master', function (req, res) {
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        console.log("mongo is connected");
        db.collection('cam').find({}).toArray(function (err, sample) {
            if (err) throw err;
            console.log(sample);
            res.send(sample);
        });
    });
});
//update cam
app.put('/add-chemical-assign-master/:choice', function (req, res) {
    console.log("_id in api to update is" + req.body.choice);
    mongoose.connect("mongodb://localhost:27017/gis", { useNewUrlParser: true }, function (err, db) {
        if (err) throw err;
        console.log("connected to db successfully to save update");
        var id = new mongoose.Types.ObjectId(req.params.choice);
        console.log(id);
        db.collection('cam').find(id).toArray(function (err, data) {
            if (err) throw err;
            console.log(data);
            db.collection('cam').updateMany({ '_id': id }, {
                $set: {
                    choice: req.body.choice,
                    choice1: req.body.choice1,
                    choice2: req.body.choice2,
                }
            });
            db.collection('cam').updateMany({ '_id': id }, {
                $set: {
                    choice: req.body.choice,
                    choice1: req.body.choice1,
                    choice2: req.body.choice2,
                }
            });
            res.send(data);
        });
        res.send(res);
        res.send({ message: "successfully updated" });
    });
});

//posting data to cpm
app.post('/add-chemical-product', function (req, res) {
    var obj = {
        newproductname: req.body.newproductname,
        DistributorName: req.body.DistributorName,
        ManufacturerName: req.body.ManufacturerName,
        Category: req.body.Category,
        Subcategory: req.body.Subcategory,
        ChemicalForm: req.body.ChemicalForm,
        Consumption: req.body.Consumption,
        Quantity: req.body.Quantity,
        ReorderLevel: req.body.ReorderLevel,
        Remark: req.body.Remark,
    };
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        db.collection('cpm').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully added");
        })
        res.send("added");
    });
});

//getting data for cpm
app.get('/add-chemical-product', function (req, res) {
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        console.log("mongo is connected");
        db.collection('cpm').find({}).toArray(function (err, sample) {
            if (err) throw err;
            console.log(sample);
            res.send(sample);
        });
    });
});
// update  of cpm
app.put('/add-chemical-product/:newproductname', function (req, res) {
    console.log("_id in api to update is" + req.body.newproductname);
    mongoose.connect("mongodb://localhost:27017/gis", { useNewUrlParser: true }, function (err, db) {
        if (err) throw err;
        console.log("connected to db successfully to save update");
        var id = new mongoose.Types.ObjectId(req.params.newproductname);
        console.log(id);
        db.collection('cpm').find(id).toArray(function (err, data) {
            if (err) throw err;
            console.log(data);
            db.collection('cpm').updateMany({ '_id': id }, {
                $set: {
                    newproductname: req.body.newproductname,
                    DistributorName: req.body.DistributorName,
                    ManufacturerName: req.body.ManufacturerName,
                    Category: req.body.Category,
                    Subcategory: req.body.Subcategory,
                    ChemicalForm: req.body.ChemicalForm,
                    Consumption: req.body.Consumption,
                    Quantity: req.body.Quantity,
                    ReorderLevel: req.body.ReorderLevel,
                    Remark: req.body.Remark,
                }
            });
            db.collection('cpm').updateMany({ '_id': id }, {
                $set: {
                    newproductname: req.body.newproductname,
                    DistributorName: req.body.DistributorName,
                    ManufacturerName: req.body.ManufacturerName,
                    Category: req.body.Category,
                    Subcategory: req.body.Subcategory,
                    ChemicalForm: req.body.ChemicalForm,
                    Consumption: req.body.Consumption,
                    Quantity: req.body.Quantity,
                    ReorderLevel: req.body.ReorderLevel,
                    Remark: req.body.Remark,
                }
            });
            res.send(data);
        });
        res.send(res);
        res.send({ message: "successfully updated" });
    });
});


// posting data to Gim
app.post('/add-garment-item', function (req, res) {
    var obj = {
        AddGarment: req.body.AddGarment,

    };
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        db.collection('gim').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully assigned");
        })
        res.send("assigned");
    });
});
// getting GIM
app.get('/add-garment-item', function (req, res) {
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        console.log("mongo is connected");
        db.collection('gim').find({}).toArray(function (err, sample) {
            if (err) throw err;
            console.log(sample);
            res.send(sample);
        });
    });
});
//update GIM
app.put('/add-garment-item/:AddGarment', function (req, res) {
    console.log("_id in api to update is" + req.body.AddGarment);
    mongoose.connect("mongodb://localhost:27017/gis", { useNewUrlParser: true }, function (err, db) {
        if (err) throw err;
        console.log("connected to db successfully to save update");
        var id = new mongoose.Types.ObjectId(req.params.AddGarment);
        console.log(id);
        db.collection('gim').find(id).toArray(function (err, data) {
            if (err) throw err;
            console.log(data);
            db.collection('gim').updateMany({ '_id': id }, {
                $set: {
                    AddGarment: req.body.AddGarment,

                }
            });
            db.collection('gim').updateMany({ '_id': id }, {
                $set: {
                    AddGarment: req.body.AddGarment,

                }
            });
            res.send(data);
        });
        res.send(res);
        res.send({ message: "successfully updated" });
    });
});

//cmm add (post data)
app.post('/add-chemical-master', function (req, res) {
    var obj = {
        newchemicalman: req.body.newchemicalman,
        address: req.body.address,
        Pincode: req.body.Pincode,
        Cperson: req.body.Cperson,
        mobile: req.body.mobile,
        landline: req.body.landline,
        Email: req.body.Email,
        gstin: req.body.gstin,
        remarks: req.body.remarks,
    };

    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        db.collection('cmm').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully added");
        })
        res.send("added");
    });
});

// getting cmm 
app.get('/add-chemical-master', function (req, res) {
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        console.log("mongo is connected");
        db.collection('cmm').find({}).toArray(function (err, sample) {
            if (err) throw err;
            console.log(sample);
            res.send(sample);
        });
    });
});

// update cmm data
app.put('/add-chemical-master/:newchemicalman', function (req, res) {
    console.log("_id in api to update is" + req.body.newchemicalman);

    mongoose.connect("mongodb://localhost:27017/gis", { useNewUrlParser: true }, function (err, db) {
        if (err) throw err;
        console.log("connected to db successfully to save update");
        var id = new mongoose.Types.ObjectId(req.params.newchemicalman);
        console.log(id);
        db.collection('cmm').find(id).toArray(function (err, data) {
            if (err) throw err;
            console.log(data);
            db.collection('cmm').updateMany({ '_id': id }, {
                $set: {
                    newchemicalman: req.body.newchemicalman,
                    address: req.body.address,
                    Pincode: req.body.Pincode,
                    Cperson: req.body.Cperson,
                    mobile: req.body.mobile,
                    landline: req.body.landline,
                    Email: req.body.Email,
                    gstin: req.body.gstin,
                    remarks: req.body.remarks,
                }
            });
            db.collection('cmm').updateMany({ '_id': id }, {
                $set: {
                    newchemicalman: req.body.newchemicalman,
                    address: req.body.address,
                    Pincode: req.body.Pincode,
                    Cperson: req.body.Cperson,
                    mobile: req.body.mobile,
                    landline: req.body.landline,
                    Email: req.body.Email,
                    gstin: req.body.gstin,
                    remarks: req.body.remarks,
                }
            });
            res.send(data);
        });
        res.send(res);
        res.send({ message: "successfully updated" });
    });
});

// DPM

// DPM master posting data
app.post('/add-supplier-master', function (req, res) {
    var obj = {
        DpmName: req.body.DpmName,
    };
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        db.collection('dpm').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully added");
        })
        res.send("added");
    });
});
//get data DPM
app.get('/add-supplier-master', function (req, res) {
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        console.log("mongo is connected");
        db.collection('dpm').find({}).toArray(function (err, sample) {
            if (err) throw err;
            console.log(sample);
            res.send(sample);
        });
    });
});

//update DPM
app.put('/add-supplier-master/:DpmName', function (req, res) {
    console.log("_id in api to update is" + req.body.AWS);
    mongoose.connect("mongodb://localhost:27017/gis", { useNewUrlParser: true }, function (err, db) {
        if (err) throw err;
        console.log("connected to db successfully to save update");
        var id = new mongoose.Types.ObjectId(req.params.AWS);
        console.log(id);
        db.collection('dpm').find(id).toArray(function (err, data) {
            if (err) throw err;
            console.log(data);
            db.collection('dpm').updateMany({ '_id': id }, {
                $set: {
                    DpmName: req.body.DpmName,
                }
            });
            db.collection('dpm').updateMany({ '_id': id }, {
                $set: {
                    DpmName: req.body.DpmName,
                }
            });
            res.send(data);
        });
        res.send(res);
        res.send({ message: "successfully updated" });
    });
});

// DMSM

// DMSM master posting data
app.post('/add-dry-material', function (req, res) {
    var obj = {
        newSupplierName: req.body.newSupplierName,
        Cperson: req.body.Cperson,
        address: req.body.address,
        Pincode: req.body.Pincode,
        mobile: req.body.mobile,
        landline: req.body.landline,
        Email: req.body.Email,
        tin: req.body.tin,
        remarks: req.body.remarks,
    };

    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        db.collection('dmsm').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully added");
        })
        res.send("added");
    });
});
//get data DMSM
app.get('/add-dry-material', function (req, res) {
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        console.log("mongo is connected");
        db.collection('dmsm').find({}).toArray(function (err, sample) {
            if (err) throw err;
            console.log(sample);
            res.send(sample);
        });
    });
});

//update DMSM
app.put('/add-dry-material/:newSupplierName', function (req, res) {
    console.log("_id in api to update is" + req.body.newSupplierName);
    mongoose.connect("mongodb://localhost:27017/gis", { useNewUrlParser: true }, function (err, db) {
        if (err) throw err;
        console.log("connected to db successfully to save update");
        var id = new mongoose.Types.ObjectId(req.params.newSupplierName);
        console.log(id);
        db.collection('dmsm').find(id).toArray(function (err, data) {
            if (err) throw err;
            console.log(data);
            db.collection('dmsm').updateMany({ '_id': id }, {
                $set: {
                    newSupplierName: req.body.newSupplierName,
                    address: req.body.address,
                    Pincode: req.body.Pincode,
                    Cperson: req.body.Cperson,
                    mobile: req.body.mobile,
                    landline: req.body.landline,
                    Email: req.body.Email,
                    tin: req.body.tin,
                    remarks: req.body.remarks,
                }
            });
            db.collection('dmsm').updateMany({ '_id': id }, {
                $set: {
                    newSupplierName: req.body.newSupplierName,
                    address: req.body.address,
                    Pincode: req.body.Pincode,
                    Cperson: req.body.Cperson,
                    mobile: req.body.mobile,
                    landline: req.body.landline,
                    Email: req.body.Email,
                    tin: req.body.tin,
                    remarks: req.body.remarks,
                }
            });
            res.send(data);
        });
        res.send(res);
        res.send({ message: "successfully updated" });
    });
});

// UQDM MAster



// UQDM master posting data
app.post('/add-unwash', function (req, res) {
    var obj = {
        DpmName: req.body.DpmName,
    };
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        db.collection('uqdm').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully added");
        })
        res.send("added");
    });
});
//get data UQDM
app.get('/add-unwash', function (req, res) {
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        console.log("mongo is connected");
        db.collection('uqdm').find({}).toArray(function (err, sample) {
            if (err) throw err;
            console.log(sample);
            res.send(sample);
        });
    });
});

//update UQDM
app.put('/add-unwash/:DpmName', function (req, res) {
    console.log("_id in api to update is" + req.body.AWS);
    mongoose.connect("mongodb://localhost:27017/gis", { useNewUrlParser: true }, function (err, db) {
        if (err) throw err;
        console.log("connected to db successfully to save update");
        var id = new mongoose.Types.ObjectId(req.params.AWS);
        console.log(id);
        db.collection('uqdm').find(id).toArray(function (err, data) {
            if (err) throw err;
            console.log(data);
            db.collection('uqdm').updateMany({ '_id': id }, {
                $set: {
                    DpmName: req.body.DpmName,
                }
            });
            db.collection('uqdm').updateMany({ '_id': id }, {
                $set: {
                    DpmName: req.body.DpmName,
                }
            });
            res.send(data);
        });
        res.send(res);
        res.send({ message: "successfully updated" });
    });
});


// FQM MAster
// FQM  master posting data
app.post('/add-financial', function (req, res) {
    var obj = {
        DpmName: req.body.DpmName,
    };
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        db.collection('fqm').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully added");
        })
        res.send("added");
    });
});
//get data FQM 
app.get('/add-financial', function (req, res) {
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        console.log("mongo is connected");
        db.collection('fqm').find({}).toArray(function (err, sample) {
            if (err) throw err;
            console.log(sample);
            res.send(sample);
        });
    });
});

//update FQM 
app.put('/add-financial/:DpmName', function (req, res) {
    console.log("_id in api to update is" + req.body.AWS);
    mongoose.connect("mongodb://localhost:27017/gis", { useNewUrlParser: true }, function (err, db) {
        if (err) throw err;
        console.log("connected to db successfully to save update");
        var id = new mongoose.Types.ObjectId(req.params.AWS);
        console.log(id);
        db.collection('fqm').find(id).toArray(function (err, data) {
            if (err) throw err;
            console.log(data);
            db.collection('fqm').updateMany({ '_id': id }, {
                $set: {
                    DpmName: req.body.DpmName,
                }
            });
            db.collection('fqm').updateMany({ '_id': id }, {
                $set: {
                    DpmName: req.body.DpmName,
                }
            });
            res.send(data);
        });
        res.send(res);
        res.send({ message: "successfully updated" });
    });
});


// TEm

// TEM MAster
// TEM  master posting data
app.post('/add-transit', function (req, res) {
    var obj = {
        DpmName: req.body.DpmName,
    };
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        db.collection('tem').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully added");
        })
        res.send("added");
    });
});
//get data TEM 
app.get('/add-transit', function (req, res) {
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        console.log("mongo is connected");
        db.collection('tem').find({}).toArray(function (err, sample) {
            if (err) throw err;
            console.log(sample);
            res.send(sample);
        });
    });
});

//update TEM 
app.put('/add-transit/:DpmName', function (req, res) {
    console.log("_id in api to update is" + req.body.AWS);
    mongoose.connect("mongodb://localhost:27017/gis", { useNewUrlParser: true }, function (err, db) {
        if (err) throw err;
        console.log("connected to db successfully to save update");
        var id = new mongoose.Types.ObjectId(req.params.AWS);
        console.log(id);
        db.collection('tem').find(id).toArray(function (err, data) {
            if (err) throw err;
            console.log(data);
            db.collection('tem').updateMany({ '_id': id }, {
                $set: {
                    DpmName: req.body.DpmName,
                }
            });
            db.collection('tem').updateMany({ '_id': id }, {
                $set: {
                    DpmName: req.body.DpmName,
                }
            });
            res.send(data);
        });
        res.send(res);
        res.send({ message: "successfully updated" });
    });
});


//DPMPM Master

app.post('/add-Dry-Process-Material-Product-Master', function (req, res) {
    var obj = {
        dryproductname: req.body.dryproductname,
        dryDistributorName: req.body.dryDistributorName,

        dryConsumption: req.body.dryConsumption,
        dryQuantity: req.body.dryQuantity,
        dryReorderLevel: req.body.dryReorderLevel,
        dryRemark: req.body.dryRemark,
    };
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        db.collection('dpmpm').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully added");
        })
        res.send("added");
    });
});

//getting data for DPMPM
app.get('/add-Dry-Process-Material-Product-Master', function (req, res) {
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        console.log("mongo is connected");
        db.collection('dpmpm').find({}).toArray(function (err, sample) {
            if (err) throw err;
            console.log(sample);
            res.send(sample);
        });
    });
});
// update  of DPMPM
app.put('/add-Dry-Process-Material-Product-Master/:dryproductname', function (req, res) {
    console.log("_id in api to update is" + req.body.dryproductname);
    mongoose.connect("mongodb://localhost:27017/gis", { useNewUrlParser: true }, function (err, db) {
        if (err) throw err;
        console.log("connected to db successfully to save update");
        var id = new mongoose.Types.ObjectId(req.params.dryproductname);
        console.log(id);
        db.collection('dpmpm').find(id).toArray(function (err, data) {
            if (err) throw err;
            console.log(data);
            db.collection('dpmpm').updateMany({ '_id': id }, {
                $set: {
                    dryproductname: req.body.dryproductname,
                    dryDistributorName: req.body.dryDistributorName,
                    dryConsumption: req.body.dryConsumption,
                    dryQuantity: req.body.dryQuantity,
                    dryReorderLevel: req.body.dryReorderLevel,
                    dryRemark: req.body.dryRemark,
                }
            });
            db.collection('dpmpm').updateMany({ '_id': id }, {
                $set: {
                    dryproductname: req.body.dryproductname,
                    dryDistributorName: req.body.dryDistributorName,

                    dryConsumption: req.body.dryConsumption,
                    dryQuantity: req.body.dryQuantity,
                    dryReorderLevel: req.body.dryReorderLevel,
                    dryRemark: req.body.dryRemark,
                }
            });
            res.send(data);
        });
        res.send(res);
        res.send({ message: "successfully updated" });
    });
});


// BBM file upload post
app.post('/fileUpload', upload.single('files'), function (req, res) {
    var obj = req.body;
    mongo.connect("mongodb://localhost:27017/BBM", function (err, db) {
        if (err) throw err;
        db.collection('bbmFile').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully added");
        })
        res.send(req.files);
    });
});

// BBM file getting
app.get('/fileUpload', function (req, res) {
    mongo.connect("mongodb://localhost:27017/BBM", function (err, db) {
        if (err) throw err;
        console.log("mongo is connected");
        db.collection('bbmFile').find({}).toArray(function (err, sample) {
            if (err) throw err;
            console.log(sample);
            res.send(sample);
        });
    });
});

// BBM update file
app.put('/fileUpload/:newBuyerBrand', function (req, res) {
    console.log("_id in api to update is" + req.body.newBuyerBrand);
    mongoose.connect("mongodb://localhost:27017/BBM", { useNewUrlParser: true }, function (err, db) {
        if (err) throw err;
        console.log("connected to db successfully to save update");
        var id = new mongoose.Types.ObjectId(req.params.newBuyerBrand);
        console.log(id);
        db.collection('bbmFile').find(id).toArray(function (err, data) {
            if (err) throw err;
            console.log(data);
            db.collection('bbmFile').updateMany({ '_id': id }, {
                $set: {
                    newBuyerBrand: req.body.newBuyerBrand,
                    address: req.body.address,
                    Pincode: req.body.Pincode,
                    Cperson: req.body.Cperson,
                    mobile: req.body.mobile,
                    altermobile: req.body.altermobile,
                    Email: req.body.Email,
                    website: req.body.website,
                    remarks: req.body.remarks,
                }
            });
            db.collection('bbmFile').updateMany({ '_id': id }, {
                $set: {
                    newBuyerBrand: req.body.newBuyerBrand,
                    address: req.body.address,
                    Pincode: req.body.Pincode,
                    Cperson: req.body.Cperson,
                    mobile: req.body.mobile,
                    altermobile: req.body.altermobile,
                    Email: req.body.Email,
                    website: req.body.website,
                    remarks: req.body.remarks,
                }
            });
            res.send(data);
        });
        res.send(res);
        res.send({ message: "successfully updated" });
    });
});

// GMM File Upload 

// GMM file upload post
app.post('/gmmFileUpload', upload.single('files'), function (req, res) {
    var obj = req.body;
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        db.collection('gmm').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully added");
        })
        res.send(req.files);
    });
});

// CADM File Upload
app.post('/cadmFileUpload', upload.single('files'), function (req, res) {
    var obj = req.body;
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        db.collection('cadmm').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully added");
        })
        res.send(req.files);
    });
});

//CMM File upload
app.post('/cmmFileUpload', upload.single('files'), function (req, res) {
    var obj = req.body;
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        db.collection('cmm').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully added");
        })
        res.send(req.files);
    });
});

//CPM File upload
app.post('/cpmFileUpload', upload.single('files'), function (req, res) {
    var obj = req.body;
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        db.collection('cpm').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully added");
        })
        res.send(req.files);
    });
});


// DPMPM FileUpload
app.post('/dpmpmFileUpload', upload.single('files'), function (req, res) {
    var obj = req.body;
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        db.collection('dpmpm').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully added");
        })
        res.send(req.files);
    });
});


// DPMSM or DMSM
app.post('/dmsmFileUpload', upload.single('files'), function (req, res) {
    var obj = req.body;
    mongo.connect("mongodb://localhost:27017/gis", function (err, db) {
        if (err) throw err;
        db.collection('dmsm').insert(obj, function (err, res) {
            if (err)
                console.log.apply(err);
            console.log("successfully added");
        })
        res.send(req.files);
    });
});








module.exports = app;